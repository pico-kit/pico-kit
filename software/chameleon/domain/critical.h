/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Chameleon Critical adapter interface.
 *
 * This header is interface header for Chameleon Critical adapter.
 */
/**
 * @defgroup chameleon_critical Chameleon Critical interface
 * @{
 */

#ifndef SOFTWARE_CHAMELEON_DOMAIN_CRITICAL_H_
#define SOFTWARE_CHAMELEON_DOMAIN_CRITICAL_H_

/**
 * @brief   Initialize global critical mutex
 */
void
c__critical_init(void);

/**
 * @brief   Enter critical section
 */
void
c__critical_enter(void);

/**
 * @brief   Exit critical section
 */
void
c__critical_exit(void);

#endif /* SOFTWARE_CHAMELEON_DOMAIN_CRITICAL_H_ */
/** @} */
