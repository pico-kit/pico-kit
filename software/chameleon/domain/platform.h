/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Chameleon platform adapter interface.
 *
 * This header is interface header for Chameleon platform adapter.
 */
/**
 * @defgroup chameleon_platform Chameleon platform interface
 * @{
 */

#ifndef SOFTWARE_CHAMELEON_DOMAIN_PLATFORM_H_
#define SOFTWARE_CHAMELEON_DOMAIN_PLATFORM_H_

/**
 * @brief 		Attribute or specifier for functions that do not return.
 *
 * This macro defines the attribute or specifier to indicate that a function
 * does not return. The specific attribute or specifier depends on the compiler
 * being used.
 *
 * - For the GNU Compiler Collection (__GNUC__), the `noreturn` attribute is
 *   used.
 * - For the ARM Compiler (__ARMCC_VERSION), the `noreturn` specifier is used.
 * - For the IAR Embedded Workbench (__ICCARM__), the `noreturn` specifier is
 *   used.
 * - For the Microsoft Visual C++ Compiler (_MSC_VER), the `noreturn` specifier
 *   is used.
 *
 * @note 		If the compiler is not supported a warning about unsupported
 * 				compiler will be generated.
 */
#if defined(__GNUC__) || defined(__DOXYGEN__)
#define C__PLATFORM_ATTR_NO_RETURN __attribute__((noreturn))
#elif defined(__ARMCC_VERSION)
#define C__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#elif defined(__ICCARM__)
#define C__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#elif defined(_MSC_VER)
#define C__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#else
#define C__PLATFORM_ATTR_NO_RETURN
#warning "Unsupported compiler"
#endif

#endif /* SOFTWARE_CHAMELEON_DOMAIN_PLATFORM_H_ */
/** @} */
