/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Chameleon CPU adapter interface.
 *
 * This header is interface header for Chameleon CPU adapter.
 */
/**
 * @defgroup chameleon_cpu Chameleon CPU interface
 * @{
 */

#ifndef SOFTWARE_CHAMELEON_DOMAIN_CPU_H_
#define SOFTWARE_CHAMELEON_DOMAIN_CPU_H_

/* Depends */
#include <stdint.h>
#include "chameleon/domain/platform.h"

/**
 * @brief   Put the CPU into sleep power mode.
 *
 * Interrupts are enabled during the sleep power mode so the system can be
 * awaken when some event occur.
 */
void
c__cpu_sleep(void);

/**
 * @brief   Calculate log2 of @a value.
 */
uint_fast8_t
c__cpu_log2(uint32_t value);

/**
 * @brief   Failure handler
 *
 * This handler is called when a failure cannot be handled. Usually when the
 * system calls this handler, it should stop further execution.
 */
C__PLATFORM_ATTR_NO_RETURN
void
c__cpu_failure_handler(void);

#endif /* SOFTWARE_CHAMELEON_DOMAIN_CPU_H_ */
/** @} */
