#
# SPDX-License-Identifier: LGPL-3.0-only
#
# software/chameleon/manifest.mk

# General

# Domain and Features
F_SRC_DIRS += software/chameleon/domain
F_INC_DIRS += software
