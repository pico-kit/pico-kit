/*
 * network_init.h
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_NETWORK_NETWORK_INIT_H_
#define REACTIVE_FEATURE_NETWORK_NETWORK_INIT_H_

#include "reactive/domain/error.h"
#include "reactive/domain/network.h"

#define R__NETWORK_SCHEDULER_INITIALIZER(instance, virtual_function_table)     \
    {                                                                          \
        .vft = (virtual_function_table),                                       \
    }

r__error
r__network_init(r__network * network, r__network_scheduler * network_scheduler);

#endif /* REACTIVE_FEATURE_NETWORK_NETWORK_INIT_H_ */
