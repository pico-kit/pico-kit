/*
 * network_create.c
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/network/network_create.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__network_create(
        r__network_scheduler * network_scheduler,
        r__network **          network)
{
    void * allocation;

    /* Validate arguments */
    if ((network_scheduler == NULL) || (network == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Allocate network instance */
    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_NETWORK,
            sizeof(r__network),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_NETWORK;
    }

    /* Construct network instance */
    r__network * l_network = allocation;
    r__network_model_ctor(l_network, network_scheduler);
    *network = l_network;
    return R__ERROR_NONE;

    /* Error handling */
FAIL_ALLOCATE_NETWORK:
    return error;
}
