/*
 * network_init.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/network/network_init.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__network_init(r__network * network, r__network_scheduler * network_scheduler)
{
    /* Validate arguments */
    if ((network == NULL) || (network_scheduler == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Construct network instance */
    r__network_model_ctor(network, network_scheduler);
    return R__ERROR_NONE;
}
