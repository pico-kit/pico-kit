/*
 * network_create.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_
#define REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_

#include "reactive/domain/error.h"
#include "reactive/domain/network.h"

/**
 * @brief       Create a new event processing network.
 *
 * This function creates a new event processing network. At least one network
 * needs to be created in order to execute event processing agents.
 *
 * In constrained embedded systems where there is no support for threading (not
 * using an RTOS) one network needs to be created. In this case the execution
 * of agents will be done in the main loop.
 *
 * In order to create a network, a network_scheduler needs to be supplied. The
 * network_scheduler abstracts all low level scheduling mechanisms, like
 * threading and event notifications.
 *
 * @param       network_scheduler Is a pointer to existing instance of
 *              network_scheduler abstraction interface. This network interface
 *              must exist as long as the network being created and can be
 *              deleted only after the last network is using it.
 * @param       network Address of a pointer to network instance.
 * @return      Error of the operation execution.
 * @retval      Error @ref R__ERROR_NONE is returned when the operation
 *              completed successfully.
 * @retval      Error @ref R__ERROR_NULL_ARGUMENT is returned when
 *              @a network_scheduler argument or @a network is a NULL pointer.
 * @retval      Error @ref R__ERROR_NO_MEMORY is returned when allocator
 *              failed to allocate sufficient memory for network instance
 *              storage.
 *
 * @thread_safe
 * @isr_unsafe
 */
r__error
r__network_create(
        r__network_scheduler * network_scheduler,
        r__network **          network);

#endif /* REACTIVE_FEATURE_NETWORK_NETWORK_CREATE_H_ */
