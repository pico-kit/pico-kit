/*
 * network_deinit.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/network/network_create.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__network_deinit(r__network * network)
{
    /* Validate argument */
    if (network == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Delete all agents that are part of this network */
    // TODO:

    r__network_model_dtor(network);
    return R__ERROR_NONE;
}
