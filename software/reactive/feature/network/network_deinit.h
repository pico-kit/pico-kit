/*
 * network_deinit.h
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_NETWORK_NETWORK_DEINIT_H_
#define REACTIVE_FEATURE_NETWORK_NETWORK_DEINIT_H_

#include "reactive/domain/error.h"
#include "reactive/domain/network.h"

r__error
r__network_deinit(r__network * network);

#endif /* REACTIVE_FEATURE_NETWORK_NETWORK_DEINIT_H_ */
