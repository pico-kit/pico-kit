/*
 * network_start.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/network/network_start.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__network_start(r__network * network)
{
    r__error error;

    /* Validate argument */
    if (network == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Start the network */
    error = r__network_model_start(network);
    if (error == R__ERROR_NONE) {
        goto FAIL_START_NETWORK;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_START_NETWORK:
    return error;
}
