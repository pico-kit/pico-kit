/*
 * queue_init.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_init.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/queue_model.h"

r__error
r__queue_init(
        r__queue *        queue,
        r__agent *        agent,
        const r__event ** storage,
        size_t            n_entries)
{
    /* Validate arguments */
    if ((storage == NULL) || (queue == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if (n_entries == 0) {
        return R__ERROR_INVALID_ARGUMENT;
    }

    /* Construct queue instance */
    r__queue_model_ctor(queue, agent, storage, n_entries);
    return R__ERROR_NONE;
}
