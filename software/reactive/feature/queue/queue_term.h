/*
 * queue_term.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_TERM_H_
#define FEATURE_QUEUE_QUEUE_TERM_H_

#include "reactive/domain/error.h"
#include "reactive/domain/queue.h"

r__error
r__queue_term(r__queue * queue);

#endif /* FEATURE_QUEUE_QUEUE_TERM_H_ */
