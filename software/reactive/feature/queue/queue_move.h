/*
 * queue_move.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_MOVE_H_
#define FEATURE_QUEUE_QUEUE_MOVE_H_

#include "reactive/domain/error.h"
#include "reactive/domain/queue.h"

r__error
r__queue_move(r__queue * source);

#endif /* FEATURE_QUEUE_QUEUE_MOVE_H_ */
