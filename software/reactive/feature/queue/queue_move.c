/*
 * queue_move.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_move.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event.h"
#include "reactive/domain/network_model.h"
#include "reactive/domain/queue_model.h"

static r__error
queue_move(r__queue * source, r__queue * destination)
{
    r__error         error;
    const r__event * event;

    while (r__queue_model_fetch(source, &event) == R__ERROR_NONE) {
        error = r__queue_model_put_fifo(destination, event);

        if (error) {
            goto FAIL_DESTINATION_FULL;
        }
    }
    return R__ERROR_NONE;
FAIL_DESTINATION_FULL:
    return error;
}

r__error
r__queue_move(r__queue * source)
{
    r__error     error;
    r__network * network;

    /* Validate arguments */
    if (source == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    network = r__network_model_proxy_get_network(
            r__agent_model_get_network_proxy(source->agent));

    /* Move events from source to agent queue with locked network */
    error = r__network_model_lock(network);
    if (error) {
        goto FAIL_NETWORK_LOCK;
    }
    error = queue_move(source, r__agent_model_get_queue(source->agent));
    if (error) {
        goto FAIL_MOVE_QUEUE;
    }
    error = r__network_model_unlock(network);
    if (error) {
        goto FAIL_NETWORK_UNLOCK;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_NETWORK_UNLOCK:
FAIL_MOVE_QUEUE:
    (void) r__network_model_unlock(network);
FAIL_NETWORK_LOCK:
    return error;
}
