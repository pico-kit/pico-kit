/*
 * queue_delete.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_delete.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/queue_model.h"

r__error
r__queue_delete(r__queue * queue)
{
    /* Validate arguments */
    if (queue == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Delete currently kept events in queue */
    r__error error = r__queue_model_delete_events(queue);
    if (error != R__ERROR_NONE) {
        goto FAIL_DELETE_EVENTS;
    }

    /* Store the storage pointer before deconstructing queue instance */
    void * storage = queue->p__queue.storage;

    /* Deconstruct queue instance */
    r__queue_model_dtor(queue);

    /* Deallocate queue instance and associated data */
    error = r__allocator_deallocate(R__ALLOCATOR_TYPE_QUEUE_DATA, storage);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_STORAGE;
    }
    error = r__allocator_deallocate(R__ALLOCATOR_TYPE_QUEUE, queue);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_QUEUE;
    }
    return error;

    /* Error handling */
FAIL_DEALLOCATE_STORAGE:
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_QUEUE, queue);
FAIL_DEALLOCATE_QUEUE:
FAIL_DELETE_EVENTS:
    return error;
}
