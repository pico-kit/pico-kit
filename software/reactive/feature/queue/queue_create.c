/*
 * queue_create.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_create.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/queue_model.h"

r__error
r__queue_create(r__sm * sm, size_t n_entries, r__queue ** queue)
{
    void * allocation;

    /* Validate arguments */
    if ((sm == NULL) || (queue == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if (n_entries == 0u) {
        return R__ERROR_INVALID_ARGUMENT;
    }

    /* Allocate queue instance */
    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_QUEUE,
            sizeof(r__queue),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_QUEUE;
    }
    r__queue * l_queue = allocation;

    /* Allocate queue variable sized storage */
    error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_QUEUE_DATA,
            sizeof(r__event *) * n_entries,
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_STORAGE;
    }
    const r__event ** l_queue_storage = allocation;

    /* Construct queue instance */
    r__queue_model_ctor(
            l_queue,
            r__agent_model_decompose(sm),
            l_queue_storage,
            n_entries);
    *queue = l_queue;
    return R__ERROR_NONE;

    /* Error handling */
FAIL_ALLOCATE_STORAGE:
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_QUEUE, l_queue);
FAIL_ALLOCATE_QUEUE:
    return error;
}
