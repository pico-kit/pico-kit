/*
 * queue_delete.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_DELETE_H_
#define FEATURE_QUEUE_QUEUE_DELETE_H_

#include "reactive/domain/error.h"
#include "reactive/domain/queue.h"

r__error
r__queue_delete(r__queue * queue);

#endif /* FEATURE_QUEUE_QUEUE_DELETE_H_ */
