/*
 * queue_term.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_term.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/queue_model.h"

r__error
r__queue_term(r__queue * queue)
{
    r__error error;

    /* Validate arguments */
    if (queue == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Delete currently kept events in queue */
    error = r__queue_model_delete_events(queue);
    if (error != R__ERROR_NONE) {
        goto FAIL_DELETE_EVENTS;
    }

    /* Deconstruct queue instance */
    r__queue_model_dtor(queue);
    return R__ERROR_NONE;

    /* Error handling */
FAIL_DELETE_EVENTS:
    return error;
}
