/*
 * queue_put.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/queue/queue_put.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"
#include "reactive/domain/queue_model.h"

r__error
r__queue_put_fifo(r__queue * queue, const r__event * event)
{
    r__error error;

    /* Validate arguments */
    if ((queue == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Put event into queue */
    error = r__queue_model_put_fifo(queue, event);
    if (error) {
        goto FAIL_PUT_FIFO_EVENT;
    }
    r__event_model_usage_acquire(event);
    return R__ERROR_NONE;

    /* Error handling */
FAIL_PUT_FIFO_EVENT:
    return error;
}

r__error
r__queue_put_lifo(r__queue * queue, const r__event * event)
{
    r__error error;

    /* Validate arguments */
    if ((queue == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Put event into queue */
    error = r__queue_model_put_lifo(queue, event);
    if (error) {
        goto FAIL_PUT_LIFO_EVENT;
    }
    r__event_model_usage_acquire(event);
    return R__ERROR_NONE;

    /* Error handling */
FAIL_PUT_LIFO_EVENT:
    return error;
}
