/*
 * queue_init.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_QUEUE_QUEUE_INIT_H_
#define FEATURE_QUEUE_QUEUE_INIT_H_

#include <stddef.h>
#include "reactive/domain/agent.h"
#include "reactive/domain/error.h"
#include "reactive/domain/event.h"
#include "reactive/domain/queue.h"

#define R__QUEUE_INITIALIZER(a_storage, a_n_entries)                           \
    R__QUEUE_MODEL_INITIALIZER(a_storage, a_n_entries)

r__error
r__queue_init(
        r__queue *        queue,
        r__agent *        agent,
        const r__event ** storage,
        size_t            n_entries);

#endif /* FEATURE_QUEUE_QUEUE_INIT_H_ */
