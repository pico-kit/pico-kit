/*
 * timer_cancel.c
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/timer/timer_cancel.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/timer_model.h"

r__error
r__timer_cancel(r__timer * timer)
{
    if (timer == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }
    r__timer_model_cancel(timer);
    return R__ERROR_NONE;
}
