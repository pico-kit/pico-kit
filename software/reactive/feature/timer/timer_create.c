/*
 * timer_create.c
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/timer/timer_create.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"
#include "reactive/domain/timer_model.h"

r__error
r__timer_create(r__sm * sm, r__event_id event_id, r__timer ** timer)
{
    void * allocation;

    /* Validate arguments */
    if ((sm == NULL) || (timer == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Allocate timer instance */
    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_TIMER,
            sizeof(r__timer),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_TIMER;
    }
    r__timer * l_timer = allocation;

    /* Construct timer instance */
    r__agent * agent = r__agent_model_decompose(sm);
    r__timer_model_ctor_dynamic(l_timer, event_id, agent);
    *timer = l_timer;
    return R__ERROR_NONE;

    /* Error handling */
FAIL_ALLOCATE_TIMER:
    return error;
}
