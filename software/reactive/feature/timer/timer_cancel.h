/*
 * timer_cancel.h
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_TIMER_TIMER_CANCEL_H_
#define REACTIVE_FEATURE_TIMER_TIMER_CANCEL_H_

#include "reactive/domain/error.h"
#include "reactive/domain/timer.h"

r__error
r__timer_cancel(r__timer * timer);

#endif /* REACTIVE_FEATURE_TIMER_TIMER_CANCEL_H_ */
