/*
 * timer_start.h
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_TIMER_TIMER_START_H_
#define REACTIVE_FEATURE_TIMER_TIMER_START_H_

#include <stdint.h>
#include "reactive/domain/error.h"
#include "reactive/domain/timer.h"

r__error
r__timer_start_after(r__timer * timer, uint32_t timeout_ms);
r__error
r__timer_start_every(r__timer * timer, uint32_t timeout_ms);

#endif /* REACTIVE_FEATURE_TIMER_TIMER_START_H_ */
