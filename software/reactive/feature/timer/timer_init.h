/*
 * timer_init.h
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_TIMER_TIMER_INIT_H_
#define REACTIVE_FEATURE_TIMER_TIMER_INIT_H_

#include "reactive/domain/error.h"
#include "reactive/domain/event.h"
#include "reactive/domain/sm.h"
#include "reactive/domain/timer.h"

r__error
r__timer_init(r__timer * timer, r__sm * sm, r__event_id event_id);

#endif /* REACTIVE_FEATURE_TIMER_TIMER_INIT_H_ */
