/*
 * timer_delete.c
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/timer/timer_delete.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"
#include "reactive/domain/timer_model.h"

r__error
r__timer_delete(r__timer * timer)
{
    /* Validate argument */
    if (timer == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    r__network * network = r__network_model_proxy_get_network(
            r__agent_model_get_network_proxy(timer->agent));

    r__error error = r__network_model_lock(network);
    if (error != R__ERROR_NONE) {
        goto FAIL_NETWORK_LOCK;
    }
    r__timer_model_cancel(timer);
    error = r__network_model_unlock(network);
    if (error != R__ERROR_NONE) {
        goto FAIL_NETWORK_UNLOCK;
    }
    r__timer_model_dtor(timer);
    error = r__allocator_deallocate(R__ALLOCATOR_TYPE_TIMER, timer);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_TIMER;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_DEALLOCATE_TIMER:
FAIL_NETWORK_UNLOCK:
FAIL_NETWORK_LOCK:
    return error;
}
