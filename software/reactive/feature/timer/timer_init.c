/*
 * timer_init.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/timer/timer_init.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"
#include "reactive/domain/timer_model.h"

r__error
r__timer_init(r__timer * timer, r__sm * sm, r__event_id event_id)
{
    r__agent * agent;

    if ((timer == NULL) || (sm == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    agent = r__agent_model_decompose(sm);
    r__timer_model_ctor_dynamic(timer, event_id, agent);
    return R__ERROR_NONE;
}
