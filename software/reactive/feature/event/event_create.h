/*
 * event_create.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef FEATURE_EVENT_CREATE_H_
#define FEATURE_EVENT_CREATE_H_

#include <stddef.h>
#include "reactive/domain/error.h"
#include "reactive/domain/event.h"

r__error
r__event_create(r__event_id event_id, r__event ** error);

r__error
r__event_create_with_data(
        r__event_id event_id,
        size_t      data_size,
        r__event ** event,
        void *      event_data);

#endif /* FEATURE_EVENT_CREATE_H_ */
