/*
 * event_init.c
 *
 *  Created on: Oct 16, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/event/event_init.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"

r__error
r__event_init(r__event * event, r__event_id event_id)
{
    /* Validate arguments */
    if (event == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Construct the event */
    r__event_model_ctor_static(event, event_id, NULL);
    return R__ERROR_NONE;
}
