/*
 * event_get.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_EVENT_EVENT_GET_H_
#define FEATURE_EVENT_EVENT_GET_H_

#include "reactive/domain/event_type.h"

inline r__event_id
r__event_get_id(const r__event * event)
{
    return event->id;
}

inline const void *
r__event_get_data(const r__event * event)
{
    return event->data;
}

#endif /* FEATURE_EVENT_EVENT_GET_H_ */
