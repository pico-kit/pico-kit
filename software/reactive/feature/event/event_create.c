/*
 * event.c
 *
 *  Created on: Oct 10, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/event/event_create.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"

r__error
r__event_create(r__event_id event_id, r__event ** event)
{
    /* Create event */
    r__error error = r__event_model_create(event_id, 0, event, NULL);
    if (error != R__ERROR_NONE) {
        goto FAIL_CREATE_EVENT;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_CREATE_EVENT:
    return error;
}

r__error
r__event_create_with_data(
        r__event_id event_id,
        size_t      data_size,
        r__event ** event,
        void *      event_data)
{
    /* Validate arguments */
    if ((event == NULL) || (event_data == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if (data_size == 0u) {
        return R__ERROR_INVALID_ARGUMENT;
    }

    /* Create event */
    r__error error =
            r__event_model_create(event_id, data_size, event, event_data);
    if (error != R__ERROR_NONE) {
        goto FAIL_CREATE_EVENT;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_CREATE_EVENT:
    return error;
}
