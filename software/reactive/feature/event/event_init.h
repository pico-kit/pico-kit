/*
 * event_init.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_EVENT_EVENT_INIT_H_
#define FEATURE_EVENT_EVENT_INIT_H_

#include "reactive/domain/error.h"
#include "reactive/domain/event.h"

#define R__EVENT_INITIALIZER(event_id)                                         \
    R__EVENT_MODEL_INITIALIZER(event_id, NULL)

r__error
r__event_init(r__event * event, r__event_id event_id);

#endif /* FEATURE_EVENT_EVENT_INIT_H_ */
