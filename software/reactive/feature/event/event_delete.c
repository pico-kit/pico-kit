/*
 * event_delete.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/event/event_delete.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"

r__error
r__event_delete(const r__event * event)
{
    /* Validate arguments */
    if (event == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Delete event */
    r__error error = r__event_model_delete(event);
    if (error != R__ERROR_NONE) {
        goto FAIL_DELETE_EVENT;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_DELETE_EVENT:
    return error;
}
