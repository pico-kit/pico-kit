/*
 * event_get.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/event/event_get.h"

extern inline r__event_id
r__event_get_id(const r__event * event);

extern inline const void *
r__event_get_data(const r__event * event);
