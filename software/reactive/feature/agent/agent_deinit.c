/*
 * agent_deinit.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/agent/agent_deinit.h"

/* Depends */
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"
#include "reactive/domain/queue_model.h"

// TODO: Make the agent be in some state
r__error
r__agent_deinit(r__agent * agent)
{
    struct r__network_proxy * network_proxy;
    r__error                  error;

    /* Validate arguments */
    if (agent == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* See if are scheduled, if yes, un-schedule us */
    network_proxy = r__agent_model_get_network_proxy(agent);
    error         = r__network_model_proxy_delete_task(network_proxy);
    if (error != R__ERROR_NONE) {
        goto FAIL_DELETE_TASK;
    }

    /* Delete all waiting events in queue */
    r__queue_model_delete_events(&agent->queue);

    /* Deconstruct agent instance */
    r__agent_model_dtor(agent);
    return R__ERROR_NONE;
FAIL_DELETE_TASK:
    return error;
}
