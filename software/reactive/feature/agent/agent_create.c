/*
 * agent_create.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/agent/agent_create.h"

/* Depends */
#include "reactive/domain/agent_model.h"
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__agent_create(
        r__network *             network,
        r__network_task_priority network_task_priority,
        r__sm_state *            sm_initial_state,
        size_t                   sm_data_size,
        size_t                   queue_storage_elements,
        r__agent **              agent)
{
    void * allocation;

    /* Validate arguments */
    if ((network == NULL) || (sm_initial_state == NULL) || (agent == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if (queue_storage_elements == 0) {
        return R__ERROR_INVALID_ARGUMENT;
    }

    /* Allocate agent instance */
    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_AGENT,
            sizeof(r__agent),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_AGENT;
    }
    r__agent * l_agent = allocation;

    /* Allocate sm_data */
    error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_AGENT_DATA,
            sm_data_size,
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_SM_DATA;
    }
    void * sm_data = allocation;

    /* Allocate queue_storage */
    error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_AGENT_DATA,
            sizeof(r__event *) * queue_storage_elements,
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_QUEUE_STORAGE;
    }
    void * queue_storage = allocation;

    /* Construct agent instance */
    r__agent_model_ctor_dynamic(
            l_agent,
            network,
            sm_initial_state,
            sm_data,
            queue_storage,
            queue_storage_elements);

    /* Create task that will execute this agent */
    struct r__network_proxy * network_proxy =
            r__agent_model_get_network_proxy(l_agent);
    error = r__network_model_proxy_create_task(
            network_proxy,
            network_task_priority);
    if (error != R__ERROR_NONE) {
        goto FAIL_CREATE_TASK;
    }
    *agent = l_agent;
    return R__ERROR_NONE;

    /* Error handling */
FAIL_CREATE_TASK:
    r__agent_model_dtor(l_agent);
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT_DATA, queue_storage);
FAIL_ALLOCATE_QUEUE_STORAGE:
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT_DATA, sm_data);
FAIL_ALLOCATE_SM_DATA:
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_AGENT, l_agent);
FAIL_ALLOCATE_AGENT:
    return error;
}
