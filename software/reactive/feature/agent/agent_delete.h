/*
 * agent_delete.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_AGENT_AGENT_DELETE_H_
#define FEATURE_AGENT_AGENT_DELETE_H_

#include "reactive/domain/agent_type.h"
#include "reactive/domain/error.h"

r__error
r__agent_delete(r__agent * agent);

#endif /* FEATURE_AGENT_AGENT_DELETE_H_ */
