/*
 * agent_deinit.h
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_
#define REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_

#include "reactive/domain/agent.h"
#include "reactive/domain/error.h"

r__error
r__agent_deinit(r__agent * agent);

#endif /* REACTIVE_FEATURE_AGENT_AGENT_DEINIT_H_ */
