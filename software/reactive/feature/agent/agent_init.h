/*
 * agent_init.h
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_FEATURE_AGENT_AGENT_INIT_H_
#define REACTIVE_FEATURE_AGENT_AGENT_INIT_H_

#include <stddef.h>
#include "reactive/domain/agent.h"
#include "reactive/domain/error.h"
#include "reactive/domain/network.h"
#include "reactive/domain/sm.h"

r__error
r__agent_init(
        r__agent *               agent,
        r__network *             network,
        r__network_task *        network_task,
        r__network_task_priority network_task_priority,
        r__sm_state *            state_machine_initial_state,
        void *                   state_machine_data,
        size_t                   state_machine_data_size,
        void *                   queue_storage,
        size_t                   queue_storage_elements);

#endif /* REACTIVE_FEATURE_AGENT_AGENT_INIT_H_ */
