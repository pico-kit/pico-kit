/*
 * agent_send.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_AGENT_AGENT_SEND_H_
#define FEATURE_AGENT_AGENT_SEND_H_

#include "reactive/domain/agent.h"
#include "reactive/domain/error.h"
#include "reactive/domain/event.h"

r__error
r__agent_send(r__agent * agent, const r__event * event);

r__error
r__agent_send_i(r__agent * agent, const r__event * event);

r__error
r__agent_send_front(r__agent * agent, const r__event * event);

r__error
r__agent_send_front_i(r__agent * agent, const r__event * event);

#endif /* FEATURE_AGENT_AGENT_SEND_H_ */
