/*
 * agent_send.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/agent/agent_send.h"

/* Depends */
#include <stdbool.h>
#include <stddef.h>
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__agent_send(r__agent * agent, const r__event * event)
{
    r__error     error;
    r__network * network;

    /* Validate arguments */
    if ((agent == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    network = r__network_model_proxy_get_network(&agent->network_proxy);

    /* Send event to agent while network is locked */
    error = r__network_model_lock(network);
    if (error) {
        goto FAIL_NETWORK_LOCK;
    }
    error = r__agent_model_send_back(agent, event);
    if (error) {
        goto FAIL_SEND_AGENT;
    }
    error = r__network_model_unlock(network);
    if (error) {
        goto FAIL_NETWORK_UNLOCK;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_NETWORK_UNLOCK:
FAIL_SEND_AGENT:
    (void) r__network_model_unlock(network);
FAIL_NETWORK_LOCK:
    return error;
}

r__error
r__agent_send_i(r__agent * agent, const r__event * event)
{
    r__error error;

    /* Validate arguments */
    if ((agent == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Send event to agent */
    error = r__agent_model_send_back(agent, event);
    if (error) {
        goto FAIL_SEND_AGENT;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_SEND_AGENT:
    return error;
}

r__error
r__agent_send_front(r__agent * agent, const r__event * event)
{
    r__error     error;
    r__network * network;

    /* Validate arguments */
    if ((agent == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    network = r__network_model_proxy_get_network(&agent->network_proxy);

    /* Send event to agent while network is locked */
    error = r__network_model_lock(network);
    if (error) {
        goto FAIL_NETWORK_LOCK;
    }
    error = r__agent_model_send_front(agent, event);
    if (error) {
        goto FAIL_SEND_AGENT;
    }
    error = r__network_model_unlock(network);
    if (error) {
        goto FAIL_NETWORK_UNLOCK;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_NETWORK_UNLOCK:
FAIL_SEND_AGENT:
    (void) r__network_model_unlock(network);
FAIL_NETWORK_LOCK:
    return error;
}

r__error
r__agent_send_front_i(r__agent * agent, const r__event * event)
{
    r__error error;

    /* Validate arguments */
    if ((agent == NULL) || (event == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }

    /* Send event to agent */
    error = r__agent_model_send_front(agent, event);
    if (error) {
        goto FAIL_SEND_AGENT;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_SEND_AGENT:
    return error;
}
