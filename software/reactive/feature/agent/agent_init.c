/*
 * agent_init.c
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/feature/agent/agent_init.h"

/* Depends */
#include "reactive/domain/agent_model.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

r__error
r__agent_init(
        r__agent *               agent,
        r__network *             network,
        r__network_task *        network_task,
        r__network_task_priority network_task_priority,
        r__sm_state *            sm_initial_state,
        void *                   sm_data,
        size_t                   sm_data_size,
        void *                   queue_storage,
        size_t                   queue_storage_elements)
{
    r__error error;

    /* Validate arguments */
    if ((agent == NULL) || (network == NULL) || (network_task == NULL)
        || (sm_initial_state == NULL) || (queue_storage == NULL)) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if ((queue_storage_elements == 0u)
        || ((sm_data_size != 0u) && (sm_data == NULL))) {
        return R__ERROR_INVALID_ARGUMENT;
    }

    /* Construct agent */
    r__agent_model_ctor_static(
            agent,
            network,
            sm_initial_state,
            sm_data,
            queue_storage,
            queue_storage_elements);

    /* Initialize network task */
    error = r__network_model_proxy_init_task(
            &agent->network_proxy,
            network_task,
            network_task_priority);
    if (error != R__ERROR_NONE) {
        goto FAIL_INIT_TASK;
    }

    return R__ERROR_NONE;
    /* Error handling */
FAIL_INIT_TASK:
    r__agent_model_dtor(agent);
    return error;
}
