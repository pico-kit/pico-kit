# manifest.mk

# General

# Domain and Feature
F_SRC_DIRS += software/reactive/domain
F_SRC_DIRS += software/reactive/domain/common
F_SRC_DIRS += software/reactive/domain/common/generic_list
F_SRC_DIRS += software/reactive/domain/common/generic_queue
F_SRC_DIRS += software/reactive/domain/common/generic_timer
F_SRC_DIRS += software/reactive/feature/agent
F_SRC_DIRS += software/reactive/feature/event
F_SRC_DIRS += software/reactive/feature/network
F_SRC_DIRS += software/reactive/feature/queue
F_SRC_DIRS += software/reactive/feature/timer
F_INC_DIRS += software

