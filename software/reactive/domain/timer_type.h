/*
 * timer_type.h
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_TIMER_TYPE_H_
#define REACTIVE_DOMAIN_TIMER_TYPE_H_

#include <stdint.h>
#include "reactive/domain/agent.h"
#include "reactive/domain/common/generic_timer/generic_timer_model.h"
#include "reactive/domain/event_model.h"
#include "reactive/domain/timer.h"

struct r__timer {
    r__generic_timer node;   //!< Timer instance
    r__event         event;  //!< Event instance used by this timer
    r__agent *       agent;  //!< Owner of this timer
};

#endif /* REACTIVE_DOMAIN_TIMER_TYPE_H_ */
