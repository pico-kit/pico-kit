/*
 * queue_model.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_QUEUE_MODEL_H_
#define DOMAIN_QUEUE_MODEL_H_

#include <stdbool.h>
#include <stddef.h>
#include "reactive/domain/error.h"
#include "reactive/domain/event.h"
#include "reactive/domain/queue_type.h"

#define R__QUEUE_MODEL_INITIALIZER(a_storage, a_n_entries)                     \
    {                                                                          \
        .queue = R__GENERIC_QUEUE_INITIALIZER(                                 \
                (void **) (a_storage),                                         \
                (a_n_entries)),                                                \
        .p__attributes = 0                                                     \
    }

void
r__queue_model_ctor(
        r__queue *        queue,
        r__agent *        agent,
        const r__event ** storage,
        size_t            n_entries);
void
r__queue_model_dtor(r__queue * queue);
bool
r__queue_model_is_empty(const r__queue * queue);
bool
r__queue_model_is_full(const r__queue * queue);
r__error
r__queue_model_put_fifo(r__queue * queue, const r__event * event);
r__error
r__queue_model_put_lifo(r__queue * queue, const r__event * event);
r__error
r__queue_model_fetch(r__queue * queue, const r__event ** event);
r__error
r__queue_model_delete_events(r__queue * queue);

#endif /* DOMAIN_QUEUE_MODEL_H_ */
