/*
 * network.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_NETWORK_H_
#define REACTIVE_DOMAIN_NETWORK_H_

typedef struct r__network r__network;

typedef struct r__network_scheduler r__network_scheduler;

typedef struct r__network_task r__network_task;

typedef enum r__network_task_priority {
    R__NETWORK_TASK_PRIORITY_LOWEST,
    R__NETWORK_TASK_PRIORITY_LOWER,
    R__NETWORK_TASK_PRIORITY_NORMAL,
    R__NETWORK_TASK_PRIORITY_HIGHER,
    R__NETWORK_TASK_PRIORITY_HIGHEST,
    R__NETWORK_TASK_PRIORITY_CRITICAL
} r__network_task_priority;

#endif /* REACTIVE_DOMAIN_NETWORK_H_ */
