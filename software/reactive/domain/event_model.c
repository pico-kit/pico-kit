/*
 * event.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/event_model.h"

/* Depends */
#include <stdatomic.h>
#include <stddef.h>
#include <stdint.h>
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/error_model.h"

enum r__event_attributes {
    R__EVENT_ATTRIBUTES_STATIC =
            0,  // Must have zero value for static allocation
    R__EVENT_ATTRIBUTES_DYNAMIC,
};

void
r__event_model_ctor_dynamic(
        r__event *  event,
        r__event_id event_id,
        void *      event_data)
{
    event->id            = event_id;
    event->data          = event_data;
    event->p__ref_count  = 0u;
    event->p__attributes = R__EVENT_ATTRIBUTES_DYNAMIC;
}

void
r__event_model_ctor_static(
        r__event *  event,
        r__event_id event_id,
        void *      event_data)
{
    event->id            = event_id;
    event->data          = event_data;
    event->p__ref_count  = 0u;
    event->p__attributes = R__EVENT_ATTRIBUTES_STATIC;
}

void
r__event_model_dtor(r__event * event)
{
    event->id            = INT32_MAX;
    event->data          = NULL;
    event->p__ref_count  = 0u;
    event->p__attributes = R__EVENT_ATTRIBUTES_STATIC;
}

void
r__event_model_usage_acquire(const r__event * event)
{
    if (event->p__attributes == R__EVENT_ATTRIBUTES_DYNAMIC) {
        r__event * dynamic_event;
        uint32_t   n_current;
        uint32_t   n_new;

        dynamic_event = (r__event *) event;
        n_current     = dynamic_event->p__ref_count;
        do {
            n_new = n_current + 1u;
        } while (!atomic_compare_exchange_weak(
                &dynamic_event->p__ref_count,
                &n_current,
                n_new));
    }
}

bool
r__event_model_usage_release(const r__event * event)
{
    bool       is_pending_deletion;
    uint32_t   n_current;
    uint32_t   n_new;
    r__event * dynamic_event;

    if (event->p__attributes != R__EVENT_ATTRIBUTES_DYNAMIC) {
        return false;
    }
    dynamic_event = (r__event *) event;
    n_current     = dynamic_event->p__ref_count;
    do {
        if (n_current > 0) {
            n_new = n_current - 1u;
        } else {
            n_new = n_current;
        }
        if (n_new > 0) {
            is_pending_deletion = false;
        } else {
            is_pending_deletion = true;
        }
    } while (!atomic_compare_exchange_weak(
            &dynamic_event->p__ref_count,
            &n_current,
            n_new));
    return is_pending_deletion;
}

bool
r__event_model_is_dynamic(const r__event * event)
{
    return event->p__attributes == R__EVENT_ATTRIBUTES_DYNAMIC;
}

r__error
r__event_model_create(
        r__event_id event_id,
        size_t      data_size,
        r__event ** event,
        void **     event_data)
{
    void * allocation;

    /* Allocate event and event_data */
    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_EVENT,
            sizeof(r__event),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_EVENT;
    }
    r__event * l_event = allocation;
    error              = r__allocator_allocate(
            R__ALLOCATOR_TYPE_EVENT_DATA,
            data_size,
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_EVENT_DATA;
    }
    void * l_event_data = allocation;

    /* Construct event */
    r__event_model_ctor_dynamic(l_event, event_id, l_event_data);
    *event = l_event;
    if (event_data != NULL) {
        *event_data = l_event_data;
    }
    return R__ERROR_NONE;

    /* Error handling */
FAIL_ALLOCATE_EVENT_DATA:
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_EVENT, event);
FAIL_ALLOCATE_EVENT:
    return error;
}

r__error
r__event_model_delete(const r__event * event)
{
    r__event * dynamic_event;
    void *     dynamic_event_data;

    /* If event is not a dynamic event then we just skip deleting it */
    if (r__event_model_usage_release(event) == false) {
        return R__ERROR_NONE;
    }
    /* Since usage release was successful we know this is dynamic event */
    dynamic_event      = (r__event *) event;
    dynamic_event_data = (void *) dynamic_event->data;
    r__event_model_dtor(dynamic_event);
    r__error error = r__allocator_deallocate(
            R__ALLOCATOR_TYPE_EVENT_DATA,
            dynamic_event_data);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_EVENT_DATA;
    }
    error = r__allocator_deallocate(R__ALLOCATOR_TYPE_EVENT, dynamic_event);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_EVENT;
    }
    return R__ERROR_NONE;
FAIL_DEALLOCATE_EVENT_DATA:
    /* Try to delete dynamic event data */
    (void) r__allocator_deallocate(R__ALLOCATOR_TYPE_EVENT, dynamic_event);
FAIL_DEALLOCATE_EVENT:
    return error;
}
