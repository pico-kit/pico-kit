/*
 * agent_model.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/agent_model.h"

/* Depends */
#include "reactive/domain/common/compiler.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"
#include "reactive/domain/network_model.h"
#include "reactive/domain/queue_model.h"
#include "reactive/domain/sm_model.h"

enum agent_attribute { AGENT_ATTRIBUTE_STATIC = 0, AGENT_ATTRIBUTE_DYNAMIC };

void
r__agent_model_ctor_dynamic(
        r__agent *        agent,
        r__network *      network,
        r__sm_state *     sm_initial_state,
        void *            sm_data,
        const r__event ** queue_storage,
        size_t            queue_storage_elements)
{
    r__sm_model_ctor(&agent->sm, sm_initial_state, sm_data);
    r__queue_model_ctor(
            &agent->queue,
            agent,
            queue_storage,
            queue_storage_elements);
    r__network_model_proxy_ctor(&agent->network_proxy, network);
    agent->p__attributes = AGENT_ATTRIBUTE_DYNAMIC;
}

void
r__agent_model_ctor_static(
        r__agent *        agent,
        r__network *      network,
        r__sm_state *     sm_initial_state,
        void *            sm_data,
        const r__event ** queue_storage,
        size_t            queue_storage_elements)
{
    r__sm_model_ctor(&agent->sm, sm_initial_state, sm_data);
    r__queue_model_ctor(
            &agent->queue,
            agent,
            queue_storage,
            queue_storage_elements);
    r__network_model_proxy_ctor(&agent->network_proxy, network);
    agent->p__attributes = AGENT_ATTRIBUTE_STATIC;
}

void
r__agent_model_dtor(r__agent * agent)
{
    agent->p__attributes = AGENT_ATTRIBUTE_STATIC;
    r__network_model_proxy_dtor(&agent->network_proxy);
    r__queue_model_dtor(&agent->queue);
    r__sm_model_dtor(&agent->sm);
}

r__error
r__agent_model_send_back(r__agent * agent, const r__event * event)
{
    r__error error;

    error = r__queue_model_put_fifo(&agent->queue, event);
    if (error) {
        goto FAIL_QUEUE_IS_FULL;
    }
    error = r__network_model_proxy_signal(&agent->network_proxy);
    if (error) {
        goto FAIL_SIGNAL_NETWORK;
    }
    return R__ERROR_NONE;
FAIL_SIGNAL_NETWORK:
FAIL_QUEUE_IS_FULL:
    return error;
}

r__error
r__agent_model_send_front(r__agent * agent, const r__event * event)
{
    r__error error;

    error = r__queue_model_put_lifo(&agent->queue, event);
    if (error) {
        goto FAIL_QUEUE_IS_FULL;
    }
    error = r__network_model_proxy_signal(&agent->network_proxy);
    if (error) {
        goto FAIL_SIGNAL_NETWORK;
    }
    return R__ERROR_NONE;
FAIL_SIGNAL_NETWORK:
FAIL_QUEUE_IS_FULL:
    return error;
}

bool
r__agent_model_is_dynamic(const r__agent * agent)
{
    return agent->p__attributes == AGENT_ATTRIBUTE_DYNAMIC;
}

bool
r__agent_model_is_static(const r__agent * agent)
{
    return agent->p__attributes == AGENT_ATTRIBUTE_STATIC;
}

r__agent *
r__agent_model_decompose(r__sm * sm)
{
    return R__COMPILER_CONTAINER_OF(sm, r__agent, sm);
}

r__error
r__network_model_dispatch_proxy(struct r__network_proxy * network_proxy)
{
    const r__event * event;

    r__agent * agent =
            R__COMPILER_CONTAINER_OF(network_proxy, r__agent, network_proxy);

    r__error error = r__network_model_proxy_wait(network_proxy);
    if (error) {
        // TODO: Make the Agent transit to the failed state
        goto FAIL_WAIT_TASK;
    }
    r__network * network = r__network_model_proxy_get_network(network_proxy);
    error                = r__network_model_lock(network);
    if (error) {
        // TODO: Make the Agent transit to the failed state
        goto FAIL_LOCK_QUEUE;
    }
    error = r__queue_model_fetch(&agent->queue, &event);
    (void) r__network_model_unlock(network);
    if (error) {
        // TODO: Make the Agent transit to the failed state
        goto FAIL_FETCH_EVENT;
    }
    error = r__sm_model_dispatch(&agent->sm, event);
    if (error) {
        (void) r__event_model_delete(event);
        // TODO: Make the Agent transit to the failed state
        goto FAIL_DISPATCH_PROCESSOR;
    }
    error = r__event_model_delete(event);
    if (error != R__ERROR_NONE) {
        goto FAIL_DELETE_EVENT;
    }
    return R__ERROR_NONE;
FAIL_DELETE_EVENT:
FAIL_DISPATCH_PROCESSOR:
FAIL_FETCH_EVENT:
FAIL_LOCK_QUEUE:
FAIL_WAIT_TASK:
    return error;
}

extern inline struct r__network_proxy *
r__agent_model_get_network_proxy(r__agent * agent);

extern inline r__queue *
r__agent_model_get_queue(r__agent * agent);
