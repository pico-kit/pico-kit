/*
 * queue.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/queue_model.h"

/* Depends */
#include "reactive/domain/common/generic_queue/generic_queue.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"

void
r__queue_model_ctor(
        r__queue *        queue,
        r__agent *        agent,
        const r__event ** storage,
        size_t            n_entries)
{
    r__generic_queue_init(&queue->p__queue, n_entries, (void **) storage);
    queue->agent = agent;
}

void
r__queue_model_dtor(r__queue * queue)
{
    r__generic_queue_term(&queue->p__queue);
}

bool
r__queue_model_is_empty(const r__queue * queue)
{
    return r__generic_queue_is_empty(&queue->p__queue);
}

bool
r__queue_model_is_full(const r__queue * queue)
{
    return r__generic_queue_is_full(&queue->p__queue);
}

r__error
r__queue_model_put_fifo(r__queue * queue, const r__event * event)
{
    if (r__generic_queue_is_full(&queue->p__queue)) {
        return R__ERROR_NO_SPACE;
    }
    r__generic_queue_put_fifo(&queue->p__queue, (void *) event);
    return R__ERROR_NONE;
}

r__error
r__queue_model_put_lifo(r__queue * queue, const r__event * event)
{
    if (r__generic_queue_is_full(&queue->p__queue)) {
        return R__ERROR_NO_SPACE;
    }
    r__generic_queue_put_lifo(&queue->p__queue, (void *) event);
    return R__ERROR_NONE;
}

r__error
r__queue_model_fetch(r__queue * queue, const r__event ** event)
{
    if (r__generic_queue_is_empty(&queue->p__queue)) {
        return R__ERROR_NO_ENTITY;
    }
    *event = r__generic_queue_get_tail(&queue->p__queue);
    return R__ERROR_NONE;
}

r__error
r__queue_model_delete_events(r__queue * queue)
{
    const r__event * event;

    while (r__queue_model_fetch(queue, &event) == R__ERROR_NONE) {
        r__event_model_delete(event);
    }
    return R__ERROR_NONE;
}
