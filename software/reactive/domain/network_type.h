/*
 * network_type.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_NETWORK_TYPE_H_
#define REACTIVE_DOMAIN_NETWORK_TYPE_H_

#include "reactive/domain/common/generic_list/generic_list_model.h"
#include "reactive/domain/error.h"
#include "reactive/domain/network.h"

/* Forward declarations */
struct r__network_proxy;

struct r__network {
    r__network_scheduler * network_scheduler;
    r__list                register_list;
};

struct r__network_task {
    const struct r__network_task_vft {
        r__error (*signal)(r__network_task * network_task);
        r__error (*wait)(r__network_task * network_task);
    } *                       vft;
    struct r__network_proxy * network_proxy;
};

struct r__network_scheduler {
    const struct r__network_scheduler_vft {
        r__error (*lock)(r__network_scheduler * network_scheduler);
        r__error (*unlock)(r__network_scheduler * network_scheduler);
        r__error (*start)(r__network_scheduler * network_scheduler);
        r__error (*stop)(r__network_scheduler * network_scheduler);
        r__error (*create_task)(
                r__network_scheduler *    network_scheduler,
                r__network_task_priority  network_task_priority,
                struct r__network_proxy * network_proxy,
                r__network_task **        network_task);
        r__error (*delete_task)(r__network_task * network_task);
        r__error (*init_task)(
                r__network_task *         network_task,
                r__network_scheduler *    network_scheduler,
                r__network_task_priority  network_task_priority,
                struct r__network_proxy * network_proxy);
        r__error (*deinit_task)(r__network_task * network_task);
    } * vft;
};

struct r__network_proxy {
    r__list           register_node;
    r__network *      network;
    r__network_task * network_task;
};

#endif /* REACTIVE_DOMAIN_NETWORK_TYPE_H_ */
