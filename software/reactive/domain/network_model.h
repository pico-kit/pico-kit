/*
 * network_model.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_NETWORK_MODEL_H_
#define REACTIVE_DOMAIN_NETWORK_MODEL_H_

#include "reactive/domain/network_type.h"

/* Network network_scheduler methods */

inline r__error
r__network_model_scheduler_lock(r__network_scheduler * network_scheduler)
{
    return network_scheduler->vft->lock(network_scheduler);
}

inline r__error
r__network_model_scheduler_unlock(r__network_scheduler * network_scheduler)
{
    return network_scheduler->vft->unlock(network_scheduler);
}

r__error
r__network_model_scheduler_start(r__network_scheduler * network_scheduler);

r__error
r__network_model_scheduler_stop(r__network_scheduler * network_scheduler);

r__error
r__network_model_scheduler_task_create(
        r__network_scheduler *    network_scheduler,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy,
        r__network_task **        network_task);

r__error
r__network_model_scheduler_task_delete(
        r__network_scheduler * network_scheduler,
        r__network_task *      network_task);

r__error
r__network_model_scheduler_task_init(
        r__network_scheduler *    network_scheduler,
        r__network_task *         network_task,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy);

r__error
r__network_model_scheduler_task_deinit(
        r__network_scheduler * network_scheduler,
        r__network_task *      network_task);

/* Network task methods */

void
r__network_model_task_ctor(
        r__network_task *                  network_task,
        const struct r__network_task_vft * vft,
        struct r__network_proxy *          network_proxy);

void
r__network_model_task_dtor(r__network_task * network_task);

inline r__error
r__network_model_task_signal(r__network_task * network_task)
{
    return network_task->vft->signal(network_task);
}

inline r__error
r__network_model_task_wait(r__network_task * network_task)
{
    return network_task->vft->wait(network_task);
}

/* Network methods */

void
r__network_model_ctor(
        r__network *           network,
        r__network_scheduler * network_scheduler);

void
r__network_model_dtor(r__network * network);

inline r__error
r__network_model_lock(r__network * network)
{
    return r__network_model_scheduler_lock(network->network_scheduler);
}

inline r__error
r__network_model_unlock(r__network * network)
{
    return r__network_model_scheduler_unlock(network->network_scheduler);
}

inline r__error
r__network_model_start(r__network * network)
{
    return r__network_model_scheduler_start(network->network_scheduler);
}

inline r__error
r__network_model_stop(r__network * network)
{
    return r__network_model_scheduler_stop(network->network_scheduler);
}

void
r__network_model_proxy_ctor(
        struct r__network_proxy * network_proxy,
        r__network *              network);

void
r__network_model_proxy_dtor(struct r__network_proxy * node);

r__error
r__network_model_proxy_create_task(
        struct r__network_proxy * network_proxy,
        r__network_task_priority  network_task_priority);

r__error
r__network_model_proxy_delete_task(struct r__network_proxy * network_proxy);

r__error
r__network_model_proxy_init_task(
        struct r__network_proxy * network_proxy,
        r__network_task *         network_task,
        r__network_task_priority  network_task_priority);

r__error
r__network_model_proxy_deinit_task(struct r__network_proxy * network_proxy);

inline r__network *
r__network_model_proxy_get_network(
        const struct r__network_proxy * network_proxy)
{
    return network_proxy->network;
}

inline r__error
r__network_model_proxy_signal(struct r__network_proxy * network_proxy)
{
    return r__network_model_task_signal(network_proxy->network_task);
}

inline r__error
r__network_model_proxy_wait(struct r__network_proxy * network_proxy)
{
    return r__network_model_task_wait(network_proxy->network_task);
}

extern r__error
r__network_model_dispatch_proxy(struct r__network_proxy * network_proxy);

#endif /* REACTIVE_DOMAIN_NETWORK_MODEL_H_ */
