/*
 * state_machine_model.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_SM_MODEL_H_
#define DOMAIN_SM_MODEL_H_

#include "reactive/domain/error.h"
#include "reactive/domain/event.h"
#include "reactive/domain/sm_type.h"

extern const r__event enter_event;
extern const r__event exit_event;
extern const r__event init_event;
extern const r__event super_event;

enum r__sm_action {
    R__SM_ACTION_HANDLED,
    R__SM_ACTION_SUPER,
    R__SM_ACTION_TRANSIT
};

#define R__SM_MODEL_INITIALIZER(sm_initial_state, sm_data)                     \
    {                                                                          \
        .state = (sm_initial_state), .data = (sm_data),                        \
    }

void
r__sm_model_ctor(r__sm * sm, r__sm_state * initial_state, void * data);
void
r__sm_model_dtor(r__sm * sm);
r__error
r__sm_model_dispatch(r__sm * sm, const r__event * event);

#endif /* DOMAIN_SM_MODEL_H_ */
