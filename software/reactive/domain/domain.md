# Domain models

@startuml
title Domain models

package Domain {
	package Agent as agent
	package Error as error
	package Event as event
	package network as network {
		component Node <<Component>> as network_proxy
		component network <<Component>> as network_scheduler
		
		network_scheduler --> network_proxy
	}
	package Queue as queue
	package "State machine" as sm
	package Timer as timer
	
	event --> error
	
	sm --> error
	sm --> event
	
	queue --> error
	queue --> event
	
	agent --> error
	agent --> sm
	agent --> event
	agent --> queue
	agent --> network_proxy
	agent -> network_scheduler
	
	timer --> error
	timer --> agent
	timer --> event
}
@enduml

@startuml
title Interaction of Agent, network and network_scheduler

package Agent {
	class agent {
	  # dispatch() : r__error
	}
}

package Network {
	class network_proxy <<Protected>> {
	  - register_node : list
	  - network : network
	  # network_task : network_task
	  + get_network() : network
	  + wait() : r__error
	  + signal() : r__error
  	  + create_task() : r__error
	  + delete_task() : r__error
	  + init_task() : r__error
	  + deinit_task() : r__error
	}
	
	class network {
	  - registered : list
	  # network_scheduler : network_scheduler
	  + lock() : r__error
	  + unlock() : r__error
	  + start() : r__error
	  + stop() : r__error
	  + create_task() : r__error
	  + delete_task() : r__error
	  + init_task() : r__error
	  + deinit_task() : r__error
	}
	
	abstract class network_task {
	  - network_proxy : network_proxy
	  + signal() : r__error {abstract}
	  + wait() : r__error {abstract}
	}
	
	abstract class network_scheduler {
	  + lock() : r__error {abstract}
	  + unlock() : r__error {abstract}
	  + start() : r__error {abstract}
	  + stop() : r__error {abstract}
	  + create_task() : r__error {abstract}
	  + delete_task() : r__error {abstract}
	  + init_task() : r__error {abstract}
	  + deinit_task() : r__error {abstract}
	}
	
	interface network_task_dispatch
	
	network "1" -r-> "0 .. n" network_proxy : owns
	network_proxy "1" -l-> "1" network : belongs to
	network_task_dispatch .l.> network_task : uses
	
	network_scheduler .l.> network_task : creates
	
	network -d-> network_scheduler : network_scheduler
    network_proxy -d-> network_task : network_task
    network_task -u-> network_proxy : network_proxy
}

package ConcreteImplementation {
	class ConcreteScheduler {
	  + lock() : r__error
	  + unlock() : r__error
	  + start() : r__error
	  + stop() : r__error
	  + create_task() : r__error
	  + delete_task() : r__error
	  + init_task() : r__error
	  + deinit_task() : r__error
	}
	
	class ConcreteTaskSchedulerProxy  {
	  + signal() : r__error
	  + wait() : r__error
	  - semaphore : ConcreteSemaphore
	}
}

agent *-d- network_proxy : composite
agent -d-|> network_task_dispatch : realizes



ConcreteScheduler -u-|> network_scheduler : realizes
ConcreteTaskSchedulerProxy -u-|> network_task : realizes
ConcreteTaskSchedulerProxy -u-> network_task_dispatch : task execution

@enduml
