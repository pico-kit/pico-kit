/*
 * generic_timer_model.h
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_COMMON_GENERIC_TIMER_GENERIC_TIMER_MODEL_H_
#define REACTIVE_DOMAIN_COMMON_GENERIC_TIMER_GENERIC_TIMER_MODEL_H_

#include <stdint.h>
#include "reactive/domain/common/generic_list/generic_list_model.h"

typedef struct r__generic_timer r__generic_timer;
typedef void(r__generic_timer_fn)(r__generic_timer *);

enum r__generic_timer_state {
    R__GENERIC_TIMER_STATE_DORMANT,
    R__GENERIC_TIMER_STATE_PENDING,
    R__GENERIC_TIMER_STATE_ACTIVE
};

struct r__generic_timer {
    r__list                     list;     //!< Linked list node
    uint32_t                    r_ticks;  //!< Relative ticks
    uint32_t                    i_ticks;  //!< Initial ticks
    r__generic_timer_fn *       fn;       //!< Callback function
    enum r__generic_timer_state state;    //!< State of timer node
};

#endif /* REACTIVE_DOMAIN_COMMON_GENERIC_TIMER_GENERIC_TIMER_MODEL_H_ */
