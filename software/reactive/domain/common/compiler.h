/*
 * compiler.h
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_SHARED_COMPILER_H_
#define REACTIVE_DOMAIN_SHARED_COMPILER_H_

/**
 * @brief       Cast a member of a structure out to the containing structure.
 * @param       ptr the pointer to the member.
 * @param       type the type of the container struct this is embedded in.
 * @param       member the name of the member within the struct.
 */
#define R__COMPILER_CONTAINER_OF(ptr, type, member)                            \
    ((type *) ((char *) (uintptr_t) (ptr) - offsetof(type, member)))

#endif /* REACTIVE_DOMAIN_SHARED_COMPILER_H_ */
