/*
 * list.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/common/generic_list/generic_list.h"

extern inline void
r__list_init(r__list * self);

extern inline void
r__list_deinit(r__list * self);

extern inline struct r__list *
r__list_next(const r__list * self);

extern inline struct r__list *
r__list_prev(const r__list * self);

extern inline void
r__list_add_after(r__list * self, r__list * after);

extern inline void
r__list_add_before(r__list * self, r__list * before);

extern inline void
r__list_remove(r__list * self);

extern inline bool
r__list_is_empty(const struct r__list * self);

extern inline bool
r__list_is_singular(const struct r__list * self);
