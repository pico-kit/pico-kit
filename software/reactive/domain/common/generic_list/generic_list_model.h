/*
 * component_list_model.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_SHARED_LIST_LIST_MODEL_H_
#define DOMAIN_SHARED_LIST_LIST_MODEL_H_

/**
 * @brief       List node object.
 * @note        All members of this structure are private.
 */
typedef struct r__list {
    struct r__list * next;  //!< Next node in linked list.
    struct r__list * prev;  //!< Previous node in linked list.
} r__list;

#endif /* DOMAIN_SHARED_LIST_LIST_MODEL_H_ */
