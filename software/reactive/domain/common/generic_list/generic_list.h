/*
 * generic_list.h
 *
 *  Created on: Oct 13, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_SHARED_GENERIC_LIST_GENERIC_LIST_H_
#define REACTIVE_DOMAIN_SHARED_GENERIC_LIST_GENERIC_LIST_H_

#include "reactive/domain/common/generic_list/generic_list_model.h"

#include <stdbool.h>
#include <stddef.h>

/**
 * @brief		Statically allocated list initializer
 */
#define R__LIST_INITIALIZER(list)                                              \
    {                                                                          \
        .next = (list), .prev = (list)                                         \
    }

/**
 * @brief      	Construct for @a FOR loop to iterate over each element in a
 * list.
 *
 * @code
 * r__list * current;
 * r__list * sentinel = &g_list_sentinel.list;
 *
 * for (R__LIST_EACH(current, sentinel)) {
 *     ... do something with @a current (excluding remove)
 * }
 * @endcode
 */
#define R__LIST_EACH(current, sentinel)                                        \
    current = (sentinel)->next;                                                \
    current != (sentinel);                                                     \
    current = (current)->next

/**
 * @brief      	Construct for FOR loop to iterate over each element in list
 * backwards.
 *
 * @code
 * r__list * current;
 * r__list * sentinel = &g_list_sentinel.list;
 *
 * for (R__LIST_BACKWARDS(current, sentinel)) {
 *     ... do something with current (excluding remove)
 * }
 * @endcode
 */
#define R__LIST_BACKWARDS(current, sentinel)                                   \
    current = (sentinel)->prev;                                                \
    current != (sentinel);                                                     \
    current = (current)->prev

/**
 * @brief      	Construct for FOR loop to iterate over each element in list
 * which is safe against element removal.
 *
 * @code
 * r__list * current;
 * r__list * iterator;
 * r__list * sentinel = &g_list_sentinel.list;
 *
 * for (R__LIST_EACH_SAFE(current, iterator, sentinel)) {
 *     ... do something with current (including remove)
 * }
 * @endcode
 */
#define R__LIST_EACH_SAFE(current, iterator, sentinel)                         \
    current = (sentinel)->next, iterator = (current)->next;                    \
    current != (sentinel);                                                     \
    current = iterator, iterator = (iterator)->next

inline void
r__list_init(r__list * self)
{
    self->next = self;
    self->prev = self;
}

inline void
r__list_deinit(r__list * self)
{
    self->next = NULL;
    self->prev = NULL;
}

inline struct r__list *
r__list_next(const r__list * self)
{
    return self->next;
}

inline struct r__list *
r__list_prev(const r__list * self)
{
    return self->prev;
}

inline void
r__list_add_after(r__list * self, r__list * after)
{
    self->next        = after->next;
    self->prev        = after;
    after->next->prev = self;
    after->next       = self;
}

inline void
r__list_add_before(r__list * self, r__list * before)
{
    self->next         = before;
    self->prev         = before->prev;
    before->prev->next = self;
    before->prev       = self;
}

inline void
r__list_remove(r__list * self)
{
    self->next->prev = self->prev;
    self->prev->next = self->next;
    /* The following `self` initialization code is necessary:
     * - since we want to support calling this function multiple times over the
     * same list node that was already removed from a list.
     * - since we want to support querying if a list is empty or not.
     */
    self->next = self;
    self->prev = self;
}

inline bool
r__list_is_empty(const struct r__list * self)
{
    return !!(self->next == self);
}

inline bool
r__list_is_singular(const struct r__list * self)
{
    return !!((self->next != self) && (self->next == self->prev));
}

#endif /* REACTIVE_DOMAIN_SHARED_GENERIC_LIST_GENERIC_LIST_H_ */
