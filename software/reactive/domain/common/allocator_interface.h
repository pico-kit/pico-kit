/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Memory allocator interface header.
 */
/**
 * @defgroup    allocator_iface Memory allocator interface
 * @brief       Memory allocator interface
 *
 * This interface needs to be implemented by adapter code. The interface only
 * provides type and function declarations, while the implementation is done in
 * a specific adapter. For more information refer to
 * `documentation/DEVELOPER_MANUAL.md`.
 *
 * @{
 */

#ifndef REACTIVE_FEATURE_COMMON_ALLOCATOR_INTERFACE_H_
#define REACTIVE_FEATURE_COMMON_ALLOCATOR_INTERFACE_H_

/* Depends */
#include <stddef.h>
#include <stdint.h>
#include "reactive/domain/error.h"

/**
 * @brief       Memory allocator types.
 *
 * For each allocation/deallocation an allocator type is specified. This
 * information can be used for debugging purposes and/or for optimization of
 * used memory pools.
 */
enum r__allocator_type {
    R__ALLOCATOR_TYPE_EVENT,              //!< Allocation of event instances
    R__ALLOCATOR_TYPE_EVENT_DATA,         //!< Allocation of event variable data
    R__ALLOCATOR_TYPE_QUEUE,              //!< Allocation of queue instances
    R__ALLOCATOR_TYPE_QUEUE_DATA,         //!< Allocation of queue variable data
    R__ALLOCATOR_TYPE_AGENT,              //!< Allocation of agent instances
    R__ALLOCATOR_TYPE_AGENT_DATA,         //!< Allocation of agent variable data
    R__ALLOCATOR_TYPE_TIMER,              //!< Allocation of timer instances
    R__ALLOCATOR_TYPE_NETWORK,            //!< Allocation of network instances
    R__ALLOCATOR_TYPE_NETWORK_SCHEDULER,  //!< Allocation of network scheduler
                                          //!< instances
    R__ALLOCATOR_TYPE_NETWORK_TASK,  //!< Allocation of network task instances
};

/**
 * @brief       Memory allocator type underlaying type.
 */
typedef uint32_t r__allocator_type;

/**
 * @brief       Allocate memory from an allocation pool.
 *
 * Allocates memory for a given usage specified in @a allocator argument.
 * Besides the allocator type in @a allocator, the function receives how many
 * bytes it needs to allocate.
 *
 * @param       allocator Specifies the type of allocator. For details about
 *              allocator types see @ref r__allocator_type.
 * @param       size Size of requested memory in bytes. Internally, this
 *              argument might be aligned up and the allocator is allowed to
 *              return a bigger memory block then what was requested here.
 * @param [out] memory Pointer to `void *` pointer which will contain memory
 *              allocated by the function.
 * @return      Error of the operation execution.
 * @retval      Error @ref R__ERROR_NONE is returned when the operation
 *              completed successfully.
 * @retval      Error @ref R__ERROR_NULL_ARGUMENT is returned when @a memory
 *              argument is a NULL pointer.
 * @retval      Error @ref R__ERROR_NO_MEMORY is returned when the underlaying
 *              memory allocator was not able to allocate memory since it is
 *              depleted.
 *
 * @pre         Argument @a allocator must be in range of @ref r__allocator_type
 *              enumerator.
 *
 * @note        When the argument @a size is equal to zero, the allocator shall
 *              set NULL pointer in @a memory argument and return
 *              @ref R__ERROR_NONE error.
 *
 * @remark      This function is thread safe and can be called from multiple
 *              threads.
 * @remark      This function is ISR safe and can be called from ISR context.
 */
r__error
r__allocator_allocate(r__allocator_type allocator, size_t size, void ** memory);

/**
 * @brief       Deallocate previosly allocated memory.
 *
 * Deallocate the memory which was previosly allocated with
 * @ref r__allocator_allocate function.
 *
 * @param       allocator Specifies the type of allocator. For details about
 *              allocator types see @ref r__allocator_type.
 * @param       memory Pointer to allocated memory.
 * @return      Error of the operation execution.
 * @retval      Error @ref R__ERROR_NONE is returned when the operation
 *              completed successfully.
 *
 * @pre         Argument @a allocator must be in range of @ref r__allocator_type
 *              enumerator.
 *
 * @note        When the @a memory argument is a NULL pointer the function shall
 *              return @ref R__ERROR_NONE error.
 *
 * @remark      This function is thread safe and can be called from multiple
 *              threads.
 * @remark      This function is ISR safe and can be called from ISR context.
 */
r__error
r__allocator_deallocate(r__allocator_type allocator, void * memory);

#endif /* REACTIVE_FEATURE_COMMON_ALLOCATOR_INTERFACE_H_ */
       /** @} */