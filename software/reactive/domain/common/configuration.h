/*
 * configuration.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_CONFIGURATION_H_
#define DOMAIN_CONFIGURATION_H_

#if !defined(R__CONFIGURATION_USE_INTEGRITY)
#define R__CONFIGURATION_USE_INTEGRITY 0
#endif

#if !defined(R__CONFIGURATION_HSM_LEVELS)
#define R__CONFIGURATION_HSM_LEVELS 16
#endif

#endif /* DOMAIN_CONFIGURATION_H_ */
