/*
 * generic_queue.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/common/generic_queue/generic_queue.h"

/* Depends */
#include <stddef.h>

void
r__generic_queue_init(
        r__generic_queue * self,
        uint32_t           n_entries,
        void **            storage)
{
    self->head      = 0u;
    self->tail      = 0u;
    self->n_entries = n_entries;
    self->n_free    = n_entries;
    self->storage   = storage;
}

void
r__generic_queue_term(r__generic_queue * self)
{
    self->head      = 0u;
    self->tail      = 0u;
    self->n_entries = 0u;
    self->n_free    = 0u;
    self->storage   = NULL;
}

void
r__generic_queue_put_fifo(r__generic_queue * self, void * item)
{
    self->storage[self->head++] = item;
    if (self->head == self->n_entries) {
        self->head = 0u;
    }
    self->n_free--;
}

void
r__generic_queue_put_lifo(r__generic_queue * self, void * item)
{
    self->tail--;
    if (self->tail >= self->n_entries) {
        self->tail = self->n_entries - 1u;
    }
    self->storage[self->tail] = item;
    self->n_free--;
}

void *
r__generic_queue_get_tail(r__generic_queue * self)
{
    void * entry;

    entry = self->storage[self->tail++];
    if (self->tail == self->n_entries) {
        self->tail = 0u;
    }
    self->n_free++;

    return entry;
}

void *
r__generic_queue_head(const r__generic_queue * self)
{
    uint32_t real_head;

    real_head = self->head;
    real_head--;
    if (real_head >= self->n_entries) {
        real_head = self->n_entries - 1u;
    }
    return self->storage[real_head];
}

void *
r__generic_queue_tail(const r__generic_queue * self)
{
    return self->storage[self->tail];
}

extern inline uint32_t
r__generic_queue_n_entries(const r__generic_queue * self);

extern inline uint32_t
r__generic_queue_n_free(const r__generic_queue * self);

extern inline uint32_t
r__generic_queue_n_occupied(const r__generic_queue * queue);

extern inline bool
r__generic_queue_is_empty(const r__generic_queue * self);

extern inline bool
r__generic_queue_is_full(const r__generic_queue * self);
