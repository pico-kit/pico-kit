/*
 * generic_queue.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_SHARED_GENERIC_QUEUE_GENERIC_QUEUE_H_
#define DOMAIN_SHARED_GENERIC_QUEUE_GENERIC_QUEUE_H_

#include "reactive/domain/common/generic_queue/generic_queue_model.h"

#include <stdbool.h>

#define R__GENERIC_QUEUE_INITIALIZER(storage, a_n_entries)                     \
    {                                                                          \
        .head = 0u, .tail = 0u, .n_entries = (a_n_entries),                    \
        .n_free = (a_n_entries), .storage = (storage)                          \
    }

void
r__generic_queue_init(
        r__generic_queue * queue,
        uint32_t           n_entries,
        void **            storage);

void
r__generic_queue_term(r__generic_queue * queue);

void
r__generic_queue_put_fifo(r__generic_queue * queue, void * item);

void
r__generic_queue_put_lifo(r__generic_queue * queue, void * item);

void *
r__generic_queue_get_tail(r__generic_queue * queue);

void *
r__generic_queue_head(const r__generic_queue * queue);

void *
r__generic_queue_tail(const r__generic_queue * queue);

inline uint32_t
r__generic_queue_n_entries(const r__generic_queue * queue)
{
    return queue->n_entries;
}

inline uint32_t
r__generic_queue_n_free(const r__generic_queue * queue)
{
    return queue->n_free;
}

inline uint32_t
r__generic_queue_n_occupied(const r__generic_queue * queue)
{
    return queue->n_entries - queue->n_free;
}

inline bool
r__generic_queue_is_empty(const r__generic_queue * queue)
{
    return queue->n_free == queue->n_entries;
}

inline bool
r__generic_queue_is_full(const r__generic_queue * queue)
{
    return queue->n_free == 0u;
}

#endif /* DOMAIN_SHARED_GENERIC_QUEUE_GENERIC_QUEUE_H_ */
