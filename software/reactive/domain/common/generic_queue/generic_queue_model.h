/*
 * queue_model.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_SHARED_QUEUE_QUEUE_MODEL_H_
#define DOMAIN_SHARED_QUEUE_QUEUE_MODEL_H_

#include <stdint.h>

typedef struct r__generic_queue {
    uint32_t head;       //!< Index pointing to head of queue.
    uint32_t tail;       //!< Index pointing to tail of queue.
    uint32_t n_entries;  //!< How many entries are put into the queue.
    uint32_t n_free;     //!< How many entries are still free.
    void **  storage;    //!< Pointer to the array which contains entries.
} r__generic_queue;

#endif /* DOMAIN_SHARED_QUEUE_QUEUE_MODEL_H_ */
