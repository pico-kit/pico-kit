/*
 * sm.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/sm_model.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/common/compiler.h"
#include "reactive/domain/common/configuration.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/event_model.h"

/**
 * @brief       HSM path structure
 *
 * This structure contains a entry or exit path of HSM states. It is generated
 * during the runtime when source and destination states are specified.
 */
struct hsm_path {
    r__sm_state * buff[R__CONFIGURATION_HSM_LEVELS];
    uint_fast8_t  index;
};

static r__error
hsm_get_state_super(r__sm * sm, r__sm_state * state)
{
    r__sm_action action;

    action = state(sm, &super_event);
    if (action != R__SM_ACTION_SUPER) {
        return R__ERROR_INVALID_SM_ACTION;
    } else {
        return R__ERROR_NONE;
    }
}

static r__error
hsm_path_enter(r__sm * sm, const struct hsm_path * entry)
{
    uint_fast8_t index = entry->index;

    while (index--) {
        r__sm_action action;

        action = entry->buff[index](sm, &enter_event);
        if (action == R__SM_ACTION_TRANSIT) {
            return R__ERROR_INVALID_SM_ENTER;
        }
    }
    return R__ERROR_NONE;
}

static r__error
hsm_path_exit(r__sm * sm, const struct hsm_path * exit)
{
    for (uint_fast8_t count = 0u; count < exit->index; count++) {
        r__sm_action action;

        action = exit->buff[count](sm, &exit_event);
        if (action == R__SM_ACTION_TRANSIT) {
            return R__ERROR_INVALID_SM_ENTER;
        }
    }
    return R__ERROR_NONE;
}

static r__error
hsm_path_build(r__sm * sm, struct hsm_path * entry, struct hsm_path * exit)
{
    r__error error;

    entry->index = 0;
    exit->index--;

    /*--  path: a) source ?== destination
     * ---------------------------------------*/
    if (exit->buff[exit->index] == entry->buff[entry->index]) {
        entry->index++;
        /* TODO: Check index boundary */
        exit->index++;
        /* TODO: Check index boundary */
        return R__ERROR_NONE;
    }
    /*--  path: b) source ?== super(destination)
     * --------------------------------*/
    error = hsm_get_state_super(sm, entry->buff[entry->index++]);
    /* TODO: Check index boundary */
    if (error) {
        return error;
    }
    entry->buff[entry->index] = sm->state;

    if (exit->buff[exit->index] == entry->buff[entry->index]) {
        return R__ERROR_NONE;
    }
    /*--  path: c) super(source) ?== super(destination)
     * -------------------------*/
    error = hsm_get_state_super(sm, exit->buff[exit->index++]);
    /* TODO: Check index boundary */
    if (error) {
        return error;
    }
    exit->buff[exit->index] = sm->state;

    if (exit->buff[exit->index] == entry->buff[entry->index]) {
        return R__ERROR_NONE;
    }
    /*--  path: d) super(source) ?== destination
     * --------------------------------*/
    entry->index--;

    if (exit->buff[exit->index] == entry->buff[entry->index]) {
        return R__ERROR_NONE;
    }
    /*--  path: e) source ?== ...super(super(destination))
     * ----------------------*/
    exit->index--;
    entry->index++;
    /* TODO: Check index boundary */
    while (entry->buff[entry->index] != &r__sm_top_state) {
        error = hsm_get_state_super(sm, entry->buff[entry->index++]);
        /* TODO: Check index boundary */
        if (error) {
            return error;
        }
        entry->buff[entry->index] = sm->state;
        if (exit->buff[exit->index] == entry->buff[entry->index]) {
            return R__ERROR_NONE;
        }
    }
    /*--  path: f) super(source) ?== ...super(super(destination))
     * ---------------*/
    exit->index++;
    /* TODO: Check index boundary */
    while (entry->index != 1u) {
        if (exit->buff[exit->index] == entry->buff[entry->index]) {
            return R__ERROR_NONE;
        }
        entry->index--;
    }
    /*--  path: g) ...super(super(source)) ?== ...super(super(destination))
     * -----*/

    for (;;) {
        error = hsm_get_state_super(sm, exit->buff[exit->index++]);
        /* TODO: Check index boundary */
        if (error) {
            return error;
        }
        exit->buff[exit->index] = sm->state;
        entry->index            = 0u;

        do {
            entry->index++;
            /* TODO: Check index boundary */
            if (exit->buff[exit->index] == entry->buff[entry->index]) {
                return R__ERROR_NONE;
            }
        } while (entry->buff[entry->index] != &r__sm_top_state);
    }
    return R__ERROR_NONE;
}

const r__event enter_event =
        R__EVENT_MODEL_INITIALIZER(R__SM_EVENT_ENTER, NULL);
const r__event exit_event = R__EVENT_MODEL_INITIALIZER(R__SM_EVENT_EXIT, NULL);
const r__event init_event = R__EVENT_MODEL_INITIALIZER(R__SM_EVENT_INIT, NULL);
const r__event super_event =
        R__EVENT_MODEL_INITIALIZER(R__SM_EVENT_SUPER, NULL);

void
r__sm_model_ctor(r__sm * sm, r__sm_state * initial_state, void * data)
{
    sm->state = initial_state;
    sm->data  = data;
}

void
r__sm_model_dtor(r__sm * sm)
{
    sm->state = NULL;
    sm->data  = NULL;
}

r__sm_action
r__sm_top_state(r__sm * sm, const r__event * event)
{
    (void) sm;
    (void) event;
    return R__SM_ACTION_HANDLED;
}

r__sm_action
r__sm_handled(r__sm * sm)
{
    (void) sm;
    return R__SM_ACTION_HANDLED;
}

r__sm_action
r__sm_super(r__sm * sm, r__sm_state * state)
{
    sm->state = state;
    return R__SM_ACTION_SUPER;
}

r__sm_action
r__sm_transit(r__sm * sm, r__sm_state * state)
{
    sm->state = state;
    return R__SM_ACTION_TRANSIT;
}

r__error
r__sm_model_dispatch(r__sm * sm, const r__event * event)
{
    r__sm_action    action;
    r__sm_state *   current_state;
    struct hsm_path entry;
    struct hsm_path exit;

    exit.index    = 0u;
    current_state = sm->state;

    do {
        exit.buff[exit.index++] = sm->state;
        action                  = sm->state(sm, event);
    } while (action == R__SM_ACTION_SUPER);

    while (action == R__SM_ACTION_TRANSIT) {
        entry.buff[0]  = sm->state;
        r__error error = hsm_path_build(sm, &entry, &exit);
        if (error) {
            return error;
        }
        error = hsm_path_exit(sm, &exit);
        if (error) {
            return error;
        }
        error = hsm_path_enter(sm, &entry);
        if (error) {
            return error;
        }
        action        = entry.buff[0](sm, &init_event);
        exit.buff[0]  = entry.buff[0];
        exit.index    = 1u;
        current_state = entry.buff[0];
    }
    sm->state = current_state;

    return R__ERROR_NONE;
}
