/*
 * network_model.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/network_model.h"

/* Depends */
#include <stddef.h>
#include "reactive/domain/common/generic_list/generic_list.h"

extern inline r__error
r__network_model_scheduler_lock(r__network_scheduler * network_scheduler);

extern inline r__error
r__network_model_scheduler_unlock(r__network_scheduler * network_scheduler);

r__error
r__network_model_scheduler_start(r__network_scheduler * network_scheduler)
{
    return network_scheduler->vft->start(network_scheduler);
}

r__error
r__network_model_scheduler_stop(r__network_scheduler * network_scheduler)
{
    return network_scheduler->vft->stop(network_scheduler);
}

r__error
r__network_model_scheduler_task_create(
        r__network_scheduler *    network_scheduler,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy,
        r__network_task **        network_task)
{
    return network_scheduler->vft->create_task(
            network_scheduler,
            network_task_priority,
            network_proxy,
            network_task);
}

r__error
r__network_model_scheduler_task_delete(
        r__network_scheduler * network_scheduler,
        r__network_task *      network_task)
{
    return network_scheduler->vft->delete_task(network_task);
}

r__error
r__network_model_scheduler_task_init(
        r__network_scheduler *    network_scheduler,
        r__network_task *         network_task,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy)
{
    return network_scheduler->vft->init_task(
            network_task,
            network_scheduler,
            network_task_priority,
            network_proxy);
}

r__error
r__network_model_scheduler_task_deinit(
        r__network_scheduler * network_scheduler,
        r__network_task *      network_task)
{
    return network_scheduler->vft->deinit_task(network_task);
}

extern inline r__error
r__network_model_task_signal(r__network_task * network_task);

extern inline r__error
r__network_model_task_wait(r__network_task * network_task);

void
r__network_model_task_ctor(
        r__network_task *                  network_task,
        const struct r__network_task_vft * vft,
        struct r__network_proxy *          network_proxy)
{
    network_task->vft           = vft;
    network_task->network_proxy = network_proxy;
}

inline void
r__network_model_task_dtor(r__network_task * network_task)
{
    network_task->network_proxy = NULL;
    network_task->vft           = NULL;
}

extern inline r__error
r__network_model_task_signal(r__network_task * network_task);

extern inline r__error
r__network_model_task_wait(r__network_task * network_task);

void
r__network_model_ctor(
        r__network *           network,
        r__network_scheduler * network_scheduler)
{
    network->network_scheduler = network_scheduler;
    r__list_init(&network->register_list);
}

void
r__network_model_dtor(r__network * network)
{
    r__list_deinit(&network->register_list);
    network->network_scheduler = NULL;
}

extern inline r__error
r__network_model_lock(r__network * network);

extern inline r__error
r__network_model_unlock(r__network * network);

extern inline r__error
r__network_model_start(r__network * network);

extern inline r__error
r__network_model_stop(r__network * network);

void
r__network_model_proxy_ctor(
        struct r__network_proxy * network_proxy,
        r__network *              network)
{
    r__list_init(&network_proxy->register_node);
    network_proxy->network      = network;
    network_proxy->network_task = NULL;
}

void
r__network_model_proxy_dtor(struct r__network_proxy * network_proxy)
{
    network_proxy->network_task = NULL;
    network_proxy->network      = NULL;
    r__list_deinit(&network_proxy->register_node);
}

r__error
r__network_model_proxy_create_task(
        struct r__network_proxy * network_proxy,
        r__network_task_priority  network_task_priority)
{
    return r__network_model_scheduler_task_create(
            network_proxy->network->network_scheduler,
            network_task_priority,
            network_proxy,
            &network_proxy->network_task);
}

r__error
r__network_model_proxy_delete_task(struct r__network_proxy * network_proxy)
{
    return r__network_model_scheduler_task_delete(
            network_proxy->network->network_scheduler,
            network_proxy->network_task);
}

r__error
r__network_model_proxy_init_task(
        struct r__network_proxy * network_proxy,
        r__network_task *         network_task,
        r__network_task_priority  network_task_priority)
{
    network_proxy->network_task = network_task;
    return r__network_model_scheduler_task_init(
            network_proxy->network->network_scheduler,
            network_proxy->network_task,
            network_task_priority,
            network_proxy);
}

r__error
r__network_model_proxy_deinit_task(struct r__network_proxy * network_proxy)
{
    return r__network_model_scheduler_task_deinit(
            network_proxy->network->network_scheduler,
            network_proxy->network_task);
}

extern inline r__network *
r__network_model_proxy_get_network(
        const struct r__network_proxy * network_proxy);

extern inline r__error
r__network_model_proxy_signal(struct r__network_proxy * network_proxy);

extern inline r__error
r__network_model_proxy_wait(struct r__network_proxy * network_proxy);
