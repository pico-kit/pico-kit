/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Domain error header.
 */
/**
 * @defgroup    domain_error Domain error
 * @brief       Domain error
 * @{
 */

#ifndef REACTIVE_DOMAIN_ERROR_H_
#define REACTIVE_DOMAIN_ERROR_H_

#include <stdint.h>

/**
 * @brief       Error type.
 *
 * All errors are represented using this type. For actual definition of
 * available errors refer to @ref domain_error_model.
 */
typedef uint32_t r__error;

#endif /* REACTIVE_DOMAIN_ERROR_H_ */
       /** @} */