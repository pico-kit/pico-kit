/*
 * sm.h
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_SM_H_
#define REACTIVE_DOMAIN_SM_H_

#include "reactive/domain/event.h"

enum r__sm_event {
    R__SM_EVENT_ENTER = -1,
    R__SM_EVENT_EXIT  = -2,
    R__SM_EVENT_INIT  = -3,
    R__SM_EVENT_SUPER = -4
};

typedef struct r__sm r__sm;
typedef uint32_t     r__sm_action;
typedef r__sm_action(r__sm_state)(r__sm * sm, const r__event * event);

r__sm_action
r__sm_top_state(r__sm * sm, const r__event * event);

r__sm_action
r__sm_handled(r__sm * sm);

r__sm_action
r__sm_super(r__sm * sm, r__sm_state * state);

r__sm_action
r__sm_transit(r__sm * sm, r__sm_state * state);

#endif /* REACTIVE_DOMAIN_SM_H_ */
