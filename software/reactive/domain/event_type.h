/*
 * event_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_EVENT_TYPE_H_
#define DOMAIN_EVENT_TYPE_H_

#include <stdatomic.h>
#include <stdint.h>

#include "reactive/domain/event.h"

struct r__event {
    r__event_id  id;
    const void * data;
    atomic_uint  p__ref_count;
    uint32_t     p__attributes;
};

#endif /* DOMAIN_EVENT_TYPE_H_ */
