/*
 * agent_model.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_AGENT_MODEL_H_
#define DOMAIN_AGENT_MODEL_H_

#include "reactive/domain/agent_type.h"

#include <stdbool.h>
#include <stddef.h>
#include "reactive/domain/error.h"

void
r__agent_model_ctor_dynamic(
        r__agent *        agent,
        r__network *      network,
        r__sm_state *     sm_initial_state,
        void *            sm_data,
        const r__event ** queue_storage,
        size_t            queue_storage_elements);

void
r__agent_model_ctor_static(
        r__agent *        agent,
        r__network *      network,
        r__sm_state *     sm_initial_state,
        void *            sm_data,
        const r__event ** queue_storage,
        size_t            queue_storage_elements);

void
r__agent_model_dtor(r__agent * agent);

r__error
r__agent_model_send_back(r__agent * agent, const r__event * event);

r__error
r__agent_model_send_front(r__agent * agent, const r__event * event);

bool
r__agent_model_is_dynamic(const r__agent * agent);

bool
r__agent_model_is_static(const r__agent * agent);

r__agent *
r__agent_model_decompose(r__sm * sm);

inline struct r__network_proxy *
r__agent_model_get_network_proxy(r__agent * agent)
{
    return &agent->network_proxy;
}

inline r__queue *
r__agent_model_get_queue(r__agent * agent)
{
    return &agent->queue;
}

#endif /* DOMAIN_AGENT_MODEL_H_ */
