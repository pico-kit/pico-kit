/*
 * timer_model.h
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_TIMER_MODEL_H_
#define REACTIVE_DOMAIN_TIMER_MODEL_H_

#include "reactive/domain/timer_type.h"

void
r__timer_model_ctor_dynamic(
        r__timer *  timer,
        r__event_id event_id,
        r__agent *  agent);
void
r__timer_model_dtor(r__timer * timer);
void
r__timer_model_cancel(r__timer * timer);

#endif /* REACTIVE_DOMAIN_TIMER_MODEL_H_ */
