/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Domain error model header.
 */
/**
 * @defgroup    domain_error_model Domain error model
 * @brief       Domain error model.
 * @{
 */

#ifndef DOMAIN_ERROR_MODEL_H_
#define DOMAIN_ERROR_MODEL_H_

#include "reactive/domain/error.h"

enum r__error {
    R__ERROR_NONE,              //!< No error occured.
    R__ERROR_NULL_ARGUMENT,     //!< A function was supplied with NULL argument.
    R__ERROR_INVALID_ARGUMENT,  //!< A function was supplied with invalid (out
                                //!< of range) argument.
    R__ERROR_INVALID_OBJECT,
    R__ERROR_NO_MEMORY,  //!< No memory is available for allocation.
    R__ERROR_INVALID_SM_ACTION,
    R__ERROR_INVALID_SM_ENTER,
    R__ERROR_NO_SPACE,
    R__ERROR_NO_ENTITY,
    R__ERROR_SCHEDULER_FAILURE,
};

#endif /* DOMAIN_ERROR_MODEL_H_ */
       /** @} */