/*
 * timer_model.c
 *
 *  Created on: Oct 19, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/timer_model.h"

/* Depends */
#include "reactive/domain/event_model.h"

void
r__timer_model_ctor_dynamic(
        r__timer *  timer,
        r__event_id event_id,
        r__agent *  agent)
{
    (void) agent;
    r__event_model_ctor_static(&timer->event, event_id, NULL);
}
void
r__timer_model_dtor(r__timer * timer)
{
    (void) timer;
}
void
r__timer_model_cancel(r__timer * timer)
{
    (void) timer;
}
