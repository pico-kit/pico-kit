/*
 * agent_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_AGENT_TYPE_H_
#define DOMAIN_AGENT_TYPE_H_

#include <stdint.h>
#include "reactive/domain/agent.h"
#include "reactive/domain/common/generic_list/generic_list_model.h"
#include "reactive/domain/network_type.h"
#include "reactive/domain/queue_type.h"
#include "reactive/domain/sm_type.h"

struct r__agent {
    r__sm                   sm;
    r__queue                queue;
    r__list                 register_node;
    struct r__network_proxy network_proxy;
    uint32_t                p__attributes;
};

#endif /* DOMAIN_AGENT_TYPE_H_ */
