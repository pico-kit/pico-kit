/*
 * sm_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_DOMAIN_SM_TYPE_H_
#define REACTIVE_DOMAIN_SM_TYPE_H_

#include "reactive/domain/sm.h"

struct r__sm {
    r__sm_state * state;
    void *        data;
};

#endif /* REACTIVE_DOMAIN_SM_TYPE_H_ */
