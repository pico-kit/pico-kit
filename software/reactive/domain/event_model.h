/*
 * event_model.h
 *
 *  Created on: Oct 10, 2023
 *      Author: nenad
 */

#ifndef DOMAIN_EVENT_MODEL_H_
#define DOMAIN_EVENT_MODEL_H_

/* Extends */
#include "reactive/domain/event_type.h"

/* Depends */
#include <stdbool.h>
#include <stddef.h>
#include "reactive/domain/error.h"

#define R__EVENT_MODEL_INITIALIZER(event_id, event_data)                       \
    {                                                                          \
        .id = (event_id), .data = (event_data), .p__ref_count = 0u,            \
        .p__attributes = 0                                                     \
    }

void
r__event_model_ctor_dynamic(
        r__event *  event,
        r__event_id event_id,
        void *      event_data);

void
r__event_model_ctor_static(
        r__event *  event,
        r__event_id event_id,
        void *      event_data);

void
r__event_model_dtor(r__event * event);

void
r__event_model_usage_acquire(const r__event * event);

bool
r__event_model_usage_release(const r__event * event);

bool
r__event_model_is_dynamic(const r__event * event);

r__error
r__event_model_create(
        r__event_id event_id,
        size_t      data_size,
        r__event ** event,
        void **     event_data);

r__error
r__event_model_delete(const r__event * event);

#endif /* DOMAIN_EVENT_MODEL_H_ */
