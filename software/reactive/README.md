# Pico-Kit Reactive

# Dependencies

This package requires the following dependencies:
- standard C99 library
- Pico-Kit Foundation library

The Pico-Kit Foundation library is compiled separately. The Pico-Kit Reactive
needs include paths to this library headers in order to compile.

# Build

The build supports the following building strategies:

- (__no build, pre-built configuration__) - Repository checkout where user is
  responsible for:
  - which reactive source directories are to be compiled into the project,
  - which adapter source directories are to be compiled into the project,
  - which include directories are needed for project sources to compile.

- (__no build, custom configuration__) - Repository checkout and then configure
  the library source where the user is responsible for:
  - which reactive source directories are to be compiled into the project,
  - which adapter source directories are to be compiled into the project,
  - which include directories are needed for project sources to compile.

  This strategy is very similar to the first listed strategy.

- (__library build__) - Repository checkout, configure and then build the
  Pico-Kit Reactive library when the user is responsible for:
  - linking the generated Pico-Kit Reactive library to the project,
  - which include directories are needed for project sources to compile.

- (__executable build__) - Repository checkout, configure and then build the
  executable with Pico-Kit Reactive library and application library. The build
  is fully controlled by the Pico-Kit Reactive. The user does not need to manage
  the build.
