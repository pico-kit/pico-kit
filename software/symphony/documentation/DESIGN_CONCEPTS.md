# Common Design Concepts

All RTOS objects share a common design concept. The overall life-cycle of an
object can be summarized as created -> in use -> destroyed.

# Create Objects

An object is created by calling its `s__xxx_create` function. The new function
returns an identifier that can be used to operate with the new object. The
actual state of an object is typically stored in an object specific control
block. The memory layout (and size needed) for the control block is
implementation specific. One should not make any specific assumptions about the
control block.  The control block layout might change and hence should be seen
as an implementation internal detail.

In order to expose control about object specific options all `s__xxx_create`
functions provide an optional `attr ` argument, which can be left as NULL by
default. It takes a pointer to an object specific attribute structure, commonly
containing the fields:

- `name` to attach a human readable name to the object for identification,
- `attr_bits` to control object-specific options, 
- `instance` to provide memory for the control block manually

The `name` attribute is only used for object identification, e.g.  using
RTOS-aware debugging. The attached string is not used for any other purposes
internally. All objects store only pointer to the string, not the actuall
string, therefore, the string must allocated during the whole lifetime of a
object.

The `instance` attribute can be used to provide memory for the control block
manually instead of relying on the implementation internal memory allocation.
One has to assure that the memory pointed to by instance is sufficient for the
objects control block structure. To ensure this, include additional
implementation specific header which contains structure definitions and
allocate the structures. Furthermore providing control block memory manually is
less portable. Thus one has to take care about implementation specific
alignment and placement requirements for instance. Refer to Memory Management
for further details.

## Object Usage

After an object has been created successfully it can be used until it is
destroyed. The actions defined for an object depends on its type. Commonly all
the `s__xxx_do_something` access function require the reference to the object
to work with as the first argument.

The access function can be assumed to apply some sort of sanity checking on the
first argument. So that it is assured one cannot accidentally call an access
function with a NULL object reference. Furthermore the concrete object type is
verified, i.e. one cannot call access functions of one object type with a
reference to another object type.

All further argument checks applied are either object and action specific or
may even be implementation specific. Thus one should always check error
function return values for `S__ERROR_NULL_ARGUMENT` or
`S__ERROR_INVALID_ARGUMENT` to assure the provided arguments were accepted.

As a rule of thumb only non-blocking access function can be used from Interrupt
Service Routines (ISR). This incorporates `s__xxx_wait` functions (and similar)
limited to be called with argument timeout set to 0, i.e. usage of
try-semantics.

## Object Destruction

Objects that are not needed anymore can be destructed on demand to free the
control block memory. Objects are not destructed implicitly. Thus one can
assume an object id to be valid until `s__xxx_destroy` is called explicitly.
The delete function finally frees the control block memory. In case of user
provided control block memory, see above, the memory must be freed manually as
well.

The only exception one has to take care of are Threads which do not have an
explicit `s__thread_destroy` function. Threads can either be detached or
joinable.  Detached threads are automatically destroyed on termination, i.e.
call to `s__thread_terminate` or `s__thread_exit` or return from thread
function. On the other hand joinable threads are kept alive until one
explicitly calls `s__thread_join`.
