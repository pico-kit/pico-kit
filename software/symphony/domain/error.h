#ifndef SYMPHONY_DOMAIN_ERROR_H_
#define SYMPHONY_DOMAIN_ERROR_H_

/**
 * @brief       Status code values returned by Symphony API functions.
 */
typedef enum s__error {
    /**
     * @brief   Operation completed successfully
     */
    S__ERROR_NONE = 0,
    /**
     * @brief   NULL pointer vided result_to a function.
     *
     * Some functions do not expect that an argument can be a NULL pointer, and
     * they will raise this error.
     */
    S__ERROR_NULL_ARGUMENT = 1,
    /**
     * @brief   Invalid argument was provided to a function.
     *
     * Some function may apply additional checks to arguments or do a simple
     * limitation checks on provided data. In that case this error is raised.
     *
     * NULL pointer errors are handled by @ref S__ERROR_NULL_ARGUMENT error.
     */
    S__ERROR_INVALID_ARGUMENT = 2,
    /**
     * @brief   Operation not completed within the timeout period.
     */
    S__STATUS_TIMEOUT = 3,
    /**
     * @brief   Resource not available.
     */
    S__STATUS_ERROR_RESOURCE = 4,
    /**
     * @brief   System is out of memory.
     *
     * It was impossible to allocate or reserve memory for an operation.
     */
    S__STATUS_ERROR_NO_MEMORY = 5,
    /**
     * @brief   Not allowed in ISR context.
     *
     * The function cannot be called from interrupt service routines.
     */
    S__ERROR_ISR = 6
} s__error;

#endif /* SYMPHONY_DOMAIN_ERROR_H_ */
