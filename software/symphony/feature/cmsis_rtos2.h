/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Symphony implementation agnostic API header.
 *
 * This header file contains Symphony API which is implementation agnostic. A
 * implementation specific definitions can be found in `symphony/domain`
 * headers.
 */
/**
 * @defgroup    symphony_api Symphony API.
 *
 * @brief       Symphony implementation agnostic API.
 *
 * All RTOS objects share a common design concept. The overall life-cycle of an
 * object can be summarized as created -> in use -> destroyed.
 *
 * ## Create Objects
 *
 * An object is created by calling its `s__xxx_create` function. The new
 * function returns an identifier that can be used to operate with the new
 * object. The actual state of an object is typically stored in an object
 * specific control block. The memory layout (and size needed) for the control
 * block is implementation specific. One should not make any specific
 * assumptions about the control block.  The control block layout might change
 * and hence should be seen as an implementation internal detail.
 *
 * In order to expose control about object specific options all `s__xxx_create`
 * functions provide an optional `attr ` argument, which can be left as NULL by
 * default. It takes a pointer to an object specific attribute structure,
 * commonly containing the fields:
 *
 * - `name` to attach a human readable name to the object for identification,
 * - `attr_bits` to control object-specific options,
 * - `instance` to provide memory for the control block manually
 *
 * The `name` attribute is only used for object identification, e.g.  using
 * RTOS-aware debugging. The attached string is not used for any other purposes
 * internally. All objects store only pointer to the string, not the actuall
 * string, therefore, the string must allocated during the whole lifetime of a
 * object.
 *
 * The `instance` attribute can be used to provide memory for the control block
 * manually instead of relying on the implementation internal memory
 * allocation. One has to assure that the memory pointed to by instance is
 * sufficient for the objects control block structure. To ensure this, include
 * additional implementation specific header which contains structure
 * definitions and allocate the structures. Furthermore providing control block
 * memory manually is less portable. Thus one has to take care about
 * implementation specific alignment and placement requirements for instance.
 * Refer to \ref symphony_memory for further details.
 *
 * ## Object Usage
 *
 * After an object has been created successfully it can be used until it is
 * destroyed. The actions defined for an object depends on its type. Commonly
 * all the `s__xxx_do_something` access function require the reference to the
 * object to work with as the first argument.
 *
 * The access function can be assumed to apply some sort of sanity checking on
 * the first argument. So that it is assured one cannot accidentally call an
 * access function with a NULL object reference. Furthermore the concrete
 * object type is verified, i.e. one cannot call access functions of one object
 * type with a reference to another object type.
 *
 * All further argument checks applied are either object and action specific or
 * may even be implementation specific. Thus one should always check error
 * function return values for `S__ERROR_NULL_ARGUMENT` or
 * `S__ERROR_INVALID_ARGUMENT` to assure the provided arguments were accepted.
 *
 * As a rule of thumb only non-blocking access function can be used from
 * Interrupt Service Routines (ISR). This incorporates `s__xxx_wait` functions
 * (and similar) limited to be called with argument timeout set to 0, i.e.
 * usage of try-semantics.
 *
 * ## Object Destruction
 *
 * Objects that are not needed anymore can be destructed on demand to free the
 * control block memory. Objects are not destructed implicitly. Thus one can
 * assume an object id to be valid until `s__xxx_destroy` is called explicitly.
 * The delete function finally frees the control block memory. In case of user
 * provided control block memory, see above, the memory must be freed manually
 * as well.
 *
 * The only exception one has to take care of are Threads which do not have an
 * explicit `s__thread_destroy` function. Threads can either be detached or
 * joinable.  Detached threads are automatically destroyed on termination, i.e.
 * call to `s__thread_terminate` or `s__thread_exit` or return from thread
 * function. On the other hand joinable threads are kept alive until one
 * explicitly calls `s__thread_join`.
 *
 * @{
 */
/**
 * @defgroup    symphony_memory Memory Management
 *
 * @brief       Information about memory management possibilities.
 *
 * The Symphony API offers two options for memory management the user can
 * choose. For object storage one can either use:
 * - Automatic Dynamic Allocation (fully portable), or
 * - Manual User-defined Allocation (implementation specific).
 *
 * In order to affect the memory allocation scheme all RTOS objects that can be
 * created on request, i.e. those having a `s__xxx_create` function, accept an
 * optional `s__xxx_attr` attr argument on creation. As a rule of thumb the
 * object attributes at least have members to assign custom control block
 * memory, i.e. `instance` member. By default, i.e. attr is NULL or `instance`
 * is NULL, Automatic Dynamic Allocation is used. Providing a pointer to user
 * memory in `instance` switches to Manual User-defined Allocation.
 */
/**
 * @ingroup     symphony_memory
 * @defgroup    symphony_dynamic_allocation Automatic Dynamic Allocation
 *
 * @brief       Details about automatic dynamic memory allocation.
 *
 * The automatic allocation is the default and viable for many use-cases.
 * Moreover it is fully portable across different implementations of the
 * Symphony API. The common drawback of dynamic memory allocation is the
 * possibility of memory fragmentation and exhaustion. Given that all needed
 * objects are created once upon system initialization and never deleted at
 * runtime this class of runtime failures can be prevented, though.
 *
 * The actual allocation strategy used is implementation specific, i.e. whether
 * global heap or preallocated memory pools are used.
 *
 * Code Example:
 * @code
 * #include "symphony/symphony.h"                  // implementation agnostic
 *
 * s__mutex mutex_id;
 * s__mutex mutex2_id;
 *
 * const s__mutex_attr mutex_attr =
 * {
 *     // human readable mutex name
 *     .name = "My Mutex",
 *     // Attribute bits
 *     .attr_bits = S__MUTEX_RECURSIVE | S__MUTEX_PRIORITY_INHERIT,
 *     // memory for control block (default)
 *     .instance = NULL,
 * };
 *
 * void create_my_mutex(void)
 * {
 *     // use default values for all attributes
 *     mutex_id = s__mutex_create(NULL);
 *     // use attributes from defined structure
 *     mutex2_id = s__mutex_create(&mutex_attr);
 * }
 * @endcode
 *
 * The Mutexes in this example are created using automatic memory allocation.
 */
/**
 * @ingroup     symphony_memory
 * @defgroup    symphony_manual_allocation Manual User-defined Allocation
 *
 * @brief       Details about manual user-defined memory allocation.
 *
 * One can get fine grained control over memory allocation by providing
 * user-defined memory. The actual requirements such user-defined memory are
 * implementation specific. Thus one needs to carefully refer to the size and
 * alignment rules of the implementation used.
 *
 * Code Example:
 * @code
 * #include "symphony/symphony.h"                  // implementation agnostic
 * #include "symphony/domain/mutex_type.h          // implementation specific
 *
 * s__mutex mutex_id;
 *
 * static s__mutex mutex_instance;
 *
 * static const s__mutex_attr mutex_attr =
 * {
 *     // human readable mutex name
 *     .name = "myThreadMutex",
 *     // attribute bites
 *     .attr_bits = S__MUTEX_RECURSIVE | S__MUTEX_PRIORITY_INHERIT,
 *     // memory for control block (user-defined)
 *     .instance = &mutex_instance,
 * };
 *
 * void create_my_mutex(void)
 * {
 *     // use attributes from defined structure
 *     mutex_id = s__mutex_create(&mutex_attr);
 * }
 * @endcode
 *
 * The above example uses user-defined memory for the mutex control block.
 * Depending on the actual implementation used one needs to include the
 * specific header file.
 */
/**
 * @defgroup    symphony_isr Calls from Interrupt Service Routines
 *
 * @brief       Considerations using Interrupt Service Routines.
 *
 * The following Symphony functions can be called from threads and Interrupt
 * Service Routines (ISR):
 * - @ref s__kernel_get_info, @ref s__kernel_get_state, osKernelGetTickCount,
 *   osKernelGetTickFreq, osKernelGetSysTimerCount, osKernelGetSysTimerFreq
 *   osThreadGetId, osThreadFlagsSet
 * - osEventFlagsSet, osEventFlagsClear, osEventFlagsGet, osEventFlagsWait
 * - osSemaphoreAcquire, osSemaphoreRelease, osSemaphoreGetCount
 * - osMemoryPoolAlloc, osMemoryPoolFree, osMemoryPoolGetCapacity,
 *   osMemoryPoolGetBlockSize, osMemoryPoolGetCount, osMemoryPoolGetSpace
 * - osMessageQueuePut, osMessageQueueGet, osMessageQueueGetCapacity,
 *   osMessageQueueGetMsgSize, osMessageQueueGetCount, osMessageQueueGetSpace
 *
 * Functions that cannot be called from an ISR are verifying the interrupt
 * status and return the status code @ref S__ERROR_ISR.
 */

#ifndef SYMPHONY_H_
#define SYMPHONY_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__CC_ARM)
#define S__NO_RETURN __declspec(noreturn)
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
#define S__NO_RETURN __attribute__((__noreturn__))
#elif defined(__GNUC__)
#define S__NO_RETURN __attribute__((__noreturn__))
#elif defined(__ICCARM__)
#define S__NO_RETURN __noreturn
#else
#define S__NO_RETURN
#endif

/**
 * @defgroup    symphony_error Error handling
 *
 * @brief       Error handling technique
 *
 * All functions that are part of Symphony API layer return @ref s__error type
 * to report the operation status and errors to the caller.
 *
 * @{
 */

/**
 * @brief       Error code values returned by Symphony API functions.
 */
typedef enum s__error {
    /**
     * @brief   Operation completed successfully
     *
     * This enumerator is returned when there was no error during the
     * operation. This enumerator will always be equal to zero, so errors can
     * be checked with simple conditions like:
     *
     * @code
     * s__error error;
     * // Call an API function setting the `error` variable
     * if (error) {
     *      // Do something in case of error
     * }
     * @endcode
     */
    S__ERROR_NONE = 0,
    /**
     * @brief   NULL pointer vided result_to a function.
     *
     * Some functions do not expect that an argument can be a NULL pointer, and
     * they will raise this error.
     */
    S__ERROR_NULL_ARGUMENT = 1,
    /**
     * @brief   Invalid argument was provided to a function.
     *
     * Some function may apply additional checks to arguments or do a simple
     * limitation checks on provided data. In that case this error is raised.
     *
     * NULL pointer errors are handled by @ref S__ERROR_NULL_ARGUMENT error.
     */
    S__ERROR_INVALID_ARGUMENT = 2,
    /**
     * @brief   Operation not completed within the timeout period.
     */
    S__STATUS_TIMEOUT = 3,
    /**
     * @brief   Resource not available.
     */
    S__STATUS_ERROR_RESOURCE = 4,
    /**
     * @brief   System is out of memory.
     *
     * It was impossible to allocate or reserve memory for an operation.
     */
    S__ERROR_NO_MEMORY = 5,
    /**
     * @brief   Not allowed in ISR context.
     *
     * The function cannot be called from interrupt service routines.
     */
    S__ERROR_ISR = 6,
    /**
     * @brief   An operation was requested while the object/system is in wrong
     *          state.
     */
    S__ERROR_WRONG_STATE = 7
} s__error;

/** @} */
/**
 * @defgroup    symphony_kernel Kernel information and control
 *
 * @brief       Provides version/system information and starts/controls the
 *              RTOS Kernel.
 *
 * The kernel Information and Control function group allows to:
 * - obtain information about the system and the underlying kernel.
 * - obtain version information about the CMSIS-RTOS API.
 * - initialize of the RTOS kernel for creating objects.
 * - start the RTOS kernel and thread switching.
 * - check the execution status of the RTOS kernel.
 *
 * @{
 */

/**
 * @brief       Version information.
 */
typedef struct s__kernel_version {
    /**
     * @brief   API version (major.minor.rev: mmnnnrrrr dec).
     */
    uint32_t api;
    /**
     * @brief   Kernel version (major.minor.rev: mmnnnrrrr dec).
     */
    uint32_t kernel;
} s__kernel_version;

/**
 * @brief       Kernel lock state.
 *
 * Kernel lock can be active and inactive. When the lock is active, kernel
 * scheduler is forbidden to change tasks (context switching is disabled), and
 * when the lock is inactive, scheduler is able to initiate context switches.
 * By default, when the system is booted the lock is inactive (
 * @ref S__KERNEL_LOCK_INACTIVE).
 */
typedef enum s__kernel_lock_state {
    S__KERNEL_LOCK_INACTIVE,  //!< Lock is inactive
    S__KERNEL_LOCK_ACTIVE     //!< Lock is active
} s__kernel_lock_state;

/// Kernel state.
typedef enum s__kernel_state {
    S__KERNEL_STATE_INACTIVE  = 0,  //!< Inactive.
    S__KERNEL_STATE_READY     = 1,  //!< Ready.
    S__KERNEL_STATE_RUNNING   = 2,  //!< Running.
    S__KERNEL_STATE_LOCKED    = 3,  //!< Locked.
    S__KERNEL_STATE_SUSPENDED = 4,  //!< Suspended.
} s__kernel_state;

/**
 * @brief       Kernel limits
 */
extern const struct s__kernel_limits {
    uint32_t   tick_freq_min_hz;
    uint32_t   tick_freq_max_hz;
    uint32_t * tick_allowed_hz;
    size_t     tick_allowed_hz_elements;
} s__kernel_limits;

/**
 * @brief       Kernel ticks type.
 */
typedef uint32_t s__kernel_ticks;

/**
 * @brief       Initialize the RTOS kernel.
 *
 * The function __s__kernel_initialize__ initializes the RTOS Kernel. Before it
 * is successfully executed, only the functions @ref s__kernel_get_info and
 * @ref s__kernel_get_state may be called.
 *
 * @return      Integer representing operation error, see @ref symphony_error
 *              for details.
 *@retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR), see @ref symphony_isr.
 *
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 *
 * @code
 * #include <assert>
 * #include "symphony/symphony.h"
 *
 * void app_main(void * argument)
 * {
 *     // ...
 *     for (;;) {}
 * }
 *
 * int main(void)
 * {
 *     s__error error;
 *     // System Initialization
 *     // ...
 *     // Initialize RTOS
 *     error = s__kernel_initialize();
 *     assert(error);
 *     // Create application main thread
 *     error = s__thread_create(app_main, NULL, NULL);
 *     assert(error);
 *     // Start main application thread execution
 *     error = s__kernel_start();
 *     assert(error);
 *     for (;;) {}
 *     return 0;
 * }
 * @endcode
 */
s__error
s__kernel_initialize(void);

/**
 * @brief       Get the RTOS kernel information.
 *
 * The function __s__kernel_get_info__ retrieves the API and kernel version of
 * the underlying RTOS kernel and a human readable identifier string for the
 * kernel. It can be safely called before the RTOS is initialized or started
 * (call to @ref s__kernel_initialize or @ref s__kernel_start).
 *
 * @param [out] version is pointer to alocated structure @ref s__kernel_version
 *              which will hold the version information. In case of an error
 *              the @a version referenced structure remains unchanged.
 * @param [out] id_buf is pointer to buffer for retreiving kernel
 *              identification string. This argument can be NULL pointer. In
 *              case of an error the @a id_buf referenced buffer remains
 *              unchanged.
 * @param       id_size is size of buffer for kernel identification string. If
 *              @a id_buf is NULL pointer, this argument must be set to zero.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_NULL_ARGUMENT is returned when @a version is a NULL
 *              pointer.
 * @retval      S__ERROR_INVALID_ARGUMENT is returned when @a id_buf is NULL
 *              pointer, but @a id_size is not zero.
 *
 * @note        Argument @a id_buf may be set to NULL if one does not need
 *              kernel identification string. When this argument is set to
 *              NULL, then argument @a id_size must also be set to zero.
 * @note        This function may be called from Interrupt Service Routines,
 *              see @ref symphony_isr.
 *
 * @code
 * #include <stdio.h>
 * #include "symphony/symphony.h"
 *
 * int main(void)
 * {
 *     s__error error;
 *     s__kernel_version version;
 *     char infobuf[100];
 *
 *     error = s__kernel_get_info(&version, infobuf, sizeof(infobuf));
 *     if (error == S__ERROR_NONE) {
 *         printf("Kernel information: %s\n", infobuf);
 *         printf("Kernel version    : %d\n", version.kernel);
 *         printf("Kernel API version: %d\n", version.api);
 *     }
 *     return 0;
 * }
 * @endcode
 */
s__error
s__kernel_get_info(s__kernel_version * version, char * id_buf, size_t id_size);

/**
 * @brief       Get the current RTOS kernel state.
 *
 * The function __s__kernel_get_state__ returns the current state of the kernel
 * and can be safely called before the RTOS is initialized or started (call to
 * @ref s__kernel_initialize or @ref s__kernel_start).
 *
 * @param [out] current_state is pointer to @ref s__kernel_state variable that
 *              will contain kernel current state. In case of an error the
 *              @a current_state referenced variable remains unchanged.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_NULL_ARGUMENT is returned when @a current_state is a
 *              NULL pointer.
 *
 * @note        This function may be called from Interrupt Service Routines,
 *              see @ref symphony_isr.
 *
 * @code
 * #include <assert>
 * #include "symphony/symphony.h"
 *
 * int main(void)
 * {
 *     s__error error;
 *     s__kernel_state current_state;
 *     // System initialization
 *     // ...
 *     error = s__kernel_get_state(&current_state);
 *     assert(error);
 *     if (current_state == S__KERNEL_STATE_INACTIVE) {
 *         error = s__kernel_initialize();
 *         assert(error);
 *     }
 *     return 0;
 * }
 * @endcode
 */
s__error
s__kernel_get_state(s__kernel_state * current_state);

/**
 * @brief       Start the RTOS kernel scheduler.
 *
 * The function __s__kernel_start__ starts the RTOS kernel and begins thread
 * switching. It will not return to its calling function in case of success.
 * Before it is successfully executed, only the functions
 * @ref s__kernel_get_info, @ref s__kernel_get_state, and object creation
 * functions `s__xxx_create` may be called.
 *
 * @return      Operation error. In case of success this function never
 *              returns. See @ref symphony_error for details.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR).
 * @retval      S__ERROR_NO_MEMORY is returned if no memory could be reserved
 *              for operation.
 * @retval      S__ERROR_WRONG_STATE is returned if the kernel was not
 *              previosly initialized.
 *
 * @pre         At least one initial thread should be created prior
 *              s__kernel_start call, see @ref s__thread_create.
 * @pre         The kernel must be initialized prior s__kernel_start call, see
 *              @ref s__kernel_initialize.
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 *
 * @code
 * #include <assert>
 * #include "symphony/symphony.h"
 *
 * int main(void)
 * {
 *     s__error error;
 *     s__kernel_state current_state;
 *     // System initialization
 *     // Kernel initialization
 *     // Thread creation
 *     // ...
 *     error = s__kernel_get_state(&current_state);
 *     if (current_state == S__KERNEL_STATE_READY) {
 *         error = s__kernel_start();
 *         // Only reached in case of error
 *         assert(error);
 *     }
 *     return 0;
 * }
 * @endcode
 */
s__error
s__kernel_start(void);

/**
 * @brief       Lock the RTOS kernel scheduler.
 *
 * The function __s__kernel_lock__ allows to lock all task switches. It returns
 * the previous value of the lock state in @a old_state. If the lock state is
 * @ref S__KERNEL_LOCK_ACTIVE then the kernel was already locked,
 * @ref S__KERNEL_LOCK_INACTIVE if it was not locked.
 *
 * @param [out] old_state is pointer to @ref s__kernel_lock_state variable that
 *              will hold previous state of scheduler lock. This pointer can be
 *              set to NULL. In case of an error the @a old_state referenced
 *              variable remains unchanged.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR).
 *
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 *
 * @code
 * s__error error;
 * s__kernel_lock_state old_state;
 * error = s__kernel_lock(&old_state);
 * assert(error);
 * // ... critical code
 * error = s__kernel_unlock(old_state);
 * assert(error);
 * @endcode
 *
 * @see         s__kernel_unlock
 */
s__error
s__kernel_lock(s__kernel_lock_state * old_state);

/**
 * @brief       Unlock the RTOS kernel scheduler.
 *
 * The function __s__kernel_unlock__ resumes from @ref s__kernel_lock.
 *
 * @param       old_state is previosly stored @ref s__kernel_lock_state
 *              variable that was obtained by @ref s__kernel_lock.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR).
 * @retval      S__ERROR_INVALID_ARGUMENT is returned when @a old_state is not
 *              @ref S__KERNEL_STATE_INACTIVE and not
 *              @ref S__KERNEL_LOCK_ACTIVE.
 *
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 * @see         s__kernel_lock
 */
s__error
s__kernel_unlock(s__kernel_lock_state old_state);

/**
 * @brief       Suspend the RTOS kernel scheduler.
 *
 * Symphony provides extension for tick-less operation which is useful for
 * applications that use extensively low-power modes where the System Tick
 * timer is also disabled. To provide a time-tick in such power-saving modes a
 * wake-up timer is used to derive timer intervals. The function
 * __s__kernel_suspend__ suspends the kernel scheduler and thus enables sleep
 * modes.
 *
 * The returned value can be used to determine the amount of system ticks until
 * the next tick-based kernel event will occur, i.e. a delayed thread becomes
 * ready again. It is recommended to set up the low power timer to generate a
 * wake-up interrupt based on this return value.
 *
 * @param [out] ticks_remaining is pointer to @ref s__kernel_ticks variable
 *              that will hold time in ticks, for how long the system can sleep
 *              or be in power-down mode. In case of an error the
 *              @a ticks_remaining referenced variable remains unchanged.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR).
 * @retval      S__ERROR_NULL_ARGUMENT is returned when @a ticks_remaining is a
 *              NULL pointer.
 *
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 *
 * @code
 * void idle_thread(void * argument)
 * {
 *     s__kernel_ticks remaining;
 *     // Mark `argument` as unused since it is not needed
 *     S__UNUSED(argument);
 *     for (;;) {
 *         // Disregard return value from s__kernel_suspend since we can prove
 *         // that no error can happen. Request suspend from kernel.
 *         (void)s__kernel_suspend(&remaining);
 *         // How long can we sleep?
 *         if (remaining != 0) {
 *             // Setup wakeup source, a timer or a watchdog.
 *             // Enter low power mode waiting for events.
 *             wait_for_event();
 *             // After wake-up adjust with cycles slept.
 *             remaining = tc;
 *         }
 *         // Resume kernel scheduler.
 *         (void)s__kernel_resume(remaining);
 *     }
 * }
 * @endcode
 * @see         s__kernel_resume
 */
s__error
s__kernel_suspend(s__kernel_ticks * ticks_remaining);

/**
 * @brief       Resume the RTOS kernel scheduler.
 *
 * Symphony provides extension for tick-less operation which is useful for
 * applications that use extensively low-power modes where the System Tick
 * timer is also disabled. To provide a time-tick in such power-saving modes a
 * wake-up timer is used to derive timer intervals. The function
 * __s__kernel_resume__ enables the kernel scheduler and thus wakes up the
 * system from sleep mode.
 *
 * @param       ticks_slept is time in ticks for how long the system was in
 *              sleep or power-down mode. This value may be zero, which means
 *              that the system slept for the whole requested time.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_ISR is returned if the function is called from an
 *              Interrupt Service Routine (ISR).
 * @retval      S__ERROR_INVALID_ARGUMENT is returned when @a ticks_slept is a
 *              bigger value than requested by @ref s__kernel_suspend.
 *
 * @pre         This function __cannot__ be called from Interrupt Service
 *              Routines, see @ref symphony_isr.
 * @see         s__kernel_suspend
 */
s__error
s__kernel_resume(s__kernel_ticks ticks_slept);

/**
 * @brief       Get the RTOS kernel tick frequency.
 *
 * The function __s__kernel_get_tick_freq__ returns the frequency of the
 * current RTOS kernel tick.
 *
 * @param [out] tick_freq_hz is a pointer to `uint32_t` variable that will hold
 *              frequency of the used System Tick timer in Hertz. In case of an
 *              error the @a tick_freq_hz referenced variable remains
 *              unchanged.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_NULL_ARGUMENT is returned when @a tick_freq_hz is a
 *              NULL pointer.
 *
 * @note        This function may be called from Interrupt Service Routines,
 *              see @ref symphony_isr.
 * @see         s__kernel_set_tick_freq
 */
s__error
s__kernel_get_tick_freq(uint32_t * tick_freq_hz);

/**
 * @brief       Set the RTOS kernel tick frequency.
 *
 * The function __s__kernel_set_tick_freq__ will set new frequency of the RTOS
 * System Tick timer. Before setting the frequency, the function will check the
 * given argument again @ref s__kernel_limits structure to see if the requested
 * frequency is valid.
 *
 * @param       tick_freq_hz is new frequency in Hertz that shall be configured
 *              by System Tick timer.
 * @return      Integer representing operation error, see @ref symphony_error
 * for details.
 * @retval      S__ERROR_NONE is returned in case of success.
 * @retval      S__ERROR_INVALID_ARGUMENT is returned when @a tick_freq_hz is
 *              outside specified limits in @ref s__kernel_limits.
 *
 * @note        This function may be called from Interrupt Service Routines,
 *              see @ref symphony_isr.
 * @see         s__kernel_set_tick_freq
 */
s__error
s__kernel_set_tick_freq(uint32_t tick_freq_hz);

/** @} */

/// Thread state.
typedef enum s__thread_state {
    S__THREAD_STATE_INACTIVE   = 0,   ///< Inactive.
    S__THREAD_STATE_READY      = 1,   ///< Ready.
    S__THREAD_STATE_RUNNING    = 2,   ///< Running.
    S__THREAD_STATE_BLOCKED    = 3,   ///< Blocked.
    S__THREAD_STATE_TERMINATED = 4,   ///< Terminated.
    S__THREAD_STATE_ERROR      = -1,  ///< Error.
    S__THREAD_STATE_RESERVED =
            0x7FFFFFFF  ///< Prevents enum down-size compiler optimization.
} s__thread_state;

/// Priority values.
typedef enum s__priority {
    S__PRIORITY_None         = 0,       ///< No priority (not initialized).
    S__PRIORITY_Idle         = 1,       ///< Reserved for Idle thread.
    S__PRIORITY_Low          = 8,       ///< Priority: low
    S__PRIORITY_Low1         = 8 + 1,   ///< Priority: low + 1
    S__PRIORITY_Low2         = 8 + 2,   ///< Priority: low + 2
    S__PRIORITY_Low3         = 8 + 3,   ///< Priority: low + 3
    S__PRIORITY_Low4         = 8 + 4,   ///< Priority: low + 4
    S__PRIORITY_Low5         = 8 + 5,   ///< Priority: low + 5
    S__PRIORITY_Low6         = 8 + 6,   ///< Priority: low + 6
    S__PRIORITY_Low7         = 8 + 7,   ///< Priority: low + 7
    S__PRIORITY_BelowNormal  = 16,      ///< Priority: below normal
    S__PRIORITY_BelowNormal1 = 16 + 1,  ///< Priority: below normal + 1
    S__PRIORITY_BelowNormal2 = 16 + 2,  ///< Priority: below normal + 2
    S__PRIORITY_BelowNormal3 = 16 + 3,  ///< Priority: below normal + 3
    S__PRIORITY_BelowNormal4 = 16 + 4,  ///< Priority: below normal + 4
    S__PRIORITY_BelowNormal5 = 16 + 5,  ///< Priority: below normal + 5
    S__PRIORITY_BelowNormal6 = 16 + 6,  ///< Priority: below normal + 6
    S__PRIORITY_BelowNormal7 = 16 + 7,  ///< Priority: below normal + 7
    S__PRIORITY_Normal       = 24,      ///< Priority: normal
    S__PRIORITY_Normal1      = 24 + 1,  ///< Priority: normal + 1
    S__PRIORITY_Normal2      = 24 + 2,  ///< Priority: normal + 2
    S__PRIORITY_Normal3      = 24 + 3,  ///< Priority: normal + 3
    S__PRIORITY_Normal4      = 24 + 4,  ///< Priority: normal + 4
    S__PRIORITY_Normal5      = 24 + 5,  ///< Priority: normal + 5
    S__PRIORITY_Normal6      = 24 + 6,  ///< Priority: normal + 6
    S__PRIORITY_Normal7      = 24 + 7,  ///< Priority: normal + 7
    S__PRIORITY_AboveNormal  = 32,      ///< Priority: above normal
    S__PRIORITY_AboveNormal1 = 32 + 1,  ///< Priority: above normal + 1
    S__PRIORITY_AboveNormal2 = 32 + 2,  ///< Priority: above normal + 2
    S__PRIORITY_AboveNormal3 = 32 + 3,  ///< Priority: above normal + 3
    S__PRIORITY_AboveNormal4 = 32 + 4,  ///< Priority: above normal + 4
    S__PRIORITY_AboveNormal5 = 32 + 5,  ///< Priority: above normal + 5
    S__PRIORITY_AboveNormal6 = 32 + 6,  ///< Priority: above normal + 6
    S__PRIORITY_AboveNormal7 = 32 + 7,  ///< Priority: above normal + 7
    S__PRIORITY_High         = 40,      ///< Priority: high
    S__PRIORITY_High1        = 40 + 1,  ///< Priority: high + 1
    S__PRIORITY_High2        = 40 + 2,  ///< Priority: high + 2
    S__PRIORITY_High3        = 40 + 3,  ///< Priority: high + 3
    S__PRIORITY_High4        = 40 + 4,  ///< Priority: high + 4
    S__PRIORITY_High5        = 40 + 5,  ///< Priority: high + 5
    S__PRIORITY_High6        = 40 + 6,  ///< Priority: high + 6
    S__PRIORITY_High7        = 40 + 7,  ///< Priority: high + 7
    S__PRIORITY_Realtime     = 48,      ///< Priority: realtime
    S__PRIORITY_Realtime1    = 48 + 1,  ///< Priority: realtime + 1
    S__PRIORITY_Realtime2    = 48 + 2,  ///< Priority: realtime + 2
    S__PRIORITY_Realtime3    = 48 + 3,  ///< Priority: realtime + 3
    S__PRIORITY_Realtime4    = 48 + 4,  ///< Priority: realtime + 4
    S__PRIORITY_Realtime5    = 48 + 5,  ///< Priority: realtime + 5
    S__PRIORITY_Realtime6    = 48 + 6,  ///< Priority: realtime + 6
    S__PRIORITY_Realtime7    = 48 + 7,  ///< Priority: realtime + 7
    S__PRIORITY_ISR          = 56,      ///< Reserved for ISR deferred thread.
    S__PRIORITY_Error =
            -1,  ///< System cannot determine priority or illegal priority.
    S__PRIORITY_Reserved =
            0x7FFFFFFF  ///< Prevents enum down-size compiler optimization.
} s__priority;

/// Entry point of a thread.
typedef void (*s__thread_fn)(void * argument);

/// Timer callback function.
typedef void (*s__timer_fn)(void * argument);

/// Timer type.
typedef enum s__timer_type {
    S__TIMER_TYPE_ONCE     = 0,  ///< One-shot timer.
    S__TIMER_TYPE_PERIODIC = 1   ///< Repeating timer.
} s__timer_type;

// Timeout value.
#define S__WAIT_FOREVER 0xFFFFFFFFU  ///< Wait forever timeout value.

// Flags options (\ref osEventFlagsWait).
#define S__FLAGS_WAIT_ANY 0x00000000U  ///< Wait for any flag (default).
#define S__FLAGS_WAIT_ALL 0x00000001U  ///< Wait for all flags.
#define S__FLAGS_NO_CLEAR                                                      \
    0x00000002U  ///< Do not clear flags which have been specified to wait for.

// Flags errors (returned by osEventFlagsXxxx).
#define S__FLAGS_ERROR          0x80000000U  ///< Error indicator.
#define S__FLAGS_ERROR_UNKNOWN  0xFFFFFFFFU  ///< S__STATUS_ERROR (-1).
#define S__FLAGS_ERROR_TIMEOUT  0xFFFFFFFEU  ///< S__STATUS_TIMEOUT (-2).
#define S__FLAGS_ERROR_RESOURCE 0xFFFFFFFDU  ///< S__STATUS_ERROR_RESOURCE (-3).
#define S__FLAGS_ERROR_PARAMETER                                               \
    0xFFFFFFFCU  ///< S__STATUS_ERROR_PARAMETER (-4).
#define S__FLAGS_ERROR_ISR          0xFFFFFFFAU  ///< S__ERROR_ISR (-6).
#define S__FLAGS_ERROR_SAFETY_CLASS 0xFFFFFFF9U  ///< osErrorSafetyClass (-7).

// Thread attributes (attr_bits in \ref s__thread_attr).
#define S__THREAD_DETACHED                                                     \
    0x00000000U  ///< Thread created in detached mode (default)
#define S__THREAD_JOINABLE 0x00000001U  ///< Thread created in joinable mode
#define S__THREAD_UNPRIVILEGED                                                 \
    0x00000002U                           ///< Thread runs in unprivileged mode
#define S__THREAD_PRIVILEGED 0x00000004U  ///< Thread runs in privileged mode

#define S__THREAD_ZONE_POS 8U  ///< MPU protected zone position
#define S__THREAD_ZONE_MSK                                                     \
    (0x3FUL << S__THREAD_ZONE_POS)  ///< MPU protected zone mask
#define S__THREAD_ZONE_VALID                                                   \
    (0x80UL << S__THREAD_ZONE_POS)  ///< MPU protected zone valid flag
#define S__THREAD_ZONE(n)                                                      \
    ((((n) << S__THREAD_ZONE_POS) & S__THREAD_ZONE_MSK)                        \
     | S__THREAD_ZONE_VALID)  ///< MPU protected zone

// Mutex attributes (attr_bits in \ref s__mutex_attr).
#define S__MUTEX_RECURSIVE        0x00000001U  ///< Recursive mutex.
#define S__MUTEX_PRIORITY_INHERIT 0x00000002U  ///< Priority inherit protocol.
#define S__MUTEX_ROBUST           0x00000008U  ///< Robust mutex.

// Object attributes (attr_bits in all objects)
#define S__SAFETY_CLASS_POS 16U  ///< Safety class position
#define S__SAFETY_CLASS_MSK                                                    \
    (0x0FUL << S__SAFETY_CLASS_POS)  ///< Safety class mask
#define S__SAFETY_CLASS_VALID                                                  \
    (0x10UL << S__SAFETY_CLASS_POS)  ///< Safety class valid flag
#define S__SAFETY_CLASS(n)                                                     \
    ((((n) << S__SAFETY_CLASS_POS) & S__SAFETY_CLASS_MSK)                      \
     | S__SAFETY_CLASS_VALID)  ///< Safety class

// Safety mode (\ref osThreadSuspendClass, \ref osThreadResumeClass and \ref
// osKernelDestroyClass).
#define S__SAFETY_WITH_SAME_CLASS                                              \
    0x00000001U  ///< Objects with same safety class.
#define S__SAFETY_WITH_LOWER_CLASS                                             \
    0x00000002U  ///< Objects with lower safety class.

// Error indication (returned by \ref s__thread_get_class and \ref
// s__thread_get_zone).
#define S__ERROR_ID 0xFFFFFFFFU  ///< S__STATUS_ERROR (-1).

/// \details Thread ID identifies the thread.
typedef struct s__thread s__thread;

/// \details Timer ID identifies the timer.
typedef struct s__timer s__timer;

/// \details Event Flags ID identifies the event flags.
typedef struct s__event_flags s__event_flags;

/// \details Mutex ID identifies the mutex.
typedef struct s__mutex s__mutex;

/// \details Semaphore ID identifies the semaphore.
typedef struct s__semaphore s__semaphore;

/// \details Memory Pool ID identifies the memory pool.
typedef struct s__memory_pool s__memory_pool;

/// \details Message Queue ID identifies the message queue.
typedef struct s__queue s__queue;

/// \details Data type that identifies secure software modules called by a
/// process.
typedef uint32_t tz_module_id;

/**
 * @brief       Attributes structure for thread.
 */
typedef struct s__thread_attr {
    const char * name;          ///< name of the thread
    uint32_t     attr_bits;     ///< attribute bits
    void *       cb_mem;        ///< memory for control block
    size_t       cb_size;       ///< size of provided memory for control block
    void *       stack_mem;     ///< memory for stack
    size_t       stack_size;    ///< size of stack
    s__priority  priority;      ///< initial thread priority (default:
                                ///< S__PRIORITY_Normal)
    tz_module_id tz_module_id;  ///< TrustZone module identifier
} s__thread_attr;

/// Attributes structure for timer.
typedef struct s__timer_attr {
    const char * name;       ///< name of the timer
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
} s__timer_attr;

/// Attributes structure for event flags.
typedef struct {
    const char * name;       ///< name of the event flags
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
} osEventFlagsAttr_t;

/// Attributes structure for mutex.
typedef struct {
    const char * name;       ///< name of the mutex
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
} s__mutex_attr;

/// Attributes structure for semaphore.
typedef struct {
    const char * name;       ///< name of the semaphore
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
} osSemaphoreAttr_t;

/// Attributes structure for memory pool.
typedef struct {
    const char * name;       ///< name of the memory pool
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
    void *       mp_mem;     ///< memory for data storage
    uint32_t     mp_size;    ///< size of provided memory for data storage
} osMemoryPoolAttr_t;

/// Attributes structure for message queue.
typedef struct {
    const char * name;       ///< name of the message queue
    uint32_t     attr_bits;  ///< attribute bits
    void *       cb_mem;     ///< memory for control block
    uint32_t     cb_size;    ///< size of provided memory for control block
    void *       mq_mem;     ///< memory for data storage
    uint32_t     mq_size;    ///< size of provided memory for data storage
} osMessageQueueAttr_t;

//  ==== Kernel Management Functions ====

/// Protect the RTOS Kernel scheduler access.
/// \param[in]     safety_class  safety class.
/// \return status code that indicates the execution status of the function.
s__error
osKernelProtect(uint32_t safety_class);

/// Destroy objects for specified safety classes.
/// \param[in]     safety_class  safety class.
/// \param[in]     mode          safety mode.
/// \return status code that indicates the execution status of the function.
s__error
osKernelDestroyClass(uint32_t safety_class, uint32_t mode);

/// Get the RTOS kernel tick count.
/// \return RTOS kernel current tick count.
uint32_t
osKernelGetTickCount(void);

/// Get the RTOS kernel tick frequency.
/// \return frequency of the kernel tick in hertz, i.e. kernel ticks per second.
uint32_t
osKernelGetTickFreq(void);

/// Get the RTOS kernel system timer count.
/// \return RTOS kernel current system timer count as 32-bit value.
uint32_t
osKernelGetSysTimerCount(void);

/// Get the RTOS kernel system timer frequency.
/// \return frequency of the system timer in hertz, i.e. timer ticks per second.
uint32_t
osKernelGetSysTimerFreq(void);

//  ==== Thread Management Functions ====

/// Create a thread and add it to Active Threads.
/// \param[in]     thread_fn          thread function.
/// \param[in]     thread_argument      pointer that is passed to the thread
/// function as start argument. \param[in]     attr          thread attributes;
/// NULL: default values. \return thread ID for reference by other functions or
/// NULL in case of error. \see symphony_concepts
int
s__thread_create(
        s__thread_fn           thread_fn,
        void *                 thread_argument,
        const s__thread_attr * attr,
        s__thread **           thread);
s__thread *
os__thread_create(
        s__thread_fn           thread_fn,
        void *                 thread_argument,
        const s__thread_attr * attr);

/// Get name of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return name as null-terminated string.
const char *
s__thread_get_name(const s__thread * thread);

/// Get safety class of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return safety class of the specified thread.
uint32_t
s__thread_get_class(const s__thread * thread);

/// Get MPU protected zone of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return MPU protected zone of the specified
/// thread.
uint32_t
s__thread_get_zone(const s__thread * thread);

/// Return the thread ID of the current running thread.
/// \return thread ID for reference by other functions or NULL in case of error.
s__thread *
s__thread_get_current(void);

/// Get current thread state of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return current thread state of the specified
/// thread.
s__thread_state
s__thread_get_state(const s__thread * thread);

/// Get stack size of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return stack size in bytes.
s__error
s__thread_get_stack_size(const s__thread * thread, size_t * size);

/// Get available stack space of a thread based on stack watermark recording
/// during execution. \param[in]     thread     thread ID obtained by \ref
/// s__thread_create or \ref s__thread_get_current. \return remaining stack
/// space in bytes.
uint32_t
osThreadGetStackSpace(s__thread thread);

/// Change priority of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \param[in]     priority      new priority value
/// for the thread function. \return status code that indicates the execution
/// status of the function.
s__error
osThreadSetPriority(s__thread thread, s__priority priority);

/// Get current priority of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return current priority value of the specified
/// thread.
s__priority
osThreadGetPriority(s__thread thread);

/// Pass control to next thread that is in state \b READY.
/// \return status code that indicates the execution status of the function.
s__error
osThreadYield(void);

/// Suspend execution of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return status code that indicates the execution
/// status of the function.
s__error
osThreadSuspend(s__thread thread);

/// Resume execution of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return status code that indicates the execution
/// status of the function.
s__error
osThreadResume(s__thread thread);

/// Detach a thread (thread storage can be reclaimed when thread terminates).
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return status code that indicates the execution
/// status of the function.
s__error
osThreadDetach(s__thread thread);

/// Wait for specified thread to terminate.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return status code that indicates the execution
/// status of the function.
s__error
osThreadJoin(s__thread thread);

/// Terminate execution of current running thread.
S__NO_RETURN void
osThreadExit(void);

/// Terminate execution of a thread.
/// \param[in]     thread     thread ID obtained by \ref s__thread_create or
/// \ref s__thread_get_current. \return status code that indicates the execution
/// status of the function.
s__error
osThreadTerminate(s__thread thread);

/// Feed watchdog of the current running thread.
/// \param[in]     ticks         \ref kernelTimer "time ticks" value until the
/// thread watchdog expires, or 0 to stop the watchdog \return status code that
/// indicates the execution status of the function.
s__error
osThreadFeedWatchdog(uint32_t ticks);

/// Protect creation of privileged threads.
/// \return status code that indicates the execution status of the function.
s__error
osThreadProtectPrivileged(void);

/// Suspend execution of threads for specified safety classes.
/// \param[in]     safety_class  safety class.
/// \param[in]     mode          safety mode.
/// \return status code that indicates the execution status of the function.
s__error
osThreadSuspendClass(uint32_t safety_class, uint32_t mode);

/// Resume execution of threads for specified safety classes.
/// \param[in]     safety_class  safety class.
/// \param[in]     mode          safety mode.
/// \return status code that indicates the execution status of the function.
s__error
osThreadResumeClass(uint32_t safety_class, uint32_t mode);

/// Terminate execution of threads assigned to a specified MPU protected zone.
/// \param[in]     zone          MPU protected zone.
/// \return status code that indicates the execution status of the function.
s__error
osThreadTerminateZone(uint32_t zone);

/// Get number of active threads.
/// \return number of active threads.
uint32_t
osThreadGetCount(void);

/// Enumerate active threads.
/// \param[out]    thread_array  pointer to array for retrieving thread IDs.
/// \param[in]     array_items   maximum number of items in array for retrieving
/// thread IDs. \return number of enumerated threads.
uint32_t
osThreadEnumerate(s__thread * thread_array, uint32_t array_items);

//  ==== Generic Wait Functions ====

/// Wait for Timeout (Time Delay).
/// \param[in]     ticks         \ref CMSIS_RTOS_TimeOutValue "time ticks" value
/// \return status code that indicates the execution status of the function.
s__error
osDelay(uint32_t ticks);

/// Wait until specified time.
/// \param[in]     ticks         absolute time in ticks
/// \return status code that indicates the execution status of the function.
s__error
osDelayUntil(uint32_t ticks);

//  ==== Timer Management Functions ====

/// Create and Initialize a timer.
/// \param[in]     func          function pointer to callback function.
/// \param[in]     type          \ref S__TIMER_TYPE_ONCE for one-shot or \ref
/// S__TIMER_TYPE_PERIODIC for periodic behavior. \param[in]     argument
/// argument to the timer callback function. \param[in]     attr          timer
/// attributes; NULL: default values. \return timer ID for reference by other
/// functions or NULL in case of error.
s__timer *
osTimerNew(
        s__timer_fn           func,
        s__timer_type         type,
        void *                argument,
        const s__timer_attr * attr);

/// Get name of a timer.
/// \param[in]     timer_id      timer ID obtained by \ref osTimerNew.
/// \return name as null-terminated string.
const char *
osTimerGetName(s__timer * timer_id);

/// Start or restart a timer.
/// \param[in]     timer_id      timer ID obtained by \ref osTimerNew.
/// \param[in]     ticks         \ref CMSIS_RTOS_TimeOutValue "time ticks" value
/// of the timer. \return status code that indicates the execution status of the
/// function.
s__error
osTimerStart(s__timer * timer_id, uint32_t ticks);

/// Stop a timer.
/// \param[in]     timer_id      timer ID obtained by \ref osTimerNew.
/// \return status code that indicates the execution status of the function.
s__error
osTimerStop(s__timer * timer_id);

/// Check if a timer is running.
/// \param[in]     timer_id      timer ID obtained by \ref osTimerNew.
/// \return 0 not running, 1 running.
uint32_t
osTimerIsRunning(s__timer * timer_id);

/// Delete a timer.
/// \param[in]     timer_id      timer ID obtained by \ref osTimerNew.
/// \return status code that indicates the execution status of the function.
s__error
osTimerDelete(s__timer * timer_id);

//  ==== Event Flags Management Functions ====

/// Create and Initialize an Event Flags object.
/// \param[in]     attr          event flags attributes; NULL: default values.
/// \return event flags ID for reference by other functions or NULL in case of
/// error.
s__event_flags *
osEventFlagsNew(const osEventFlagsAttr_t * attr);

/// Get name of an Event Flags object.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \return name as null-terminated string.
const char *
osEventFlagsGetName(s__event_flags * ef_id);

/// Set the specified Event Flags.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \param[in]     flags         specifies the flags that shall
/// be set. \return event flags after setting or error code if highest bit set.
uint32_t
osEventFlagsSet(s__event_flags * ef_id, uint32_t flags);

/// Clear the specified Event Flags.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \param[in]     flags         specifies the flags that shall
/// be cleared. \return event flags before clearing or error code if highest bit
/// set.
uint32_t
osEventFlagsClear(s__event_flags * ef_id, uint32_t flags);

/// Get the current Event Flags.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \return current event flags.
uint32_t
osEventFlagsGet(s__event_flags * ef_id);

/// Wait for one or more Event Flags to become signaled.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \param[in]     flags         specifies the flags to wait
/// for. \param[in]     options       specifies flags options (osFlagsXxxx).
/// \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue or 0 in case of no
/// time-out. \return event flags before clearing or error code if highest bit
/// set.
uint32_t
osEventFlagsWait(
        s__event_flags * ef_id,
        uint32_t         flags,
        uint32_t         options,
        uint32_t         timeout);

/// Delete an Event Flags object.
/// \param[in]     ef_id         event flags ID obtained by \ref
/// osEventFlagsNew. \return status code that indicates the execution status of
/// the function.
s__error
osEventFlagsDelete(s__event_flags * ef_id);

//  ==== Mutex Management Functions ====

/// Create and Initialize a Mutex object.
/// \param[in]     attr          mutex attributes; NULL: default values.
/// \return mutex ID for reference by other functions or NULL in case of error.
s__mutex *
s__mutex_create(const s__mutex_attr * attr);

/// Get name of a Mutex object.
/// \param[in]     mutex_id      mutex ID obtained by \ref s__mutex_create.
/// \return name as null-terminated string.
const char *
osMutexGetName(s__mutex * mutex_id);

/// Acquire a Mutex or timeout if it is locked.
/// \param[in]     mutex_id      mutex ID obtained by \ref s__mutex_create.
/// \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue or 0 in case of no
/// time-out. \return status code that indicates the execution status of the
/// function.
s__error
osMutexAcquire(s__mutex * mutex_id, uint32_t timeout);

/// Release a Mutex that was acquired by \ref osMutexAcquire.
/// \param[in]     mutex_id      mutex ID obtained by \ref s__mutex_create.
/// \return status code that indicates the execution status of the function.
s__error
osMutexRelease(s__mutex * mutex_id);

/// Get Thread which owns a Mutex object.
/// \param[in]     mutex_id      mutex ID obtained by \ref s__mutex_create.
/// \return thread ID of owner thread or NULL when mutex was not acquired.
s__thread
osMutexGetOwner(s__mutex * mutex_id);

/// Delete a Mutex object.
/// \param[in]     mutex_id      mutex ID obtained by \ref s__mutex_create.
/// \return status code that indicates the execution status of the function.
s__error
osMutexDelete(s__mutex * mutex_id);

//  ==== Semaphore Management Functions ====

/// Create and Initialize a Semaphore object.
/// \param[in]     max_count     maximum number of available tokens.
/// \param[in]     initial_count initial number of available tokens.
/// \param[in]     attr          semaphore attributes; NULL: default values.
/// \return semaphore ID for reference by other functions or NULL in case of
/// error.
s__semaphore *
osSemaphoreNew(
        uint32_t                  max_count,
        uint32_t                  initial_count,
        const osSemaphoreAttr_t * attr);

/// Get name of a Semaphore object.
/// \param[in]     semaphore_id  semaphore ID obtained by \ref osSemaphoreNew.
/// \return name as null-terminated string.
const char *
osSemaphoreGetName(s__semaphore * semaphore_id);

/// Acquire a Semaphore token or timeout if no tokens are available.
/// \param[in]     semaphore_id  semaphore ID obtained by \ref osSemaphoreNew.
/// \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue or 0 in case of no
/// time-out. \return status code that indicates the execution status of the
/// function.
s__error
osSemaphoreAcquire(s__semaphore * semaphore_id, uint32_t timeout);

/// Release a Semaphore token up to the initial maximum count.
/// \param[in]     semaphore_id  semaphore ID obtained by \ref osSemaphoreNew.
/// \return status code that indicates the execution status of the function.
s__error
osSemaphoreRelease(s__semaphore * semaphore_id);

/// Get current Semaphore token count.
/// \param[in]     semaphore_id  semaphore ID obtained by \ref osSemaphoreNew.
/// \return number of tokens available.
uint32_t
osSemaphoreGetCount(s__semaphore * semaphore_id);

/// Delete a Semaphore object.
/// \param[in]     semaphore_id  semaphore ID obtained by \ref osSemaphoreNew.
/// \return status code that indicates the execution status of the function.
s__error
osSemaphoreDelete(s__semaphore * semaphore_id);

//  ==== Memory Pool Management Functions ====

/// Create and Initialize a Memory Pool object.
/// \param[in]     block_count   maximum number of memory blocks in memory pool.
/// \param[in]     block_size    memory block size in bytes.
/// \param[in]     attr          memory pool attributes; NULL: default values.
/// \return memory pool ID for reference by other functions or NULL in case of
/// error.
s__memory_pool *
osMemoryPoolNew(
        uint32_t                   block_count,
        uint32_t                   block_size,
        const osMemoryPoolAttr_t * attr);

/// Get name of a Memory Pool object.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return name as null-terminated string.
const char *
osMemoryPoolGetName(s__memory_pool * mp_id);

/// Allocate a memory block from a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue
/// or 0 in case of no time-out. \return address of the allocated memory block
/// or NULL in case of no memory is available.
void *
osMemoryPoolAlloc(s__memory_pool * mp_id, uint32_t timeout);

/// Return an allocated memory block back to a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \param[in]     block         address of the allocated
/// memory block to be returned to the memory pool. \return status code that
/// indicates the execution status of the function.
s__error
osMemoryPoolFree(s__memory_pool * mp_id, void * block);

/// Get maximum number of memory blocks in a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return maximum number of memory blocks.
uint32_t
osMemoryPoolGetCapacity(s__memory_pool * mp_id);

/// Get memory block size in a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return memory block size in bytes.
uint32_t
osMemoryPoolGetBlockSize(s__memory_pool * mp_id);

/// Get number of memory blocks used in a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return number of memory blocks used.
uint32_t
osMemoryPoolGetCount(s__memory_pool * mp_id);

/// Get number of memory blocks available in a Memory Pool.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return number of memory blocks available.
uint32_t
osMemoryPoolGetSpace(s__memory_pool * mp_id);

/// Delete a Memory Pool object.
/// \param[in]     mp_id         memory pool ID obtained by \ref
/// osMemoryPoolNew. \return status code that indicates the execution status of
/// the function.
s__error
osMemoryPoolDelete(s__memory_pool * mp_id);

//  ==== Message Queue Management Functions ====

/// Create and Initialize a Message Queue object.
/// \param[in]     msg_count     maximum number of messages in queue.
/// \param[in]     msg_size      maximum message size in bytes.
/// \param[in]     attr          message queue attributes; NULL: default values.
/// \return message queue ID for reference by other functions or NULL in case of
/// error.
s__queue *
osMessageQueueNew(
        uint32_t                     msg_count,
        uint32_t                     msg_size,
        const osMessageQueueAttr_t * attr);

/// Get name of a Message Queue object.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return name as null-terminated string.
const char *
osMessageQueueGetName(s__queue * mq_id);

/// Put a Message into a Queue or timeout if Queue is full.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \param[in]     msg_ptr       pointer to buffer with
/// message to put into a queue. \param[in]     msg_prio      message priority.
/// \param[in]     timeout       \ref CMSIS_RTOS_TimeOutValue or 0 in case of no
/// time-out. \return status code that indicates the execution status of the
/// function.
s__error
osMessageQueuePut(
        s__queue *   mq_id,
        const void * msg_ptr,
        uint8_t      msg_prio,
        uint32_t     timeout);

/// Get a Message from a Queue or timeout if Queue is empty.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \param[out]    msg_ptr       pointer to buffer for
/// message to get from a queue. \param[out]    msg_prio      pointer to buffer
/// for message priority or NULL. \param[in]     timeout       \ref
/// CMSIS_RTOS_TimeOutValue or 0 in case of no time-out. \return status code
/// that indicates the execution status of the function.
s__error
osMessageQueueGet(
        s__queue * mq_id,
        void *     msg_ptr,
        uint8_t *  msg_prio,
        uint32_t   timeout);

/// Get maximum number of messages in a Message Queue.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return maximum number of messages.
uint32_t
osMessageQueueGetCapacity(s__queue * mq_id);

/// Get maximum message size in a Message Queue.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return maximum message size in bytes.
uint32_t
osMessageQueueGetMsgSize(s__queue * mq_id);

/// Get number of queued messages in a Message Queue.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return number of queued messages.
uint32_t
osMessageQueueGetCount(s__queue * mq_id);

/// Get number of available slots for messages in a Message Queue.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return number of available slots for messages.
uint32_t
osMessageQueueGetSpace(s__queue * mq_id);

/// Reset a Message Queue to initial empty state.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return status code that indicates the execution status
/// of the function.
s__error
osMessageQueueReset(s__queue * mq_id);

/// Delete a Message Queue object.
/// \param[in]     mq_id         message queue ID obtained by \ref
/// osMessageQueueNew. \return status code that indicates the execution status
/// of the function.
s__error
osMessageQueueDelete(s__queue * mq_id);

//  ==== Handler Functions ====

/// Handler for expired thread watchdogs.
/// \param[in]     thread thread ID obtained by \ref s__thread_create or \ref
/// s__thread_get_current. \return new watchdog reload value or 0 to stop the
/// watchdog.
uint32_t
osWatchdogAlarm_Handler(s__thread thread);

// ==== Zone Management Function ====

/// Setup MPU protected zone (called when zone changes).
/// \param[in]     zone          zone number.
void
osZoneSetup_Callback(uint32_t zone);

//  ==== Exception Faults ====

/// Resume normal operation when exiting exception faults
void
osFaultResume(void);

#ifdef __cplusplus
}
#endif

#endif /* SYMPHONY_H_ */
/** @} */
