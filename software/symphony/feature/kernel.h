/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Brief description of file.
 *
 * Detailed description of file.
 */

#ifndef SYMPHONY_FEATURE_KERNEL_H_
#define SYMPHONY_FEATURE_KERNEL_H_

/* Depends */
#include "symphony/domain/kernel.h"

extern s__kernel s__kernel_instance;

#define s__kernel_init()   s__kernel_init_instance(&s__kernel_instance)
#define s__kernel_deinit() s__kernel_deinit_instance(&s__kernel_instance)

void
s__kernel_init_instance(s__kernel * kernel);

void
s__kernel_deinit_instance(s__kernel * kernel);

#endif /* SYMPHONY_FEATURE_KERNEL_H_ */
