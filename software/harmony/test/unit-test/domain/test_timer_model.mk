F_PROJECT_ROOT = ../../../../..

F_PROJECT_NAME = test_domain_timer_model

F_SRC_FILES += software/harmony/domain/timer_model.c
F_SRC_FILES += software/harmony/domain/common/list_model.c
F_SRC_FILES += software/harmony/test/unit-test/domain/test_timer_model.c
F_INC_DIRS += software
F_FEATURES += debug warnings

include $(F_PROJECT_ROOT)/build/make/test.mk

