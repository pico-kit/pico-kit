F_PROJECT_ROOT = ../../../../..

F_PROJECT_NAME = test_domain_task_model

F_SRC_FILES += software/harmony/domain/task_model.c
F_SRC_FILES += software/harmony/test/unit-test/domain/test_task_model.c
F_INC_DIRS += software

include $(F_PROJECT_ROOT)/build/make/test.mk
