F_PROJECT_ROOT = ../../../../..

F_PROJECT_NAME = test_domain_list_model

F_SRC_FILES += software/harmony/domain/common/list_model.c
F_SRC_FILES += software/harmony/test/unit-test/domain/test_list_model.c
F_INC_DIRS += software

include $(F_PROJECT_ROOT)/build/make/test.mk

