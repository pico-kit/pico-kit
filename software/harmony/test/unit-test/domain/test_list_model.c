/*
 * test_list_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "harmony/domain/common/list_model.h"

/* Depends */
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

static h__list empty_list;
static h__list single_entry_list;
static h__list single_entry_list_item;
static h__list double_entry_list;
static h__list double_entry_list_item_1;
static h__list double_entry_list_item_2;

static void
fff_setup(void)
{
    /* Register resets */
    // FFF_FAKES_LIST(RESET_FAKE);

    /* Reset common FFF internal structures */
    FFF_RESET_HISTORY();
}

static void
empty_list_setup(void)
{
    fff_setup();
    h__list_model_init(&empty_list);
}

static void
empty_list_teardown(void)
{
    h__list_model_deinit(&empty_list);
}

static void
single_entry_list_setup(void)
{
    fff_setup();
    h__list_model_init(&single_entry_list);
    h__list_model_init(&single_entry_list_item);
    h__list_model_add_after(&single_entry_list_item, &single_entry_list);
}

static void
single_entry_list_teardown(void)
{
    h__list_model_remove(&single_entry_list_item);
    h__list_model_deinit(&single_entry_list_item);
    h__list_model_deinit(&single_entry_list);
}

static void
double_entry_list_setup(void)
{
    fff_setup();
    h__list_model_init(&double_entry_list);
    h__list_model_init(&double_entry_list_item_1);
    h__list_model_init(&double_entry_list_item_2);
    h__list_model_add_after(&double_entry_list_item_2, &double_entry_list);
    h__list_model_add_after(&double_entry_list_item_1, &double_entry_list);
}

static void
double_entry_list_teardown(void)
{
    h__list_model_remove(&double_entry_list_item_1);
    h__list_model_remove(&double_entry_list_item_2);
    h__list_model_deinit(&double_entry_list_item_2);
    h__list_model_deinit(&double_entry_list_item_1);
    h__list_model_deinit(&double_entry_list);
}

V__TEST(test_init, fff_setup, NULL)
{
    /* Preconditions */
    h__list a_list;

    /* UUT */
    h__list_model_init(&a_list);

    /* Validation */
    V__ASSERT_EQ_PTR(a_list.next, &a_list);
    V__ASSERT_EQ_PTR(a_list.prev, &a_list);
}

V__TEST(test_deinit, fff_setup, NULL)
{
    /* Preconditions */
    h__list a_list;
    h__list_model_init(&a_list);

    /* UUT */
    h__list_model_deinit(&a_list);

    /* Validation */
    V__ASSERT_EQ_PTR(a_list.next, a_list.prev);
}

V__TEST(test_next, empty_list_setup, empty_list_teardown)
{
    /* UUT */
    h__list * next = h__list_model_next(&empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(next, &empty_list);
}

V__TEST(test_prev, empty_list_setup, empty_list_teardown)
{
    /* UUT */
    h__list * prev = h__list_model_prev(&empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(prev, &empty_list);
}

V__TEST(test_add_after, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item;

    h__list_model_init(&list_item);

    /* UUT */
    h__list_model_add_after(&list_item, &empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(h__list_model_next(&empty_list), &list_item);
    V__ASSERT_EQ_PTR(h__list_model_prev(&empty_list), &list_item);
}

V__TEST(test_add_after_2, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item_1;
    h__list list_item_2;

    h__list_model_init(&list_item_1);
    h__list_model_init(&list_item_2);

    /* UUT */
    h__list_model_add_after(&list_item_2, &empty_list);
    h__list_model_add_after(&list_item_1, &empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(h__list_model_next(&empty_list), &list_item_1);
    V__ASSERT_EQ_PTR(h__list_model_next(&list_item_1), &list_item_2);
    V__ASSERT_EQ_PTR(h__list_model_next(&list_item_2), &empty_list);
    V__ASSERT_EQ_PTR(h__list_model_prev(&empty_list), &list_item_2);
    V__ASSERT_EQ_PTR(h__list_model_prev(&list_item_2), &list_item_1);
    V__ASSERT_EQ_PTR(h__list_model_prev(&list_item_1), &empty_list);
}

V__TEST(test_add_before, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item;

    h__list_model_init(&list_item);

    /* UUT */
    h__list_model_add_before(&list_item, &empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(h__list_model_prev(&empty_list), &list_item);
    V__ASSERT_EQ_PTR(h__list_model_next(&empty_list), &list_item);
}

V__TEST(test_add_before_2, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item_1;
    h__list list_item_2;

    h__list_model_init(&list_item_1);
    h__list_model_init(&list_item_2);

    /* UUT */
    h__list_model_add_before(&list_item_1, &empty_list);
    h__list_model_add_before(&list_item_2, &empty_list);

    /* Validation */
    V__ASSERT_EQ_PTR(h__list_model_next(&empty_list), &list_item_1);
    V__ASSERT_EQ_PTR(h__list_model_next(&list_item_1), &list_item_2);
    V__ASSERT_EQ_PTR(h__list_model_next(&list_item_2), &empty_list);
    V__ASSERT_EQ_PTR(h__list_model_prev(&empty_list), &list_item_2);
    V__ASSERT_EQ_PTR(h__list_model_prev(&list_item_2), &list_item_1);
    V__ASSERT_EQ_PTR(h__list_model_prev(&list_item_1), &empty_list);
}

V__TEST(test_is_empty_true, empty_list_setup, empty_list_teardown)
{
    /* UUT */
    bool is_empty = h__list_model_is_empty(&empty_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_empty, true);
}

V__TEST(test_is_empty_false,
        single_entry_list_setup,
        single_entry_list_teardown)
{
    /* UUT */
    bool is_empty = h__list_model_is_empty(&single_entry_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_empty, false);
}

V__TEST(test_is_empty_after_false, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item;
    h__list_model_init(&list_item);
    h__list_model_add_after(&list_item, &empty_list);

    /* UUT */
    bool is_empty = h__list_model_is_empty(&empty_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_empty, false);
}

V__TEST(test_is_empty_before_false, empty_list_setup, empty_list_teardown)
{
    /* Preconditions */
    h__list list_item;
    h__list_model_init(&list_item);
    h__list_model_add_before(&list_item, &empty_list);

    /* UUT */
    bool is_empty = h__list_model_is_empty(&empty_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_empty, false);
}

V__TEST(test_is_singular_true,
        single_entry_list_setup,
        single_entry_list_teardown)
{
    /* UUT */
    bool is_singular = h__list_model_is_singular(&single_entry_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_singular, true);
}

V__TEST(test_is_singular_false,
        double_entry_list_setup,
        double_entry_list_teardown)
{
    /* UUT */
    bool is_singular = h__list_model_is_singular(&double_entry_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_singular, false);
}

V__TEST(test_is_singular_empty_false, empty_list_setup, empty_list_teardown)
{
    /* UUT */
    bool is_singular = h__list_model_is_singular(&empty_list);

    /* Validation */
    V__ASSERT_EQ_BOOL(is_singular, false);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_init,
        &test_deinit,
        &test_next,
        &test_prev,
        &test_add_after,
        &test_add_after_2,
        &test_add_before,
        &test_add_before_2,
        &test_is_empty_true,
        &test_is_empty_false,
        &test_is_empty_after_false,
        &test_is_empty_before_false,
        &test_is_singular_true,
        &test_is_singular_false,
        &test_is_singular_empty_false,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
