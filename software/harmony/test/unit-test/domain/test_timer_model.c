/*
 * test_timer_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* Tested header */
#include "harmony/domain/timer_model.h"

/* Depends */
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

FAKE_VOID_FUNC(h__osal_timer_start, uint32_t)
FAKE_VALUE_FUNC(uint32_t, h__osal_timer_to_ticks, uint32_t)

FAKE_VOID_FUNC(dummy_fn_empty, h__timer *, void *)
FAKE_VOID_FUNC(sentinel_map_fn, h__timer *, void *)

static void
reset_fakes(void)
{
    RESET_FAKE(h__osal_timer_start);
    RESET_FAKE(h__osal_timer_to_ticks);
    RESET_FAKE(dummy_fn_empty);
    RESET_FAKE(sentinel_map_fn);
    FFF_RESET_HISTORY();
}

V__TEST(test_ctor, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer          timer;
    h__timer_sentinel dummy_sentinel;
    int               dummy_fn_arg = 0;

    /* UUT */
    h__timer_model_ctor(
            &timer,
            &dummy_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_FALSE(h__timer_model_is_active(&timer));
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_dtor, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer          timer;
    h__timer_sentinel dummy_sentinel;
    int               dummy_fn_arg = 0;
    h__timer_model_ctor(
            &timer,
            &dummy_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_dtor(&timer);

    /* Validation */
    V__ASSERT_FALSE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_sentinel_initializer, NULL, NULL)
{
    /* Preconditions */
    /* UUT */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_FALSE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
}

V__TEST(test_start_after_1_pretick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 1;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 1);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 1);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_1_1_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 1;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_start_after(&timer, 1);

    /* UUT */
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_FALSE(h__timer_model_is_active(&timer));
    V__ASSERT_FALSE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 1);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 1);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg0_val, &timer);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg1_val, &dummy_fn_arg);
}

V__TEST(test_start_after_2_pretick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 2;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 2);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 2);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_2_1_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 2;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 2);
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 2);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_2_2_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 2;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 2);
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_FALSE(h__timer_model_is_active(&timer));
    V__ASSERT_FALSE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 2);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 1);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg0_val, &timer);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg1_val, &dummy_fn_arg);
}

V__TEST(test_start_after_3_pretick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 3);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 3);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_3_1_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 3);
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 3);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_3_2_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 3);
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_TRUE(h__timer_model_is_active(&timer));
    V__ASSERT_TRUE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 3);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 0);
}

V__TEST(test_start_after_3_3_tick, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);

    /* UUT */
    h__timer_model_start_after(&timer, 3);
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }
    if (h__timer_model_sentinel_is_any_active(&timer_sentinel)) {
        h__timer_model_sentinel_tick(&timer_sentinel);
    }

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_FALSE(h__timer_model_is_active(&timer));
    V__ASSERT_FALSE(h__timer_model_sentinel_is_any_active(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_val, 3);
    V__ASSERT_EQ_UINT32(dummy_fn_empty_fake.call_count, 1);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg0_val, &timer);
    V__ASSERT_EQ_PTR(dummy_fn_empty_fake.arg1_val, &dummy_fn_arg);
}

V__TEST(test_get_remaining_ticks_after_1, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    h__osal_timer_to_ticks_fake.return_val = 1;
    h__timer_model_ctor(&timer, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer, 1);

    /* UUT */
    uint32_t remaining = h__timer_model_get_remaining_ticks(&timer);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(remaining, 1);
}

V__TEST(test_get_remaining_ticks_after_2, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    h__osal_timer_to_ticks_fake.return_val = 2;
    h__timer_model_ctor(&timer, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer, 2);

    /* UUT */
    uint32_t remaining = h__timer_model_get_remaining_ticks(&timer);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(remaining, 2);
}

V__TEST(test_get_remaining_ticks_after_3, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(&timer, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer, 3);

    /* UUT */
    uint32_t remaining = h__timer_model_get_remaining_ticks(&timer);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(remaining, 3);
}

V__TEST(test_get_remaining_ticks_2_after_3, reset_fakes, NULL)
{
    /* Preconditions */
    uint32_t          osal_timer_to_ticks_return_seq[] = {3, 6};
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer1;
    h__timer timer2;
    SET_RETURN_SEQ(h__osal_timer_to_ticks, osal_timer_to_ticks_return_seq, 2);
    h__timer_model_ctor(&timer1, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_ctor(&timer2, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer1, 3);
    h__timer_model_start_after(&timer2, 6);

    /* UUT */
    uint32_t remaining1 = h__timer_model_get_remaining_ticks(&timer1);
    uint32_t remaining2 = h__timer_model_get_remaining_ticks(&timer2);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer1));
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer2));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 2);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[0], 3);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[1], 6);
    V__ASSERT_EQ_UINT32(remaining1, 3);
    V__ASSERT_EQ_UINT32(remaining2, 6);
}

V__TEST(test_get_remaining_ticks_3_after_3, reset_fakes, NULL)
{
    /* Preconditions */
    uint32_t          osal_timer_to_ticks_return_seq[] = {3, 6, 9};
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer1;
    h__timer timer2;
    h__timer timer3;
    SET_RETURN_SEQ(h__osal_timer_to_ticks, osal_timer_to_ticks_return_seq, 9);
    h__timer_model_ctor(&timer1, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_ctor(&timer2, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_ctor(&timer3, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer1, 3);
    h__timer_model_start_after(&timer2, 6);
    h__timer_model_start_after(&timer3, 9);

    /* UUT */
    uint32_t remaining1 = h__timer_model_get_remaining_ticks(&timer1);
    uint32_t remaining2 = h__timer_model_get_remaining_ticks(&timer2);
    uint32_t remaining3 = h__timer_model_get_remaining_ticks(&timer3);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer1));
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer2));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 3);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[0], 3);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[1], 6);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[2], 9);
    V__ASSERT_EQ_UINT32(remaining1, 3);
    V__ASSERT_EQ_UINT32(remaining2, 6);
    V__ASSERT_EQ_UINT32(remaining3, 9);
}

V__TEST(test_get_remaining_ticks_3_after_3_reversed, reset_fakes, NULL)
{
    /* Preconditions */
    uint32_t          osal_timer_to_ticks_return_seq[] = {9, 6, 3};
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer1;
    h__timer timer2;
    h__timer timer3;
    SET_RETURN_SEQ(h__osal_timer_to_ticks, osal_timer_to_ticks_return_seq, 9);
    h__timer_model_ctor(&timer1, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_ctor(&timer2, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_ctor(&timer3, &timer_sentinel, &dummy_fn_empty, NULL);
    h__timer_model_start_after(&timer3, 9);
    h__timer_model_start_after(&timer2, 6);
    h__timer_model_start_after(&timer1, 3);

    /* UUT */
    uint32_t remaining1 = h__timer_model_get_remaining_ticks(&timer1);
    uint32_t remaining2 = h__timer_model_get_remaining_ticks(&timer2);
    uint32_t remaining3 = h__timer_model_get_remaining_ticks(&timer3);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer1));
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer2));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.call_count, 3);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[0], 9);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[1], 6);
    V__ASSERT_EQ_UINT32(h__osal_timer_to_ticks_fake.arg0_history[2], 3);
    V__ASSERT_EQ_UINT32(remaining1, 3);
    V__ASSERT_EQ_UINT32(remaining2, 6);
    V__ASSERT_EQ_UINT32(remaining3, 9);
}

V__TEST(test_sentinel_map_active_0_count, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    int sentinel_map_fn_arg = 0;

    /* UUT */
    h__timer_model_sentinel_map_active(
            &timer_sentinel,
            sentinel_map_fn,
            &sentinel_map_fn_arg);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(sentinel_map_fn_fake.call_count, 0);
}

V__TEST(test_sentinel_map_active_1_after_count, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer;
    int      dummy_fn_arg                  = 0;
    int      sentinel_map_fn_arg           = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_start_after(&timer, 3);

    /* UUT */
    h__timer_model_sentinel_map_active(
            &timer_sentinel,
            sentinel_map_fn,
            &sentinel_map_fn_arg);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(sentinel_map_fn_fake.call_count, 1);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg0_val, &timer);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg1_val, &sentinel_map_fn_arg);
}

V__TEST(test_sentinel_map_active_2_after_count, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer1;
    h__timer timer2;
    int      dummy_fn_arg                  = 0;
    int      sentinel_map_fn_arg           = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer1,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_ctor(
            &timer2,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_start_after(&timer1, 3);
    h__timer_model_start_after(&timer2, 6);

    /* UUT */
    h__timer_model_sentinel_map_active(
            &timer_sentinel,
            sentinel_map_fn,
            &sentinel_map_fn_arg);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer1));
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer2));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(sentinel_map_fn_fake.call_count, 2);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg0_history[0], &timer1);
    V__ASSERT_EQ_PTR(
            sentinel_map_fn_fake.arg1_history[0],
            &sentinel_map_fn_arg);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg0_history[1], &timer2);
    V__ASSERT_EQ_PTR(
            sentinel_map_fn_fake.arg1_history[1],
            &sentinel_map_fn_arg);
}

V__TEST(test_sentinel_map_active_2_after_count_reversed, reset_fakes, NULL)
{
    /* Preconditions */
    h__timer_sentinel timer_sentinel =
            H__TIMER_MODEL_SENTINEL_INITIALIZER(&timer_sentinel);
    h__timer timer1;
    h__timer timer2;
    int      dummy_fn_arg                  = 0;
    int      sentinel_map_fn_arg           = 0;
    h__osal_timer_to_ticks_fake.return_val = 3;
    h__timer_model_ctor(
            &timer1,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_ctor(
            &timer2,
            &timer_sentinel,
            &dummy_fn_empty,
            &dummy_fn_arg);
    h__timer_model_start_after(&timer2, 3);
    h__timer_model_start_after(&timer1, 3);

    /* UUT */
    h__timer_model_sentinel_map_active(
            &timer_sentinel,
            sentinel_map_fn,
            &sentinel_map_fn_arg);

    /* Validation */
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer1));
    V__ASSERT_TRUE(H__TIMER_MODEL_IS_VALID(&timer2));
    V__ASSERT_TRUE(H__TIMER_MODEL_SENTINEL_IS_VALID(&timer_sentinel));
    V__ASSERT_EQ_UINT32(sentinel_map_fn_fake.call_count, 2);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg0_history[0], &timer2);
    V__ASSERT_EQ_PTR(
            sentinel_map_fn_fake.arg1_history[0],
            &sentinel_map_fn_arg);
    V__ASSERT_EQ_PTR(sentinel_map_fn_fake.arg0_history[1], &timer1);
    V__ASSERT_EQ_PTR(
            sentinel_map_fn_fake.arg1_history[1],
            &sentinel_map_fn_arg);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_ctor,
        &test_dtor,
        &test_sentinel_initializer,
        &test_start_after_1_pretick,
        &test_start_after_1_1_tick,
        &test_start_after_2_pretick,
        &test_start_after_2_1_tick,
        &test_start_after_2_2_tick,
        &test_start_after_3_pretick,
        &test_start_after_3_1_tick,
        &test_start_after_3_2_tick,
        &test_start_after_3_3_tick,
        &test_get_remaining_ticks_after_1,
        &test_get_remaining_ticks_after_2,
        &test_get_remaining_ticks_after_3,
        &test_get_remaining_ticks_2_after_3,
        &test_get_remaining_ticks_3_after_3,
        &test_get_remaining_ticks_3_after_3_reversed,
        &test_sentinel_map_active_0_count,
        &test_sentinel_map_active_1_after_count,
        &test_sentinel_map_active_2_after_count,
        &test_sentinel_map_active_2_after_count_reversed,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
