/*
 * test_task_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "harmony/domain/task_model.h"

/* Depends */
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS
FAKE_VOID_FUNC(dummy_task_fn, h__task *, void *)
FAKE_VOID_FUNC(
        h__scheduler_model_node_ctor,
        h__scheduler_node *,
        h__scheduler *,
        h__scheduler_node_priority)
FAKE_VOID_FUNC(h__scheduler_model_node_dtor, h__scheduler_node *)

#define FFF_FAKES_LIST(FAKE)                                                   \
    FAKE(dummy_task_fn)                                                        \
    FAKE(h__scheduler_model_node_ctor)

#define DUMMY_TASK_NAME      "a_task_name"
#define DUMMY_TASK_PRIORITY  1
#define DUMMY_TASK_FN        dummy_task_fn
#define DUMMY_TASK_ARG       &dummy_task_arg
#define DUMMY_TASK_SCHEDULER &dummy_task_scheduler

static h__scheduler dummy_task_scheduler;
static h__task      dummy_task;
static int          dummy_task_arg;

static void
fff_setup(void)
{
    /* Register resets */
    FFF_FAKES_LIST(RESET_FAKE);

    /* Reset common FFF internal structures */
    FFF_RESET_HISTORY();
}

static void
dummy_task_setup(void)
{
    fff_setup();

    h__task_model_ctor(
            /* task */ &dummy_task,
            /* scheduler */ DUMMY_TASK_SCHEDULER,
            DUMMY_TASK_PRIORITY,
            DUMMY_TASK_FN,
            DUMMY_TASK_ARG,
            DUMMY_TASK_NAME);
}

static void
dummy_task_teardown(void)
{
    h__task_model_dtor(&dummy_task);
}

V__TEST(task_ctor, fff_setup, NULL)
{
    /* Preconditions */
    h__task          task;
    h__scheduler     dummy_scheduler;
    h__task_priority dummy_priority = 1;
    int              dummy_arg;
    const char *     dummy_name = "a task name";

    /* UUT */
    h__task_model_ctor(
            &task,
            &dummy_scheduler,
            dummy_priority,
            dummy_task_fn,
            &dummy_arg,
            dummy_name);

    /* Validation */
    V__ASSERT_EQ_STRING(task.name, dummy_name);
    V__ASSERT_TRUE(task.fn == dummy_task_fn);
    V__ASSERT_EQ_PTR(task.args, &dummy_arg);
    V__ASSERT_EQ_UINT32(h__scheduler_model_node_ctor_fake.call_count, 1);
    V__ASSERT_EQ_PTR(
            h__scheduler_model_node_ctor_fake.arg0_val,
            &task.scheduler_node);
    V__ASSERT_EQ_PTR(
            h__scheduler_model_node_ctor_fake.arg1_val,
            &dummy_scheduler);
    V__ASSERT_EQ_UINT32(
            h__scheduler_model_node_ctor_fake.arg2_val,
            dummy_priority);
}

V__TEST(task_dtor, fff_setup, NULL)
{
    /* Preconditions */
    h__task      task;
    h__scheduler dummy_scheduler;
    int          dummy_priority = 1;
    int          dummy_arg;
    const char * dummy_name = "a task name";
    h__task_model_ctor(
            &task,
            &dummy_scheduler,
            dummy_priority,
            dummy_task_fn,
            &dummy_arg,
            dummy_name);

    /* UUT */
    h__task_model_dtor(&task);

    /* Validation */
    V__ASSERT_EQ_STRING(task.name, "deleted");
    V__ASSERT_TRUE(task.fn == NULL);
    V__ASSERT_EQ_PTR(task.args, NULL);
    V__ASSERT_EQ_UINT32(h__scheduler_model_node_dtor_fake.call_count, 1);
    V__ASSERT_EQ_PTR(
            h__scheduler_model_node_dtor_fake.arg0_val,
            &task.scheduler_node);
}

V__TEST(task_get_name, dummy_task_setup, dummy_task_teardown)
{
    const char * name;

    /* UUT */
    name = h__task_model_get_name(&dummy_task);

    V__ASSERT_EQ_STRING(name, DUMMY_TASK_NAME);
}

V__TEST(task_get_fn, dummy_task_setup, dummy_task_teardown)
{
    h__task_fn * fn;

    /* UUT */
    fn = h__task_model_get_fn(&dummy_task);

    V__ASSERT_TRUE(fn == DUMMY_TASK_FN);
}

V__TEST(task_get_args, dummy_task_setup, dummy_task_teardown)
{
    void * args;

    /* UUT */
    args = h__task_model_get_args(&dummy_task);

    V__ASSERT_EQ_PTR(args, DUMMY_TASK_ARG);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &task_ctor,
        &task_dtor,
        &task_get_name,
        &task_get_fn,
        &task_get_args,
};
const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
