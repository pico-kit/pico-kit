/*
 * tasker_model.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_TASKER_MODEL_H_
#define HARMONY_DOMAIN_TASKER_MODEL_H_

/* Extends */
#include "harmony/domain/tasker_type.h"

/* Depends */
#include <stdbool.h>
#include "harmony/domain/common/osal_interface.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"
#include "harmony/domain/timer_model.h"

#define H__TASKER_MODEL_IDLE_TASK_NAME "idle task"

#define H__TASKER_MODEL_INITIALIZER(instance, a_osal)                          \
    {                                                                          \
        .scheduler = H__SCHEDULER_MODEL_INITIALIZER(                           \
                (instance).scheduler,                                          \
                (a_osal)),                                                     \
        .idle_task = H__TASK_MODEL_INITIALIZER(                                \
                (instance).idle_task,                                          \
                &(instance).scheduler,                                         \
                H__TASK_PRIORITY_IDLE,                                         \
                h__tasker_idle_task_fn,                                        \
                &(instance).scheduler,                                         \
                H__TASKER_MODEL_IDLE_TASK_NAME),                               \
    }

#define H__TASKER_MODEL_IS_VALID(tasker)                                       \
    (((tasker) != NULL) && (H__SCHEDULER_MODEL_IS_VALID(&(tasker)->scheduler)) \
     && (H__TASK_MODEL_IS_VALID(&(tasker)->idle_task))                         \
     && (H__TASK_MODEL_IS_VALID(&(tasker)->timer_task))                        \
     && (H__TIMER_MODEL_SENTINEL_IS_VALID(&(tasker)->timer_sentinel)))

void
h__tasker_model_ctor(h__tasker * tasker, h__osal * osal);

void
h__tasker_model_dtor(h__tasker * tasker);

inline void
h__tasker_model_lock(h__tasker * tasker)
{
    h__scheduler_model_lock(&tasker->scheduler);
}

inline void
h__tasker_model_unlock(h__tasker * tasker)
{
    h__scheduler_model_unlock(&tasker->scheduler);
}

inline void
h__tasker_model_run(h__tasker * tasker)
{
    h__scheduler_model_node_pending(
            h__task_model_get_scheduler_node(&tasker->idle_task));
    h__scheduler_model_run(&tasker->scheduler);
}

inline void
h__tasker_model_stop(h__tasker * tasker)
{
    h__scheduler_model_stop(&tasker->scheduler);
}

inline bool
h__tasker_model_is_dormant(const h__tasker * tasker)
{
    return h__scheduler_model_is_dormant(&tasker->scheduler);
}

inline bool
h__tasker_model_is_idle(const h__tasker * tasker)
{
    return h__scheduler_model_is_idle(&tasker->scheduler);
}

inline bool
h__tasker_model_is_running(const h__tasker * tasker)
{
    return h__scheduler_model_is_running(&tasker->scheduler);
}

inline h__scheduler *
h__tasker_model_get_scheduler(h__tasker * tasker)
{
    return &tasker->scheduler;
}

inline h__timer_sentinel *
h__tasker_model_get_timer_sentinel(h__tasker * tasker)
{
    return &tasker->timer_sentinel;
}

void
h__tasker_idle_task_fn(h__task * task, void * args);

void
h__tasker_model_timer_task(h__task * task, void * args);

#endif /* HARMONY_DOMAIN_TASKER_MODEL_H_ */
