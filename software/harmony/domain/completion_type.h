/*
 * completion_type.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMPLETION_TYPE_H_
#define HARMONY_DOMAIN_COMPLETION_TYPE_H_

/* Extends */
#include "harmony/domain/completion.h"

/* Depends */
#include <stdatomic.h>
#include "harmony/domain/task.h"

struct h__completion {
    h__task *   task;
    atomic_uint counter;
};

#endif /* HARMONY_DOMAIN_COMPLETION_TYPE_H_ */
