/*
 * list_type.h
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMMON_LIST_TYPE_H_
#define HARMONY_DOMAIN_COMMON_LIST_TYPE_H_

/* Extends */
#include "harmony/domain/common/list.h"

/**
 * @brief       List node object.
 * @note        All members of this structure are private.
 */
struct h__list {
    struct h__list * next;  //!< Next node in linked list.
    struct h__list * prev;  //!< Previous node in linked list.
};

#endif /* HARMONY_DOMAIN_COMMON_LIST_TYPE_H_ */
