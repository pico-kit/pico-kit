/*
 * list_model.h
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMMON_LIST_MODEL_H_
#define HARMONY_DOMAIN_COMMON_LIST_MODEL_H_

/* Extends */
#include "harmony/domain/common/list_type.h"

/* Depends */
#include <stdbool.h>
#include <stddef.h>

/**
 * @brief		Statically allocated list initializer
 */
#define H__LIST_MODEL_INITIALIZER(instance)                                    \
    {                                                                          \
        .next = (instance), .prev = (instance)                                 \
    }

#define H__LIST_MODEL_IS_VALID(list)                                           \
    (((list) != NULL) && ((list)->next != NULL) && ((list)->prev != NULL))

/**
 * @brief      	Construct for @a FOR loop to iterate over each element in a
 * list.
 *
 * @code
 * h__list * current;
 * h__list * sentinel = &g_list_sentinel.list;
 *
 * for (H__LIST_EACH(current, sentinel)) {
 *     ... do something with @a current (excluding remove)
 * }
 * @endcode
 */
#define H__LIST_MODEL_EACH(current, sentinel)                                  \
    current = (sentinel)->next;                                                \
    current != (sentinel);                                                     \
    current = (current)->next

/**
 * @brief      	Construct for FOR loop to iterate over each element in list
 * backwards.
 *
 * @code
 * h__list * current;
 * h__list * sentinel = &g_list_sentinel.list;
 *
 * for (H__LIST_BACKWARDS(current, sentinel)) {
 *     ... do something with current (excluding remove)
 * }
 * @endcode
 */
#define H__LIST_MODEL_BACKWARDS(current, sentinel)                             \
    current = (sentinel)->prev;                                                \
    current != (sentinel);                                                     \
    current = (current)->prev

/**
 * @brief      	Construct for FOR loop to iterate over each element in list
 * which is safe against element removal.
 *
 * @code
 * h__list * current;
 * h__list * iterator;
 * h__list * sentinel = &g_list_sentinel.list;
 *
 * for (H__LIST_EACH_SAFE(current, iterator, sentinel)) {
 *     ... do something with current (including remove)
 * }
 * @endcode
 */
#define H__LIST_MODEL_EACH_SAFE(current, iterator, sentinel)                   \
    current = (sentinel)->next, iterator = (current)->next;                    \
    current != (sentinel);                                                     \
    current = iterator, iterator = (iterator)->next

inline void
h__list_model_init(h__list * node)
{
    node->next = node;
    node->prev = node;
}

inline void
h__list_model_deinit(h__list * node)
{
    node->next = NULL;
    node->prev = NULL;
}

inline struct h__list *
h__list_model_next(const h__list * node)
{
    return node->next;
}

inline struct h__list *
h__list_model_prev(const h__list * node)
{
    return node->prev;
}

inline void
h__list_model_add_after(h__list * node, h__list * after)
{
    node->next        = after->next;
    node->prev        = after;
    after->next->prev = node;
    after->next       = node;
}

inline void
h__list_model_add_before(h__list * node, h__list * before)
{
    node->next         = before;
    node->prev         = before->prev;
    before->prev->next = node;
    before->prev       = node;
}

inline void
h__list_model_remove(h__list * node)
{
    node->next->prev = node->prev;
    node->prev->next = node->next;
    /* The following `node` initialization code is necessary:
     * - since we want to support calling this function multiple times over the
     * same list node that was already removed from a list.
     * - since we want to support querying if a list is empty or not.
     */
    node->next = node;
    node->prev = node;
}

inline bool
h__list_model_is_empty(const struct h__list * node)
{
    return !!(node->next == node);
}

inline bool
h__list_model_is_singular(const struct h__list * node)
{
    return !!((node->next != node) && (node->next == node->prev));
}

#endif /* HARMONY_DOMAIN_COMMON_LIST_MODEL_H_ */
