/*
 * port_model.h
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMMON_PORT_MODEL_H_
#define HARMONY_DOMAIN_COMMON_PORT_MODEL_H_

/* Depends */
#include <stdint.h>

uint_fast8_t
h__port_find_first_set(uint32_t value);

#endif /* HARMONY_DOMAIN_COMMON_PORT_MODEL_H_ */
