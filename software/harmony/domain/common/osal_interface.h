/*
 * osal.h
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMMON_OSAL_INTERFACE_H_
#define HARMONY_DOMAIN_COMMON_OSAL_INTERFACE_H_

/* Depends */
#include <stdint.h>

typedef struct h__osal h__osal;

void
h__osal_lock(h__osal * osal);

void
h__osal_unlock(h__osal * osal);

void
h__osal_signal(h__osal * osal);

void
h__osal_wait(h__osal * osal);

void
h__osal_global_lock(void);
void
h__osal_global_unlock(void);

void
h__osal_timer_start(uint32_t ticks);
void
h__osal_timer_stop(void);
uint32_t
h__osal_timer_to_ticks(uint32_t time_in_ms);

#endif /* HARMONY_DOMAIN_COMMON_OSAL_INTERFACE_H_ */
