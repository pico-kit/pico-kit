/*
 * list_model.c
 *
 *  Created on: Oct 11, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/common/list_model.h"

extern inline void
h__list_model_init(h__list * node);

extern inline void
h__list_model_deinit(h__list * node);

extern inline struct h__list *
h__list_model_next(const h__list * node);

extern inline struct h__list *
h__list_model_prev(const h__list * node);

extern inline void
h__list_model_add_after(h__list * node, h__list * after);

extern inline void
h__list_model_add_before(h__list * node, h__list * before);

extern inline void
h__list_model_remove(h__list * node);

extern inline bool
h__list_model_is_empty(const struct h__list * node);

extern inline bool
h__list_model_is_singular(const struct h__list * node);
