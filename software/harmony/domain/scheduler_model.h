/*
 * scheduler_model.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_SCHEDULER_MODEL_H_
#define HARMONY_DOMAIN_SCHEDULER_MODEL_H_

/* Extends */
#include "harmony/domain/scheduler_type.h"

/* Depends */
#include <stdbool.h>
#include "harmony/domain/common/list_model.h"
#include "harmony/domain/common/osal_interface.h"

#define H__SCHEDULER_MODEL_INITIALIZER(instance, a_osal)                       \
    {                                                                          \
        .osal = (a_osal),                                                      \
        .ready_groups[0] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[0]),        \
        .ready_groups[1] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[1]),        \
        .ready_groups[2] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[2]),        \
        .ready_groups[3] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[3]),        \
        .ready_groups[4] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[4]),        \
        .ready_groups[5] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[5]),        \
        .ready_groups[6] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[6]),        \
        .ready_groups[7] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[7]),        \
        .ready_groups[8] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[8]),        \
        .ready_groups[9] =                                                     \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[9]),        \
        .ready_groups[10] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[10]),       \
        .ready_groups[11] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[11]),       \
        .ready_groups[12] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[12]),       \
        .ready_groups[13] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[13]),       \
        .ready_groups[14] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[14]),       \
        .ready_groups[15] =                                                    \
                H__LIST_MODEL_INITIALIZER(&(instance).ready_groups[15]),       \
        .state = H__SCHEDULER_STATE_IDLE,                                      \
    }

#define H__SCHEDULER_MODEL_NODE_INITIALIZER(instance, a_scheduler, a_priority) \
    {                                                                          \
        .scheduler = (a_scheduler), .priority = (a_priority),                  \
        .operation_node =                                                      \
                H__LIST_MODEL_INITIALIZER(&(instance).operation_node),         \
        .state = H__SCHEDULER_NODE_STATE_SUSPENDED,                            \
    }

#define H__SCHEDULER_MODEL_IS_VALID(scheduler)                                 \
    (((scheduler) != NULL)                                                     \
     && ((scheduler)->state != H__SCHEDULER_STATE_DORMANT))

#define H__SCHEDULER_MODEL_NODE_IS_VALID(scheduler_node)                       \
    (((scheduler_node) != NULL)                                                \
     && ((scheduler_node)->state != H__SCHEDULER_NODE_STATE_DORMANT))

void
h__scheduler_model_ctor(h__scheduler * scheduler, h__osal * osal);

void
h__scheduler_model_dtor(h__scheduler * scheduler);

inline void
h__scheduler_model_lock(h__scheduler * scheduler)
{
    h__osal_lock(scheduler->osal);
}

inline void
h__scheduler_model_unlock(h__scheduler * scheduler)
{
    h__osal_unlock(scheduler->osal);
}

inline void
h__scheduler_model_sleep(h__scheduler * scheduler)
{
    h__osal_wait(scheduler->osal);
}

void
h__scheduler_model_run(h__scheduler * scheduler);

void
h__scheduler_model_stop(h__scheduler * scheduler);

inline bool
h__scheduler_model_is_dormant(const h__scheduler * scheduler)
{
    return !!(scheduler->state == H__SCHEDULER_STATE_DORMANT);
}

inline bool
h__scheduler_model_is_idle(const h__scheduler * scheduler)
{
    return !!(scheduler->state == H__SCHEDULER_STATE_IDLE);
}

inline bool
h__scheduler_model_is_running(const h__scheduler * scheduler)
{
    return !!(scheduler->state == H__SCHEDULER_STATE_RUNNING);
}

void
h__scheduler_model_node_ctor(
        h__scheduler_node *        node,
        h__scheduler *             scheduler,
        h__scheduler_node_priority priority);

void
h__scheduler_model_node_dtor(h__scheduler_node * node);

inline bool
h__scheduler_model_node_is_suspended(const h__scheduler_node * node)
{
    return node->state == H__SCHEDULER_NODE_STATE_SUSPENDED;
}

inline bool
h__scheduler_model_node_is_ready(const h__scheduler_node * node)
{
    return node->state == H__SCHEDULER_NODE_STATE_READY;
}

inline h__scheduler *
h__scheduler_model_node_get_scheduler(const h__scheduler_node * node)
{
    return node->scheduler;
}

void
h__scheduler_model_node_pending(h__scheduler_node * node);

inline bool
h__scheduler_model_node_resume(h__scheduler_node * node)
{
    bool           retval;
    h__scheduler * scheduler = node->scheduler;

    h__list_model_add_after(
            /* node */ &node->operation_node,
            /* after */ &scheduler->ready_groups[node->priority]);
    scheduler->ready_group |= 0x1u << node->priority;
    if (node->priority > scheduler->current->priority) {
        retval = true;
    } else {
        retval = false;
    }
    node->state = H__SCHEDULER_NODE_STATE_READY;
    h__osal_signal(scheduler->osal);
    return retval;
}

inline void
h__scheduler_model_node_suspend(h__scheduler_node * node)
{
    h__scheduler * scheduler = node->scheduler;

    h__list_model_remove(&node->operation_node);
    if (h__list_model_is_empty(&scheduler->ready_groups[node->priority])) {
        scheduler->ready_group &= ~(0x1u << node->priority);
    }
    node->state = H__SCHEDULER_NODE_STATE_SUSPENDED;
}

extern void
h__scheduler_model_node_execute(h__scheduler_node * node);

#endif /* HARMONY_DOMAIN_SCHEDULER_MODEL_H_ */
