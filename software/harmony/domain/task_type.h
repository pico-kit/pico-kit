/*
 * task_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_TASK_TYPE_H_
#define HARMONY_DOMAIN_TASK_TYPE_H_

/* Extends */
#include "harmony/domain/task.h"

/* Depends */
#include "harmony/domain/common/list_type.h"
#include "harmony/domain/scheduler_type.h"

struct h__task {
    h__scheduler_node scheduler_node;
    h__task_fn *      fn;
    void *            args;
    const char *      name;
};

#endif /* HARMONY_DOMAIN_TASK_TYPE_H_ */
