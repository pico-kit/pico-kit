/*
 * tasker_model.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/tasker_model.h"

/* Depends */
#include <stddef.h>
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/timer_model.h"

void
h__tasker_model_ctor(h__tasker * tasker, h__osal * osal)
{
    h__scheduler_model_ctor(&tasker->scheduler, osal);
    h__task_model_ctor(
            /* task */ &tasker->idle_task,
            /* scheduler */ &tasker->scheduler,
            /* priority */ H__TASK_PRIORITY_IDLE,
            /* fn */ h__tasker_idle_task_fn,
            /* args */ &tasker->scheduler,
            /* name */ H__TASKER_MODEL_IDLE_TASK_NAME);
}

void
h__tasker_model_dtor(h__tasker * tasker)
{
    h__task_model_dtor(&tasker->idle_task);
    h__scheduler_model_dtor(&tasker->scheduler);
}

extern inline void
h__tasker_model_lock(h__tasker * tasker);

extern inline void
h__tasker_model_unlock(h__tasker * tasker);

extern void
h__tasker_model_run(h__tasker * tasker);

extern void
h__tasker_model_stop(h__tasker * tasker);

extern inline bool
h__tasker_model_is_dormant(const h__tasker * tasker);

extern inline bool
h__tasker_model_is_idle(const h__tasker * tasker);

extern inline bool
h__tasker_model_is_running(const h__tasker * tasker);

extern inline h__scheduler *
h__tasker_model_get_scheduler(h__tasker * tasker);

void
h__tasker_idle_task_fn(h__task * task, void * args)
{
    (void) task;

    h__scheduler_model_sleep(args);
}

void
h__tasker_model_timer_task(h__task * task, void * args)
{
    (void) task;

    h__timer_model_sentinel_tick(args);
}
