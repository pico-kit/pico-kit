/*
 * scheduler.h
 *
 *  Created on: Feb 19, 2024
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_SCHEDULER_H_
#define HARMONY_DOMAIN_SCHEDULER_H_

/* Depends */
#include <stdint.h>

typedef struct h__scheduler h__scheduler;

typedef enum h__scheduler_state {
    H__SCHEDULER_STATE_DORMANT,
    H__SCHEDULER_STATE_IDLE,
    H__SCHEDULER_STATE_RUNNING
} h__scheduler_state;

typedef struct h__scheduler_node h__scheduler_node;

typedef uint_fast8_t h__scheduler_node_priority;

typedef enum h__scheduler_node_state {
    H__SCHEDULER_NODE_STATE_DORMANT,
    H__SCHEDULER_NODE_STATE_READY,
    H__SCHEDULER_NODE_STATE_SUSPENDED,
    H__SCHEDULER_NODE_STATE_TERMINATED
} h__scheduler_node_state;

#endif /* HARMONY_DOMAIN_SCHEDULER_H_ */
