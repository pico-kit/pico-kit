/*
 * scheduler_model.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/scheduler_model.h"

/* Depends */
#include <stddef.h>
#include "foundation/platform.h"
#include "harmony/domain/common/port_interface.h"

static h__scheduler_node *
scheduler_node_from_operation_node(h__list * operation_node)
{
    return F__PLATFORM_CONTAINER_OF(
            /* ptr */ operation_node,
            /* type */ h__scheduler_node,
            /* member */ operation_node);
}

void
h__scheduler_model_ctor(h__scheduler * scheduler, h__osal * osal)
{
    scheduler->current     = NULL;
    scheduler->ready_group = 0u;
    for (size_t i = 0u; i < H__SCHEDULER_READY_GROUPS; i++) {
        h__list_model_init(&scheduler->ready_groups[i]);
    }
    scheduler->osal  = osal;
    scheduler->state = H__SCHEDULER_STATE_IDLE;
}

void
h__scheduler_model_dtor(h__scheduler * scheduler)
{
    h__list * curr;
    h__list * it;

    scheduler->state = H__SCHEDULER_STATE_DORMANT;
    scheduler->osal  = NULL;
    /* Remove all tasks from ready list */
    for (size_t i = 0u; i < H__SCHEDULER_READY_GROUPS; i++) {
        for (H__LIST_MODEL_EACH_SAFE(curr, it, &scheduler->ready_groups[i])) {
            h__list_model_remove(curr);
        }
    }
    scheduler->ready_group = 0u;
    scheduler->current     = NULL;
}

extern inline void
h__scheduler_model_lock(h__scheduler * scheduler);

extern inline void
h__scheduler_model_unlock(h__scheduler * scheduler);

extern inline void
h__scheduler_model_sleep(h__scheduler * scheduler);

void
h__scheduler_model_run(h__scheduler * scheduler)
{
    scheduler->state = H__SCHEDULER_STATE_RUNNING;
    while (scheduler->state == H__SCHEDULER_STATE_RUNNING) {
        uint_fast16_t       highest_priority;
        h__scheduler_node * node;

        h__osal_lock(scheduler->osal);
        highest_priority = h__port_find_first_set(scheduler->ready_group);
        node             = scheduler_node_from_operation_node(
                h__list_model_next(&scheduler->ready_groups[highest_priority]));
        scheduler->current = node;
        h__osal_unlock(scheduler->osal);
        h__scheduler_model_node_execute(node);
    }
}

void
h__scheduler_model_stop(h__scheduler * scheduler)
{
    scheduler->state = H__SCHEDULER_STATE_IDLE;
}

extern inline bool
h__scheduler_model_is_dormant(const h__scheduler * scheduler);

extern inline bool
h__scheduler_model_is_idle(const h__scheduler * scheduler);

extern inline bool
h__scheduler_model_is_running(const h__scheduler * scheduler);

void
h__scheduler_model_node_ctor(
        h__scheduler_node *        node,
        h__scheduler *             scheduler,
        h__scheduler_node_priority priority)
{
    node->scheduler = scheduler;
    node->priority  = priority;
    h__list_model_init(&node->operation_node);
    node->state = H__SCHEDULER_NODE_STATE_SUSPENDED;
}

void
h__scheduler_model_node_dtor(h__scheduler_node * node)
{
    node->state = H__SCHEDULER_NODE_STATE_TERMINATED;
    h__list_model_deinit(&node->operation_node);
    node->priority  = 0u;
    node->scheduler = NULL;
}

extern inline bool
h__scheduler_model_node_is_suspended(const h__scheduler_node * node);

extern inline bool
h__scheduler_model_node_is_ready(const h__scheduler_node * node);

extern inline h__scheduler *
h__scheduler_model_node_get_scheduler(const h__scheduler_node * node);

void
h__scheduler_model_node_pending(h__scheduler_node * node)
{
    h__scheduler * scheduler = node->scheduler;

    h__list_model_add_after(
            /* node */ &node->operation_node,
            /* after */ &scheduler->ready_groups[node->priority]);
    scheduler->ready_group |= 0x1u << node->priority;
    node->state = H__SCHEDULER_NODE_STATE_READY;
}

extern inline bool
h__scheduler_model_node_resume(h__scheduler_node * node);

extern inline void
h__scheduler_model_node_suspend(h__scheduler_node * node);
