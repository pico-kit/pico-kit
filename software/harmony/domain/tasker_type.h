/*
 * tasker_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_TASKER_TYPE_H_
#define HARMONY_DOMAIN_TASKER_TYPE_H_

/* Extends */
#include "harmony/domain/tasker.h"

/* Depends */
#include "harmony/domain/scheduler_type.h"
#include "harmony/domain/task_type.h"
#include "harmony/domain/timer_type.h"

struct h__tasker {
    h__scheduler      scheduler;
    h__task           idle_task;
    h__task           timer_task;
    h__timer_sentinel timer_sentinel;
};

#endif /* HARMONY_DOMAIN_TASKER_TYPE_H_ */
