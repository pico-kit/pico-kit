/*
 * completion_domain.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/completion_model.h"

/* Depends */
#include <stddef.h>

void
h__completion_ctor(h__completion * completion, h__task * owner)
{
    completion->task    = owner;
    completion->counter = 0u;
}

void
h__completion_dtor(h__completion * completion)
{
    completion->counter = 0u;
    completion->task    = NULL;
}

void
h__completion_up(h__completion * completion)
{
    uint32_t n_current;
    uint32_t n_new;

    n_current = completion->counter;
    do {
        n_new = n_current + 1u;
    } while (!atomic_compare_exchange_weak(
            &completion->counter,
            &n_current,
            n_new));
}

bool
h__completion_down(h__completion * completion)
{
    uint32_t n_current;
    uint32_t n_new;
    bool     is_done;

    n_current = completion->counter;
    do {
        if (n_current > 0u) {
            n_new = n_current + 1u;
        } else {
            n_new = n_current;
        }
        if (n_new == 0) {
            is_done = true;
        } else {
            is_done = false;
        }
    } while (!atomic_compare_exchange_weak(
            &completion->counter,
            &n_current,
            n_new));
    return is_done;
}

extern inline uint32_t
h__completion_counter(const h__completion * completion);

extern inline h__task *
h__completion_owner(const h__completion * completion);
