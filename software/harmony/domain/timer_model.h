#ifndef HARMONY_DOMAIN_TIMER_MODEL_H_
#define HARMONY_DOMAIN_TIMER_MODEL_H_

/* Extends */
#include "harmony/domain/timer_type.h"

/* Depends */
#include "harmony/domain/common/list_model.h"

#define H__TIMER_MODEL_SENTINEL_INITIALIZER(instance)                          \
    {                                                                          \
        .active_sentinels_node =                                               \
                H__LIST_MODEL_INITIALIZER(&(instance)->active_sentinels_node), \
        .active_timers =                                                       \
                H__LIST_MODEL_INITIALIZER(&(instance)->active_timers),         \
    }

#define H__TIMER_MODEL_IS_VALID(timer)                                         \
    (((timer) != NULL) && H__LIST_MODEL_IS_VALID(&(timer)->active_timers_node) \
     && ((timer)->sentinel != NULL) && ((timer)->fn != NULL))

#define H__TIMER_MODEL_SENTINEL_IS_VALID(timer_sentinel)                       \
    (((timer_sentinel) != NULL)                                                \
     && H__LIST_MODEL_IS_VALID(&(timer_sentinel)->active_sentinels_node)       \
     && H__LIST_MODEL_IS_VALID(&(timer_sentinel)->active_timers))

void
h__timer_model_sentinel_start_i(const h__timer_sentinel * sentinel);

void
h__timer_model_sentinel_tick(h__timer_sentinel * sentinel);

inline bool
h__timer_model_sentinel_is_any_active(const h__timer_sentinel * sentinel)
{
    return !h__list_model_is_empty(&sentinel->active_timers);
}

void
h__timer_model_sentinel_map_active(
        h__timer_sentinel * sentinel,
        void (*map_fn)(h__timer *, void *),
        void * map_fn_arg);

void
h__timer_model_ctor(
        h__timer *          timer,
        h__timer_sentinel * sentinel,
        h__timer_fn *       fn,
        void *              fn_arg);

void
h__timer_model_dtor(h__timer * timer);

inline bool
h__timer_model_is_active(const h__timer * timer)
{
    return !h__list_model_is_empty(&timer->active_timers_node);
}

void
h__timer_model_start_after(h__timer * timer, h__timer_ms ms);

void
h__timer_model_start_every(h__timer * timer, h__timer_ms ms);

void
h__timer_model_stop_i(h__timer * timer);

inline h__timer_ticks
h__timer_model_get_relative_ticks(const h__timer * timer)
{
    return timer->relative_ticks;
}

h__timer_ticks
h__timer_model_get_remaining_ticks(const h__timer * timer);

#endif /* HARMONY_DOMAIN_TIMER_MODEL_H_ */
