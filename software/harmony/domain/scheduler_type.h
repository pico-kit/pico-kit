/*
 * tasker_type.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_SCHEDULER_TYPE_H_
#define HARMONY_DOMAIN_SCHEDULER_TYPE_H_

/* Extends */
#include "harmony/domain/scheduler.h"

/* Depends */
#include <stdbool.h>
#include "harmony/domain/common/list_type.h"
#include "harmony/domain/common/osal_interface.h"

#define H__SCHEDULER_READY_GROUPS 16

struct h__scheduler {
    h__scheduler_node *         current;
    uint_fast16_t               ready_group;
    h__list                     ready_groups[H__SCHEDULER_READY_GROUPS];
    h__osal *                   osal;
    volatile h__scheduler_state state;
};

struct h__scheduler_node {
    h__scheduler *             scheduler;
    h__scheduler_node_priority priority;
    h__list                    operation_node;
    h__scheduler_node_state    state;
};

#endif /* HARMONY_DOMAIN_SCHEDULER_TYPE_H_ */
