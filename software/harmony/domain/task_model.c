/*
 * task_model.c
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/task_model.h"

/* Depends */
#include <stddef.h>
#include "harmony/domain/common/list_model.h"
#include "harmony/domain/scheduler_model.h"

void
h__task_model_ctor(
        h__task *        task,
        h__scheduler *   scheduler,
        h__task_priority priority,
        h__task_fn *     fn,
        void *           args,
        const char *     name)
{
    h__scheduler_model_node_ctor(&task->scheduler_node, scheduler, priority);
    task->fn   = fn;
    task->args = args;
    task->name = name != NULL ? name : "unnamed";
}

void
h__task_model_dtor(h__task * task)
{
    task->name = "deleted";
    task->args = NULL;
    task->fn   = NULL;
    h__scheduler_model_node_dtor(&task->scheduler_node);
}

extern inline h__scheduler_node *
h__task_model_get_scheduler_node(h__task * task);

extern inline const char *
h__task_model_get_name(const h__task * task);

extern inline h__task_fn *
h__task_model_get_fn(const h__task * task);

extern inline void *
h__task_model_get_args(const h__task * task);

extern inline h__task *
h__task_model_task_from_scheduler_node(h__scheduler_node * scheduler_node);

void
h__scheduler_model_node_execute(h__scheduler_node * scheduler_node)
{
    h__task * task;

    task = h__task_model_task_from_scheduler_node(scheduler_node);
    task->fn(task, task->args);
}
