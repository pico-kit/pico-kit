
/* Implements */
#include "harmony/domain/timer_model.h"

/* Depends */
#include "foundation/platform.h"
#include "harmony/domain/common/list_model.h"
#include "harmony/domain/common/osal_interface.h"

static void
timer_sentinel_insert(h__timer_sentinel * sentinel, h__timer * new_timer)
{
    h__list * node;

    for (H__LIST_MODEL_EACH(node, &sentinel->active_timers)) {
        h__timer * timer = F__PLATFORM_CONTAINER_OF(
                /* ptr */ node,
                /* type */ h__timer,
                /* member */ active_timers_node);
        if (timer->relative_ticks > new_timer->relative_ticks) {
            timer->relative_ticks -= new_timer->relative_ticks;
            break;
        }
        new_timer->relative_ticks -= timer->relative_ticks;
    }
    h__list_model_add_before(&new_timer->active_timers_node, node);
}

void
h__timer_model_sentinel_start_i(const h__timer_sentinel * sentinel)
{
    (void) sentinel;
    h__osal_timer_start(1);
}

void
h__timer_model_sentinel_tick(h__timer_sentinel * sentinel)
{
    /* See if we have any active timers */
    h__list * it;

    /* Get the first timer in list */
    h__list *  node  = h__list_model_next(&sentinel->active_timers);
    h__timer * timer = F__PLATFORM_CONTAINER_OF(
            /* ptr */ node,
            /* type */ h__timer,
            /* member */ active_timers_node);
    /* Decrement the first timer relative tick */
    timer->relative_ticks--;
    /* Execute all elapsed timer callbacks */
    for (H__LIST_MODEL_EACH_SAFE(node, it, &sentinel->active_timers)) {
        timer = F__PLATFORM_CONTAINER_OF(
                /* ptr */ node,
                /* type */ h__timer,
                /* member */ active_timers_node);

        if (timer->relative_ticks != 0u) {
            break;
        }
        h__list_model_remove(node);
        if (timer->initial_ticks != 0u) {
            timer->relative_ticks = timer->initial_ticks;
            timer_sentinel_insert(sentinel, timer);
        }
        timer->fn(timer, timer->fn_arg);
    }
}

extern inline bool
h__timer_model_sentinel_is_any_active(const h__timer_sentinel * sentinel);

void
h__timer_model_sentinel_map_active(
        h__timer_sentinel * sentinel,
        void (*map_fn)(h__timer *, void *),
        void * map_fn_arg)
{
    h__list * it;
    h__list * current;

    for (H__LIST_MODEL_EACH_SAFE(current, it, &sentinel->active_timers)) {
        h__timer * timer = F__PLATFORM_CONTAINER_OF(
                /* ptr */ current,
                /* type */ h__timer,
                /* member */ active_timers_node);
        map_fn(timer, map_fn_arg);
    }
}

void
h__timer_model_ctor(
        h__timer *          timer,
        h__timer_sentinel * sentinel,
        h__timer_fn *       fn,
        void *              fn_arg)
{
    h__list_model_init(&timer->active_timers_node);
    timer->sentinel       = sentinel;
    timer->initial_ticks  = 0u;
    timer->relative_ticks = 0u;
    timer->fn             = fn;
    timer->fn_arg         = fn_arg;
}

void
h__timer_model_dtor(h__timer * timer)
{
    timer->fn_arg         = NULL;
    timer->fn             = NULL;
    timer->relative_ticks = 0u;
    timer->initial_ticks  = 0u;
    timer->sentinel       = NULL;
    h__list_model_deinit(&timer->active_timers_node);
}

extern inline bool
h__timer_model_is_active(const h__timer * timer);

void
h__timer_model_start_after(h__timer * timer, h__timer_ms ms)
{
    h__timer_ticks after_ticks;

    after_ticks = h__osal_timer_to_ticks(ms);
    if (after_ticks == 0) {
        after_ticks = 1;
    }
    timer->initial_ticks  = 0u;
    timer->relative_ticks = after_ticks;
    timer_sentinel_insert(timer->sentinel, timer);
}

void
h__timer_model_start_every(h__timer * timer, h__timer_ms ms)
{
    h__timer_ticks every_ticks;

    every_ticks = h__osal_timer_to_ticks(ms);
    if (every_ticks == 0) {
        every_ticks = 1;
    }
    timer->initial_ticks  = every_ticks;
    timer->relative_ticks = every_ticks;
    timer_sentinel_insert(timer->sentinel, timer);
}

void
h__timer_model_stop_i(h__timer * timer)
{
    h__list_model_remove(&timer->active_timers_node);
    timer->initial_ticks  = 0u;
    timer->relative_ticks = 0u;
}

extern inline h__timer_ticks
h__timer_model_get_relative_ticks(const h__timer * timer);

h__timer_ticks
h__timer_model_get_remaining_ticks(const h__timer * timer)
{
    h__list *           node;
    h__timer_sentinel * sentinel  = timer->sentinel;
    h__timer_ticks      remaining = 0u;

    for (H__LIST_MODEL_EACH(node, &sentinel->active_timers)) {
        const h__timer * current = F__PLATFORM_CONTAINER_OF(
                /* ptr */ node,
                /* type */ h__timer,
                /* member */ active_timers_node);
        remaining += current->relative_ticks;
        if (current == timer) {
            break;
        }
    }
    return remaining;
}
