/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Harmony error header.
 *
 * This header file contains Harmony errors.
 */
/**
 * @defgroup    harmony_error Harmony error
 *
 * @brief       Harmony error API.
 *
 * @{
 */

#ifndef HARMONY_DOMAIN_ERROR_H_
#define HARMONY_DOMAIN_ERROR_H_

#define H__ERROR_TIMER_INIT_TIMER_NULL      0x0100u
#define H__ERROR_TIMER_INIT_TASKER_NULL     0x0101u
#define H__ERROR_TIMER_INIT_FN_NULL         0x0102u
#define H__ERROR_TIMER_INIT_TASK_INVALID    0x0103u
#define H__ERROR_TIMER_DEINIT_TIMER_INVALID 0x0104u

#endif /* HARMONY_DOMAIN_ERROR_H_ */
/** @} */
