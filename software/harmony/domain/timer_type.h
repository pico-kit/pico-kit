#ifndef HARMONY_DOMAIN_TIMER_TYPE_H_
#define HARMONY_DOMAIN_TIMER_TYPE_H_

/* Extends */
#include "harmony/domain/timer.h"

/* Depends */
#include <stdint.h>
#include "harmony/domain/common/list_type.h"

struct h__timer_sentinel {
    h__list active_sentinels_node;
    h__list active_timers;
};

typedef uint32_t h__timer_ticks;

struct h__timer {
    h__list active_timers_node;         //!< Linked list node for active_timers.
    h__timer_sentinel * sentinel;       //!< Sentinel that handles this timer.
    h__timer_ticks      initial_ticks;  //!< Initial time in ticks.
    h__timer_ticks      relative_ticks;  //!< Relative time in ticks.
    h__timer_fn *       fn;              //!< Callback function.
    void *              fn_arg;          //!< Callback function arugument.
};

#endif /* HARMONY_DOMAIN_TIMER_TYPE_H_ */
