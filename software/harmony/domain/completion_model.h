/*
 * completion_domain.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_COMPLETION_MODEL_H_
#define HARMONY_DOMAIN_COMPLETION_MODEL_H_

/* Extends */
#include "harmony/domain/completion_type.h"

/* Depends */
#include <stdbool.h>
#include <stdint.h>

void
h__completion_ctor(h__completion * completion, h__task * owner);
void
h__completion_dtor(h__completion * completion);
void
h__completion_up(h__completion * completion);
bool
h__completion_down(h__completion * completion);

inline uint32_t
h__completion_counter(const h__completion * completion)
{
    return completion->counter;
}

inline h__task *
h__completion_owner(const h__completion * completion)
{
    return completion->task;  // cppcheck-suppress nullPointerRedundantCheck
}

#endif /* HARMONY_DOMAIN_COMPLETION_MODEL_H_ */
