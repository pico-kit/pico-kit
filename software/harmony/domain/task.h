/*
 * task.h
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_TASK_H_
#define HARMONY_DOMAIN_TASK_H_

/* Depends */
#include <stdint.h>

typedef struct h__task h__task;

/**
 * "brief		Task priority
 *
 *

    TASK_PRIORITY_IDLE: Represents the lowest priority level reserved for the
 idle task.

    TASK_PRIORITY_LOWEST: Represents the next priority level after the idle
 task, indicating the lowest non-idle priority.

    TASK_PRIORITY_LOWER: Represents a priority level lower than the normal
 priority but higher than the lowest priority.

    TASK_PRIORITY_NORMAL: Represents the priority level considered as normal or
 default.

    TASK_PRIORITY_HIGHER: Represents a priority level higher than the normal
 priority but lower than the highest priority.

    TASK_PRIORITY_HIGHEST: Represents the highest priority level.

    TASK_PRIORITY_CRITICAL: Represents a priority level higher than the highest
 priority, indicating critical tasks.

    TASK_PRIORITY_SYSTEM: Represents a priority level reserved for system-level
 tasks or operations.
 *
 */
typedef enum h__task_priority {
    H__TASK_PRIORITY_IDLE          = 0,
    H__TASK_PRIORITY_LOWEST        = 1,
    H__TASK_PRIORITY_ABOVE_LOWEST  = 2,
    H__TASK_PRIORITY_BELOW_LOWER   = 3,
    H__TASK_PRIORITY_LOWER         = 4,
    H__TASK_PRIORITY_ABOVE_LOWER   = 5,
    H__TASK_PRIORITY_BELOW_NORMAL  = 6,
    H__TASK_PRIORITY_NORMAL        = 7,
    H__TASK_PRIORITY_ABOVE_NORMAL  = 8,
    H__TASK_PRIORITY_BELOW_HIGHER  = 9,
    H__TASK_PRIORITY_HIGHER        = 10,
    H__TASK_PRIORITY_ABOVE_HIGHER  = 11,
    H__TASK_PRIORITY_BELOW_HIGHEST = 12,
    H__TASK_PRIORITY_HIGHEST       = 13,
    H__TASK_PRIORITY_CRITICAL      = 14,
    H__TASK_PRIORITY_SYSTEM        = 15
} h__task_priority;

typedef void(h__task_fn)(h__task *, void *);

#endif /* HARMONY_DOMAIN_TASK_H_ */
