
/* Implements */
#include "harmony/domain/global_state.h"

/* Depends */
#include "harmony/domain/timer_model.h"

h__timer_sentinel h__g__global_state_timer_sentinel =
        H__TIMER_MODEL_SENTINEL_INITIALIZER(&h__g__global_state_timer_sentinel);
