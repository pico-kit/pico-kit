/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Harmony timer types header.
 *
 * This header file contains Harmony timer types.
 */

#ifndef HARMONY_DOMAIN_TIMER_H_
#define HARMONY_DOMAIN_TIMER_H_

#include <stdint.h>

typedef struct h__timer_sentinel h__timer_sentinel;

typedef struct h__timer h__timer;

/**
 * @brief       Timer handler function pointer.
 *
 * The timer handler function is a function with the following prototype:
 *
 * @code
 * void timer_handler(h__timer * timer, void * arg);
 * @endcode
 *
 * This function is called once when the timer has elapsed. For timers that are
 * single shot (see @ref h__timer_start_after) it will be called once it is
 * elapsed, for repeating timers (see @ref h__timer_start_every) it will be
 * called multiple times whenever the timer elapses. Stopping the timer (see
 * @ref h__timer_stop) does not call this function.
 *
 * The first argument, @a timer, is a pointer to timer instance that called this
 * function. The second argument, @a fn_arg, is the passed parameter to
 * @ref h__timer_init function.
 */
typedef void(h__timer_fn)(h__timer * timer, void * fn_arg);

typedef uint32_t h__timer_ms;

#endif /* HARMONY_DOMAIN_TIMER_H_ */
