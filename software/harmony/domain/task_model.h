/*
 * task_model.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_DOMAIN_TASK_MODEL_H_
#define HARMONY_DOMAIN_TASK_MODEL_H_

/* Extends */
#include "harmony/domain/task_type.h"

/* Depends */
#include <stdbool.h>
#include "foundation/platform.h"
#include "harmony/domain/scheduler_model.h"

#define H__TASK_MODEL_INITIALIZER(                                             \
        instance,                                                              \
        a_scheduler,                                                           \
        a_priority,                                                            \
        a_fn,                                                                  \
        a_args,                                                                \
        a_name)                                                                \
    {                                                                          \
        .scheduler_node = H__SCHEDULER_MODEL_NODE_INITIALIZER(                 \
                (instance).scheduler_node,                                     \
                (a_scheduler),                                                 \
                (a_priority)),                                                 \
        .fn = (a_fn), .args = (a_args), .name = (a_name),                      \
    }

#define H__TASK_MODEL_IS_VALID(task)                                           \
    (((task) != NULL)                                                          \
     && (H__SCHEDULER_MODEL_NODE_IS_VALID(&(task)->scheduler_node))            \
     && ((task)->fn != NULL))

void
h__task_model_ctor(
        h__task *        task,
        h__scheduler *   scheduler,
        h__task_priority priority,
        h__task_fn *     fn,
        void *           args,
        const char *     name);

void
h__task_model_dtor(h__task * task);

inline h__scheduler_node *
h__task_model_get_scheduler_node(h__task * task)
{
    return &task->scheduler_node;
}

inline const char *
h__task_model_get_name(const h__task * task)
{
    return task->name;
}

inline h__task_fn *
h__task_model_get_fn(const h__task * task)
{
    return task->fn;
}

inline void *
h__task_model_get_args(const h__task * task)
{
    return task->args;
}

inline h__task *
h__task_model_task_from_scheduler_node(h__scheduler_node * scheduler_node)
{
    return F__PLATFORM_CONTAINER_OF(scheduler_node, h__task, scheduler_node);
}

#endif /* HARMONY_DOMAIN_TASK_MODEL_H_ */
