/*
 * tasker_critical.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/tasker/tasker_critical.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/tasker_model.h"

void
h__tasker_critical_lock(h__tasker * tasker)
{
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);
    h__tasker_model_lock(tasker);
}

void
h__tasker_critical_unlock(h__tasker * tasker)
{
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);
    h__tasker_model_unlock(tasker);
}

F__PLATFORM_ATTR_NO_RETURN
void
h__validator_failure_handler(const char * expression, uint32_t error)
{
    (void) expression;
    (void) error;
    while (1)
        ;
}
