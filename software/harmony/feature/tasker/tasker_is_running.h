/*
 * tasker_is_running.h
 *
 *  Created on: Feb 13, 2024
 *      Author: nenad
 */

#ifndef FEATURE_TASKER_TASKER_IS_RUNNING_H_
#define FEATURE_TASKER_TASKER_IS_RUNNING_H_

/* Depends */
#include <stdbool.h>
#include "harmony/domain/tasker.h"

bool
h__tasker_is_running(const h__tasker * tasker);

#endif /* FEATURE_TASKER_TASKER_IS_RUNNING_H_ */
