/*
 * tasker_deinit.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/tasker/tasker_critical.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/tasker_model.h"

void
h__tasker_deinit(h__tasker * tasker)  // cppcheck-suppress constParameter
{
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);

    h__tasker_model_dtor(tasker);
}
