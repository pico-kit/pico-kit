/*
 * tasker_init.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASKER_TASKER_INIT_H_
#define HARMONY_FEATURE_TASKER_TASKER_INIT_H_

/* Depends */
#include "harmony/domain/common/osal_interface.h"
#include "harmony/domain/tasker.h"

#define H__TASKER_INITIALIZER(instance, h__osal_context)                       \
    H__TASKER_MODEL_INITIALIZER(instance, h__osal_context)

void
h__tasker_init(h__tasker * tasker, h__osal * osal);

#endif /* HARMONY_FEATURE_TASKER_TASKER_INIT_H_ */
