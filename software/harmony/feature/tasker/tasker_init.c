/*
 * tasker_init.c
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/tasker/tasker_init.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/task_model.h"
#include "harmony/domain/tasker_model.h"

void
h__tasker_init(h__tasker * tasker, h__osal * osal)
{
    F__VALIDATOR_PRECONDITION((tasker != NULL), 0x1);

    h__tasker_model_ctor(tasker, osal);
}
