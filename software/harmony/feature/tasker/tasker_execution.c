/*
 * tasker_execution.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/tasker/tasker_execution.h"

/* Depends */
#include <assert.h>
#include "foundation/validator.h"
#include "harmony/domain/common/osal_interface.h"
#include "harmony/domain/global_state.h"
#include "harmony/domain/tasker_model.h"
#include "harmony/domain/timer_model.h"

void
h__tasker_execution_start(h__tasker * tasker)
{
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(h__tasker_model_is_idle(tasker), 0x1);
    h__osal_global_lock();
    h__timer_model_sentinel_start_i(&h__g__global_state_timer_sentinel);
    h__osal_global_unlock();
    h__tasker_model_run(tasker);
}

void
h__tasker_execution_stop(h__tasker * tasker)
{
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(h__tasker_model_is_running(tasker), 0x1);
    h__tasker_model_stop(tasker);
}
