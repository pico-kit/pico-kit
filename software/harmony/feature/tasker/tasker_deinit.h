/*
 * tasker_deinit.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASKER_TASKER_DEINIT_H_
#define HARMONY_FEATURE_TASKER_TASKER_DEINIT_H_

/* Depends */
#include "harmony/domain/tasker.h"

void
h__tasker_deinit(h__tasker * tasker);

#endif /* HARMONY_FEATURE_TASKER_TASKER_DEINIT_H_ */
