/*
 * tasker_critical.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASKER_TASKER_CRITICAL_H_
#define HARMONY_FEATURE_TASKER_TASKER_CRITICAL_H_

/* Depends */
#include "harmony/domain/tasker.h"

void
h__tasker_critical_lock(h__tasker * tasker);

void
h__tasker_critical_unlock(h__tasker * tasker);

#endif /* HARMONY_FEATURE_TASKER_TASKER_CRITICAL_H_ */
