/*
 * tasker_execution.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASKER_TASKER_EXECUTION_H_
#define HARMONY_FEATURE_TASKER_TASKER_EXECUTION_H_

/* Depends */
#include "harmony/domain/tasker.h"

void
h__tasker_execution_start(h__tasker * tasker);

void
h__tasker_execution_stop(h__tasker * tasker);

#endif /* HARMONY_FEATURE_TASKER_TASKER_EXECUTION_H_ */
