/*
 * tasker_is_running.c
 *
 *  Created on: Feb 13, 2024
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/tasker/tasker_is_running.h"

/* Depends */
#include "harmony/domain/tasker_model.h"

bool
h__tasker_is_running(const h__tasker * tasker)
{
    return h__tasker_model_is_running(tasker);
}
