/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Timer implementation.
 */

/* Implements */
#include "harmony/feature/timer.h"

/* Depends */
#include "foundation/error.h"
#include "foundation/validator.h"
#include "harmony/domain/common/osal_interface.h"
#include "harmony/domain/error.h"
#include "harmony/domain/tasker_model.h"
#include "harmony/domain/timer_model.h"

int
h__timer_init(
        h__timer *                   timer,
        h__tasker *                  tasker,
        h__timer_fn *                fn,
        void *                       fn_arg,
        const struct h__timer_attr * attr)
{
    F__VALIDATOR_PRECONDITION(timer != NULL, H__ERROR_TIMER_INIT_TIMER_NULL);
    F__VALIDATOR_PRECONDITION(tasker != NULL, H__ERROR_TIMER_INIT_TASKER_NULL);
    F__VALIDATOR_PRECONDITION(fn != NULL, H__ERROR_TIMER_INIT_FN_NULL);
    F__VALIDATOR_INVARIANT(
            H__TASKER_MODEL_IS_VALID(tasker),
            H__ERROR_TIMER_INIT_TASK_INVALID);

    h__timer_sentinel * sentinel = h__tasker_model_get_timer_sentinel(tasker);
    h__timer_model_ctor(timer, sentinel, fn, fn_arg);

    return F__ERR_NONE;
}

int
h__timer_deinit(h__timer * timer)
{
    F__VALIDATOR_PRECONDITION(
            H__TIMER_MODEL_IS_VALID(timer),
            H__ERROR_TIMER_DEINIT_TIMER_INVALID);

    h__osal_global_lock();
    if (h__timer_model_is_active(timer)) {
        h__timer_model_stop_i(timer);
    }
    h__osal_global_unlock();
    h__timer_model_dtor(timer);

    return F__ERR_NONE;
}

bool
h__timer_is_valid(const h__timer * timer)
{
    return H__TIMER_MODEL_IS_VALID(timer);
}

void
h__timer_start_after(h__timer * timer, uint32_t after_ms)
{
    F__VALIDATOR_PRECONDITION(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__osal_global_lock();
    h__timer_model_start_after(timer, after_ms);
    h__osal_global_unlock();
}

void
h__timer_start_after_i(h__timer * timer, uint32_t after_ms)
{
    F__VALIDATOR_INVARIANT(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__timer_model_start_after(timer, after_ms);
}

void
h__timer_start_every(h__timer * timer, uint32_t every_ms)
{
    F__VALIDATOR_PRECONDITION(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__osal_global_lock();
    h__timer_model_start_every(timer, every_ms);
    h__osal_global_unlock();
}

void
h__timer_start_every_i(h__timer * timer, uint32_t every_ms)
{
    F__VALIDATOR_INVARIANT(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__timer_model_start_every(timer, every_ms);
}

void
h__timer_stop(h__timer * timer)
{
    F__VALIDATOR_PRECONDITION(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__osal_global_lock();
    h__timer_model_stop_i(timer);
    h__osal_global_unlock();
}

void
h__timer_stop_i(h__timer * timer)
{
    F__VALIDATOR_INVARIANT(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    h__timer_model_stop_i(timer);
}

bool
h__timer_is_active(const h__timer * timer)
{
    F__VALIDATOR_PRECONDITION(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    return h__timer_model_is_active(timer);
}

uint32_t
h__timer_get_remaining(const h__timer * timer)
{
    F__VALIDATOR_PRECONDITION(H__TIMER_MODEL_IS_VALID(timer), 0x1);

    if (h__timer_model_is_active(timer)) {
        return h__timer_model_get_remaining_ticks(timer);
    } else {
        return 0u;
    }
}
