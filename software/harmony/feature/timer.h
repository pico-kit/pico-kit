/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Harmony timer feature API header.
 *
 * This header file contains Harmony timer feature API.
 *
 * Header containing interface for timer:
 * - initialization,
 * - deinitialization
 * - starting and stopping
 * - getting the state of timer
 */
/**
 * @ingroup     harmony
 * @defgroup    harmony_timer_feature Harmony timer feature
 *
 * @brief       Timer feature API.
 *
 * @{
 */

#ifndef HARMONY_FEATURE_TIMER_H_
#define HARMONY_FEATURE_TIMER_H_

/* Depends */
#include <stdbool.h>
#include "harmony/domain/tasker.h"
#include "harmony/domain/timer.h"

/**
 * @brief        Timer attributes
 */
struct h__timer_attr {
    /**
     * @brief    A human readable name of timer object for identification.
     */
    const char * name;
};

/**
 * @brief       Create and initialize a timer object.
 *
 * Before using a timer it needs to be created then initialized.
 *
 * @param       tasker is a pointer to tasker object that will handle the timer
 *              object. This parameter must be a pointer to a valid object of
 *              @ref h__tasker.
 * @param       fn is a pointer to timer handler function. This parameter must
 *              be a non-NULL pointer. For details about this function refer to
 *              @ref h__timer_fn.
 * @param       fn_arg is a pointer to timer handler function argument. This
 *              pointer is just stored into @a timer object and then it is
 *              passed to the function. The life duration of the pointed
 *              argument must be equal to or bigger then the life duration of
 *              the timer object. If the timer handler function does not need
 *              this argument, the parameter can be set to NULL.
 * @param       attr is a pointer to optional timer attribute structure, see
 *              @ref h__timer_attr. When attributes are not needed, use the NULL
 *              pointer here.
 * @param [out] timer is a pointer to timer pointer that will point to newly
 *              created object. This parameter must be a non-NULL pointer to
 *              `h__timer *` variable.
 * @return      Integer representing operation error status. For details on
 *              error codes see @ref foundation_error.
 * @retval      F__ERR_NONE `(0x0000u)` is returned when the operation completed
 *              succesfully. See @ref F__ERR_NONE for details.
 * @retval      F__ERR_NO_MEM `(0x0001u)` is returned when the operation could
 *              not be completed because of insufficient free heap memory. See
 *              @ref F__ERR_NO_MEM for details.
 *
 * @pre         Parameter @a tasker must point to a valid tasker object.
 * @pre         Parameter @a fn must be a non-NULL function pointer.
 * @pre         Parameter @a timer must be a non-NULL pointer.
 */
int
h__timer_create(
        h__tasker *                  tasker,
        h__timer_fn *                fn,
        void *                       fn_arg,
        const struct h__timer_attr * attr,
        h__timer **                  timer);

/**
 * @brief       Initialize a timer object.
 *
 * Before using a timer it needs to be initialized.
 *
 * @param       timer is a pointer to timer object that will be initialized.
 *              This parameter must be a non-NULL pointer to an allocated
 *              @ref h__timer structure.
 * @param       tasker is a pointer to tasker object that will handle the timer
 *              object. This parameter must be a pointer to a valid object of
 *              @ref h__tasker.
 * @param       fn is a pointer to timer handler function. This parameter must
 *              be a non-NULL pointer. For details about this function refer to
 *              @ref h__timer_fn.
 * @param       fn_arg is a pointer to timer handler function argument. This
 *              pointer is just stored into @a timer object and then it is
 *              passed to the function. The life duration of the pointed
 *              argument must be equal to or bigger then the life duration of
 *              the timer object. If the timer handler function does not need
 *              this argument, the parameter can be set to NULL.
 * @param       attr is a pointer to optional timer attribute structure, see
 *              @ref h__timer_attr. When attributes are not needed, use the NULL
 *              pointer here.
 * @return      Integer representing operation error status. For details on
 *              error codes see @ref foundation_error.
 * @retval      F__ERR_NONE `(0x0000u)` is returned when the operation completed
 *              succesfully. See @ref F__ERR_NONE for details.
 *
 * @pre         Parameter @a timer must be a non-NULL pointer.
 * @pre         Parameter @a tasker must point to a valid tasker object.
 * @pre         Parameter @a fn must be a non-NULL function pointer.
 */
int
h__timer_init(
        h__timer *                   timer,
        h__tasker *                  tasker,
        h__timer_fn *                fn,
        void *                       fn_arg,
        const struct h__timer_attr * attr);

int
h__timer_deinit(h__timer * timer);

bool
h__timer_is_valid(const h__timer * timer);

void
h__timer_start_after(h__timer * timer, uint32_t after_ms);

void
h__timer_start_after_i(h__timer * timer, uint32_t after_ms);

void
h__timer_start_every(h__timer * timer, uint32_t every_ms);

void
h__timer_start_every_i(h__timer * timer, uint32_t every_ms);

void
h__timer_stop(h__timer * timer);

void
h__timer_stop_i(h__timer * timer);

bool
h__timer_is_active(const h__timer * timer);

uint32_t
h__timer_get_remaining(const h__timer * timer);

#endif /* HARMONY_FEATURE_TIMER_H_ */
/** @} */
