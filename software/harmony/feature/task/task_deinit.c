/*
 * task_deinit.c
 *
 *  Created on: Oct 27, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/task/task_deinit.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

void
h__task_deinit(h__task * task)
{
    h__scheduler * scheduler;

    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);

    scheduler = h__scheduler_model_node_get_scheduler(&task->scheduler_node);
    h__scheduler_model_lock(scheduler);
    if (h__scheduler_model_node_is_ready(&task->scheduler_node)) {
        h__scheduler_model_node_suspend(&task->scheduler_node);
    }
    h__scheduler_model_unlock(scheduler);
    h__task_model_dtor(task);
}
