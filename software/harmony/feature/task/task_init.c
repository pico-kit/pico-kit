/*
 * task_init.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/task/task_init.h"

/* Depends */
#include "foundation/validator.h"
#include "harmony/domain/task_model.h"
#include "harmony/domain/tasker_model.h"

void
h__task_init(
        h__task *        task,
        h__tasker *      tasker,
        h__task_priority priority,
        h__task_fn *     fn,
        void *           args,
        const char *     name)
{
    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(tasker != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(fn != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(
            (priority >= H__TASK_PRIORITY_LOWEST)
                    && (priority <= H__TASK_PRIORITY_HIGHEST),
            0x1);

    h__task_model_ctor(
            task,
            h__tasker_model_get_scheduler(tasker),
            priority,
            fn,
            args,
            name);
}
