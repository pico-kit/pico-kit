/*
 * task_term.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASK_TASK_DEINIT_H_
#define HARMONY_FEATURE_TASK_TASK_DEINIT_H_

/* Depends */
#include "harmony/domain/task_type.h"

void
h__task_deinit(h__task * task);

#endif /* HARMONY_FEATURE_TASK_TASK_DEINIT_H_ */
