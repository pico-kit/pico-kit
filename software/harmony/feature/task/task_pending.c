/*
 * task_pending.c
 *
 *  Created on: Feb 13, 2024
 *      Author: nenad
 */

/* Implementing */
#include "harmony/feature/task/task_pending.h"

/* Depends */
#include "foundation/validator.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

void
h__task_pending(h__task * task)
{
    h__scheduler * scheduler;

    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(
            h__scheduler_model_is_idle(h__scheduler_model_node_get_scheduler(
                    &task->scheduler_node)),
            0x1);

    scheduler = h__scheduler_model_node_get_scheduler(&task->scheduler_node);
    h__scheduler_model_lock(scheduler);
    if (h__scheduler_model_node_is_suspended(&task->scheduler_node)) {
        h__scheduler_model_node_pending(&task->scheduler_node);
    }
    h__scheduler_model_unlock(scheduler);
}
