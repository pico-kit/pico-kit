/*
 * task_ready.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef FEATURE_TASK_TASK_RESUME_H_
#define FEATURE_TASK_TASK_RESUME_H_

/* Depends */
#include <stdbool.h>
#include "harmony/domain/task.h"

bool
h__task_resume(h__task * task);
bool
h__task_resume_i(h__task * task);

#endif /* FEATURE_TASK_TASK_RESUME_H_ */
