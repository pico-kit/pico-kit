/*
 * task_resueme.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implement */
#include "harmony/feature/task/task_resume.h"

/* Depends */
#include "foundation/validator.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

static bool
resume_task(h__task * task)
{
    if (h__scheduler_model_node_is_suspended(&task->scheduler_node)) {
        return h__scheduler_model_node_resume(&task->scheduler_node);
    }
    return false;
}

bool
h__task_resume(h__task * task)
{
    h__scheduler * scheduler;
    bool           is_preemted;

    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(
            h__scheduler_model_is_idle(h__scheduler_model_node_get_scheduler(
                    &task->scheduler_node)),
            0x1);

    scheduler = h__scheduler_model_node_get_scheduler(&task->scheduler_node);
    h__scheduler_model_lock(scheduler);
    is_preemted = resume_task(task);
    h__scheduler_model_unlock(scheduler);
    return is_preemted;
}

bool
h__task_resume_i(h__task * task)
{
    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);

    return resume_task(task);
}
