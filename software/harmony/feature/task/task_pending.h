/*
 * task_pending.h
 *
 *  Created on: Feb 13, 2024
 *      Author: nenad
 */

#ifndef FEATURE_TASK_TASK_PENDING_H_
#define FEATURE_TASK_TASK_PENDING_H_

/* Depends */
#include "harmony/domain/task.h"

void
h__task_pending(h__task * task);

#endif /* FEATURE_TASK_TASK_PENDING_H_ */
