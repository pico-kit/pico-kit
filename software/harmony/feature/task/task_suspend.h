/*
 * task_suspend.h
 *
 *  Created on: Oct 28, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASK_TASK_SUSPEND_H_
#define HARMONY_FEATURE_TASK_TASK_SUSPEND_H_

/* Depends */
#include "harmony/domain/task.h"

void
h__task_suspend(h__task * task);
void
h__task_suspend_i(h__task * task);

#endif /* HARMONY_FEATURE_TASK_TASK_SUSPEND_H_ */
