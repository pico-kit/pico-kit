/*
 * task_suspend.c
 *
 *  Created on: Oct 28, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/task/task_suspend.h"

/* Depends */
#include "foundation/validator.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

static void
suspend_task(h__task * task)
{
    if (h__scheduler_model_node_is_ready(&task->scheduler_node)) {
        h__scheduler_model_node_suspend(&task->scheduler_node);
    }
}

void
h__task_suspend(h__task * task)
{
    h__scheduler * scheduler;

    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(
            h__scheduler_model_is_idle(h__scheduler_model_node_get_scheduler(
                    &task->scheduler_node)),
            0x1);

    scheduler = h__scheduler_model_node_get_scheduler(&task->scheduler_node);
    h__scheduler_model_lock(scheduler);
    suspend_task(task);
    h__scheduler_model_unlock(scheduler);
}

void
h__task_suspend_i(h__task * task)
{
    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);

    suspend_task(task);
}
