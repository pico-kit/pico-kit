/*
 * task_init.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_TASK_TASK_INIT_H_
#define HARMONY_FEATURE_TASK_TASK_INIT_H_

/* Depends */
#include "harmony/domain/task.h"
#include "harmony/domain/tasker.h"

void
h__task_init(
        h__task *        task,
        h__tasker *      tasker,
        h__task_priority priority,
        h__task_fn *     fn,
        void *           args,
        const char *     name);

#endif /* HARMONY_FEATURE_TASK_TASK_INIT_H_ */
