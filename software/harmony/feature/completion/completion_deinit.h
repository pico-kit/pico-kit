/*
 * completion_deinit.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_COMPLETION_COMPLETION_DEINIT_H_
#define HARMONY_FEATURE_COMPLETION_COMPLETION_DEINIT_H_

/* Depends */
#include "harmony/domain/completion.h"

void
h__completion_deinit(h__completion * completion);

#endif /* HARMONY_FEATURE_COMPLETION_COMPLETION_DEINIT_H_ */
