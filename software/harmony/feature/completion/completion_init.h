/*
 * completion_init.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_COMPLETION_COMPLETION_INIT_H_
#define HARMONY_FEATURE_COMPLETION_COMPLETION_INIT_H_

/* Depends */
#include "harmony/domain/completion.h"
#include "harmony/domain/task.h"

void
h__completion_init(h__completion * completion, h__task * task);

#endif /* HARMONY_FEATURE_COMPLETION_COMPLETION_INIT_H_ */
