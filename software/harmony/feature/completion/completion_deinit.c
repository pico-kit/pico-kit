/*
 * completion_deinit.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/completion/completion_deinit.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/completion_model.h"

void
h__completion_deinit(h__completion * completion)
{
    F__VALIDATOR_PRECONDITION(completion != NULL, 0x1);

    h__completion_dtor(completion);
}
