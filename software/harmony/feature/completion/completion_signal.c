/*
 * completion_signal.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/completion/completion_signal.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/completion_model.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

void
h__completion_signal(const h__completion * completion)
{
    h__scheduler_node * tasker_node;
    h__scheduler *      tasker;

    F__VALIDATOR_PRECONDITION(completion != NULL, 0x1);
    tasker_node =
            h__task_model_get_scheduler_node(h__completion_owner(completion));
    tasker = h__scheduler_model_node_get_scheduler(tasker_node);
    h__scheduler_model_lock(tasker);
    h__scheduler_model_node_resume(tasker_node);
    h__scheduler_model_unlock(tasker);
}

void
h__completion_signal_i(const h__completion * completion)
{
    h__scheduler_node * tasker_node;

    F__VALIDATOR_PRECONDITION(completion != NULL, 0x1);

    tasker_node =
            h__task_model_get_scheduler_node(h__completion_owner(completion));
    h__scheduler_model_node_resume(tasker_node);
}
