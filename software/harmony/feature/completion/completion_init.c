/*
 * completion_init.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/completion/completion_init.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/completion_model.h"

void
h__completion_init(h__completion * completion, h__task * task)
{
    F__VALIDATOR_PRECONDITION(completion != NULL, 0x1);
    F__VALIDATOR_PRECONDITION(task != NULL, 0x1);

    h__completion_ctor(completion, task);
}
