/*
 * completion_wait.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_COMPLETION_COMPLETION_WAIT_H_
#define HARMONY_FEATURE_COMPLETION_COMPLETION_WAIT_H_

/* Depends */
#include <stdbool.h>
#include "harmony/domain/completion.h"

bool
h__completion_wait(h__completion * completion);

#endif /* HARMONY_FEATURE_COMPLETION_COMPLETION_WAIT_H_ */
