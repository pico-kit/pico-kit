/*
 * completion_signal.h
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

#ifndef HARMONY_FEATURE_COMPLETION_COMPLETION_SIGNAL_H_
#define HARMONY_FEATURE_COMPLETION_COMPLETION_SIGNAL_H_

/* Depends */
#include "harmony/domain/completion.h"

void
h__completion_signal(const h__completion * completion);
void
h__completion_signal_i(const h__completion * completion);

#endif /* HARMONY_FEATURE_COMPLETION_COMPLETION_SIGNAL_H_ */
