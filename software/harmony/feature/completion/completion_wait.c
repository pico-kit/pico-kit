/*
 * completion_wait.c
 *
 *  Created on: Oct 30, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/feature/completion/completion_wait.h"

/* Depends */
#include <stddef.h>
#include "foundation/validator.h"
#include "harmony/domain/completion_model.h"
#include "harmony/domain/scheduler_model.h"
#include "harmony/domain/task_model.h"

bool
h__completion_wait(h__completion * completion)
{
    h__scheduler_node * task_node;
    h__scheduler *      tasker;
    bool                is_done;

    F__VALIDATOR_PRECONDITION(completion != NULL, 0x1);

    task_node =
            h__task_model_get_scheduler_node(h__completion_owner(completion));
    tasker = h__scheduler_model_node_get_scheduler(task_node);
    h__scheduler_model_lock(tasker);
    is_done = h__completion_down(completion);
    if (is_done) {
        h__scheduler_model_node_suspend(task_node);
    }
    h__scheduler_model_unlock(tasker);
    return is_done;
}
