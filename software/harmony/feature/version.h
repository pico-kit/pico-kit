/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Harmony version feature API header.
 *
 * This header file contains Harmony version feature API.
 */
/**
 * @ingroup     harmony
 * @defgroup    harmony_version Harmony version feature
 *
 * @brief       Version feature API.
 *
 * @{
 */

#ifndef HARMONY_FEATURE_HARMONY_H_
#define HARMONY_FEATURE_HARMONY_H_

#define H__VERSION 0x010000u

#define H__VERSION_MAJOR 1u

#define H__VERSION_MINOR 0u

#define H__VERSION_PATCH 0u

#define H__VERSION_STRING "1.0.0"

#endif /* HARMONY_FEATURE_HARMONY_H_ */
/** @} */
