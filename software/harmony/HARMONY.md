# Harmony

Harmony is a software module of Pico-Kit library.

# Module structure

The breakdown of Harmony module directory structure:

```
+--firmware
|  +-- harmony
+--software
   +-- harmony
       +-- domain
       +-- feature
       |   +-- completion
       |   +-- task
       |   +-- tasker
       |   +-- timer
       +-- test
```

- `firmware/harmony` - This directory contains Harmony adapters.
- `software/harmony` - This directory contains Harmony generic sources:
  - `domain` - Contains the domain layer of the Harmony module.
  - `feature` - Contains code related to various features of the Harmony
    module:
    - `completion`: This directory contains code related to completion
      functionality.
    - `task`: This directory contains code related to task management.
    - `tasker`: This directory contains code related to task handling.
    - `timer`: This directory contains code related to timing functionality.
  - `test`: This directory contains test-related files.

## Domain Overview

The domain layer encapsulates core business logic and rules, independent of
implementation details. It consists of entities, value objects, aggregates,
and domain services, representing key concepts and behavior.

 - Entities represent business objects,
 - Value objects handle specific values or concepts,
 - Aggregates maintain consistency, and domain services encapsulate complex
   logic.

The domain defines the objects and is usually used by application to only
statically instantiate instances which are then managed by the features. The
Harmony module does not support dynamic object allocation so the application
can decide on how to optimally allocate the instances.

## Features Overview

Features are self-contained components that encapsulate specific functionality
without being tied to implementation details.

- **Tasker**: Provides task handling capabilities.
- **Task**: Manages task-related operations.
- **Completion**: Handles completion functionality.
- **Timer**: Deals with timing functionality.

# Tasker feature

The Tasker component is a cooperative scheduler designed to schedule task
instances based on their priority. It supports key operations such as
initialization, de-initialization, running registered tasks, and stopping task
processing. Additionally, the Tasker component provides the ability to lock and
unlock the scheduler to implement critical sections within the application.

The scheduler supports tasks with the same priority and utilizes a round-robin
scheduling technique for task scheduling.

However, no task starvation reduction technique is implemented.

# Task feature

# Completion feature

# Timer feature

