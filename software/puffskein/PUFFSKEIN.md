# Puffskein

Puffskein is a software module of Pico-Kit library.

# Module structure

The breakdown of Puffskein module directory structure:

```
+--firmware
|  +-- puffskein
+--software
   +-- puffskein
       +-- domain
       +-- feature
       +-- test
```

- `firmware/puffskein` - This directory contains Puffskein adapters.
- `software/puffskein` - This directory contains Puffskein generic sources:
  - `domain` - Contains the domain layer of the Puffskein module.
  - `feature` - Contains code related to various features of the Puffskein
    module.
  - `test`: This directory contains test-related files.

## Domain Overview

The domain layer encapsulates core business logic and rules, independent of
implementation details. It consists of entities, value objects, aggregates,
and domain services, representing key concepts and behavior.

 - Entities represent business objects,
 - Value objects handle specific values or concepts,
 - Aggregates maintain consistency, and domain services encapsulate complex
   logic.

The domain defines the objects and is usually used by application to only
statically instantiate instances which are then managed by the features. The
Puffskein module does not support dynamic object allocation so the application
can decide on how to optimally allocate the instances.

## Features Overview

Features are self-contained components that encapsulate specific functionality
without being tied to implementation details.
