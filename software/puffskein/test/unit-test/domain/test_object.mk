F_PROJECT_ROOT = ../../../../..

F_PROJECT_NAME = test_feature_object

F_SRC_FILES += software/puffskein/feature/object.cpp
F_SRC_FILES += software/puffskein/domain/object_model.cpp
F_SRC_FILES += software/puffskein/test/unit-test/domain/test_object.cpp
F_INC_DIRS += software
F_FEATURES += c17
F_SYSTEM_LIBRARIES += stdc++

include $(F_PROJECT_ROOT)/build/make/test.mk

