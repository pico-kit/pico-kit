/*
 * test_object.cpp
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "puffskein/feature/object.hpp"

/* Depends */
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

V__TEST(test_init, NULL, NULL)
{
    /* Preconditions */
    puffskein::Object object;

    /* UUT */

    /* Validation */
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_init,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
