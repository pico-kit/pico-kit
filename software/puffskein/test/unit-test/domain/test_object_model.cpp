/*
 * test_object_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "puffskein/domain/object_model.hpp"

/* Depends */
#include <cstdint>
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

class DummyObjectModel : private puffskein::ObjectModel
{
   public:
    DummyObjectModel() : puffskein::ObjectModel(1) {}
    std::uintptr_t id() const
    {
        return puffskein::ObjectModel::id();
    }
    std::uint32_t type() const
    {
        return puffskein::ObjectModel::type();
    }
    bool is_same(const DummyObjectModel & rhs) const
    {
        return puffskein::ObjectModel::is_same(rhs);
    }
    ~DummyObjectModel() {}
};

V__TEST(test_init, NULL, NULL)
{
    /* Preconditions */
    DummyObjectModel object;

    /* UUT */
    auto type = object.type();

    /* Validation */
    V__ASSERT_EQ_UINT32(type, 1);
}

V__TEST(test_init_2, NULL, NULL)
{
    /* Preconditions */
    DummyObjectModel object1;
    DummyObjectModel object2;

    /* UUT */
    auto id1 = object1.id();
    auto id2 = object2.id();

    /* Validation */
    V__ASSERT_TRUE(id1 != id2);
}

V__TEST(test_is_same_false, NULL, NULL)
{
    /* Preconditions */
    DummyObjectModel object1;
    DummyObjectModel object2;

    /* UUT */
    auto is_same = object1.is_same(object2);

    /* Validation */
    V__ASSERT_FALSE(is_same);
}

V__TEST(test_is_same_true, NULL, NULL)
{
    /* Preconditions */
    DummyObjectModel object1;

    /* UUT */
    auto is_same = object1.is_same(object1);

    /* Validation */
    V__ASSERT_TRUE(is_same);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_init,
        &test_init_2,
        &test_is_same_false,
        &test_is_same_true,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
