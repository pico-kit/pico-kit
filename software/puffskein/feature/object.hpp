/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        object_model.hpp
 * @date        2024-07-23
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-23  (NRA) Initial implementation
 *
 * @brief       Object model header.
 *
 * This header file contains Object model API, which is part of Puffskein.
 */
/**
 * @ingroup     puffskein
 * @defgroup    puffskein_object_model Pufffskein object model
 *
 * @brief       Pufffskein object model API.
 *
 * @{
 */

#ifndef PUFFSKEIN_FEATURE_OBJECT_H_
#define PUFFSKEIN_FEATURE_OBJECT_H_

// Extends

// Depends
#include "puffskein/domain/object_model.hpp"

namespace puffskein
{

class Object;
class Integer;
class Type;

class Object : private ObjectModel
{
   public:
    Object();
    ~Object() override;

   protected:
   private:
};

}  // namespace puffskein

#endif /* PUFFSKEIN_FEATURE_OBJECT_H_ */
