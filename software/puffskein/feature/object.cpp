// Implements
#include "puffskein/feature/object.hpp"
#include "puffskein/domain/object_model.hpp"

// Depends

using namespace puffskein;

Integer &
id(const Object & object);
Type &
type(const Object & object);

puffskein::Object::Object() : puffskein::ObjectModel(1) {}

puffskein::Object::~Object() {}
