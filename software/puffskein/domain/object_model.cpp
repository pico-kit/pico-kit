// Implements
#include "puffskein/domain/object_model.hpp"

// Depends
#include <atomic>

using namespace puffskein;

ObjectModel::ObjectModel(std::uint32_t type) : object_type(type) {}
