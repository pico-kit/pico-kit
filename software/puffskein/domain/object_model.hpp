/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        object_model.hpp
 * @date        2024-07-23
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-23  (NRA) Initial implementation
 *
 * @brief       Object model header.
 *
 * This header file contains Object model API, which is part of Puffskein.
 */
/**
 * @ingroup     puffskein
 * @defgroup    puffskein_object_model Pufffskein object model
 *
 * @brief       Pufffskein object model API.
 *
 * @{
 */

#ifndef PUFFSKEIN_OBJECT_MODEL_H_
#define PUFFSKEIN_OBJECT_MODEL_H_

// Extends

// Depends
#include <cstdint>

namespace puffskein
{

enum object_type {
    OBJECT_TYPE_BOOLEAN,
    OBJECT_TYPE_INTEGER,
};

class ObjectModel
{
   public:
    virtual ~ObjectModel() {};

   protected:
    std::uint32_t type() const
    {
        return object_type;
    }
    std::uintptr_t id() const
    {
        return reinterpret_cast<std::uintptr_t>(this);
    }
    bool is_same(const ObjectModel & rhs) const
    {
        return id() == rhs.id();
    }
    explicit ObjectModel(std::uint32_t type);

   private:
    std::uint32_t object_type;
};

}  // namespace puffskein

#endif /* PUFFSKEIN_OBJECT_MODEL_H_ */
