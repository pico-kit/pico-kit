/*
 * test.c
 *
 *  Created on: Feb 21, 2021
 *      Author: nenad
 */

/* Implements */
#include "verifinex/domain/test.h"

/* Depends */
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

struct context {
    uint32_t current_test_index;
    uint32_t total_asserts;
    bool     has_failed;
};

static struct context l__context;

static void
flag_failure(void)
{
    l__context.has_failed = true;
}

static void
report_expected_failure(
        const char * actual_expr,
        const char * actual_val,
        const char * expected_expr,
        const char * expected_val,
        uint32_t     line)
{
    const struct v__test_descriptor * current_test;

    current_test = G__ENABLED_TESTS[l__context.current_test_index];
    (void) fprintf(
            stderr,
            "Assert failed in %s at line:%u\n  ACTUAL: %s = %s\n  EXPECTED: %s "
            "= %s\n",
            current_test->name,
            line,
            actual_expr,
            actual_val,
            expected_expr,
            expected_val);
    (void) fflush(stderr);
}

static void
report_unexpected_failure(
        const char * actual_expr,
        const char * actual_val,
        const char * unexpected_expr,
        const char * unexpected_val,
        uint32_t     line)
{
    const struct v__test_descriptor * current_test;

    current_test = G__ENABLED_TESTS[l__context.current_test_index];
    (void) fprintf(
            stderr,
            "Assert failed in %s at line:%u\n  ACTUAL: %s = %s\n  UNEXPECTED: "
            "%s = %s\n",
            current_test->name,
            line,
            actual_expr,
            actual_val,
            unexpected_expr,
            unexpected_val);
    (void) fflush(stderr);
}

bool
v__test_equal_bool(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;

    if (actual != expected) {
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual ? "true" : "false",
                expected_expr,
                expected ? "true" : "false",
                line);
        return false;
    }
    return true;
}

bool
v__test_equal_int32(
        int32_t      actual,
        const char * actual_expr,
        int32_t      expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;
    bool is_equal = (actual == expected);

    if (!is_equal) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status = snprintf(actual_str, sizeof(actual_str), "%d", actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(expected_str, sizeof(expected_str), "%d", expected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual_str,
                expected_expr,
                expected_str,
                line);
    }
    return is_equal;
}

bool
v__test_equal_uint32(
        uint32_t     actual,
        const char * actual_expr,
        uint32_t     expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;
    bool is_equal = (actual == expected);

    if (!is_equal) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status = snprintf(actual_str, sizeof(actual_str), "%u", actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(expected_str, sizeof(expected_str), "%u", expected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual_str,
                expected_expr,
                expected_str,
                line);
    }
    return is_equal;
}

bool
v__test_equal_uintptr(
        uintptr_t    actual,
        const char * actual_expr,
        uintptr_t    expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;
    bool is_equal = (actual == expected);

    if (!is_equal) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status = snprintf(actual_str, sizeof(actual_str), "%lu", actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(expected_str, sizeof(expected_str), "%lu", expected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual_str,
                expected_expr,
                expected_str,
                line);
    }
    return is_equal;
}

bool
v__test_equal_size(
        size_t       actual,
        const char * actual_expr,
        size_t       expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;
    bool is_equal = (actual == expected);

    if (!is_equal) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status =
                snprintf(actual_str, sizeof(actual_str), "%" PRIuPTR, actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(
                expected_str,
                sizeof(expected_str),
                "%" PRIuPTR,
                expected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual_str,
                expected_expr,
                expected_str,
                line);
    }
    return is_equal;
}

bool
v__test_equal_ptr(
        const void * actual,
        const char * actual_expr,
        const void * expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;

    if (actual != expected) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status = snprintf(actual_str, sizeof(actual_str), "%p", actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(expected_str, sizeof(expected_str), "%p", expected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual == NULL ? "NULL" : actual_str,
                expected_expr,
                expected == NULL ? "NULL" : expected_str,
                line);
        return false;
    } else {
        return true;
    }
}

/*
 * TODO: Make string comparation more reliable.
 */
bool
v__test_equal_string(
        const char * actual,
        const char * actual_expr,
        const char * expected,
        const char * expected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;

    bool is_equal = true;
    if ((actual == NULL) || (expected == NULL)) {
        is_equal = false;
    } else if (strncmp(actual, expected, 4096) != 0) {
        is_equal = false;
    }
    if (!is_equal) {
        flag_failure();
        report_expected_failure(
                actual_expr,
                actual == NULL ? "NULL" : actual,
                expected_expr,
                expected == NULL ? "NULL" : expected,
                line);
        return false;
    } else {
        return true;
    }
}

bool
v__test_nequal_ptr(
        const void * actual,
        const char * actual_expr,
        const void * unexpected,
        const char * unexpected_expr,
        uint32_t     line)
{
    l__context.total_asserts++;

    if (actual == unexpected) {
        char actual_str[19];
        char expected_str[19];

        memset(actual_str, 0, sizeof(actual_str));
        memset(expected_str, 0, sizeof(expected_str));
        int status = snprintf(actual_str, sizeof(actual_str), "%p", actual);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        status = snprintf(expected_str, sizeof(expected_str), "%p", unexpected);
        if (status < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return false;
        }
        flag_failure();
        report_unexpected_failure(
                actual_expr,
                actual == NULL ? "NULL" : actual_str,
                unexpected_expr,
                unexpected == NULL ? "NULL" : expected_str,
                line);
        return false;
    } else {
        return true;
    }
}

static int
unit_test_execute(void)
{
    int printed;

    printed =
            fprintf(stdout,
                    "Executing %lu tests...\n",
                    (unsigned long) G__TOTAL_TESTS);
    if (printed < 0) {
        (void) fprintf(stderr, "Failed to print to stdout.\n");
        return 1;
    }
    for (size_t i = 0u; i < G__TOTAL_TESTS; i++) {
        const struct v__test_descriptor * current_test;

        current_test = G__ENABLED_TESTS[l__context.current_test_index];
        printed =
                fprintf(stdout,
                        "Running %4lu: %s...\n",
                        (unsigned long) i,
                        current_test->name);
        if (printed < 0) {
            (void) fprintf(stderr, "Failed to print to stdout.\n");
            return 1;
        }
        (void) fflush(stdout);
        if (current_test->setup != NULL) {
            current_test->setup();
        }
        current_test->function();
        if (current_test->teardown != NULL) {
            current_test->teardown();
        }
        if (l__context.has_failed) {
            return 2;
        }
        l__context.current_test_index++;
    }
    (void) fprintf(stdout, "Total asserts: %u\n", l__context.total_asserts);
    (void) fprintf(stdout, "Test(s) OK\n");
    (void) fflush(stdout);
    return 0;
}

int
main(void)
{
    return unit_test_execute();
}
