/*
 * unit_test.h
 *
 *  Created on: Nov 17, 2021
 *      Author: Nenad.Radulovic
 */

#ifndef VERIFINEX_DOMAIN_UNIT_TEST_H_
#define VERIFINEX_DOMAIN_UNIT_TEST_H_

int
v__unit_test_execute(void);

#endif /* VERIFINEX_DOMAIN_UNIT_TEST_H_ */
