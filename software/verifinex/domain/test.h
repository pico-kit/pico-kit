/*
 * nk_test.hpp
 *
 *  Created on: Feb 21, 2021
 *      Author: nenad
 */

#ifndef VERIFINEX_DOMAIN_TEST_H_
#define VERIFINEX_DOMAIN_TEST_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void(v__test_function)(void);
typedef void(v__test_setup)(void);
typedef void(v__test_teardown)(void);

struct v__test_descriptor {
    const char *       file;
    const char *       name;
    v__test_function * function;
    v__test_setup *    setup;
    v__test_teardown * teardown;
};

typedef struct v__test_descriptor v__test_descriptor;

extern const v__test_descriptor * const G__ENABLED_TESTS[];
extern const size_t                     G__TOTAL_TESTS;

#define V__TEST(test_name, setup_fn, teardown_fn)                              \
    static void                     test_function_##test_name(void);           \
    static const v__test_descriptor test_name = {                              \
            __FILE__,                                                          \
            #test_name,                                                        \
            test_function_##test_name,                                         \
            setup_fn,                                                          \
            teardown_fn,                                                       \
    };                                                                         \
    static void test_function_##test_name(void)

bool
v__test_equal_bool(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_int64(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
bool
v__test_equal_int32(
        int32_t      actual,
        const char * actual_expr,
        int32_t      expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_int16(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_int8(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_uint64(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
bool
v__test_equal_uint32(
        uint32_t     actual,
        const char * actual_expr,
        uint32_t     expected,
        const char * expected_expr,
        uint32_t     line);
bool
v__test_equal_uintptr(
        uintptr_t    actual,
        const char * actual_expr,
        uintptr_t    expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_uint16(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_uint8(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
bool
v__test_equal_size(
        size_t       actual,
        const char * actual_expr,
        size_t       expected,
        const char * expected_expr,
        uint32_t     line);

bool
v__test_equal_ptr(
        const void * actual,
        const char * actual_expr,
        const void * expected,
        const char * expected_expr,
        uint32_t     line);

bool
v__test_equal_string(
        const char * actual,
        const char * actual_expr,
        const char * expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_equal_mem(
        const void * actual,
        size_t       actual_size,
        const char * actual_expr,
        const void * expected,
        size_t       expected_size,
        const char * expected_expr,
        uint32_t     line);

void
v__test_nequal_bool(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_int64(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_int32(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_int16(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_int8(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_uint64(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_uint32(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_uint16(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_uint8(
        bool         actual,
        const char * actual_expr,
        bool         expected,
        const char * expected_expr,
        uint32_t     line);
bool
v__test_nequal_ptr(
        const void * actual,
        const char * actual_expr,
        const void * unexpected,
        const char * unexpected_expr,
        uint32_t     line);

void
v__test_nequal_string(
        const char * actual,
        const char * actual_expr,
        const char * expected,
        const char * expected_expr,
        uint32_t     line);
void
v__test_nequal_mem(
        const void * actual,
        size_t       actual_size,
        const char * actual_expr,
        const void * expected,
        size_t       expected_size,
        const char * expected_expr,
        uint32_t     line);

#define V__ASSERT_EQ_BOOL(actual, expected)                                    \
    do {                                                                       \
        const bool l_actual   = (actual);                                      \
        const bool l_expected = (expected);                                    \
        if (v__test_equal_bool(                                                \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_TRUE(actual) V__ASSERT_EQ_BOOL(actual, true)

#define V__ASSERT_FALSE(actual) V__ASSERT_EQ_BOOL(actual, false)

#define V__ASSERT_EQ_UINTPTR(actual, expected)                                 \
    do {                                                                       \
        const uintptr_t l_actual   = (actual);                                 \
        const uintptr_t l_expected = (expected);                               \
        if (v__test_equal_uintptr(                                             \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_EQ_UINT32(actual, expected)                                  \
    do {                                                                       \
        const uint32_t l_actual   = (actual);                                  \
        const uint32_t l_expected = (expected);                                \
        if (v__test_equal_uint32(                                              \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_EQ_INT32(actual, expected)                                   \
    do {                                                                       \
        const int32_t l_actual   = (actual);                                   \
        const int32_t l_expected = (expected);                                 \
        if (v__test_equal_int32(                                               \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_EQ_SIZE(actual, expected)                                    \
    do {                                                                       \
        const size_t l_actual   = (actual);                                    \
        const size_t l_expected = (expected);                                  \
        if (v__test_equal_size(                                                \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_EQ_PTR(actual, expected)                                     \
    do {                                                                       \
        const void * const l_actual   = (actual);                              \
        const void * const l_expected = (expected);                            \
        if (v__test_equal_ptr(                                                 \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_NEQ_PTR(actual, unexpected)                                  \
    do {                                                                       \
        const void * const l_actual     = (actual);                            \
        const void * const l_unexpected = (unexpected);                        \
        if (v__test_nequal_ptr(                                                \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_unexpected,                                              \
                    #unexpected,                                               \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_NOT_NULL(actual)                                             \
    do {                                                                       \
        const void * const l_actual = (actual);                                \
        if (v__test_nequal_ptr(l_actual, #actual, NULL, "NULL", __LINE__)      \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#define V__ASSERT_EQ_STRING(actual, expected)                                  \
    do {                                                                       \
        const char * const l_actual   = (actual);                              \
        const char * const l_expected = (expected);                            \
        if (v__test_equal_string(                                              \
                    l_actual,                                                  \
                    #actual,                                                   \
                    l_expected,                                                \
                    #expected,                                                 \
                    __LINE__)                                                  \
            == false) {                                                        \
            return;                                                            \
        }                                                                      \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif /* VERIFINEX_DOMAIN_TEST_H_ */
