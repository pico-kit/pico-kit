/*
 * test_ordered_map.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* UUT header */
#include "foundation/ordered_map.h"

/* Depends */
#include <assert.h>
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

V__TEST(test_indefinite_initializer, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__INDEFINITE_ORDERED_MAP_T(struct my_map_entry);
    struct my_map_entry c_array[10] = {
            {1, 'a'},
            {2, 'b'},
            {3, 'c'},
            {4, 'd'},
            {5, 'e'},
            {6, 'f'},
            {7, 'g'},
            {8, 'h'},
            {9, 'i'},
            {10, 'j'}};

    /* UUT */
    struct my_map_type my_map = F__INDEFINITE_ORDERED_MAP_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_TRUE(my_map.array.size == 0);
    V__ASSERT_TRUE(my_map.capacity == 10);
    V__ASSERT_NOT_NULL(my_map.array.storage);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].key, 1);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].element, 'a');
    V__ASSERT_EQ_INT32(my_map.array.storage[1].key, 2);
    V__ASSERT_EQ_INT32(my_map.array.storage[1].element, 'b');
    V__ASSERT_EQ_INT32(my_map.array.storage[2].key, 3);
    V__ASSERT_EQ_INT32(my_map.array.storage[2].element, 'c');
    V__ASSERT_EQ_INT32(my_map.array.storage[3].key, 4);
    V__ASSERT_EQ_INT32(my_map.array.storage[3].element, 'd');
    V__ASSERT_EQ_INT32(my_map.array.storage[4].key, 5);
    V__ASSERT_EQ_INT32(my_map.array.storage[4].element, 'e');
    V__ASSERT_EQ_INT32(my_map.array.storage[5].key, 6);
    V__ASSERT_EQ_INT32(my_map.array.storage[5].element, 'f');
    V__ASSERT_EQ_INT32(my_map.array.storage[6].key, 7);
    V__ASSERT_EQ_INT32(my_map.array.storage[6].element, 'g');
    V__ASSERT_EQ_INT32(my_map.array.storage[7].key, 8);
    V__ASSERT_EQ_INT32(my_map.array.storage[7].element, 'h');
    V__ASSERT_EQ_INT32(my_map.array.storage[8].key, 9);
    V__ASSERT_EQ_INT32(my_map.array.storage[8].element, 'i');
    V__ASSERT_EQ_INT32(my_map.array.storage[9].key, 10);
    V__ASSERT_EQ_INT32(my_map.array.storage[9].element, 'j');
}

V__TEST(test_indefinite_from_initializer, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry       F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_entry_array F__INDEFINITE_ARRAY_T(struct my_map_entry);
    struct my_map_entry       c_array[10] = {
            {1, 'a'},
            {2, 'b'},
            {3, 'c'},
            {4, 'd'},
            {5, 'e'},
            {6, 'f'},
            {7, 'g'},
            {8, 'h'},
            {9, 'i'},
            {10, 'j'}};
    struct my_map_type F__INDEFINITE_ORDERED_MAP_FROM_T(
            struct my_map_entry_array);

    /* UUT */
    struct my_map_type my_map = F__INDEFINITE_ORDERED_MAP_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.array.size, 0);
    V__ASSERT_EQ_SIZE(my_map.capacity, 10);
    V__ASSERT_NOT_NULL(my_map.array.storage);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].key, 1);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].element, 'a');
    V__ASSERT_EQ_INT32(my_map.array.storage[1].key, 2);
    V__ASSERT_EQ_INT32(my_map.array.storage[1].element, 'b');
    V__ASSERT_EQ_INT32(my_map.array.storage[2].key, 3);
    V__ASSERT_EQ_INT32(my_map.array.storage[2].element, 'c');
    V__ASSERT_EQ_INT32(my_map.array.storage[3].key, 4);
    V__ASSERT_EQ_INT32(my_map.array.storage[3].element, 'd');
    V__ASSERT_EQ_INT32(my_map.array.storage[4].key, 5);
    V__ASSERT_EQ_INT32(my_map.array.storage[4].element, 'e');
    V__ASSERT_EQ_INT32(my_map.array.storage[5].key, 6);
    V__ASSERT_EQ_INT32(my_map.array.storage[5].element, 'f');
    V__ASSERT_EQ_INT32(my_map.array.storage[6].key, 7);
    V__ASSERT_EQ_INT32(my_map.array.storage[6].element, 'g');
    V__ASSERT_EQ_INT32(my_map.array.storage[7].key, 8);
    V__ASSERT_EQ_INT32(my_map.array.storage[7].element, 'h');
    V__ASSERT_EQ_INT32(my_map.array.storage[8].key, 9);
    V__ASSERT_EQ_INT32(my_map.array.storage[8].element, 'i');
    V__ASSERT_EQ_INT32(my_map.array.storage[9].key, 10);
    V__ASSERT_EQ_INT32(my_map.array.storage[9].element, 'j');
}

V__TEST(test_indefinite_init, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry       F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_entry_array F__INDEFINITE_ARRAY_T(struct my_map_entry);
    struct my_map_entry       c_array[10] = {
            {1, 'a'},
            {2, 'b'},
            {3, 'c'},
            {4, 'd'},
            {5, 'e'},
            {6, 'f'},
            {7, 'g'},
            {8, 'h'},
            {9, 'i'},
            {10, 'j'}};
    struct my_map_type F__INDEFINITE_ORDERED_MAP_FROM_T(
            struct my_map_entry_array);
    struct my_map_type my_map;

    /* UUT */
    F__INDEFINITE_ORDERED_MAP_INIT(&my_map, c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.array.size, 0);
    V__ASSERT_EQ_SIZE(my_map.capacity, 10);
    V__ASSERT_NOT_NULL(my_map.array.storage);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].key, 1);
    V__ASSERT_EQ_INT32(my_map.array.storage[0].element, 'a');
    V__ASSERT_EQ_INT32(my_map.array.storage[1].key, 2);
    V__ASSERT_EQ_INT32(my_map.array.storage[1].element, 'b');
    V__ASSERT_EQ_INT32(my_map.array.storage[2].key, 3);
    V__ASSERT_EQ_INT32(my_map.array.storage[2].element, 'c');
    V__ASSERT_EQ_INT32(my_map.array.storage[3].key, 4);
    V__ASSERT_EQ_INT32(my_map.array.storage[3].element, 'd');
    V__ASSERT_EQ_INT32(my_map.array.storage[4].key, 5);
    V__ASSERT_EQ_INT32(my_map.array.storage[4].element, 'e');
    V__ASSERT_EQ_INT32(my_map.array.storage[5].key, 6);
    V__ASSERT_EQ_INT32(my_map.array.storage[5].element, 'f');
    V__ASSERT_EQ_INT32(my_map.array.storage[6].key, 7);
    V__ASSERT_EQ_INT32(my_map.array.storage[6].element, 'g');
    V__ASSERT_EQ_INT32(my_map.array.storage[7].key, 8);
    V__ASSERT_EQ_INT32(my_map.array.storage[7].element, 'h');
    V__ASSERT_EQ_INT32(my_map.array.storage[8].key, 9);
    V__ASSERT_EQ_INT32(my_map.array.storage[8].element, 'i');
    V__ASSERT_EQ_INT32(my_map.array.storage[9].key, 10);
    V__ASSERT_EQ_INT32(my_map.array.storage[9].element, 'j');
}

V__TEST(test_definite_initializer, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10);

    /* UUT */
    struct my_map_type my_map = F__DEFINITE_ORDERED_MAP_INITIALIZER(&my_map);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 0);
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[3].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[3].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[4].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[4].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[5].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[5].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[6].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[6].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[7].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[7].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[8].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[8].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[9].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[9].element, 0);
}

V__TEST(test_definite_init, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;

    /* UUT */
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* Validation */
    V__ASSERT_TRUE(my_map.base.array.size == 0);
    V__ASSERT_TRUE(my_map.base.capacity == 10);
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[3].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[3].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[4].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[4].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[5].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[5].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[6].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[6].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[7].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[7].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[8].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[8].element, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[9].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[9].element, 0);
}

V__TEST(test_each_empty, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    char                  index = 0;
    struct my_map_entry * current;
    for (F__ORDERED_MAP_EACH(&my_map.base, current)) {
        current->element = index++;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 0);
}

V__TEST(test_each_empty_index, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    size_t                index;
    struct my_map_entry * current;
    /* We must ensure that the element of type `char` can hold index values */
    assert(my_map.base.capacity < INT8_MAX);
    for (F__ORDERED_MAP_EACH_INDEX(&my_map.base, current, index)) {
        current->element = (char) index;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 0);
}

V__TEST(test_insert_at_0_a_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry = {0, 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_0_a_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry = {0, 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 1);
}

V__TEST(test_insert_at_0_a_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry = {0, 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_0_a_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry = {0, 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_TRUE(my_map.base.array.storage[0].element == 'a');
}

V__TEST(test_insert_at_0_a_search_0_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry = {.key = 0, .element = 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry = NULL;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_a_search_0_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry = {.key = 0, .element = 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, entry.key);
    V__ASSERT_EQ_INT32(searched_entry->element, entry.element);
}

V__TEST(test_insert_at_0_a_search_1_is_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry = {.key = 0, .element = 'a'};
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_PTR(searched_entry, NULL);
}

V__TEST(test_insert_at_0_1_a_b_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_0_1_a_b_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 2);
}

V__TEST(test_insert_at_0_1_a_b_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_0_1_a_b_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].element, 'a');
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].key, 1);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].element, 'b');
}

V__TEST(test_insert_at_0_1_a_b_search_0_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry = NULL;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_1_a_b_search_0_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 0);
    V__ASSERT_EQ_INT32(searched_entry->element, 'a');
}

V__TEST(test_insert_at_0_1_a_b_search_1_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry = NULL;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_1_a_b_search_1_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 1);
    V__ASSERT_EQ_INT32(searched_entry->element, 'b');
}

V__TEST(test_insert_at_0_1_a_b_search_2_is_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 2, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_PTR(searched_entry, NULL);
}

V__TEST(test_insert_at_1_0_a_b_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_1_0_a_b_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 2);
}

V__TEST(test_insert_at_1_0_a_b_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_1_0_a_b_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_TRUE(my_map.base.array.storage[0].element == 'a');
    V__ASSERT_TRUE(my_map.base.array.storage[1].element == 'b');
}

V__TEST(test_insert_at_1_0_a_b_search_0_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry = NULL;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_1_0_a_b_search_0_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 0);
    V__ASSERT_EQ_INT32(searched_entry->element, 'a');
}

V__TEST(test_insert_at_1_0_a_b_search_1_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry = NULL;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_1_0_a_b_search_1_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 1);
    V__ASSERT_EQ_INT32(searched_entry->element, 'b');
}

V__TEST(test_insert_at_1_0_a_b_search_2_is_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 2, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_PTR(searched_entry, NULL);
}

V__TEST(test_insert_at_0_1_2_a_b_c_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_0_1_2_a_b_c_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 3);
}

V__TEST(test_insert_at_0_1_2_a_b_c_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_0_1_2_a_b_c_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].key, 0);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[0].element, 'a');
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].key, 1);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[1].element, 'b');
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].key, 2);
    V__ASSERT_EQ_INT32(my_map.base.array.storage[2].element, 'c');
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_0_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_0_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 0, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 0);
    V__ASSERT_EQ_INT32(searched_entry->element, 'a');
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_1_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_1_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 1, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 1);
    V__ASSERT_EQ_INT32(searched_entry->element, 'b');
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_2_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 2, &searched_entry);

    /* Validation */
    V__ASSERT_NOT_NULL(searched_entry);
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_2_value, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 2, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_INT32(searched_entry->key, 2);
    V__ASSERT_EQ_INT32(searched_entry->element, 'c');
}

V__TEST(test_insert_at_0_1_2_a_b_c_search_3_is_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    struct my_map_entry entry;
    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* UUT */
    struct my_map_entry * searched_entry;
    F__ORDERED_MAP_SEARCH(&my_map.base, 3, &searched_entry);

    /* Validation */
    V__ASSERT_EQ_PTR(searched_entry, NULL);
}

V__TEST(test_insert_at_1_2_0_b_c_a_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_1_2_0_b_c_a_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 3);
}

V__TEST(test_insert_at_1_2_0_b_c_a_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_1_2_0_b_c_a_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_TRUE(my_map.base.array.storage[0].element == 'a');
    V__ASSERT_TRUE(my_map.base.array.storage[1].element == 'b');
    V__ASSERT_TRUE(my_map.base.array.storage[2].element == 'c');
}

V__TEST(test_insert_at_2_0_1_c_a_b_capacity, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.capacity, 10);
}

V__TEST(test_insert_at_2_0_1_c_a_b_size, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_map.base.array.size, 3);
}

V__TEST(test_insert_at_2_0_1_c_a_b_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_NOT_NULL(my_map.base.array.storage);
}

V__TEST(test_insert_at_2_0_1_c_a_b_storage, NULL, NULL)
{
    /* Preconditions */
    struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
    struct my_map_type  F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10)
            my_map;
    F__DEFINITE_ORDERED_MAP_INIT(&my_map);

    /* UUT */
    struct my_map_entry entry;
    entry.key     = 2;
    entry.element = 'c';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 0;
    entry.element = 'a';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    entry.key     = 1;
    entry.element = 'b';
    F__ORDERED_MAP_INSERT(&my_map.base, &entry);

    /* Validation */
    V__ASSERT_TRUE(my_map.base.array.storage[0].element == 'a');
    V__ASSERT_TRUE(my_map.base.array.storage[1].element == 'b');
    V__ASSERT_TRUE(my_map.base.array.storage[2].element == 'c');
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_indefinite_initializer,
        &test_indefinite_from_initializer,
        &test_indefinite_init,
        &test_definite_initializer,
        &test_definite_init,
        &test_each_empty,
        &test_each_empty_index,
        &test_insert_at_0_a_capacity,
        &test_insert_at_0_a_size,
        &test_insert_at_0_a_storage_not_null,
        &test_insert_at_0_a_storage,
        &test_insert_at_0_a_search_0_not_null,
        &test_insert_at_0_a_search_0_value,
        &test_insert_at_0_a_search_1_is_null,
        &test_insert_at_0_1_a_b_capacity,
        &test_insert_at_0_1_a_b_size,
        &test_insert_at_0_1_a_b_storage_not_null,
        &test_insert_at_0_1_a_b_storage,
        &test_insert_at_0_1_a_b_search_0_not_null,
        &test_insert_at_0_1_a_b_search_0_value,
        &test_insert_at_0_1_a_b_search_1_not_null,
        &test_insert_at_0_1_a_b_search_1_value,
        &test_insert_at_0_1_a_b_search_2_is_null,
        &test_insert_at_1_0_a_b_capacity,
        &test_insert_at_1_0_a_b_size,
        &test_insert_at_1_0_a_b_storage_not_null,
        &test_insert_at_1_0_a_b_storage,
        &test_insert_at_1_0_a_b_search_0_not_null,
        &test_insert_at_1_0_a_b_search_0_value,
        &test_insert_at_1_0_a_b_search_1_not_null,
        &test_insert_at_1_0_a_b_search_1_value,
        &test_insert_at_1_0_a_b_search_2_is_null,
        &test_insert_at_0_1_2_a_b_c_capacity,
        &test_insert_at_0_1_2_a_b_c_size,
        &test_insert_at_0_1_2_a_b_c_storage_not_null,
        &test_insert_at_0_1_2_a_b_c_storage,
        &test_insert_at_0_1_2_a_b_c_search_0_not_null,
        &test_insert_at_0_1_2_a_b_c_search_0_value,
        &test_insert_at_0_1_2_a_b_c_search_1_not_null,
        &test_insert_at_0_1_2_a_b_c_search_1_value,
        &test_insert_at_0_1_2_a_b_c_search_2_not_null,
        &test_insert_at_0_1_2_a_b_c_search_2_value,
        &test_insert_at_0_1_2_a_b_c_search_3_is_null,
        &test_insert_at_1_2_0_b_c_a_capacity,
        &test_insert_at_1_2_0_b_c_a_size,
        &test_insert_at_1_2_0_b_c_a_storage_not_null,
        &test_insert_at_1_2_0_b_c_a_storage,
        &test_insert_at_2_0_1_c_a_b_capacity,
        &test_insert_at_2_0_1_c_a_b_size,
        &test_insert_at_2_0_1_c_a_b_storage_not_null,
        &test_insert_at_2_0_1_c_a_b_storage,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
