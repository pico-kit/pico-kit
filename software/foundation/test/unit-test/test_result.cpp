/*
 * test_list_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "foundation/result.hpp"

/* Depends */
#include <stdint.h>
#include <utility>
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

FAKE_VOID_FUNC(f__validator_failure_handler, const char *, uint32_t)

#define FFF_FAKES_LIST(FAKE) FAKE(f__validator_failure_handler)

static void
fff_setup(void)
{
    /* Register resets */
    FFF_FAKES_LIST(RESET_FAKE);

    /* Reset common FFF internal structures */
    FFF_RESET_HISTORY();
}

V__TEST(void_construction_implicit_not_ok, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<void> result;

    /* Validation */
    V__ASSERT_FALSE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(void_construction_implicit_error, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<void> result;

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::UNSPECIFIED);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(void_construction_explicit_not_ok, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<void> result {pk::Error::NO_MEM};

    /* Validation */
    V__ASSERT_FALSE(result.is_ok());
    V__ASSERT_TRUE(result.error() == pk::Error::NO_MEM);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(void_construction_explicit_ok, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<void> result;

    /* UUT */
    result.ok();

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(void_construction_explicit_error, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<void> result {pk::Error::NO_MEM};

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NO_MEM);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(void_construction_explicit_ok_return, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<void> result;

    /* UUT */
    auto new_result = result.ok();

    /* Validation */
    V__ASSERT_TRUE(new_result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};
    auto            is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_is_checked_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};
    auto            is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_FALSE(is_checked);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_is_value_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};
    auto            is_value = result.is_value();

    /* Validation */
    V__ASSERT_TRUE(is_value);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_is_ok_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};

    /* Validation */
    V__ASSERT_TRUE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_error_none, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_unchecked_value_fail, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {1};

    /* Validation */
    V__ASSERT_EQ_INT32(result.value(), 1);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
}

V__TEST(int_implicit_value_checked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a          = result.is_ok();
    auto                  is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
}

V__TEST(int_implicit_value_checked_is_checked_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a          = result.is_ok();
    auto                  is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_TRUE(is_checked);
}

V__TEST(int_implicit_value_checked_is_value_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a        = result.is_ok();
    auto                  is_value = result.is_value();

    /* Validation */
    V__ASSERT_TRUE(is_value);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_checked_is_ok_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_TRUE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_checked_error_none, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_value_checked_value_1, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {1};
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_EQ_INT32(result.value(), 1);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_undefined_unchecked_is_defined_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;
    auto            is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_FALSE(is_defined);
}

V__TEST(int_undefined_unchecked_is_checked_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;
    auto            is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_FALSE(is_checked);
}

V__TEST(int_undefined_unchecked_is_value_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;
    auto            is_value = result.is_value();

    /* Validation */
    V__ASSERT_FALSE(is_value);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_undefined_unchecked_is_ok_false_fail, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;

    /* Validation */
    V__ASSERT_FALSE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNDEFINED);
}

V__TEST(int_undefined_unchecked_error_unspecified, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::UNSPECIFIED);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_undefined_unchecked_value_fail, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    [[maybe_unused]] int a = result.value();

    /* Validation */
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 3);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNDEFINED);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[1],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[2],
            pk::result::FAIL_RESULT_IS_NOT_VALUE);
}

V__TEST(int_undefined_checked_error_unspecified_fail, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result;

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::UNSPECIFIED);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNDEFINED);
}

V__TEST(int_undefined_checked_value_fail, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    [[maybe_unused]] int a = result.value();

    /* Validation */
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 2);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNDEFINED);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[1],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
}

V__TEST(int_implicit_error_unchecked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {pk::Error::NO_MEM};
    auto            is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
}

V__TEST(int_implicit_error_unchecked_is_checked_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {pk::Error::NO_MEM};
    auto            is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_FALSE(is_checked);
}

V__TEST(int_implicit_error_unchecked_is_value_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {pk::Error::NO_MEM};
    auto            is_value = result.is_value();

    /* Validation */
    V__ASSERT_FALSE(is_value);
}

V__TEST(int_implicit_error_unchecked_is_ok_false, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {pk::Error::NO_MEM};

    /* Validation */
    V__ASSERT_FALSE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_error_unchecked_error, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int> result {pk::Error::NO_MEM};

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NO_MEM);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_implicit_error_unchecked_value_fail, fff_setup, NULL)
{
    /* Preconditions */

    /* UUT */
    pk::Result<int>       result {pk::Error::NO_MEM};
    [[maybe_unused]] auto a = result.value();

    /* Validation */
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 2);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[1],
            pk::result::FAIL_RESULT_IS_NOT_VALUE);
}

V__TEST(int_explicit_value_unchecked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    auto is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
}

V__TEST(int_explicit_value_unchecked_is_checked_false, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    auto is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_FALSE(is_checked);
}

V__TEST(int_explicit_value_unchecked_is_value_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    auto is_value = result.is_value();

    /* Validation */
    V__ASSERT_TRUE(is_value);
}

V__TEST(int_explicit_value_unchecked_is_ok_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);

    /* Validation */
    V__ASSERT_TRUE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_explicit_value_unchecked_error_none, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_explicit_value_unchecked_value_fail, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);

    /* Validation */
    V__ASSERT_EQ_INT32(result.value(), 1);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
}

V__TEST(int_explicit_value_checked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    auto is_defined = result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
}

V__TEST(int_explicit_value_checked_is_checked_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    auto is_checked = result.is_checked();

    /* Validation */
    V__ASSERT_TRUE(is_checked);
}

V__TEST(int_explicit_value_checked_is_value_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    auto is_value = result.is_value();

    /* Validation */
    V__ASSERT_TRUE(is_value);
}

V__TEST(int_explicit_value_checked_is_ok_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_TRUE(result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_explicit_value_checked_error_none, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_TRUE(result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_explicit_value_checked_value_1, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    result.ok(1);
    [[maybe_unused]] auto a = result.is_ok();

    /* Validation */
    V__ASSERT_EQ_INT32(result.value(), 1);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_is_defined_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);
    auto is_defined = new_result.is_defined();

    /* Validation */
    V__ASSERT_TRUE(is_defined);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_is_checked_false, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);
    auto is_checked = new_result.is_checked();

    /* Validation */
    V__ASSERT_FALSE(is_checked);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_is_value_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);
    auto is_value   = new_result.is_value();

    /* Validation */
    V__ASSERT_TRUE(is_value);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_is_ok_true, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);

    /* Validation */
    V__ASSERT_TRUE(new_result.is_ok());
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_error_none, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);

    /* Validation */
    V__ASSERT_TRUE(new_result.error() == pk::Error::NONE);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

V__TEST(int_ok_return_unchecked_value_fail, fff_setup, NULL)
{
    /* Preconditions */
    pk::Result<int> result;

    /* UUT */
    auto new_result = result.ok(1);

    /* Validation */
    V__ASSERT_EQ_INT32(new_result.value(), 1);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 1);
    V__ASSERT_EQ_UINT32(
            f__validator_failure_handler_fake.arg1_history[0],
            pk::result::FAIL_RESULT_IS_UNCHECKED);
}

V__TEST(composite_implicit_value_checked_move_semantics, fff_setup, NULL)
{
    bool     move_cstr_flag = false;
    bool     move_eq_flag   = false;
    uint32_t destruction    = 0u;
    class A
    {
       public:
        A() = delete;

        ~A()
        {
            (*destr)++;
        }

        A(bool * move_cstr_flag, bool * move_eq_flag, uint32_t * destr) :
            move_cstr_flag(move_cstr_flag), move_eq_flag(move_eq_flag),
            destr(destr)
        {
        }

        A(const A & other) :
            move_cstr_flag(other.move_cstr_flag),
            move_eq_flag(other.move_eq_flag), destr(other.destr)
        {
            *move_cstr_flag = false;
            *move_eq_flag   = false;
        }

        A & operator=(const A & other)
        {
            move_cstr_flag  = other.move_cstr_flag;
            move_eq_flag    = other.move_eq_flag;
            destr           = other.destr;
            *move_cstr_flag = false;
            *move_eq_flag   = false;
            return *this;
        }

        A(A && other) :
            move_cstr_flag(other.move_cstr_flag),
            move_eq_flag(other.move_eq_flag), destr(other.destr)
        {
            other.move_cstr_flag = nullptr;
            other.move_eq_flag   = nullptr;
            *move_cstr_flag      = true;
            *move_eq_flag        = false;
        }

        A & operator=(A && other)
        {
            move_cstr_flag       = other.move_cstr_flag;
            move_eq_flag         = other.move_eq_flag;
            destr                = other.destr;
            other.move_cstr_flag = nullptr;
            other.move_eq_flag   = nullptr;
            *move_cstr_flag      = false;
            *move_eq_flag        = true;
            return *this;
        }

        bool *     move_cstr_flag;
        bool *     move_eq_flag;
        uint32_t * destr;
    };

    {
        A                     a(&move_cstr_flag, &move_eq_flag, &destruction);
        pk::Result<A>         result {std::move(a)};
        [[maybe_unused]] auto is_ok = result.is_ok();

        V__ASSERT_TRUE(result.value().move_cstr_flag == &move_cstr_flag);
        V__ASSERT_TRUE(result.value().move_eq_flag == &move_eq_flag);
    }

    V__ASSERT_TRUE(move_cstr_flag);
    V__ASSERT_FALSE(move_eq_flag);
    V__ASSERT_EQ_UINT32(destruction, 2);
    V__ASSERT_EQ_UINT32(f__validator_failure_handler_fake.call_count, 0);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &void_construction_implicit_not_ok,
        &void_construction_implicit_error,
        &void_construction_explicit_not_ok,
        &void_construction_explicit_ok,
        &void_construction_explicit_error,
        &void_construction_explicit_ok_return,
        &int_implicit_value_unchecked_is_defined_true,
        &int_implicit_value_unchecked_is_checked_false,
        &int_implicit_value_unchecked_is_value_true,
        &int_implicit_value_unchecked_is_ok_true,
        &int_implicit_value_unchecked_error_none,
        &int_implicit_value_unchecked_value_fail,
        &int_implicit_value_checked_is_defined_true,
        &int_implicit_value_checked_is_checked_true,
        &int_implicit_value_checked_is_value_true,
        &int_implicit_value_checked_is_ok_true,
        &int_implicit_value_checked_error_none,
        &int_implicit_value_checked_value_1,
        &int_undefined_unchecked_is_defined_false,
        &int_undefined_unchecked_is_checked_false,
        &int_undefined_unchecked_is_value_false,
        &int_undefined_unchecked_is_ok_false_fail,
        &int_undefined_unchecked_error_unspecified,
        &int_undefined_unchecked_value_fail,
        &int_implicit_error_unchecked_is_defined_true,
        &int_implicit_error_unchecked_is_checked_false,
        &int_implicit_error_unchecked_is_value_false,
        &int_implicit_error_unchecked_is_ok_false,
        &int_implicit_error_unchecked_error,
        &int_implicit_error_unchecked_value_fail,
        &int_explicit_value_unchecked_is_defined_true,
        &int_explicit_value_unchecked_is_checked_false,
        &int_explicit_value_unchecked_is_value_true,
        &int_explicit_value_unchecked_is_ok_true,
        &int_explicit_value_unchecked_error_none,
        &int_explicit_value_unchecked_value_fail,
        &int_explicit_value_checked_is_defined_true,
        &int_explicit_value_checked_is_checked_true,
        &int_explicit_value_checked_is_value_true,
        &int_explicit_value_checked_is_ok_true,
        &int_explicit_value_checked_error_none,
        &int_explicit_value_checked_value_1,
        &int_ok_return_unchecked_is_defined_true,
        &int_ok_return_unchecked_is_checked_false,
        &int_ok_return_unchecked_is_value_true,
        &int_ok_return_unchecked_is_ok_true,
        &int_ok_return_unchecked_error_none,
        &int_ok_return_unchecked_value_fail,
        &composite_implicit_value_checked_move_semantics,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
