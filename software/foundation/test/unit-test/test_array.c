/*
 * test_list_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "foundation/array.h"

/* Depends */
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

V__TEST(test_indefinite_initializer, NULL, NULL)
{
    /* Preconditions */
    int                  c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_array_type F__INDEFINITE_ARRAY_T(int);

    /* UUT */
    struct my_array_type my_array = F__INDEFINITE_ARRAY_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_TRUE(my_array.size == 10);
    V__ASSERT_TRUE(my_array.storage != NULL);
    V__ASSERT_TRUE(my_array.storage[0] == 1);
    V__ASSERT_TRUE(my_array.storage[1] == 2);
    V__ASSERT_TRUE(my_array.storage[2] == 3);
    V__ASSERT_TRUE(my_array.storage[3] == 4);
    V__ASSERT_TRUE(my_array.storage[4] == 5);
    V__ASSERT_TRUE(my_array.storage[5] == 6);
    V__ASSERT_TRUE(my_array.storage[6] == 7);
    V__ASSERT_TRUE(my_array.storage[7] == 8);
    V__ASSERT_TRUE(my_array.storage[8] == 9);
    V__ASSERT_TRUE(my_array.storage[9] == 10);
}

V__TEST(test_indefinite_init, NULL, NULL)
{
    /* Preconditions */
    int                  c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_array_type F__INDEFINITE_ARRAY_T(int);
    struct my_array_type my_array;

    /* UUT */
    F__INDEFINITE_ARRAY_INIT(&my_array, c_array);

    /* Validation */
    V__ASSERT_TRUE(my_array.size == 10);
    V__ASSERT_TRUE(my_array.storage != NULL);
    V__ASSERT_TRUE(my_array.storage[0] == 1);
    V__ASSERT_TRUE(my_array.storage[1] == 2);
    V__ASSERT_TRUE(my_array.storage[2] == 3);
    V__ASSERT_TRUE(my_array.storage[3] == 4);
    V__ASSERT_TRUE(my_array.storage[4] == 5);
    V__ASSERT_TRUE(my_array.storage[5] == 6);
    V__ASSERT_TRUE(my_array.storage[6] == 7);
    V__ASSERT_TRUE(my_array.storage[7] == 8);
    V__ASSERT_TRUE(my_array.storage[8] == 9);
    V__ASSERT_TRUE(my_array.storage[9] == 10);
}

V__TEST(test_definite_initializer, NULL, NULL)
{
    /* Preconditions */
    struct my_array_type F__DEFINITE_ARRAY_T(int, 10);

    /* UUT */
    struct my_array_type my_array = F__DEFINITE_ARRAY_INITIALIZER(&my_array);

    /* Validation */
    V__ASSERT_TRUE(my_array.base.size == 10);
    V__ASSERT_TRUE(my_array.base.storage != NULL);
    V__ASSERT_TRUE(my_array.base.storage[0] == 0);
    V__ASSERT_TRUE(my_array.base.storage[1] == 0);
    V__ASSERT_TRUE(my_array.base.storage[2] == 0);
    V__ASSERT_TRUE(my_array.base.storage[3] == 0);
    V__ASSERT_TRUE(my_array.base.storage[4] == 0);
    V__ASSERT_TRUE(my_array.base.storage[5] == 0);
    V__ASSERT_TRUE(my_array.base.storage[6] == 0);
    V__ASSERT_TRUE(my_array.base.storage[7] == 0);
    V__ASSERT_TRUE(my_array.base.storage[8] == 0);
    V__ASSERT_TRUE(my_array.base.storage[9] == 0);
}

V__TEST(test_definite_init, NULL, NULL)
{
    /* Preconditions */
    struct my_array_type F__DEFINITE_ARRAY_T(int, 10);
    struct my_array_type my_array;

    /* UUT */
    F__DEFINITE_ARRAY_INIT(&my_array);

    /* Validation */
    V__ASSERT_TRUE(my_array.base.size == 10);
    V__ASSERT_TRUE(my_array.base.storage != NULL);
    V__ASSERT_TRUE(my_array.base.storage[0] == 0);
    V__ASSERT_TRUE(my_array.base.storage[1] == 0);
    V__ASSERT_TRUE(my_array.base.storage[2] == 0);
    V__ASSERT_TRUE(my_array.base.storage[3] == 0);
    V__ASSERT_TRUE(my_array.base.storage[4] == 0);
    V__ASSERT_TRUE(my_array.base.storage[5] == 0);
    V__ASSERT_TRUE(my_array.base.storage[6] == 0);
    V__ASSERT_TRUE(my_array.base.storage[7] == 0);
    V__ASSERT_TRUE(my_array.base.storage[8] == 0);
    V__ASSERT_TRUE(my_array.base.storage[9] == 0);
}

V__TEST(test_each, NULL, NULL)
{
    /* Preconditions */
    struct my_array_type F__DEFINITE_ARRAY_T(int, 3) my_array;
    F__DEFINITE_ARRAY_INIT(&my_array);

    /* UUT */
    int   index = 0;
    int * current;
    for (F__ARRAY_EACH(&my_array.base, current)) {
        *current = 5 + index++;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 3);
    V__ASSERT_TRUE(my_array.elements[0] == 5);
    V__ASSERT_TRUE(my_array.elements[1] == 6);
    V__ASSERT_TRUE(my_array.elements[2] == 7);
}

V__TEST(test_each_index, NULL, NULL)
{
    /* Preconditions */
    struct my_array_type F__DEFINITE_ARRAY_T(int, 5) my_array;
    F__DEFINITE_ARRAY_INIT(&my_array);

    /* UUT */
    size_t index;
    int *  current;
    for (F__ARRAY_EACH_INDEX(&my_array.base, current, index)) {
        *current = (int) index;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 5);
    V__ASSERT_TRUE(my_array.elements[0] == 0);
    V__ASSERT_TRUE(my_array.elements[1] == 1);
    V__ASSERT_TRUE(my_array.elements[2] == 2);
    V__ASSERT_TRUE(my_array.elements[3] == 3);
    V__ASSERT_TRUE(my_array.elements[4] == 4);
}

V__TEST(test_copy, NULL, NULL)
{
    /* Preconditions */
    struct my_array_type F__DEFINITE_ARRAY_T(int, 5);
    struct my_array_type my_array_a;
    F__DEFINITE_ARRAY_INIT(&my_array_a);

    struct my_array_type my_array_b;
    F__DEFINITE_ARRAY_INIT(&my_array_b);

    size_t index;
    int *  current;
    for (F__ARRAY_EACH_INDEX(&my_array_a.base, current, index)) {
        *current = (int) index;
    }

    /* UUT */
    if (my_array_a.base.size == my_array_b.base.size) {
        F__ARRAY_COPY(&my_array_a.base, &my_array_b.base);
    }

    /* Validation */
    V__ASSERT_TRUE(my_array_b.elements[0] == 0);
    V__ASSERT_TRUE(my_array_b.elements[1] == 1);
    V__ASSERT_TRUE(my_array_b.elements[2] == 2);
    V__ASSERT_TRUE(my_array_b.elements[3] == 3);
    V__ASSERT_TRUE(my_array_b.elements[4] == 4);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &test_indefinite_initializer,
        &test_indefinite_init,
        &test_definite_initializer,
        &test_definite_init,
        &test_each,
        &test_each_index,
        &test_copy,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
