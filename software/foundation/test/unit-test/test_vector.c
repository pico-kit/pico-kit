/*
 * test_list_model.c
 *
 *  Created on: Oct 31, 2023
 *      Author: nenad
 */

/* V__TEST header */
#include "foundation/array.h"
#include "foundation/vector.h"

/* Depends */
#include <stdint.h>
#include "fff.h"
#include "verifinex/domain/test.h"

DEFINE_FFF_GLOBALS

V__TEST(indefinite_initializer_capacity_10, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_vector_type F__INDEFINITE_VECTOR_T(int);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.capacity, 10);
}

V__TEST(indefinite_initializer_size_0, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_vector_type F__INDEFINITE_VECTOR_T(int);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.array.size, 0);
}

V__TEST(indefinite_initializer_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_vector_type F__INDEFINITE_VECTOR_T(int);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_PTR(my_vector.array.storage, c_array);
}

V__TEST(indefinite_from_initializer_capacity_10, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_array_type  F__INDEFINITE_ARRAY_T(int);
    struct my_vector_type F__INDEFINITE_VECTOR_FROM_T(struct my_array_type);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.capacity, 10);
}

V__TEST(indefinite_from_initializer_size_0, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_array_type  F__INDEFINITE_ARRAY_T(int);
    struct my_vector_type F__INDEFINITE_VECTOR_FROM_T(struct my_array_type);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.array.size, 0);
}

V__TEST(indefinite_from_initializer_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    int                   c_array[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    struct my_array_type  F__INDEFINITE_ARRAY_T(int);
    struct my_vector_type F__INDEFINITE_VECTOR_FROM_T(struct my_array_type);

    /* UUT */
    struct my_vector_type my_vector = F__INDEFINITE_VECTOR_INITIALIZER(c_array);

    /* Validation */
    V__ASSERT_EQ_PTR(my_vector.array.storage, c_array);
}

V__TEST(definite_initializer_capacity_10, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);

    /* UUT */
    struct my_vector_type my_vector =
            F__DEFINITE_VECTOR_INITIALIZER(&my_vector);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 10);
}

V__TEST(definite_initializer_size_0, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);

    /* UUT */
    struct my_vector_type my_vector =
            F__DEFINITE_VECTOR_INITIALIZER(&my_vector);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 0);
}

V__TEST(definite_initializer_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);

    /* UUT */
    struct my_vector_type my_vector =
            F__DEFINITE_VECTOR_INITIALIZER(&my_vector);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(definite_init_capacity_10, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);
    struct my_vector_type my_vector;

    /* UUT */
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 10);
}

V__TEST(definite_init_size_0, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);
    struct my_vector_type my_vector;

    /* UUT */
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 0);
}

V__TEST(definite_init_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 10);
    struct my_vector_type my_vector;

    /* UUT */
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_each_empty, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int   index = 0;
    int * current;
    for (F__VECTOR_EACH(&my_vector.base, current)) {
        *current = 5 + index++;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 0);
}

V__TEST(test_each_empty_index, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 5) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    size_t index;
    int *  current;
    for (F__VECTOR_EACH_INDEX(&my_vector.base, current, index)) {
        *current = (int) index;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 0);
}

V__TEST(test_insert_at_0_1_capacity_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 3);
}

V__TEST(test_insert_at_0_1_size_1, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 1);
}

V__TEST(test_insert_at_0_1_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_insert_at_0_1_value, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
}

V__TEST(test_insert_at_0_2_capacity_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item_a = 4;
    F__VECTOR_INSERT(&my_vector.base, &item_a, 0);
    int item_b = 5;
    F__VECTOR_INSERT(&my_vector.base, &item_b, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 3);
}

V__TEST(test_insert_at_0_2_size_2, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item_a = 4;
    F__VECTOR_INSERT(&my_vector.base, &item_a, 0);
    int item_b = 5;
    F__VECTOR_INSERT(&my_vector.base, &item_b, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 2);
}

V__TEST(test_insert_at_0_2_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item_a = 4;
    F__VECTOR_INSERT(&my_vector.base, &item_a, 0);
    int item_b = 5;
    F__VECTOR_INSERT(&my_vector.base, &item_b, 0);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_insert_at_0_2_value_5_4, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item_a = 4;
    F__VECTOR_INSERT(&my_vector.base, &item_a, 0);
    int item_b = 5;
    F__VECTOR_INSERT(&my_vector.base, &item_b, 0);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 5);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 4);
}

V__TEST(test_insert_at_1_2_capacity_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 3);
}

V__TEST(test_insert_at_1_2_size_2, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 2);
}

V__TEST(test_insert_at_1_2_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_insert_at_1_2_value_4_5, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 5);
}

V__TEST(test_insert_at_0_3_capacity_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 3);
}

V__TEST(test_insert_at_0_3_size_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 3);
}

V__TEST(test_insert_at_0_3_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_insert_at_0_3_value_6_5_4, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 6);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 5);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[2], 4);
}

V__TEST(test_insert_at_1_3_capacity_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.capacity, 3);
}

V__TEST(test_insert_at_1_3_size_3, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 3);
}

V__TEST(test_insert_at_1_3_storage_not_null, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_NOT_NULL(my_vector.base.array.storage);
}

V__TEST(test_insert_at_1_3_value_4_6_5, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 6);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[2], 5);
}

V__TEST(test_insert_at_2_3_size, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 2);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.capacity == 3);
    V__ASSERT_TRUE(my_vector.base.array.size == 3);
}

V__TEST(test_insert_at_0_1_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.array.storage[0] == 4);
}

V__TEST(test_insert_at_0_2_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.array.storage[0] == 5);
    V__ASSERT_TRUE(my_vector.base.array.storage[1] == 4);
}

V__TEST(test_insert_at_1_2_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(uint32_t, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    uint32_t item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[0], 4);
}

V__TEST(test_insert_at_0_3_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(uint32_t, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);

    /* UUT */
    uint32_t item = 4u;
    F__VECTOR_INSERT(&my_vector.base, &item, 0u);
    item = 5u;
    F__VECTOR_INSERT(&my_vector.base, &item, 0u);
    item = 6u;
    F__VECTOR_INSERT(&my_vector.base, &item, 0u);

    /* Validation */
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[0], 6u);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[1], 5u);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[2], 4u);
}

V__TEST(test_insert_at_1_3_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(uint32_t, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    uint32_t item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[0], 4);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[1], 6);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[2], 5);
}

V__TEST(test_insert_at_2_3_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 2);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 5);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[2], 6);
}

V__TEST(test_insert_at_2_3_reversed_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 5);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[1], 6);
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[2], 4);
}

V__TEST(test_insert_at_0_remove_0_1_0_size, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 0);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.capacity == 3);
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 0);
}

V__TEST(test_insert_at_0_remove_0_2_0_size, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 0);
    F__VECTOR_REMOVE(&my_vector.base, 0);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.capacity == 3);
    V__ASSERT_EQ_SIZE(my_vector.base.array.size, 0);
}

V__TEST(test_insert_at_0_remove_0_2_1_size, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 0);

    /* Validation */
    V__ASSERT_TRUE(my_vector.base.capacity == 3);
    V__ASSERT_TRUE(my_vector.base.array.size == 1);
}

V__TEST(test_insert_at_0_remove_0_2_1_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 0);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
}

V__TEST(test_insert_at_1_remove_0_2_1_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 0);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 5);
}

V__TEST(test_insert_at_1_remove_1_2_1_check, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    int item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);

    /* UUT */
    F__VECTOR_REMOVE(&my_vector.base, 1);

    /* Validation */
    V__ASSERT_EQ_INT32(my_vector.base.array.storage[0], 4);
}

V__TEST(test_each, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(uint32_t, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    uint32_t item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 2);

    /* UUT */
    uint32_t * current;
    size_t     index = 0u;
    for (F__VECTOR_EACH(&my_vector.base, current)) {
        *current += 1;
        index++;
    }

    /* Validation */
    V__ASSERT_TRUE(index == 3);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[0], 5);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[1], 6);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[2], 7);
}

V__TEST(test_each_index, NULL, NULL)
{
    /* Preconditions */
    struct my_vector_type F__DEFINITE_VECTOR_T(uint32_t, 3) my_vector;
    F__DEFINITE_VECTOR_INIT(&my_vector);
    uint32_t item = 4;
    F__VECTOR_INSERT(&my_vector.base, &item, 0);
    item = 5;
    F__VECTOR_INSERT(&my_vector.base, &item, 1);
    item = 6;
    F__VECTOR_INSERT(&my_vector.base, &item, 2);

    /* UUT */
    uint32_t * current;
    size_t     index;
    for (F__VECTOR_EACH_INDEX(&my_vector.base, current, index)) {
        *current += (uint32_t) index;  // Drop of bits that do not fit uint32_t
    }

    /* Validation */
    V__ASSERT_TRUE(index == 3);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[0], 4);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[1], 6);
    V__ASSERT_EQ_UINT32(my_vector.base.array.storage[2], 8);
}

const v__test_descriptor * const G__ENABLED_TESTS[] = {
        &indefinite_initializer_capacity_10,
        &indefinite_initializer_size_0,
        &indefinite_initializer_storage_not_null,
        &indefinite_from_initializer_capacity_10,
        &indefinite_from_initializer_size_0,
        &indefinite_from_initializer_storage_not_null,
        &definite_initializer_capacity_10,
        &definite_initializer_size_0,
        &definite_initializer_storage_not_null,
        &definite_init_size_0,
        &definite_init_capacity_10,
        &definite_init_storage_not_null,
        &test_each_empty,
        &test_each_empty_index,
        &test_insert_at_0_1_capacity_3,
        &test_insert_at_0_1_size_1,
        &test_insert_at_0_1_storage_not_null,
        &test_insert_at_0_1_value,
        &test_insert_at_0_2_capacity_3,
        &test_insert_at_0_2_size_2,
        &test_insert_at_0_2_storage_not_null,
        &test_insert_at_0_2_value_5_4,
        &test_insert_at_1_2_capacity_3,
        &test_insert_at_1_2_size_2,
        &test_insert_at_1_2_storage_not_null,
        &test_insert_at_1_2_value_4_5,
        &test_insert_at_0_3_capacity_3,
        &test_insert_at_0_3_size_3,
        &test_insert_at_0_3_storage_not_null,
        &test_insert_at_0_3_value_6_5_4,
        &test_insert_at_1_3_capacity_3,
        &test_insert_at_1_3_size_3,
        &test_insert_at_1_3_storage_not_null,
        &test_insert_at_1_3_value_4_6_5,
        &test_insert_at_2_3_size,
        &test_insert_at_0_1_check,
        &test_insert_at_0_2_check,
        &test_insert_at_1_2_check,
        &test_insert_at_0_3_check,
        &test_insert_at_1_3_check,
        &test_insert_at_2_3_check,
        &test_insert_at_2_3_reversed_check,
        &test_insert_at_0_remove_0_1_0_size,
        &test_insert_at_0_remove_0_2_0_size,
        &test_insert_at_0_remove_0_2_1_size,
        &test_insert_at_0_remove_0_2_1_check,
        &test_insert_at_1_remove_0_2_1_check,
        &test_insert_at_1_remove_1_2_1_check,
        &test_each,
        &test_each_index,
};

const size_t G__TOTAL_TESTS =
        sizeof(G__ENABLED_TESTS) / sizeof(G__ENABLED_TESTS[0]);
