/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        panic.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation panic API header.
 *
 * This header file contains panic API.
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_panic Panic
 *
 * @brief       Panic API.
 *
 * @{
 */

#ifndef FOUNDATION_PANIC_H_
#define FOUNDATION_PANIC_H_

/* Depends */
#include "foundation/platform.h"

#define F__PANIC_ASSERT(x)                                                     \
    do {                                                                       \
        if (!(x)) {                                                            \
            f__panic_handler(x);                                               \
        }                                                                      \
        while (0)
/**
 * @brief 		Panic handler for assertion failures.
 *
 * This function is called when an assertion failure occurs during runtime. It
 * provides information about the failure.
 *
 * @param 		message The expression or meessage that triggered the failure.
 *
 * @note		This function does not return. It is marked with the
 *         		@ref F__PLATFORM_ATTR_NO_RETURN attribute to indicate that
 *         		execution does not continue after the failure handler is called.
 *
 * @note 		This function is typically called by an assert when an assertion
 *              failure occurs. The provided information can be used for
 *              debugging purposes or to generate error reports.
 */
F__PLATFORM_ATTR_NO_RETURN
void
f__panic_handler(const char * message);

#endif /* FOUNDATION_PANIC_H_ */
/** @} */
