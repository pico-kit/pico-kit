/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        ordered_map.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation ordered map API header.
 *
 * This header file contains ordered map API.
 *
 * Header contains interface for ordered map:
 * - initialization and deinitialization,
 * - insert and remove
 * - getting the state
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_ordered_map Ordered map
 *
 * @brief       Ordered map API.
 *
 * @{
 */

#ifndef F__ORDERED_MAP_H_
#define F__ORDERED_MAP_H_

/* Extends */
#include "foundation/vector.h"

/* Depends */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief       Indefinite ordered map template structure.
 *
 * Use this macro as a template for creating indefinite ordered map structures
 * of particular key/element pair types.
 *
 * To create an indefinite ordered map structure of with key of type `int32_t`
 * and element of type `size_t` do the following:
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, size_t);
 * @endcode
 *
 * @note        All the macros in this header file will work with any structure
 *              that contains member called `key`. Typically you would want to
 *              use this macro to define a structure.
 *
 * @param       key_type is any of integral types supporting comparison
 *              operations. Typically a fixed width integer type used for keys.
 *              Key type can be any of the following: `uint8_t`, `int8_t`,
 *              `uint16_t`, `int16_t`, `uint32_t`, `int32_t`, `uint64_t`,
 *              `int64_t`, `uintptr_t`, `intptr_t`, `size_t`.
 * @param       element_type is any C type that should be stored in ordered map.
 *
 * @note        This is a macro type declaration.
 */
#define F__ORDERED_MAP_ENTRY(key_type, element_type)                           \
    {                                                                          \
        key_type     key;                                                      \
        element_type element;                                                  \
    }

/**
 * @brief       Indefinite ordered map template structure.
 *
 * Use this macro as a template for creating indefinite ordered map structures
 * of particular key/element type.
 *
 * To create an indefinite ordered map structure of type int32_t/int32_t do the
 * following:
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, size_t);
 * struct my_map_type F__INDEFINITE_ORDERED_MAP_T(my_map_entry);
 * @endcode
 *
 * @param       entry_type is a structure, typically defined by @ref
 *              F__ORDERED_MAP_ENTRY macro, that contains a member called `key`.
 *
 * @note        This is a macro type declaration.
 */
#define F__INDEFINITE_ORDERED_MAP_T(entry_type)                                \
    F__INDEFINITE_VECTOR_T(entry_type)

/**
 * @brief       Indefinite ordered map template structure.
 *
 * Use this macro as a template for creating indefinite ordered map structures
 * of particular key/element type.
 *
 * To create an indefinite ordered map structure of type int32_t/int32_t do the
 * following:
 *
 * @code
 * struct my_map_entry       F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_entry_array F__INDEFINITE_ARRAY_T(struct my_map_entry);
 * struct my_map_entry       c_array[10] = { ... };
 *
 * struct my_map_type F__INDEFINITE_ORDERED_MAP_FROM_T(
 *          struct my_map_entry_array);
 * @endcode
 *
 * @param       array_typw
 *
 * @note        This is a macro type declaration.
 */
#define F__INDEFINITE_ORDERED_MAP_FROM_T(array_type)                           \
    F__INDEFINITE_VECTOR_FROM_T(array_type)

/**
 * @brief       Static indefinite ordered map initializer.
 *
 * Indefinite ordered map initializer is used to statically initialize an
 * indefinite ordered map.
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__INDEFINITE_ORDERED_MAP_T(struct my_map_entry);
 * struct my_map_entry c_array[10] = { ... };
 *
 * struct my_map_type my_map = F__INDEFINITE_ORDERED_MAP_INITIALIZER(c_array);
 * @endcode
 *
 * @param       c_entry_array is a pointer to C array for allocation of keys and
 *              elements.
 *
 * @note        This is a macro type definition.
 */
#define F__INDEFINITE_ORDERED_MAP_INITIALIZER(c_entry_array)                   \
    F__INDEFINITE_VECTOR_INITIALIZER(c_entry_array)

/**
 * @brief       Initialize indefinite ordered map instance during runtime.
 *
 * Use this macro when you want to initialize an indefinite sortem map during
 * runtime.
 *
 * @code
 * int32_t keys[10];
 * int32_t elements[10];
 * struct my_map_type F__INDEFINITE_ORDERED_MAP_T(int32_t, int32_t) my_map;
 *
 * F__INDEFINITE_ORDERED_MAP_INIT(&my_map, c_key_array, c_element_array);
 * @endcode
 *
 * @param       indefinite_ordered_map is a pointer to array structure instance
 *              defined by @ref F__INDEFINITE_ORDERED_MAP_T macro.
 * @param       c_key_array is a pointer to C array for allocation of keys.
 * @param       c_element_array is a pointer to C array for allocation of
 *              elements.
 *
 * @note        This is a macro function.
 */
#define F__INDEFINITE_ORDERED_MAP_INIT(indefinite_ordered_map, c_entry_array)  \
    F__INDEFINITE_VECTOR_INIT(indefinite_ordered_map, c_entry_array)

/**
 * @brief       Definite array template structure.
 *
 * Use this macro as a template for creating definite array structures of
 * particular type and size.
 *
 * To create a definite array structure of type int32_t with 10 elements do the
 * following:
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10) my_map;
 * @endcode
 *
 * @param       element_type is any C type that should be stored in array.
 * @param       size is an integer specifying how big that array should be.
 *
 * @note        This is a macro type declaration.
 */
#define F__DEFINITE_ORDERED_MAP_T(entry_type, size)                            \
    F__DEFINITE_VECTOR_T(entry_type, size)

/**
 * @brief       Static definite ordered map initializer.
 *
 * This initializer is used to statically initialize a definite ordered map.
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10);
 *
 * struct my_map_type my_map = F__DEFINITE_ORDERED_MAP_INITIALIZER(&my_map);
 * @endcode
 *
 * @param       definite_ordered_map is a pointer to defininite ordered map
 *              structure defined by @ref F__DEFINITE_ORDERED_MAP_T macro which
 *              is to be initialized.
 *
 * @note        This is a macro type definition.
 */
#define F__DEFINITE_ORDERED_MAP_INITIALIZER(definite_ordered_map)              \
    F__DEFINITE_VECTOR_INITIALIZER(definite_ordered_map)

/**
 * @brief       Initialize defininite ordered map instance during runtime.
 *
 * Use this macro when you want to initialize a defininite ordered map during
 * runtime.
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10) my_map;
 *
 * F__DEFINITE_ORDERED_MAP_INIT(&my_map);
 * @endcode
 *
 * @param       ordered map is a pointer to indefininite ordered map structure
 *              defined by @ref F__DEFINITE_VECTOR_T macro.
 *
 * @note        This is a macro function.
 */
#define F__DEFINITE_ORDERED_MAP_INIT(definite_ordered_map)                     \
    F__DEFINITE_VECTOR_INIT(definite_ordered_map)

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              ordered map.
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10) my_map;
 * F__DEFINITE_ORDERED_MAP_INIT(&my_map);
 *
 * struct my_map_entry * current;
 * for (F__ORDERED_MAP_EACH(&my_map.base, current))
 * {
 *     *current = ...; // Do something with current
 * }
 * @endcode
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in ordered map.
 *
 * @note        This is a macro for loop helper.
 */
#define F__ORDERED_MAP_EACH(indefinite_ordered_map, current)                   \
    F__VECTOR_EACH((indefinite_ordered_map), (current))

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              ordered map and an index.
 *
 * @code
 * struct my_map_entry F__ORDERED_MAP_ENTRY(int32_t, char);
 * struct my_map_type F__DEFINITE_ORDERED_MAP_T(struct my_map_entry, 10) my_map;
 * F__DEFINITE_ORDERED_MAP_INIT(&my_map);
 *
 * size_t                index;
 * struct my_map_entry * current;
 * for (F__ORDERED_MAP_EACH_INDEX(&my_map.base, current, index))
 * {
 *     *current = ... index; // Do something with current and index
 * }
 * @endcode
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in ordered map.
 * @param       index is a variable name that is going to hold current index.
 *
 * @note        This is a macro for loop helper.
 */
#define F__ORDERED_MAP_EACH_INDEX(indefinite_ordered_map, current, index)      \
    F__VECTOR_EACH_INDEX((indefinite_ordered_map), (current), (index))

/**
 * @brief       Insert an entry into ordered map.
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       entry is a pointer to entry that needs to be inserted into
 *              ordered map. The entry will be copied into ordered map at proper
 *              location in ordered map.
 *
 * @note        This is a macro function.
 */
#define F__ORDERED_MAP_INSERT(indefinite_ordered_map, entry)                   \
    do {                                                                       \
        size_t l_low_  = 0u;                                                   \
        size_t l_high_ = (indefinite_ordered_map)->array.size;                 \
        while (l_low_ < l_high_) {                                             \
            size_t l_mid_ = (l_low_ + l_high_) / 2u;                           \
            if ((indefinite_ordered_map)->array.storage[l_mid_].key            \
                < (entry)->key) {                                              \
                l_low_ = l_mid_ + 1u;                                          \
            } else {                                                           \
                l_high_ = l_mid_;                                              \
            }                                                                  \
        }                                                                      \
        F__VECTOR_INSERT((indefinite_ordered_map), (entry), l_low_);           \
    } while (0)

/**
 * @brief       Search for an entry with key into ordered map.
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       key_value is value of key to search for.
 * @param       index is a pointer to a `size_t` variable that will contain the
 *              index to entry. If this variable is set to `SIZE_MAX` then it
 *              means the entry with given `key_value` was not found.
 *
 * @note        This is a macro function.
 */
#define F__ORDERED_MAP_SEARCH_INDEX(indefinite_ordered_map, key_value, index)  \
    do {                                                                       \
        size_t l_low_  = 0u;                                                   \
        size_t l_high_ = (indefinite_ordered_map)->array.size;                 \
        *(index)       = SIZE_MAX;                                             \
        while (l_low_ < l_high_) {                                             \
            size_t l_mid_ = (l_low_ + l_high_) / 2u;                           \
            if ((indefinite_ordered_map)->array.storage[l_mid_].key            \
                < (key_value)) {                                               \
                l_low_ = l_mid_ + 1u;                                          \
            } else if (                                                        \
                    (indefinite_ordered_map)->array.storage[l_mid_].key        \
                    > (key_value)) {                                           \
                l_high_ = l_mid_;                                              \
            } else {                                                           \
                *(index) = l_mid_;                                             \
                break;                                                         \
            }                                                                  \
        }                                                                      \
    } while (0)

/**
 * @brief       Search for an entry with key into ordered map.
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       key_value is value of key to search for.
 * @param       entry is a pointer to a pointer variable that will contain the
 *              pointer to found entry. If this pointer is set to NULL, then it
 *              means the entry with given `key_value` was not found.
 *
 * @note        This is a macro function.
 */
#define F__ORDERED_MAP_SEARCH(indefinite_ordered_map, key_value, entry)        \
    do {                                                                       \
        size_t l_index_;                                                       \
        F__ORDERED_MAP_SEARCH_INDEX(                                           \
                (indefinite_ordered_map),                                      \
                (key_value),                                                   \
                &l_index_);                                                    \
        if (l_index_ != SIZE_MAX) {                                            \
            *(entry) = &(indefinite_ordered_map)->array.storage[l_index_];     \
        } else {                                                               \
            *(entry) = NULL;                                                   \
        }                                                                      \
    } while (0)

/**
 * @brief       Remove an entry with key into ordered map.
 *
 * @param       indefinite_ordered_map is a pointer to indefinite ordered map.
 * @param       key_value is value of key to search for.
 * @param       entry is a pointer to a pointer variable that will contain the
 *              pointer to found entry. If this pointer is set to NULL, then it
 *              means the entry with given `key_value` was not found.
 *
 * @note        This is a macro function.
 */
#define F__ORDERED_MAP_REMOVE_INDEX(indefinite_ordered_map, index)             \
    do {                                                                       \
        size_t l_index_ = (index);                                             \
        if (l_index_ != SIZE_MAX) {                                            \
            F__VECTOR_REMOVE((indefinite_ordered_map), l_index_);              \
        }                                                                      \
    } while (0)

#define F__ORDERED_MAP_REMOVE(indefinite_ordered_map, key_value)               \
    do {                                                                       \
        size_t l_index_;                                                       \
        F__ORDERED_MAP_SEARCH_INDEX(                                           \
                (indefinite_ordered_map),                                      \
                (key_value),                                                   \
                &l_index_);                                                    \
        F__ORDERED_MAP_REMOVE_INDEX((indefinite_ordered_map), l_index_);       \
        \                                                                      \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif /* F__ORDERED_MAP_H_ */
/** @} */
