/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief       Foundation API header.
 *
 * This header file contains Foundation  API.
 */
/**
 * @defgroup    foundation Foundation
 *
 * @brief       Foundation API.
 *
 * @{
 */
/** @} */
