/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        vector.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation vector API header.
 *
 * This header file contains vector API.
 *
 * Header contains interface for timer:
 * - initialization and deinitialization,
 * - insert and remove
 * - getting the state
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_vector Vector
 *
 * @brief       Vector API.
 *
 * @{
 */

#ifndef FOUNDATION_VECTOR_H_
#define FOUNDATION_VECTOR_H_

/* Extends */
#include "foundation/array.h"

/* Depends */
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief       Indefinite vector template structure.
 *
 * Use this macro as a template for creating indefinite vector structures of
 * particular type.
 *
 * To create an indefinite vector structure of type int32_t do the following:
 *
 * @code
 * struct my_vector F__INDEFINITE_VECTOR_T(int32_t);
 * @endcode
 *
 * @param       element_type is any C type that should be stored in vector.
 *
 * @note        This is a macro type declaration.
 */
#define F__INDEFINITE_VECTOR_T(element_type)                                   \
    {                                                                          \
        struct F__INDEFINITE_ARRAY_T(element_type) array;                      \
        size_t capacity;                                                       \
    }

/**
 * @brief       Indefinite vector template structure.
 *
 * Use this macro as a template for creating indefinite vector structures of
 * particular type.
 *
 * To create an indefinite vector structure of type int32_t do the following:
 *
 * @code
 * struct my_array F__INDEFINITE_ARRAY_T(int32_t);
 * struct my_vector F__INDEFINITE_VECTOR_FROM_T(struct my_array);
 * @endcode
 *
 * @param       array_type is any array type (@see F__INDEFINITE_ARRAY_T) that
 *              should be used to store vector data.
 *
 * @note        This is a macro type declaration.
 */
#define F__INDEFINITE_VECTOR_FROM_T(array_type)                                \
    {                                                                          \
        array_type array;                                                      \
        size_t     capacity;                                                   \
    }

/**
 * @brief       Static indefinite vector initializer.
 *
 * Indefinite vector initializer is used to statically initialize an indefinite
 * vector.
 *
 * @code
 * struct my_array_type F__ARRAY_T(int32_t, 10);
 * struct my_array_type my_array = F__ARRAY_INITIALIZER();
 *
 * struct my_vector_type F__INDEFINITE_VECTOR_T(int32_t);
 * struct my_vector_type my_vector =
 *     F__INDEFINITE_VECTOR_INITIALIZER(&my_array);
 * @endcode
 *
 * @param       c_array is a pointer to plain C array of elements.
 *
 * @note        This is a macro type definition.
 */
#define F__INDEFINITE_VECTOR_INITIALIZER(c_array)                              \
    {                                                                          \
        .array =                                                               \
                {                                                              \
                        .storage = (c_array),                                  \
                        .size    = 0u,                                         \
                },                                                             \
        .capacity = (sizeof((c_array)) / sizeof((c_array)[0])),                \
    }

/**
 * @brief       Initialize indefinite vector instance during runtime.
 *
 * Use this macro when you want to initialize an indefinite vector during
 * runtime.
 *
 * @code
 * struct my_array_type F__ARRAY_T(int32_t, 10);
 * struct my_array_type my_array;
 *
 * struct my_vector_type F__INDEFINITE_VECTOR_T(int32_t);
 * struct my_vector_type my_vector;
 *
 * F__ARRAY_INIT(&my_array);
 * F__INDEFINITE_VECTOR_INIT(&my_vector, &my_array);
 * @endcode
 *
 * @param       vector is a pointer to indefinite array structure defined by
 *              @ref F__INDEFINITE_VECTOR_T macro.
 * @param       c_array is a pointer to plain C array of elements that will
 *              contain actual element instances.
 *
 * @note        This is a macro function.
 */
#define F__INDEFINITE_VECTOR_INIT(vector, c_array)                             \
    do {                                                                       \
        (vector)->array.storage = (c_array);                                   \
        (vector)->array.size    = 0u;                                          \
        (vector)->capacity      = (sizeof((c_array)) / sizeof((c_array)[0]));  \
    } while (0)

/**
 * @brief       Definite vector template structure.
 *
 * Use this macro as a template for creating definite vector structures of
 * particular type.
 *
 * To create an definite vector structure of type int32_t with 10 elements do
 * the following:
 *
 * @code
 * struct my_vector_def F__DEFINITE_VECTOR_T(int32_t, 10);
 * @endcode
 *
 * @param       element_type is any C type that should be stored in vector.
 * @param       size is an integer specifying how big that array should be.
 *
 * @note        This is a macro type declaration.
 */
#define F__DEFINITE_VECTOR_T(element_type, size)                               \
    {                                                                          \
        struct F__INDEFINITE_VECTOR_T(element_type) base;                      \
        element_type elements[size];                                           \
    }

/**
 * @brief       Definite vector template structure from indefinite.
 *
 * Use this macro as a template for creating definite vector structures of
 * particular type.
 *
 * To create an definite vector structure of type int32_t with 10 elements do
 * the following:
 *
 * @code
 * struct my_vector F__INDEFINITE_VECTOR_T(int32_t);
 * struct my_vector_def F__DEFINITE_VECTOR_FROM_T(
 *     struct my_vector, int32_t, 10);
 * @endcode
 *
 * You can create multiple definite vectors from single indefinite vector. Both
 * definite vectors are then accessible using indefinite vector pointer.
 *
 * @code
 * struct my_vector F__INDEFINITE_VECTOR_T(int32_t);
 *
 * struct F__DEFINITE_VECTOR_FROM_T(struct my_vector, int32_t, 10)
 *     my_vector_of_10 = F__DEFINITE_VECTOR_INITIALIZER(&my_vector_of_10);
 *
 * struct F__DEFINITE_VECTOR_FROM_T(struct my_vector, int32_t, 20)
 *     my_vector_of_20 = F__DEFINITE_VECTOR_INITIALIZER(&my_vector_of_20);
 *
 * void function_a(struct my_vector * vector);
 *
 * function_a(&my_vector_of_10.base);
 * function_a(&my_vector_of_20.base);
 * @endcode
 *
 * @param       indefinite_vector_type is a indefinite vector type defined by
 *              @ref F__INDEFINITE_VECTOR_T macro.
 * @param       element_type is any C type that should be stored in vector.
 * @param       size is an integer specifying how big that array should be.
 *
 * @note        This is a macro type declaration.
 */
#define F__DEFINITE_VECTOR_FROM_T(indefinite_vector_type, element_type, size)  \
    {                                                                          \
        indefinite_vector_type base;                                           \
        element_type           elements[size];                                 \
    }

/**
 * @brief       Static definite vector initializer.
 *
 * This initializer is used to statically initialize a definite vector.
 *
 * @code
 * struct my_vector_type F__DEFINITE_VECTOR_T(int32_t, 10);
 *
 * struct my_vector_type my_vector = F__DEFINITE_VECTOR_INITIALIZER(&my_vector);
 * @endcode
 *
 * @param       vector is a pointer to defininite vector structure defined by
 *              @ref F__DEFINITE_VECTOR_T or @ref F__DEFINITE_VECTOR_FROM_T
 *              macro which is to be initialized.
 *
 * @note        This is a macro type definition.
 */
#define F__DEFINITE_VECTOR_INITIALIZER(vector)                                 \
    {                                                                          \
        .base = F__INDEFINITE_VECTOR_INITIALIZER((vector)->elements),          \
    }

/**
 * @brief       Initialize defininite vector instance during runtime.
 *
 * Use this macro when you want to initialize a defininite vector during
 * runtime.
 *
 * @code
 * struct my_vector_type F__DEFINITE_VECTOR_T(int32_t, 10) my_vector;
 *
 * F__INDEFINITE_VECTOR_INIT(&my_vector);
 * @endcode
 *
 * @param       vector is a pointer to indefinite vector structure defined by
 *              @ref F__DEFINITE_VECTOR_T macro.
 *
 * @note        This is a macro function.
 */
#define F__DEFINITE_VECTOR_INIT(vector)                                        \
    do {                                                                       \
        F__INDEFINITE_VECTOR_INIT(&(vector)->base, (vector)->elements);        \
        memset(&(vector)->elements, 0, sizeof((vector)->elements));            \
    } while (0)

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              vector.
 *
 * @code
 * struct my_array_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
 * F__DEFINITE_ARRAY_INIT(&my_vector);
 *
 * int * current;
 * for (F__VECTOR_EACH(&my_vector.base, current))
 * {
 *     *current = ...; // Do something with current
 * }
 * @endcode
 *
 * @param       indefinite_vector is a pointer to indefinite vector.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in vector.
 *
 * @note        This is a macro for loop helper.
 */
#define F__VECTOR_EACH(indefinite_vector, current)                             \
    F__ARRAY_EACH(&(indefinite_vector)->array, (current))

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              vector and an index.
 *
 * @code
 * struct my_array_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
 * F__DEFINITE_ARRAY_INIT(&my_vector);
 *
 * int * current;
 * size_t index;
 * for (F__VECTOR_EACH(&my_vector.base, current, index))
 * {
 *     *current = ... index; // Do something with current and index
 * }
 * @endcode
 *
 * @param       indefinite_vector is a pointer to indefinite vector.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in vector.
 * @param       index is a variable name that is going to hold current index.
 *
 * @note        This is a macro for loop helper.
 */
#define F__VECTOR_EACH_INDEX(indefinite_vector, current, index)                \
    F__ARRAY_EACH_INDEX(&(indefinite_vector)->array, (current), (index))

/**
 * @brief       Insert an item into the vector at position specified by index.
 *
 * @code
 * struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
 * F__DEFINITE_VECTOR_INIT(&my_vector);
 * int item = 4;
 * F__VECTOR_INSERT(&my_vector.base, &item, 0);
 * @endcode
 *
 * @param       indefinite_vector is a pointer to indefinite vector.
 * @param       item is a pointer to item that needs to be inserted.
 * @param       index is a `size_t` value that specifies at which location to
 *              insert the @a item. The item can be inserted at locations
 *              starting from `0` to vector's `size`.
 *
 * @pre         The value of @a index must be in range: `[0 ... size]`.
 *              The macro has undefined bahaviour when using @a index value
 *              which is greater than `size` value of the vector.
 *
 * @note        This is a macro function.
 */
#define F__VECTOR_INSERT(indefinite_vector, item, index)                       \
    do {                                                                       \
        size_t l_index_ = (index);                                             \
        size_t l_size_  = (indefinite_vector)->array.size++;                   \
        if (l_index_ < l_size_) {                                              \
            size_t l_blocks_ = l_size_ - l_index_;                             \
            size_t l_block_size_ =                                             \
                    sizeof((indefinite_vector)->array.storage[0]) * l_blocks_; \
            memmove(&(indefinite_vector)->array.storage[l_index_ + 1u],        \
                    &(indefinite_vector)->array.storage[l_index_],             \
                    l_block_size_);                                            \
        }                                                                      \
        (indefinite_vector)->array.storage[l_index_] = *(item);                \
    } while (0)

/**
 * @brief       Remove an item from the vector at position specified by index.
 *
 * @code
 * struct my_vector_type F__DEFINITE_VECTOR_T(int, 3) my_vector;
 * F__DEFINITE_VECTOR_INIT(&my_vector);
 * int item = 4;
 * F__VECTOR_INSERT(&my_vector.base, &item, 0);
 * F__VECTOR_REMOVE(&my_vector.base, 0);
 * @endcode
 *
 * @param       indefinite_vector is a pointer to indefinite vector.
 * @param       index is a `size_t` value that specifies at which location to
 *              remove the item. The item can be removed from locations
 *              starting from `0` up to vector's `size` exclusive.
 *
 * @pre         The value of @a index must be in range: `[0 ... size)`.
 *              The macro has undefined bahaviour when using @a index value
 *              which is greater than or equal to `size` value of the
 *              vector.
 *
 * @note        This is a macro function.
 */
#define F__VECTOR_REMOVE(indefinite_vector, index)                             \
    do {                                                                       \
        size_t l_size_  = (indefinite_vector)->array.size--;                   \
        size_t l_index_ = (index);                                             \
        if (l_index_ < (l_size_ - 1)) {                                        \
            size_t l_blocks_ = l_size_ - l_index_ - 1;                         \
            size_t l_block_size_ =                                             \
                    sizeof((indefinite_vector)->array.storage[0]) * l_blocks_; \
            memmove(&(indefinite_vector)->array.storage[l_index_],             \
                    &(indefinite_vector)->array.storage[l_index_ + 1u],        \
                    l_block_size_);                                            \
        }                                                                      \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif /* FOUNDATION_VECTOR_H_ */
/** @} */
