/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        result.hpp
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation result C++ API header.
 *
 * This header file contains result C++ API.
 */

#ifndef FOUNDATION_RESULT_HPP_
#define FOUNDATION_RESULT_HPP_

#include <type_traits>
#include <utility>
#include "foundation/error.hpp"
#include "foundation/validator.h"

namespace pk
{

/**
 * @ingroup     foundation
 * @defgroup    foundation_result_cpp Result (C++)
 *
 * @brief       Result C++ API.
 *
 * Common result handling of all Pico-Kit feature functions.
 *
 * @{
 */

/**
 * @brief       Namespace containing common Result tools
 */
namespace result
{

/**
 * @brief       Failure of result when it is not checked.
 */
static constexpr uint32_t FAIL_RESULT_IS_UNCHECKED =
        F__VALIDATOR_COMPONENT_RESULT | 0x0001u;

/**
 * @brief       Failure of result when value is requested but it contains error.
 */
static constexpr uint32_t FAIL_RESULT_IS_NOT_VALUE =
        F__VALIDATOR_COMPONENT_RESULT | 0x0002u;

/**
 * @brief       Failure of result when it is undefined.
 */
static constexpr uint32_t FAIL_RESULT_IS_UNDEFINED =
        F__VALIDATOR_COMPONENT_RESULT | 0x0003u;

}  // namespace result

/**
 * @brief       Template declaration for Result class
 *
 * This template accespts argument `T` which is type that needs to be contained
 * within the Result class.
 */
template <typename T, typename = void>
class Result;

/**
 * @brief       Template specialization for non-void Result class
 *
 * This template accespts argument `T` which is type that needs to be contained
 * within the Result class.
 */
template <typename T>
class [[nodiscard]] Result<
        T,
        typename std::enable_if<!std::is_void<T>::value>::type>
{
   public:
    /**
     * @brief   Default constructor.
     *
     * Default constructor will set the result state to @ref
     * pk::result::State::UNDEFINED value and error value to @ref
     * pk::Error::UNSPECIFIED.
     */
    Result() : state(State::UNDEFINED), error_value(Error::UNSPECIFIED) {}

    /**
     * @brief   Class copy constructor.
     *
     * @param   other is a const lvalue Result.
     */
    Result(const Result & other) : state(other.state)
    {
        if (other.is_value()) {
            value_container = other.value_container;
        } else if (other.is_error()) {
            error_value = other.error_value;
        }
    }

    /**
     * @brief   Class copy assignment operator.
     *
     * @param   other is a const lvalue Result.
     *
     * @return  Returns reference to this instance that contains the copy of
     *          @a other class.
     */
    Result & operator=(const Result & other)
    {
        if (this != &other) {
            if (is_value()) {
                value_container.~T();
            }
            if (other.is_value()) {
                value_container = other.value_container;
            } else if (other.is_error()) {
                error_value = other.error_value;
            }
            state = other.state;
        }
        return *this;
    }

    /**
     * @brief   Class move constructor.
     *
     * @param   other is a rvalue Result.
     */
    Result(Result && other) : state(other.state)
    {
        if (other.is_value()) {
            value_container = std::move(other.value_container);
            other.value_container.~T();
        } else if (other.is_error()) {
            error_value = other.error_value;
        }
        other.state = State::UNDEFINED;
    }

    /**
     * @brief   Move assignment operator
     *
     * @param   other is a rvalue Result.
     *
     * @return  Returns reference to this instance that contains the moved copy
     *          of @a other class.
     */
    Result & operator=(Result && other)
    {
        if (this != &other) {
            if (is_value()) {
                value_container.~T();
            }
            state = other.state;
            if (other.is_value()) {
                value_container = std::move(other.value_container);
                other.value_container.~T();
            } else if (other.is_error()) {
                error_value = other.error_value;
            }
            other.state = State::UNDEFINED;
        }
        return *this;
    }

    /**
     * @brief   Class destructor
     */
    ~Result()
    {
        if (is_value()) {
            value_container.~T();
        }
    }

    /**
     * @brief   Error constructor.
     *
     * Error constructor will construct Result class containing the
     * @a error_value error.
     *
     * @param   error_value is @ref pk::Error enumerator of error.
     *
     * @code
     * // Create a result containing NO_MEM error
     * pk::Result<int32_t> result {pk::Error::NO_MEM};
     * @endcode
     */
    explicit Result(Error error_value) :
        state(State::UNCHECKED_ERROR), error_value(error_value)
    {
    }

    /**
     * @brief   Value rvalue reference constructor (move semantics).
     *
     * Overloaded value constructor will construct Result class containing the
     * @a value. This is the variant that has move semantics.
     *
     * @param   value is rvalue value that will be stored into Result class.
     *
     * @code
     * // Create a result containing MyClass with value 4
     * pk::Result<MyClass> result {MyClass(4)};
     * @endcode
     */
    explicit Result(T && value) :
        state(State::UNCHECKED_VALUE), value_container(std::move(value))
    {
    }

    /**
     * @brief   Value lvalue reference constructor (copy semantics).
     *
     * Overloaded value constructor will construct Result class containing the
     * @a value. This is the variant that has copy semantics.
     *
     * @param   value is a const lvalue value that will be stored into Result
     *          class.
     *
     * @code
     * // Create a result containing value 4
     * pk::Result<int32_t> result {4};
     * @endcode
     */
    explicit Result(const T & value) :
        state(State::UNCHECKED_VALUE), value_container(value)
    {
    }

    /**
     * @brief   Returns if result is checked.
     */
    bool is_checked() const
    {
        return (state == State::CHECKED_ERROR)
               || (state == State::CHECKED_VALUE);
    }

    /**
     * @brief   Returns if result is defined.
     */
    bool is_defined() const
    {
        return state != State::UNDEFINED;
    }

    /**
     * @brief   Returns if result is error.
     */
    bool is_error() const
    {
        return (state == State::CHECKED_ERROR)
               || (state == State::UNCHECKED_ERROR);
    }

    /**
     * @brief   Returns if result is value.
     */
    bool is_value() const
    {
        return (state == State::CHECKED_VALUE)
               || (state == State::UNCHECKED_VALUE);
    }

    bool has_value()
    {
        F__VALIDATOR_INVARIANT(is_defined(), result::FAIL_RESULT_IS_UNDEFINED);
        switch (state) {
            case State::UNCHECKED_VALUE:
                state = State::CHECKED_VALUE;
                [[fallthrough]];
            case State::CHECKED_VALUE:
                return true;
            case State::UNCHECKED_ERROR:
                state = State::CHECKED_ERROR;
                [[fallthrough]];
            case State::CHECKED_ERROR:
                return false;
            default:
                return false;
        }
    }

    bool has_error()
    {
        return !has_value();
    }

    [[nodiscard]] bool is_ok()
    {
        return has_value();
    }

    [[nodiscard]] Error error()
    {
        switch (state) {
            case State::UNCHECKED_ERROR:
                state = State::CHECKED_ERROR;
                [[fallthrough]];
            case State::CHECKED_ERROR:
                return error_value;
            case State::UNCHECKED_VALUE:
                state = State::CHECKED_VALUE;
                [[fallthrough]];
            case State::CHECKED_VALUE:
                return Error::NONE;
            default:
                return Error::UNSPECIFIED;
        }
    }

    const T & value() const F__VALIDATOR
    {
        F__VALIDATOR_INVARIANT(is_defined(), result::FAIL_RESULT_IS_UNDEFINED);
        F__VALIDATOR_INVARIANT(is_checked(), result::FAIL_RESULT_IS_UNCHECKED);
        F__VALIDATOR_INVARIANT(is_value(), result::FAIL_RESULT_IS_NOT_VALUE);
        return value_container;
    }

    T & value() F__VALIDATOR
    {
        F__VALIDATOR_INVARIANT(is_defined(), result::FAIL_RESULT_IS_UNDEFINED);
        F__VALIDATOR_INVARIANT(is_checked(), result::FAIL_RESULT_IS_UNCHECKED);
        F__VALIDATOR_INVARIANT(is_value(), result::FAIL_RESULT_IS_NOT_VALUE);
        return value_container;
    }

    Result & ok(T && value)
    {
        if (is_value()) {
            value_container.~T();
        }
        state           = State::UNCHECKED_VALUE;
        value_container = std::move(value);
        return *this;
    }

    Result & ok(const T & value)
    {
        if (is_value()) {
            value_container.~T();
        }
        state           = State::UNCHECKED_VALUE;
        value_container = value;
        return *this;
    }

    Result & fail(Error error)
    {
        if (is_value()) {
            value_container.~T();
        }
        state       = State::UNCHECKED_ERROR;
        error_value = error;
        return *this;
    }

   private:
    enum class State {
        UNDEFINED,
        UNCHECKED_ERROR,
        CHECKED_ERROR,
        UNCHECKED_VALUE,
        CHECKED_VALUE
    };
    State state;
    union {
        Error error_value;
        T     value_container;
    };
};

/**
 * @brief       Template specialization for void Result class
 *
 * This template specialization creates a Result class that contains no value.
 */
template <>
class [[nodiscard]] Result<void>
{
   public:
    Result() : error_value(Error::UNSPECIFIED) {}
    explicit Result(Error error_value) : error_value(error_value) {}

    [[nodiscard]] bool is_ok() const
    {
        return error_value == Error::NONE;
    }

    [[nodiscard]] Error error() const
    {
        return error_value;
    }

    Result<void> & ok()
    {
        error_value = Error::NONE;
        return *this;
    }

   private:
    Error error_value;
};

/** @} */

}  // namespace pk

#endif /* FOUNDATION_RESULT_HPP_ */
