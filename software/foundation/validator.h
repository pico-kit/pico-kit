/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        validator.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation validator API header.
 *
 * This header file contains validator API.
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_validator Validator
 *
 * @brief       Validator API.
 *
 * @{
 */

#ifndef FOUNDATION_VALIDATOR_H_
#define FOUNDATION_VALIDATOR_H_

/* Depends */
#include <stdint.h>
#include "foundation/platform.h"

#ifdef __cplusplus
extern "C" {
#endif

#define F__VALIDATOR

#define F__VALIDATOR_COMPONENT_RESULT 0x00010000u

/**
 * @brief		Validator assert macro with simple text
 *
 * @param		text is a pointer to string containing error message.
 * @param		expression is expression that needs to evaluate to @a true.
 * @param		error is integer representing the error.
 *
 * @note        This is a macro function.
 */
#ifdef NDEBUG
#define F__VALIDATOR_ASSERT_TEXT(text, expression, error)
#else
#define F__VALIDATOR_ASSERT_TEXT(text, expression, error)                      \
    do {                                                                       \
        if (!(expression)) {                                                   \
            f__validator_failure_handler(text, (error));                       \
        }                                                                      \
    } while (0)
#endif

/**
 * @brief		Validator assert macro.
 *
 * @param		expression is expression that needs to evaluate to @a true.
 * @param		error is integer representing the error.
 *
 * @note        This is a macro function.
 */
#define F__VALIDATOR_ASSERT(expression, error)                                 \
    F__VALIDATOR_ASSERT_TEXT(#expression, expression, error)

/**
 * @brief		Validator precondition macro.
 *
 * @param		expression is expression that needs to evaluate to @a true.
 * @param		error is integer representing the error.
 *
 * @note        This is a macro function.
 */
#define F__VALIDATOR_PRECONDITION(expression, error)                           \
    F__VALIDATOR_ASSERT_TEXT(#expression, expression, error)

/**
 * @brief		Validator postcondition macro.
 *
 * @param		expression is expression that needs to evaluate to @a true.
 * @param		error is integer representing the error.
 *
 * @note        This is a macro function.
 */
#define F__VALIDATOR_POSTCONDITION(expression, error)                          \
    F__VALIDATOR_ASSERT_TEXT(#expression, expression, error)

/**
 * @brief		Validator invariant macro.
 *
 * @param		expression is expression that needs to evaluate to @a true.
 * @param		error is integer representing the error.
 *
 * @note        This is a macro function.
 */
#define F__VALIDATOR_INVARIANT(expression, error)                              \
    F__VALIDATOR_ASSERT_TEXT(#expression, expression, error)

/**
 * @brief 		Validator failure handler for validation failures.
 *
 * This function is called when an assertion failure occurs during runtime. It
 * provides information about the failure.
 *
 * @param 		message The expression or meessage that triggered the failure.
 *
 * @note		This function does not return. It is marked with the
 *         		@ref F__PLATFORM_ATTR_NO_RETURN attribute to indicate that
 *         		execution does not continue after the failure handler is called.
 *
 * @note 		This function is typically called by an assert when an assertion
 *              failure occurs. The provided information can be used for
 *              debugging purposes or to generate error reports.
 *
 * @note        This is a macro function.
 */
#if defined(NDEBUG)
F__PLATFORM_ATTR_NO_RETURN
#endif
void
f__validator_failure_handler(const char * text, uint32_t error_code);

#ifdef __cplusplus
}
#endif

#endif /* FOUNDATION_VALIDATOR_H_ */
/** @} */
