/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        absolute.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Absolute header.
 *
 * This header file contains absolute API, which is part of numeric.
 */
/**
 * @ingroup     foundation_numeric
 * @defgroup    foundation_absolute Absolute
 *
 * @brief       Absolute API.
 *
 * @{
 */

#ifndef FOUNDATION_NUMERIC_ABSOLUTE_H_
#define FOUNDATION_NUMERIC_ABSOLUTE_H_

// Extends

// Depends
#include <type_traits>

namespace foundation
{
namespace numeric
{

/**
 * @brief       Absolute template variant for signed integral types.
 */
template <typename T>
typename std::enable_if<std::is_signed<T>::value, T>::type
absolute(T value)
{
    return (value < T(0)) ? -value : value;
}

/**
 * @brief       Absolute template variant for unsigned integral types.
 */
template <typename T>
typename std::enable_if<std::is_unsigned<T>::value, T>::type
absolute(T value)
{
    return value;
}
}  // namespace numeric
}  // namespace foundation

#endif  // FOUNDATION_NUMERIC_ABSOLUTE_H_
/** @} */
