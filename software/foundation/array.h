/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        array.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation array API header.
 *
 * This header file contains array API.
 *
 * Header contains interface for timer:
 * - initialization and deinitialization,
 * - accessing members
 * - iterating
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_array Array
 *
 * @brief       Array API.
 *
 * # Array definition and creation
 *
 * There are two variants of array depending on how they are created:
 * - indefinite array and
 * - definite array
 *
 * Both arrays can contain fixed number of elements. In case of indefinite
 * array, the number of elements is different from instannce to instance, while
 * all instances of definite array have the same size.
 *
 * ## Indefinite array declaration and initialization
 *
 * To define a new indefinite array type use @ref F__INDEFINITE_ARRAY_T macro.
 * This macro accepts stored element type.
 *
 * The following are few examples of indefinite array usage:
 *
 * @code
 * // Declare indefinite array of integers
 * struct my_array_type F__INDEFINITE_ARRAY_T(int);
 * @endcode
 *
 * @code
 * // Declare indefinite array of structs
 * struct my_struct_type { int a; };
 * struct my_array_type F__INDEFINITE_ARRAY_T(struct my_struct_type);
 * @endcode
 *
 * @code
 * // Combining with typedef
 * typedef struct my_array_type F__INDEFINITE_ARRAY_T(int) my_int_array;
 * @endcode
 *
 * After declaring an indefinite array type it can be statically allocated and
 * initialized with @ref F__INDEFINITE_ARRAY_INITIALIZER macro:
 *
 * @code
 * // This example demonstrates declaration and instance creation of indefinite
 * // array in static way. The array is containing integers which are stored in
 * // array_storage C array.
 *
 * // Declaration
 * struct my_array_type F__INDEFINITE_ARRAY_T(int);
 * int array_storage[10];
 *
 * // Static initialization
 * struct my_array_type my_array = F__INDEFINITE_ARRAY_INITIALIZER(
 *     array_storage);
 * @endcode
 *
 * Indefinite array type can be dynamically allocated and initialized with
 * @ref F__INDEFINITE_ARRAY_INIT macro:
 *
 * @code
 * // This example demonstrates declaration and instance creation of indefinite
 * // array in dynamic way. The array is containing integers which are stored in
 * // array_storage C array.
 *
 * // Declaration
 * struct my_array_type F__INDEFINITE_ARRAY_T(int) my_array;
 * int array_storage[10];
 *
 * // Static initialization
 * F__INDEFINITE_ARRAY_INIT(&my_array, array_storage);
 * @endcode
 *
 * ## Definite array declaration and initialization
 *
 * To define a new definite array type use @ref F__DEFINITE_ARRAY_T macro. This
 * macro accepts stored element type and number of elements.
 *
 * The following are few examples of definite array usage:
 *
 * @code
 * // Declare definite array type of 10 integers
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 10);
 * @endcode
 *
 * @code
 * // Declare definite array of 10 structs
 * struct my_struct_type { int a; };
 * struct my_array_type F__DEFINITE_ARRAY_T(struct my_struct_type, 10);
 * @endcode
 *
 * @code
 * // Combining with typedef
 * typedef struct my_array_type F__DEFINITE_ARRAY_T(int, 10) my_int_array;
 * @endcode
 *
 * After declaring an definite array type it can be statically allocated and
 * initialized with @ref F__DEFINITE_ARRAY_INITIALIZER macro:
 *
 * @code
 * // This example demonstrates declaration and instance creation of definite
 * // array in static way. The array is containing 10 integers.
 *
 * // Declaration
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 10);
 *
 * // Static initialization
 * struct my_array_type my_array = F__DEFINITE_ARRAY_INITIALIZER(&my_array);
 * @endcode
 *
 * Indefinite array type can be dynamically allocated and initialized with
 * @ref F__DEFINITE_ARRAY_INIT macro:
 *
 * @code
 * // This example demonstrates declaration and instance creation of definite
 * // array in dynamic way. The array is containing integers which are stored in
 * // array_storage C array.
 *
 * // Declaration
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 10) my_array;
 *
 * // Static initialization
 * F__INDEFINITE_ARRAY_INIT(&my_array);
 * @endcode
 *
 * # Array usage
 *
 * Arrays store the elements in structure member called `elements`, which is
 * defined by @ref F__INDEFINITE_ARRAY_T macro.
 *
 * After declaration and creation array are used in the following ways:
 *
 * @code
 * // Declaration of array type
 * struct my_int_array F__DEFINITE_ARRAY_T(int, 20);
 *
 * // Allocation of array
 * struct my_int_array my_array_var;
 *
 * // Runtime initialization
 * F__DEFINITE_ARRAY_INIT(&my_array_var);
 *
 * // Read element 5 from my_array_var array and store into variable a
 * int a = my_array_var.elements[5];
 *
 * // Write value 2 to element 3
 * my_array_var.elements[3] = 2;
 *
 * // Iterating through array elements
 * // Set 5 to every element
 * int * current;
 * for (F__ARRAY_EACH(my_array_var.base, current)) {
 *      // Do something with current value
 *      // ...
 *      *current = 5;
 * }
 *
 * // Add 5 to every element
 * for (F__ARRAY_EACH(my_array_var.base, current)) {
 *      // Do something with current value
 *      // ...
 *      *current += 5;
 * }
 *
 * // Sometimes you need index as well, the following example adds 5 + i to
 * // each element, resulting in elements having values: 5, 6, 7, etc.
 * size_t i;
 * for (F__ARRAY_EACH_INDEX(my_array_var.base, current, i)) {
 *      // Do something with current value
 *      // ...
 *      *current += 5 + i;
 * }
 * @endcode
 *
 * @{
 */

#ifndef FOUNDATION_ARRAY_H_
#define FOUNDATION_ARRAY_H_

/* Extends */

/* Depends */
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief       Indefinite array template structure.
 *
 * Use this macro as a template for creating indefinite array structures of
 * particular type.
 *
 * To create an indefinite array structure of type int32_t do the following:
 *
 * @code
 * struct my_vector F__INDEFINITE_ARRAY_T(int32_t);
 * @endcode
 *
 * @param       element_type is any C type that should be stored in array.
 *
 * @note        This is a macro type declaration.
 */
#define F__INDEFINITE_ARRAY_T(element_type)                                    \
    {                                                                          \
        element_type * storage;                                                \
        size_t         size;                                                   \
    }

/**
 * @brief       Static indefinite array initializer.
 *
 * This initializer is used to statically initialize an indefinite array.
 *
 * @code
 * int32_t integers[10];
 * struct my_array_type F__INDEFINITE_ARRAY_T(int32_t);
 *
 * struct my_array_type my_array = F__INDEFINITE_ARRAY_INITIALIZER(integers);
 * @endcode
 *
 * @param       c_array is a pointer to C array of elements.
 *
 * @note        This is a macro type definition.
 */
#define F__INDEFINITE_ARRAY_INITIALIZER(c_array)                               \
    {                                                                          \
        .storage = (c_array),                                                  \
        .size    = (sizeof((c_array)) / sizeof((c_array)[0])),                 \
    }

/**
 * @brief       Initialize indefinite array instance during runtime.
 *
 * Use this macro when you want to initialize an indefinite array during
 * runtime.
 *
 * @code
 * int32_t my_array_storage[10];
 * struct my_array_type F__INDEFINITE_ARRAY_T(int32_t) my_array;
 *
 * F__INDEFINITE_ARRAY_INIT(&my_array, my_array_storage);
 * @endcode
 *
 * @param       indefinite_array is a pointer to array structure instance
 *              defined by @ref F__INDEFINITE_ARRAY_T macro.
 * @param       c_array is a pointer to C array of elements that will contain
 *              actual element instances.
 *
 * @note        This is a macro function.
 */
#define F__INDEFINITE_ARRAY_INIT(indefinite_array, c_array)                    \
    do {                                                                       \
        (indefinite_array)->storage = (c_array);                               \
        (indefinite_array)->size = (sizeof((c_array)) / sizeof((c_array)[0])); \
    } while (0)

/**
 * @brief       Definite array template structure.
 *
 * Use this macro as a template for creating definite array structures of
 * particular type and size.
 *
 * To create a definite array structure of type int32_t with 10 elements do the
 * following:
 *
 * @code
 * struct my_array F__DEFINITE_ARRAY_T(int32_t, 10);
 * @endcode
 *
 * @param       element_type is any C type that should be stored in array.
 * @param       size is an integer specifying how big that array should be.
 *
 * @note        This is a macro type declaration.
 */
#define F__DEFINITE_ARRAY_T(element_type, size)                                \
    {                                                                          \
        struct F__INDEFINITE_ARRAY_T(element_type) base;                       \
        element_type elements[size];                                           \
    }

/**
 * @brief       Definite array template structure from indefinite.
 *
 * Use this macro as a template for creating definite array structures of
 * particular type and size.
 *
 * To create an array structure of type int32_t with 10 elements do the
 * following:
 *
 * @code
 * struct my_array F__INDEFINITE_ARRAY_T(int32_t);
 *
 * struct F__ARRAY_VECTOR_FROM_T(struct my_array, int32_t, 10)
 *     my_array_of_10 = F__DEFINITE_ARRAY_INITIALIZER(&my_array_of_10);
 *
 * struct F__ARRAY_VECTOR_FROM_T(struct my_array, int32_t, 20)
 *     my_array_of_20 = F__DEFINITE_ARRAY_INITIALIZER(&my_array_of_20);
 *
 * void function_a(struct my_array * array);
 *
 * function_a(&my_array_of_10.base);
 * function_a(&my_array_of_20.base);
 * @endcode
 *
 * @param       indefinite_array_type is a indefinite array type defined by
 *              @ref F__INDEFINITE_ARRAY_T macro.
 * @param       element_type is any C type that should be stored in array.
 * @param       size is an integer specifying how big that array should be.
 *
 * @note        This is a macro type declaration.
 */
#define F__DEFINITE_ARRAY_FROM_T(indefinite_array_type, element_type, size)    \
    {                                                                          \
        indefinite_array_type base;                                            \
        element_type          elements[size];                                  \
    }

/**
 * @brief       Static definite array initializer.
 *
 * This initializer is used to statically initialize a definite array.
 *
 * @code
 * struct my_array_type F__DEFINITE_ARRAY_T(int32_t, 10);
 *
 * struct my_array_type my_array = F__DEFINITE_ARRAY_INITIALIZER(&my_array);
 * @endcode
 *
 * @param       definite_array is a pointer to defininite array structure
 *              defined by @ref F__DEFINITE_VECTOR_T or
 *              @ref F__DEFINITE_ARRAY_FROM_T macro which is to be initialized.
 *
 * @note        This is a macro type definition.
 */
#define F__DEFINITE_ARRAY_INITIALIZER(definite_array)                            \
    {                                                                            \
        .base     = F__INDEFINITE_ARRAY_INITIALIZER((definite_array)->elements), \
        .elements = {0},                                                         \
    }

/**
 * @brief       Initialize definite array instance durint runtimer.
 *
 * Use this macro when you want to initialize an definite array during runtime.
 *
 * @code
 * struct my_array_type F__DEFINITE_ARRAY_T(int32_t, 10) my_array;
 *
 * F__DEFINITE_ARRAY_INIT(&my_array);
 * @endcode
 *
 * @param       definite_array is a pointer to array structure instance defined
 *              by @ref F__DEFINITE_ARRAY_T macro.
 *
 * @note        This is a macro function.
 */
#define F__DEFINITE_ARRAY_INIT(definite_array)                                 \
    do {                                                                       \
        F__INDEFINITE_ARRAY_INIT(                                              \
                &(definite_array)->base,                                       \
                (definite_array)->elements);                                   \
        memset(&(definite_array)->elements,                                    \
               0,                                                              \
               sizeof((definite_array)->elements));                            \
    } while (0)

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              array.
 *
 * @param       indefinite_array is a pointer to indefinite array.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in array.
 *
 * @code
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 3) my_array;
 * F__DEFINITE_ARRAY_INIT(&my_array);
 *
 * int * current;
 * for (F__ARRAY_EACH(&my_array.base, current))
 * {
 *     *current = ...; // Do something with current
 * }
 * @endcode
 *
 * @note        This is a macro for loop helper.
 */
#define F__ARRAY_EACH(indefinite_array, current)                               \
    size_t i = (current = &(indefinite_array)->storage[0], 0u);                \
    i < (indefinite_array)->size;                                              \
    current = &(indefinite_array)->storage[i + 1], i++

/**
 * @brief       Construct for @a FOR loop to iterate over each element in an
 *              array and an index.
 *
 * @param       indefinite_array is a pointer to indefinite array.
 * @param       current is a variable name that is going to hold pointer to a
 *              current element in array.
 * @param       index is a variable name that is going to hold current index.
 *
 * @code
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 3) my_array;
 * F__DEFINITE_ARRAY_INIT(&my_array);
 *
 * int * current;
 * size_t index;
 * for (F__ARRAY_EACH_INDEX(&my_array.base, current, index))
 * {
 *     *current = ... index; // Do something with current and index
 * }
 * @endcode
 *
 * @note        This is a macro for loop helper.
 */
#define F__ARRAY_EACH_INDEX(indefinite_array, current, index)                  \
    size_t i = (index = 0, current = &(indefinite_array)->storage[0], 0u);     \
    i < (indefinite_array)->size;                                              \
    current = &(indefinite_array)->storage[i + 1], index++, i++

/**
 * @brief       Copy contents of `src` to `dst` indefinite array.
 *
 * @code
 * struct my_array_type F__DEFINITE_ARRAY_T(int, 5);
 * struct my_array_type my_array_a;
 * F__DEFINITE_ARRAY_INIT(&my_array_a);
 *
 * struct my_array_type my_array_b;
 * F__DEFINITE_ARRAY_INIT(&my_array_b);
 *
 * assert(my_array_a.base.size == my_array_b.base.size);
 * F__ARRAY_COPY(&my_array_a.base, &my_array_b.base);
 * @endcode
 *
 * @param       indefinite_array_src is a pointer to source indefinite array.
 * @param       indefinite_array_dst is a pointer to destination indefinite
 *              array.
 *
 * @pre         The source and destination sizes must be equal.
 *
 * @note        This is a macro function.
 */
#define F__ARRAY_COPY(indefinite_array_src, indefinite_array_dst)              \
    memcpy((indefinite_array_dst)->storage,                                    \
           (indefinite_array_src)->storage,                                    \
           ((indefinite_array_dst)->size                                       \
            * sizeof(*(indefinite_array_src)->storage)))

typedef struct f__array_uint8   F__INDEFINITE_ARRAY_T(uint8_t) f__array_uint8;
typedef struct f__array_int8    F__INDEFINITE_ARRAY_T(int8_t) f__array_int8;
typedef struct f__array_uint16  F__INDEFINITE_ARRAY_T(uint16_t) f__array_uint16;
typedef struct f__array_int16   F__INDEFINITE_ARRAY_T(int16_t) f__array_int16;
typedef struct f__array_uint32  F__INDEFINITE_ARRAY_T(uint32_t) f__array_uint32;
typedef struct f__array_int32   F__INDEFINITE_ARRAY_T(int32_t) f__array_int32;
typedef struct f__array_uint64  F__INDEFINITE_ARRAY_T(uint64_t) f__array_uint64;
typedef struct f__array_int64   F__INDEFINITE_ARRAY_T(int64_t) f__array_int64;
typedef struct f__array_uintptr F__INDEFINITE_ARRAY_T(uintptr_t)
        f__array_uintptr;
typedef struct f__array_intptr F__INDEFINITE_ARRAY_T(intptr_t) f__array_intptr;
typedef struct f__array_size   F__INDEFINITE_ARRAY_T(size_t) f__array_size;
typedef struct f__array_void
F__INDEFINITE_ARRAY_T(void *) f__array_void;
typedef struct f__array_any
F__INDEFINITE_ARRAY_T(void) f__array_any;

#endif /* FOUNDATION_ARRAY_H_ */
/** @} */
