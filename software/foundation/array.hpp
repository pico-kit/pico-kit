/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        array.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation array API header.
 *
 * This header file contains array API.
 *
 * Header contains interface for timer:
 * - initialization and deinitialization,
 * - accessing members
 * - iterating
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_array_cpp Array (C++)
 *
 * @brief       Array API (C++).
 *
 * # Array definition and creation
 *
 * There are two variants of array depending on how they are created:
 * - indefinite array and
 * - definite array
 *
 * Both arrays can contain fixed number of elements. In case of indefinite
 * array, the number of elements is different from instannce to instance, while
 * all instances of definite array have the same size.
 *
 * @{
 */

#ifndef FOUNDATION_ARRAY_HPP_
#define FOUNDATION_ARRAY_HPP_

#include <cstddef>

namespace pk
{
namespace array
{

template <typename T>
class IndefiniteArray
{
   public:
   protected:
   private:
    std::size_t nbr_of_elements;
    T *         elements;
};

template <typename T>
using Array = IndefiniteArray<T>;

}  // namespace array
}  // namespace pk

#endif /* FOUNDATION_ARRAY_HPP_ */
