/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Panic implementation.
 */

/* Implements */
#include "foundation/panic.h"

/* Depends */
#include <stdio.h>
#include <stdlib.h>

F__PLATFORM_ATTR_NO_RETURN
void
f__panic_handler(const char * message)
{
    if (message != NULL) {
        fputs(message, stderr);
    } else {
        fputs("Invalid message provided to panic_handler().", stderr);
    }
    abort();
}
