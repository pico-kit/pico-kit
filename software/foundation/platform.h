/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        platform.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation platform API header.
 *
 * This header file contains platform API.
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_platform Platform
 *
 * @brief       Platform API.
 *
 * @{
 */

#ifndef FOUNDATION_PLATFORM_H_
#define FOUNDATION_PLATFORM_H_

#include <stddef.h>
#include <stdint.h>

/**
 * @brief Macro that represents the current function name.
 *
 * This macro expands to a string literal that represents the name of the
 * current function. The specific value depends on the compiler and language
 * standard being used.
 *
 * - For C++ code compiled with the GNU Compiler Collection (__GNUC__), the
 *   `__PRETTY_FUNCTION__` macro is used, which provides demangled function
 *   names.
 * - For C99 code (__STDC_VERSION__ >= 199901L), the `__func__` macro is used,
 *   as required by the C99 standard.
 * - For older versions of GCC (__GNUC__ >= 2) that do not have `__func__`, the
 *   `__FUNCTION__` macro is used.
 * - For ARM Compiler (__ARMCC_VERSION), the `__func__` macro is used.
 * - For IAR Embedded Workbench (__ICCARM__), the `__func__` macro is used.
 * - For Microsoft Visual C++ Compiler (_MSC_VER), the `__FUNCTION__` macro is
 *   used.
 *
 * If none of the conditions are met, the macro is defined as `(char *) 0`,
 * indicating that the detection of the function name failed.
 *
 * @note        This is a macro compiler attribute.
 */
#if defined __cplusplus && defined __GNUC__
/* Use g++'s demangled names in C++.  */
#define F__PLATFORM_FUNCTION __PRETTY_FUNCTION__
#elif defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
/* C99 requires the use of __func__.  */
#define F__PLATFORM_FUNCTION __func__
#elif defined(__GNUC__) && __GNUC__ >= 2
/* Older versions of gcc don't have __func__ but can use __FUNCTION__.  */
#define F__PLATFORM_FUNCTION __FUNCTION__
#elif defined(__ARMCC_VERSION)
/* ARM Compiler (__ARMCC_VERSION) supports __func__ macro. */
#define F__PLATFORM_FUNCTION __func__
#elif defined(__ICCARM__)
/* IAR Embedded Workbench (__ICCARM__) supports __func__ macro. */
#define F__PLATFORM_FUNCTION __func__
#elif defined(_MSC_VER)
/* Microsoft Visual C++ Compiler (_MSC_VER) supports __FUNCTION__ macro. */
#define F__PLATFORM_FUNCTION __FUNCTION__
#else
/* Failed to detect function name support. */
#define F__PLATFORM_FUNCTION ((char *) 0)
#endif

/**
 * @brief 		Attribute or specifier for functions that do not return.
 *
 * This macro defines the attribute or specifier to indicate that a function
 * does not return. The specific attribute or specifier depends on the compiler
 * being used.
 *
 * - For the GNU Compiler Collection (__GNUC__), the `noreturn` attribute is
 *   used.
 * - For the ARM Compiler (__ARMCC_VERSION), the `noreturn` specifier is used.
 * - For the IAR Embedded Workbench (__ICCARM__), the `noreturn` specifier is
 *   used.
 * - For the Microsoft Visual C++ Compiler (_MSC_VER), the `noreturn` specifier
 *   is used.
 *
 * @note 		If the compiler is not supported the attribute request will not
 * 				be fulfilled.
 *
 * @note        This is a macro function.
 */
#if defined(__GNUC__)
#define F__PLATFORM_ATTR_NO_RETURN __attribute__((noreturn))
#elif defined(__ARMCC_VERSION)
#define F__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#elif defined(__ICCARM__)
#define F__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#elif defined(_MSC_VER)
#define F__PLATFORM_ATTR_NO_RETURN __declspec(noreturn)
#else
#define F__PLATFORM_ATTR_NO_RETURN
#endif

/**
 * @brief 		Retrieves the containing structure pointer from a member
 * pointer.
 *
 * This macro calculates the address of the structure that contains the
 * specified member pointer. It does so by subtracting the offset of the member
 * within the structure from the given member pointer.
 *
 * @param 		ptr The member pointer.
 * @param 		type The type of the containing structure.
 * @param 		member The name of the member within the structure.
 * @return 		Pointer to the containing structure.
 *
 * @note 		This macro assumes a compiler with standard behavior for type
 * 				punning and structure layout. It has been verified to work with
 * 				the ARM, IAR and MSVC compilers.
 *
 * @note        This is a macro function.
 */
#define F__PLATFORM_CONTAINER_OF(ptr, type, member)                            \
    ((type *) ((char *) (uintptr_t) (ptr) - offsetof(type, member)))

/**
 * @brief 		Retrieves the const-containing structure pointer from a const
 * 				member pointer.
 *
 * This macro calculates the address of the structure that contains the
 * specified const member pointer. It does so by subtracting the offset of
 * the const member within the structure from the given const member pointer.
 *
 * @param 		ptr The const member pointer.
 * @param 		type The type of the const-containing structure.
 * @param 		member The name of the const member within the structure.
 * @return 		Pointer to the const-containing structure.
 *
 * @note 		This macro assumes a compiler with standard behavior for type
 * 				punning and structure layout. It should be used in conjunction
 * 				with the @ref F__PLATFORM_CONTAINER_OF macro for non-const
 * 				member pointers. It has been verified to work with the ARM, IAR
 * 				and MSVC compilers.
 *
 * @note        This is a macro function.
 */
#define F__PLATFORM_CONTAINER_OF_CONST(ptr, type, member)                      \
    ((type *) ((const char *) (uintptr_t) (ptr) - offsetof(type, member)))

#endif /* FOUNDATION_PLATFORM_H_ */
/** @} */
