/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        error.hpp
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation error C++ API header.
 *
 * This header file contains error C++ API.
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_error_cpp Error (C++)
 *
 * @brief       Error C++ API.
 *
 * Common exception (error) handling of all Pico-Kit feature functions.
 *
 * @{
 */

#ifndef FOUNDATION_ERROR_HPP_
#define FOUNDATION_ERROR_HPP_

#include <cstdint>
#include "foundation/error.h"

namespace pk
{

/**
 * @brief       Error code values returned by Pico-Kit feature functions.
 */
enum class Error : std::uint16_t {
    /**
     * @brief   Operation completed successfully
     *
     * This enumerator is returned when there was no error during the
     * operation. This enumerator will always be equal to zero, so errors can
     * be checked with simple conditions like:
     *
     * @code
     * int error;
     * // Call an API function setting the `error` variable
     * if (error) {
     *      // Do something in case of error
     * }
     * @endcode
     */
    NONE = F__ERR_NONE,

    /**
     * @brief       Operation could not be completed because of insufficient
     *              free heap memory.
     *
     * It was impossible to allocate or reserve heap memory for an operation.
     */
    NO_MEM = F__ERR_NO_MEM,

    /**
     * @brief       Unspecified error
     */
    UNSPECIFIED = F__ERR_UNSPECIFIED
};

}  // namespace pk

#endif /* FOUNDATION_ERROR_HPP_ */
/** @} */
