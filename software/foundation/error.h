/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file        error.h
 * @date        2024-07-01
 *
 * @author      (NRA) Nenad Radulovic (nenad.b.radulovic@gmail.com)
 *
 * @li          2024-07-01  (NRA) Initial implementation
 *
 * @brief       Foundation error API header.
 *
 * This header file contains error API.
 */
/**
 * @ingroup     foundation
 * @defgroup    foundation_error Error
 *
 * @brief       Error API.
 *
 * Common exception (error) handling of all Pico-Kit feature functions.
 *
 * @{
 */

#ifndef FOUNDATION_ERROR_H_
#define FOUNDATION_ERROR_H_

/**
 * @brief       Error code values returned by Pico-Kit feature functions.
 */
enum f__err {
    /**
     * @brief   Operation completed successfully
     *
     * This enumerator is returned when there was no error during the
     * operation. This enumerator will always be equal to zero, so errors can
     * be checked with simple conditions like:
     *
     * @code
     * int error;
     * // Call an API function setting the `error` variable
     * if (error) {
     *      // Do something in case of error
     * }
     * @endcode
     */
    F__ERR_NONE = 0x0000u,

    /**
     * @brief       Operation could not be completed because of insufficient
     *              free heap memory.
     *
     * It was impossible to allocate or reserve heap memory for an operation.
     */
    F__ERR_NO_MEM = 0x0001u,

    /**
     * @brief       Unspecified error
     */
    F__ERR_UNSPECIFIED = 0xffffu,
};

#endif /* FOUNDATION_ERROR_H_ */
/** @} */
