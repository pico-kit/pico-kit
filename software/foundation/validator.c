/*
 * SPDX-License-Identifier: LGPL-3.0-only
 */
/**
 * @file
 * @brief   Validator implementation.
 */

/* Implements */
#include "foundation/validator.h"

/* Depends */
#include <stdio.h>

#include "foundation/panic.h"

F__PLATFORM_ATTR_NO_RETURN
void
f__validator_failure_handler(const char * text, uint32_t error_code)
{
    static char buffer[100];

    int status = snprintf(
            buffer,
            sizeof(buffer),
            "Error code: %u\n%s\n",
            error_code,
            text);
    if (status < 0) {
        f__panic_handler("Failed to handle validator error");
    } else {
        f__panic_handler(buffer);
    }
}
