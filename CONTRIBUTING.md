# Contributing

---
## Introduction

We'd love to accept your sample apps and patches!

Please, have a look at developer guidelines and coding standard in 
[documentation/DEVELOPER_MANUAL.md]

---
## Forking

Only owners of the Pico Kit project have write permission to the Git
repositories of the project. Contributors should *fork* `pico-kit.git` into
their own account, then work on this forked repository.

Note that the fork only has to be performed once.

---
## Contributing a patch

1. Create your change to the repo in question.
   * Fork the repo, develop and test your code changes.
   * Ensure that your code is clear and comprehensible.
2. Submit a pull request.
3. The repo owner will review your request. If it is approved, the change will
   be merged. If it needs additional work, the repo owner will respond with
   useful comments.

[documentation/DEVELOPER_MANUAL.md]: documentation/DEVELOPER_MANUAL.md
