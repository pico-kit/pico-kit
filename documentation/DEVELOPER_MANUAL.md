# Developer manual

# Table of contents

- [Developer manual](#developer-manual)
- [Table of contents](#table-of-contents)
  - [Intro](#intro)
  - [Software granularity hierarchy](#software-granularity-hierarchy)
  - [Style guide and rules](#style-guide-and-rules)
    - [Abbreviations](#abbreviations)
    - [General naming rules](#general-naming-rules)
    - [Function classes naming rules](#function-classes-naming-rules)
    - [Structure and union member naming rules](#structure-and-union-member-naming-rules)
    - [Right margin](#right-margin)
    - [Indentation](#indentation)
    - [Fixed or predefined values](#fixed-or-predefined-values)
    - [Comments](#comments)
    - [Error handling guideline](#error-handling-guideline)
  - [Pico-Kit Architecture](#pico-kit-architecture)
    - [Dependency Inversion Pattern](#dependency-inversion-pattern)
      - [Problem](#problem)
      - [Solution](#solution)
    - [Domain](#domain)
    - [Feature](#feature)
  - [Development processes](#development-processes)
    - [Repository rules](#repository-rules)
    - [Pull requests (PR)](#pull-requests-pr)
    - [Creating commits](#creating-commits)
    - [Creating branches](#creating-branches)
  - [Building HTML or PDF documentation](#building-html-or-pdf-documentation)
    - [Developer machine setup](#developer-machine-setup)
  - [Testing Pico-Kit Event Driven System implementation](#testing-pico-kit-event-driven-system-implementation)
    - [Testing on Ubuntu 24.04 machine](#testing-on-ubuntu-2404-machine)
    - [Testing on openSUSE Leap 15.2 machine](#testing-on-opensuse-leap-152-machine)
    - [Testing using Docker](#testing-using-docker)
  - [Version information](#version-information)


## Intro

__Intended audience__ of this document:

* Developers who will work on Pico-Kit sources (required).
* Testers who test Pico-Kit sources (informative).
* Users of the Pico-Kit (informative).

This document describes:
- coding style guidelines and rules used in Pico-Kit sources,
- software granularity,
- design patterns that should be used,
- development process.

## Software granularity hierarchy

If we are to abstract from particular languages, frameworks and their own
interpretations, the abstract software granularity hierarchy in Pico-Kit
software package is the following:

- __Package__ - Plain and simple, a __package__ is a working collection of
  connected functional modules.
- __Module__ - As the very name implies, the motivation of a __module__ is
  modularity. Contrary to what many claim, it does not really imply code reuse.
  There are many modules which are not really reusable, and don't fit with
  anything they were not designed to.

  It is important to separate different software layers, that makes software
  much easier to implement and maintain, and should the need to reimplement
  something like a front end to a different GUI framework, modularity enables
  that to happen in an easy and safe manner, without breaking code all over the
  place.

  A __module__ encapsulates a collection of components which all serve a common
  purpose as defined by the __module__ requirements. A __module__ should be
  self-contained and complete, and while not really usable on its own, it
  should be able to work in conjunction with any conforming implementation.
- __Component__ - In terms of granularity the __component__ sits between the
  module and the object. The purpose of a __component__ is to put together a
  collection of general purpose objects to form a purpose specific unit.

  As the name implies, unlike the Module, the __component__ is not
  "self-contained", it is a part of a larger functional whole.
- __Object__ - __Objects__ are the smaller building blocks of components.
  __Objects__ are collections of primitives and couple them together to serve a
  lower level, more universal while still somewhat specific purpose.
- __Primitive__ - __Primitives__ are the smallest, simplest and lowest level of
  software development granularity. It's basically just integer and real
  numbers and functions or operators, although most languages have their own
  additional "first class citizens".

  There is very little what you can do with __primitives__, and at the same
  time, it is at a such a low level that you can accomplish practically
  everything with it. It is just very, very verbose, insanely complicated and
  impossibly tedious to accomplish while directly working with __primitives__.

```plantuml
@startuml
title Software granularity hierarchy

package Package {
  package Module {
    package Component {
      component Object {
        component Primitive
      }
    }
  }
}
@enduml
```

## Style guide and rules
 
Style guide is a set of rules which are aimed to help create readable and
maintainable code.  By writing code which looks the same way across the code
base will help others read and understand the code. By using the same
conventions for spaces, newlines and indentation chances are reduced that
future changes will produce huge unreadable diffs. By following the common
patterns for code structure and by using language features consistently will

### Abbreviations

The Pico-Kit sources shall use the following abbreviations:

| Full name                 | Usages      |
|---------------------------|-------------|
| Error                     | err         |
| Attrubute                 | attr        |
| Vector                    | vect        |
| Array                     | arr         |
| Queue                     | q           |
| State machine             | sm          |
| Event                     | evt         |
| Channel                   | chn         |
| Agent                     | agt         |
| Network                   | netw        |
| Task                      | tsk         |
| Tasker                    | tskr        |
| Scheduler                 | sched       |
| Thread                    | thr         |
| Kernel                    | krnl        |
| Semaphore                 | sem         |
| Mutex                     | mtx         |
| Timer                     | tmr         |
| Memory                    | mem         |
| Iterator                  | it          |

### General naming rules

- __R0100__: All functions, variables and types are using lower case names,
  words are delimited by underscore `_` character. Examples:

  ```c
  void do_something(void);
  uint32_t some_variable;
  ```

- __R0101__: All macro names are in `UPPERCASE` style, words are delimited by
  underscore `_` character.
- __R0102__: All functions, variables and types are prefixed with module
  prefix:
  - Symphony module: `s__`,
  - Harmony module: `h__`,
  - Reactive module: `r__`, 
  - Foundation module: `f__`,
  - Chameleon module: `c__`,
  - Verifinex module: `v__`.
- __R0103__: All typedefs are named in the same manner as structure or union
  types. No suffixes (like the usual `_t` suffix) are allowed.
- __R0104__: All macro names are prefixed with module prefix: 
  - Symphony module: `S__`,
  - Harmony module: `H__`,
  - Reactive module: `R__`,
  - Foundation module: `F__`,
  - Chameleon module: `C__`,
  - Verifinex module: `V__`.
- __R0105__: In addition to __R0102__, the global public variables are
  additionally prefixed with `g__`, for example, a global variable in Reactive
  module results in `r__g__` prefix.
- __R0106__: In addition to __R0102__, the global local (file scope) variables
  are additionally prefixed with `l__`, for example, a global local scope
  variable in Reactive module results in `r__l__` prefix.
- __R0107__: In addition to __R0102__, the global private variables or
  functions are additionally prefixed with `p__`, for example, a global private
  variable or function in Reactive module results in `r__p__` prefix.
- __R0108__: The name of API functions is constructed in the following way:

  ```
  [module]__[component]_[function_name]
  ```

  Where:
  - `[module]` is standard prefix as defined by __R0102__.
  - `__` is component and function name separator.
  - `[component]` is the name of the component which function is part of.
  - `[function_name]` is the name of the function.

  Examples:
  - In the following example, the function name is  `do_something` and it is
    part of API for `list` component in Reactive module:

    ```c
    void r__list_do_something(void);
    ```

  - The function name is `cancel` and it is part of API for `timer` component in
    Reactive module:

    ```c
    void r__timer_cancel(r__timer * tmr);
    ```

  - The function name is `long_name_operation` and it is part of API for
    `long_name_component` component:

    ```c
    void r__long_name_component_long_name_operation(void);
    ```

### Function classes naming rules

There are two function classes defined:
- __I class__ - For proper operation these functions require that regular
  interrupts are locked.
- __S class__ - For proper operation these functions require that preemptive
  scheduler is locked.

All API functions may be suffixed if they fall are into either I or S class
categories: 
- __R0200__: - I class - Regular interrupts are locked. Function suffix is `_i`. 
- __R0201__: - S class - Scheduler switching is locked. Function suffix is `_s`.

### Structure and union member naming rules

If a structure is part of public API and has members that are private and public
the private members are named differently. Members which are private and not to
be accessed and modified by application code must follow the following naming
rules:

- __R0400__: Private members must use `p__` prefix in order to tell to
  programmer that the member is private to the structure.

If a structure is not itended to be used by application code (for example, using
an opaque pointer pattern) it does not need to have prefixed members.

### Right margin

- __R0500__: The hard wrap is set to column 80.

### Indentation

- __R0600__: Macro body starts either on column 45 if the body fits in between
  the column and the right ruler defined by __R0500__. If the macro body does
  not fit or the macro name is too long, the macro body starts on next line
  with `\` continuation character located one tab before the right margin
  defined by __R0500__ rule, which is the column 77. The body on next line start
  at column 9 (two tabs).

  ```c
  #define F_SAFETY__HAS_STARTED()                                           \
          (f__p__safety_critical_has_started == true)

  #define F_SAFETY__START()                                                 \
          do {                                                              \
              f__p__safety_critical_has_started = true;                     \
          } while (0)
  ```

### Fixed or predefined values

Never code a fixed value directly in code. Always use a macro for this job:

```c
#define SOME_LIMIT_CONDITION 23
int32_t configurable_size_array[SOME_LIMIT_CONDITION];
```

### Comments

The Rob Pike article "Notes on programming in C" section about comments is spot
on:

```
A delicate matter, requiring taste and judgement. I tend to err on the side
of eliminating comments, for several reasons. First, if the code is clear, and
uses good type names and variable names, it should explain itself.  Second,
comments aren't checked by the compiler, so there is no guarantee they're
right, especially after the code is modified.  A misleading comment can be very
confusing. Third, the issue of typography: comments clutter code.

But I do comment sometimes. Almost exclusively, I use them as an
introduction to what follows. Examples: explaining the use of global variables
and types (the one thing I always comment in large programs); as an
introduction to an unusual or critical procedure; or to mark off sections of a
large computation.
```

### Error handling guideline

- __G100__: Error handling in Pico-Kit is using _return value_ method. All
  function prefer returning the type `int` to notify about operation status:

  `int some_function(void);`

- __G101__: The overall look of functions needs to conform to the following
  style:

  ```c
  int function(void)
  {
      int error;

      error = do_something1();

      if (error) goto CLEAN_1;

      error = do_something2();

      if (error) goto CLEAN_2;

      error = do_something3();

      if (error) goto CLEAN_3;

      return R__ERR_NONE;
  CLEAN_3:
          undo_something2();
  CLEAN_2:
          undo_something1();
  CLEAN_1:
      return error;
  }
  ```

- __G102__: If a function does not need to undo actions, the following style
  is also acceptable:

  ```c
  int function(void)
  {
      int error;
      error = do_something1();

      if (error) {
          return error;
      }
      error = do_something2();

      if (error) {
          return error;
      }
      error = do_something3();

      if (error) {
          return error;
      }
      return R__ERR_NONE;
  }
  ```

- __G103__: If a function needs only to ensure that arguments are valid the
  following style should be used:

  ```c
  int function(arg_type arg1, arg_type arg2, arg_type arg3)
  {
      if (F_ARG_IS_INVALID(arg1)) {
          return R__ERR_X1;
      }
      if (F_ARG_IS_INVALID(arg2)) {
          return R__ERR_X2;
      }
      if (F_ARG_IS_INVALID(arg3)) {
          return R__ERR_X3;
      }

      /* ... */

      return R__ERR_NONE;
  }
  ```

## Pico-Kit Architecture

Some common patterns used in Pico-Kit package architecture:
- Dependency Inversion Pattern

### Dependency Inversion Pattern

#### Problem

In software often comes a case where __Component A__ depends on __Component
B__ but that breaks abstraction layers and introduces a word that is common in
software: spaghetti code. Spaghetti code is the general term used for any
source code that's hard to understand because it has no defined structure.
While an end user might not see anything wrong with a program, a programmer
might find it virtually illegible if the code base's flow is too
convoluted—like a bowl of twisted, tangled spaghetti. To resolve this problem
use the pattern commonly called `Dependency Inversion Pattern`.

```plantuml
@startuml
component "Component A" <<Component>> {
  file component_a.c as source_a
  file component_a.h as interface_a
  interface_a <.. source_a : realizes
}

component "Component B" <<Component>> {
  file component_b.c as source_b
  file component_b.h as interface_b
  source_b ..> interface_b : realizes
}

source_a -> interface_b
@enduml
```

#### Solution

```plantuml
@startuml
component "Component A" <<Component>> {
  file component_a.c as source_a
  file component_a.h as interface_a
  file component_a_dependency_interface.h as dependency_interface
  source_a -> dependency_interface : depends
  interface_a <.. source_a : realizes
}

component "Component B" <<Component>> {
  file component_b.c as source_b
  file component_b.h as interface_b
  source_b ..> interface_b : realizes
}

component "Component A dependency adapter" <<Component>> {
   file component_a_dependency_adapter.c as adapter
}

dependency_interface <. adapter : realizes
adapter -> interface_b : depends
@enduml
```

### Domain

### Feature

## Development processes

### Repository rules

1. Direct push to `master` or `main` is forbidden.
2. Always create pull requests which are associated with a issue number.
3. Always create commit messages which contain reference to issue number.

### Pull requests (PR)

![Pull request use-case](pull_request_use_case.png)

The Author/Reviewer(s)/Repo admin workflow is the following:

1. When __Author__ starts working on a task it first creates a branch.
2. While working on the task it pushes commits to the branch created in step 1.
3. When the __Author__ completes the task it creates a PR with a description
   and specifies the reviewers in the __Reviewers__ group. This creates a
   notification for involved reviewers.  __Author__ and __Repo Admin__ are
   waiting on __Reviewers__ approval(s).
4. Once notified, all reviewers review the commits by reading the code.
5. If a reviewer from __Reviewers__ group find an issue it will create a change
   request. This is done by creating a comment for code or file. This creation
   notifies __Author__ that it needs to take an action.
6. __Author__
   implements changes and pushes the change commits on the same branch.
7.  __Reviewers__ review the commits again and confirms the issue is corrected
   by closing the change request.
8. When __Reviewers__ finds no more issues it will approve the PR. This
  generates notification for __Repo Admin__. At this point __Repo Admin__ will
  evaluate if all mandatory reviews and approvals are in place. If not, it
  still waits for reviews and approvals of other reviewers in __Reviewers__.
9. During a code review a merge conflict may arise. This merge conflict is
  detected by repository and the repository notifies the __Author__.
10. __Author__ implements merge fixes and pushes the merge commits to the same
    branch.
11. Once merge conflicts are resolved the repository notifies the __Author__.
12. __Author__ has completed all merge fixes and then resets the review(s).
    This creates another batch of notification for __Reviewers__ and __Repo
    Admin__. Since the review process is restarted actors go through steps 4 to
    8.
13. Once all issues are resolved and all reviews and approvals are in place
    __Repo Admin__ may initiate merge procedure.  Once the merging is done the
    repository will notify __Author__.
14. __Author__ closes the task and proceeds to delete the branch created in
    step 1. This step completes the PR procedure.

### Creating commits

When creating commits do not put multiple unrelated changes into a single
commit. For example, `daily commit` which contains work for whole day on
multiple things should be avoided. If it is not possible to avoid these ugly
commits always add `WIP` to the commit section.

Each commit brief message should contain the issue number so it can be easily
associated with issue management software. Usually, the issue number is a set
of string and numbers, like: `NCC-11`, `PK-12`, etc. Many issue management
software supports linking an issue with commit message only if the issue number
is the first thing commit brief message. Divide issue number from commit brief
text using `:` delimiter. Examples of commit brief messages are:

- `NCC-11: Created Python template repository.`
- `PK-12: WIP: Updateing the library build system.`

Besides writing commit brief message it is encouraged to also write down commit
description text which in greater detail explains the commit.

The commit brief text should not be longer then 80 columns.

The commit description text shall be hard wrapped to 80 columns. Exceptions to
this rule are URL, file paths or other text that should not be divided. 

Each commit message should be signed-off by the author, use the `-s` or
`--signoff` flag:

```shell
git commit -s
```

It is also desirable to always GPG-sign commits, use the `-S` flag:

```shell
git commit -s -S
```

For instructions how to setup git for GPG-sign refer to:
https://docs.gitlab.com/ee/user/project/repository/signed_commits/gpg.html

### Creating branches

This project uses GitLab Flow.

GitLab Flow is a simpler alternative to GitFlow that combines feature-driven
development and feature branching with issue tracking.

With GitFlow, developers create a develop branch and make that the default
while GitLab Flow works with the main branch right away.

GitLab Flow is great when you want to maintain multiple environments and when
you prefer to have a staging environment separate from the production
environment. Then, whenever the main branch is ready to be deployed, you can
merge back into the production branch and release it.

Thus, this strategy offers propers isolation between environments allowing
developers to maintain several versions of software in different environments.

While GitHub Flow assumes that you can deploy into production whenever you
merge a feature branch into the master, GitLab Flow seeks to resolve that issue
by allowing the code to pass through internal environments before it reaches
production.

The following branches are used in this workflow:
- `main` or `master` branch. This branch is the default working branch. All
  developers are branching out from this branch. All of the work eventually
  gets merged back to this branch.
- `feature` branch. This branch is created from `main` or `master` branch. The
  feature branch is created when a new issue is started on. The name of this
  branch should include the issue number. 
  The name of the branch is constructed in the following manner:
  1. First, each feature branch is started with: `feature/`
  2. Second, the feature branch should reference the issue number:
     `feature/ncc-11`. The issue string is converted to lowercase.
  3. After issue number the issue name is stated:
     `feature/ncc-11-create-project-documentation`

  Both, issue number and issue name must follow the rules:
  1. All white space characters are converted to `-`
  2. If there are multiple consecutive space characters they are all represented
     with single `-` symbol. For example, if the issue number and name is
     `NCC - 11: Create Project template (Python)` the branch name shall be
     `feature/ncc-11-create-project-template-python`.
  After you merge a feature branch, you should remove it from the source
  control software. In GitLab, you can do this when merging. Removing finished
  branches ensures that the list of branches shows only work in progress. It
  also ensures that if someone reopens the issue, they can use the same branch
  name without causing problems.  
- `bugfix` branch. This branch is functionally the same as feature branch. The
  only change is naming of the branch so it gets more attention from the
  developers. These types of branches are created when a developer starts on
  issues which are of `bug` type.
- `release` branch. You need to work with release branches if you need to
  release software to the outside world. In this case, each branch contains a
  minor version, such as `release/2-3-stable` or `release/2-4-stable`. Create
  stable branches using main as a starting point, and branch as late as
  possible. By doing this, you minimize the length of time during which you
  have to apply bug fixes to multiple branches. After announcing a release
  branch, only add serious bug fixes to the branch. If possible, first merge
  these bug fixes into main, and then cherry-pick them into the release branch.
  If you start by merging into the release branch, you might forget to
  cherry-pick them into main, and then you’d encounter the same bug in
  subsequent releases. Merging into main and then cherry-picking into release
  is called an “upstream first” policy. Every time you include a bug fix in a
  release branch, increase the patch version (to comply with Semantic
  Versioning) by setting a new tag.

If a merge involves many commits, it may seem more difficult to undo. You might
consider solving this by squashing all the changes into one commit just before
merging by using the GitLab Squash-and-Merge feature. Fortunately, you can undo
a merge with all its commits. The way to do this is by reverting the merge
commit. Preserving this ability to revert a merge is a good reason to always
use the “no fast-forward” (`--no-ff`) strategy when you merge manually.

Avoid merge commits by just using rebase to reorder the commits after the
commits on the main branch. Using rebase prevents a merge commit when merging
main into your feature branch, and it creates a neat linear history. However,
you should avoid rebasing commits in a feature branch that you’re sharing with
others.

## Building HTML or PDF documentation

This component uses Doxygen comments in code to build additional HTML or PDF
documentation.

### Developer machine setup

In order to execute unit-tests the following criteria must be met:

1. Using Ubuntu/Debian/openSUSE Leap machine
2. GNU make is installed
3. Doxygen is installed
4. Preferably `dot` utility is installed

## Testing Pico-Kit Event Driven System implementation

Along the sources unit-tests are bundled in `tests` directory.

Unit-testing can be run in the following ways:

- natively on developers machine
- using Docker virtual machine

### Testing on Ubuntu 24.04 machine

In order to execute unit-tests install the following run-time dependencies:

```shell
sudo apt-get install git make gcc bear clang-format clang-tidy
make test
```

### Testing on openSUSE Leap 15.2 machine

Install the following dependencies:

```shell
sudo zypper install gcc11 git make bear clang-format clang-tidy
GCC_CC=gcc-11 make test
```

### Testing using Docker

The main docker file is `Dockerfile` in top-level project directory. Start the
testing using this Dockerfile. On a Linux machine do:

```shell
make docker-test
```

## Version information

Given a version number `MAJOR.MINOR.PATCH`, increment the:

- `MAJOR` version when you make incompatible API changes,
- `MINOR` version when you add functionality in a backwards compatible manner,
  and
- `PATCH` version when you make backwards compatible bug fixes.

Additional labels for pre-release and build metadata are available as
extensions to the `MAJOR.MINOR.PATCH` format.

