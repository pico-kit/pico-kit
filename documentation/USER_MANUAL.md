# User manual

# Table of contents

- [User manual](#user-manual)
- [Table of contents](#table-of-contents)
  - [Intro](#intro)
  - [General](#general)
    - [Create object](#create-object)
    - [Object Usage](#object-usage)
    - [Object Destruction](#object-destruction)
  - [Harmony](#harmony)
    - [Timers](#timers)
      - [Creation](#creation)
- [Build system](#build-system)
  - [Overview](#overview)


## Intro

__Intended audience__ of this document:

* Users of the Pico-Kit.

## General

### Create object

There are two different approaches to object creation and initialization:

- creation of objects using heap memory,
- creation of objects using statically allocated memory.

All Pico-Kit objects share a common design concept. The overall life-cycle of an
object instance can be summarized as created -> in use -> destroyed.

An object is created by calling its `xxx_create` function. The create function
returns an identifier (as a pointer to object) that can be used to operate with
the new object. The memory layout (and size needed) for the control block is
implementation specific. One should not make any specific assumptions about the
object structure.  The object structure layout might change and hence should be
seen as an implementation internal detail.

In case when object is allocated statically, it must be initilized by calling
its `xxx_init` function. The init function uses the given allocated object and
does the necessary initalization. Object created with `xxx_create` functions are
already initilized when they are returned.

In order to expose control about object specific options many `xxx_create` and
`xxx_create` functions provide an optional `attr ` argument, which can be left
as NULL by default. It takes a pointer to an object specific attribute
structure, commonly containing the fields:

- `name` to attach a human readable name to the object for identification,
- `attr_bits` to control object-specific options,
- `storage` to provide additional memory for the object to use.

The `name` attribute is only used for object identification, e.g.  using
Pico-Kit-aware debugging. The attached string is not used for any other purposes
internally. All objects store only pointer to the string, not the actuall
string, therefore, the string must allocated during the whole lifetime of a
object.


### Object Usage

After an object has been created successfully it can be used until it is
destroyed. The actions defined for an object depends on its type. Commonly
all the `xxx_do_something` access function require the reference to the
object to work with as the first argument.

The access function can be assumed to apply some sort of sanity checking on
the first argument. So that it is assured one cannot accidentally call an
access function with a NULL object reference. Furthermore the concrete
object contents are verified.

As a rule of thumb only non-blocking access function can be used from Interrupt
Service Routines (ISR).

### Object Destruction

Objects that are not needed anymore can be destructed on demand to free the
allocated memory. Objects are not destructed implicitly. Thus one can assume an
object to be valid until `xxx_destroy` is called explicitly.

The delete function finally frees the allocated memory. In case of user provided
memory (statically allocated memory), see above, the memory must be freed
manually as well.

## Harmony

### Timers

#### Creation

# Build system

## Overview

An Pico-Kit project can be seen as an amalgamation of a number of components.
For example, for an application that needs state machines, there could be:

- The Pico-Kit core libraries (`foundation`)
- The adapters (`firmware/*`)
- The Pico-Kit cooperative scheduler (`harmony`)
- The Pico-Kit state machine dispatcher (`reactive`)
- Main code tying it all together

The Pico-Kit makes these components explicit and configurable. To do that, when
a project is compiled, the build system will look up all the components in the
Pico-Kit directories, the project directories and (optionally) in additional
custom component directories. It then allows the user to configure the Pico-Kit
project using a text-based menu system to customize each component. After the
components in the project are configured, the build system will compile the
project.

