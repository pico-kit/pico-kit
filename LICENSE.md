# License

__Copyright (c) 2024 Nenad Radulovic <nenad.b.radulovic@gmail.com>__

The Pico-Kit software is distributed under open source 
`GNU LESSER GENERAL PUBLIC LICENSE (LGPL)`, version `Version 3, 29 June 2007`.
A copy of license file is found in `LICENSE` in the Pico-Kit root directory.

# 3rd party software

The Pico-Kit project uses 3rd party projects:
- Project `Kconfiglib` which is released under ISC license located in
  `build/Kconfiglib` directory:
  __Copyright (c) 2011-2019, Ulf Magnusson <ulfalizer@gmail.com>__
- Project `fff` which is released under MIT license located in `external/fff`
  directory:
  __Copyright (c) 2010 Michael Long__

