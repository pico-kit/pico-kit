/*
 * File:   pk-reactive_port_definition.c
 * Author: nenad
 *
 * Created on April 12, 2023, 5:09 PM
 */

#include <xc.h>
#include "mcc_generated_files/system/system.h"
#include "pico-kit-chameleon.h"

void
pic18f_tick_init(void)
{
    TICK_OverflowCallbackRegister(c__callback_tick_handler);
} /*
   * File:   pk-reactive_port_definition.h
   * Author: nenad
   *
   * Created on April 12, 2023, 5:08 PM
   */

#ifndef PK_REACTIVE_PORT_DEFINITION_H
#define PK_REACTIVE_PORT_DEFINITION_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <xc.h>

struct c__lock {
    uint8_t lock_state;
};

#define r__port_lock_init()                                                    \
    do {                                                                       \
        GIEL = 0;                                                              \
        IPEN = 1;                                                              \
    } while (0)

#define c__lock_save(value)                                                    \
    do {                                                                       \
        (value).lock_state = GIEL;                                             \
        GIEL               = 0;                                                \
    } while (0)

#define c__lock_restore(value)                                                 \
    do {                                                                       \
        GIEL = (value).lock_state & 1;                                         \
    } while (0)

#define c__cpu_sleep()                                                         \
    do {                                                                       \
        SLEEP();                                                               \
    } while (0)

#define c__cpu_wakeup()

#define c__tick_init() pic18f_tick_init()

void
pic18f_tick_init(void);

#ifdef __cplusplus
}
#endif

#endif /* PK_REACTIVE_PORT_DEFINITION_H */
