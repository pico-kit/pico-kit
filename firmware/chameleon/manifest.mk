# manifest.mk

# gcc-any-any-linux adapter
F_SRC_FILES_$(CONFIG_CHAMELEON_ADAPTER_GCC_ANY_ANY_LINUX) += \
    firmware/chameleon/gcc-any-any-linux_platform_chameleon_adapter.c

# gcc-cm0-stm32f0-none adapter
F_SRC_FILES_$(CONFIG_CHAMELEON_ADAPTER_GCC_CM0_STM32F0_NONE) += \
    firmware/chameleon/gcc-cm0-stm32f0-none_platform_chameleon_adapter.c

# xc8-pic18-2xk40 adapter
F_SRC_FILES_$(CONFIG_CHAMELEON_ADAPTER_XC8_PIC18_2XK40_NONE) += \
    firmware/chameleon/xc8-pic18-2xk40-none_platform_chameleon_adapter.c

