/*
 * gcc_cm0_stm32f0_none_lock.c
 *
 *  Created on: Nov 8, 2023
 *      Author: nenad
 */

/* Implements */
#include "chameleon/domain/cpu.h"

/* Depends */
#include <stdint.h>

// use mutex

void
c__critical_init(void)
{
    // Initialize mutex
}

void
c__critical_enter(void)
{
    // Lock mutex
}

void
c__critical_exit(void)
{
    // Unlock mutex
}
/*
 * gcc_cm0_stm32f0_none_cpu.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "chameleon/domain/cpu.h"

/* Depends */
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

void
c__cpu_sleep(void)
{
    // Use a semaphore
}

uint_fast8_t
c__cpu_log2(uint32_t value)
{
    return 31u - (unsigned int) __builtin_clz(value);
}

C__PLATFORM_ATTR_NO_RETURN void
c__cpu_failure_handler(void)
{
    // Assert false?
    while (1)
        ;
}
