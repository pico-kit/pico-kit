/*
 * gcc_cm0_stm32f0_none_lock.c
 *
 *  Created on: Nov 8, 2023
 *      Author: nenad
 */

/* Implements */
#include "chameleon/cpu.h"

/* Depends */
#include <stdint.h>

uint32_t s_isr_nesting;

void
c__critical_init(void)
{
}

void
c__critical_enter(void)
{
    __asm("cpsid i" : : : "memory");  // Disable global interrupts
    s_isr_nesting++;
    __asm("dsb 0xF" : : : "memory");  // Data synchronization barrier
    __asm("isb 0xF" : : : "memory");  // Instruction synchronization barrier
}

void
c__critical_exit(void)
{
    s_isr_nesting--;
    if (s_isr_nesting == 0u) {
        __asm("cpsie i" : : : "memory");  // Enable global interrupts
    }
}
/*
 * gcc_cm0_stm32f0_none_cpu.c
 *
 *  Created on: Nov 7, 2023
 *      Author: nenad
 */

/* Implements */
#include "chameleon/cpu.h"

/* Depends */
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include "cmsis_compiler.h"

static void
print_register_contents(uint32_t * stack_ptr)
{
    printf("Register Contents:\n");
    printf("R0:  0x%08" PRIx32 "\n", stack_ptr[0]);
    printf("R1:  0x%08" PRIx32 "\n", stack_ptr[1]);
    printf("R2:  0x%08" PRIx32 "\n", stack_ptr[2]);
    printf("R3:  0x%08" PRIx32 "\n", stack_ptr[3]);
    printf("R12: 0x%08" PRIx32 "\n", stack_ptr[4]);
    printf("LR:  0x%08" PRIx32 "\n", stack_ptr[5]);
    printf("PC:  0x%08" PRIx32 "\n", stack_ptr[6]);
    printf("PSR: 0x%08" PRIx32 "\n", stack_ptr[7]);
}

static void
print_exception_info(uint32_t * stack_ptr)
{
    uint32_t exception_num = ((stack_ptr[6] & 0xFF000000) >> 24);
    printf("Exception Address/Reason: 0x%08" PRIx32 "\n", stack_ptr[6]);
    printf("Exception Number: %" PRIx32 "\n", exception_num);
}

static void
print_stack_contents(uint32_t * stack_ptr)
{
    printf("Stack Contents:\n");
    for (int i = 0; i < 16; i++) {
        printf("0x%08" PRIx32 ": 0x%08" PRIx32 "\n",
               (uint32_t) (stack_ptr + i),
               *(stack_ptr + i));
    }
}

static void
print_backtrace(uint32_t * frame_ptr)
{
    printf("Backtrace:\n");
    while (frame_ptr != NULL) {
        uint32_t return_address = *(frame_ptr + 1);
        printf("0x%08" PRIx32 "\n", (uint32_t) return_address);
        frame_ptr = (uint32_t *) *frame_ptr;
    }
}

static void __attribute__((noreturn))
assert_failed_handler(uint32_t * stack_ptr)
{
    // Print register contents
    print_register_contents(stack_ptr);

    // Print exception information (if applicable)
    print_exception_info(stack_ptr);

    // Print stack contents
    print_stack_contents(stack_ptr);

    // Print backtrace
    uint32_t * frame_ptr = (uint32_t *) __builtin_frame_address(0);
    print_backtrace(frame_ptr);
    // Enter an infinite loop
    while (1) {
        // Add any necessary error handling or debugging code here
    }
}

void
c__cpu_sleep(void)
{
    __asm("dsb 0xF" : : : "memory");  // Data synchronization barrier
    __asm("wfi");                     // Wait for interrupt
    __asm("isb 0xF" : : : "memory");  // Instruction synchronization barrier
}

uint_fast8_t
c__cpu_log2(uint32_t value)
{
    return 31u - (unsigned int) __builtin_clz(value);
}

C__PLATFORM_ATTR_NO_RETURN void
c__cpu_failure_handler(void)
{
    uint32_t * stack_ptr = (uint32_t *) __get_MSP();
    assert_failed_handler(stack_ptr);
}
/*
 * stdatomic.c
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

/* Implements */
#include <stdatomic.h>

/* Depends */
#include <cmsis_compiler.h>
#include <stdbool.h>
#include <stdint.h>

static unsigned int
irq_disable(void)
{
    unsigned int retval = __get_PRIMASK();
    __disable_irq();
    return retval;
}

static void
irq_restore(unsigned int irq_status)
{
    if (irq_status == 0u) {
        __enable_irq();
    }
}

bool
__atomic_compare_exchange_4(
        volatile void * ptr,
        void *          expected,
        unsigned int    desired,
        bool            weak,
        int             success_memorder,
        int             failure_memorder)
{
    (void) weak;
    (void) success_memorder;
    (void) failure_memorder;
    unsigned int mask = irq_disable();
    unsigned int cur  = *(volatile unsigned int *) ptr;
    if (cur != *(unsigned int *) expected) {
        *(unsigned int *) expected = cur;
        irq_restore(mask);
        return false;
    }
    *(volatile unsigned int *) ptr = desired;
    irq_restore(mask);
    return true;
}
