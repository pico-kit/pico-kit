/*
 * agent_scheduler.h
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

#ifndef ADAPTER_AGENT_SCHEDULER_H_
#define ADAPTER_AGENT_SCHEDULER_H_

#include "harmony/domain/completion_type.h"
#include "harmony/domain/task_type.h"
#include "harmony/domain/tasker_model.h"
#include "harmony/domain/tasker_type.h"
#include "harmony/feature/completion/completion_init.h"
#include "harmony/feature/task/task_init.h"
#include "harmony/feature/tasker/tasker_init.h"
#include "reactive/domain/network_type.h"
#include "reactive/feature/network/network_init.h"

struct r__harmony_scheduler {
    r__network_scheduler scheduler;
    h__tasker            harmony_tasker;
};

struct r__harmony_task {
    r__network_task task;
    h__task         harmony_task;
    h__completion   harmony_completion;
};

#define R__HARMONY_SCHEDULER_INITIALIZER(instance, a_osal)                     \
    {                                                                          \
        .scheduler = R__NETWORK_SCHEDULER_INITIALIZER(                         \
                (instance).scheduler,                                          \
                &r__harmony_network_scheduler_vft),                            \
        .harmony_tasker =                                                      \
                H__TASKER_INITIALIZER((instance).harmony_tasker, a_osal),      \
    }

#define R__HARMONY_TASK_INITIALIZER(instance)

extern const struct r__network_scheduler_vft r__harmony_network_scheduler_vft;

extern const struct r__network_task_vft r__harmony_network_task_vft;

#endif /* ADAPTER_AGENT_SCHEDULER_H_ */
