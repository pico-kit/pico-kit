# manifest.mk

# Allocator adapter
F_SRC_FILES_$(CONFIG_REACTIVE_ALLOCATOR_ADAPTER_STD) += \
    firmware/reactive/std_allocator_reactive_adapter.c

# Scheduler adapter
F_INC_DIRS_$(CONFIG_REACTIVE_SCHEDULER_ADAPTER_HARMONY) += firmware
F_SRC_FILES_$(CONFIG_REACTIVE_SCHEDULER_ADAPTER_HARMONY) += \
    firmware/reactive/harmony_scheduler_reactive_adapter.c
F_SRC_FILES_$(CONFIG_REACTIVE_SCHEDULER_ADAPTER_HARMONY) += \
    firmware/reactive/harmony_scheduler_instance_reactive_adapter.c
