/*
 * agent_scheduler.c
 *
 *  Created on: Oct 12, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/harmony_scheduler_reactive_adapter.h"
#include "reactive/domain/network_type.h"

/* Depends */
#include "harmony/domain/completion_type.h"
#include "harmony/domain/task_type.h"
#include "harmony/domain/tasker_type.h"
#include "harmony/feature/completion.h"
#include "harmony/feature/task.h"
#include "harmony/feature/tasker.h"
#include "reactive/domain/common/allocator_interface.h"
#include "reactive/domain/common/compiler.h"
#include "reactive/domain/error_model.h"
#include "reactive/domain/network_model.h"

static struct r__harmony_scheduler *
get_scheduler(r__network_scheduler * network_scheduler)
{
    struct r__harmony_scheduler * scheduler = R__COMPILER_CONTAINER_OF(
            network_scheduler,
            struct r__harmony_scheduler,
            scheduler);
    return scheduler;
}

static struct r__harmony_task *
get_task(r__network_task * network_task)
{
    struct r__harmony_task * task = R__COMPILER_CONTAINER_OF(
            network_task,
            struct r__harmony_task,
            task);
    return task;
}

static h__task_priority
reactive_priority_to_harmony_priority(r__network_task_priority priority)
{
    switch (priority) {
        case R__NETWORK_TASK_PRIORITY_LOWEST:
            return H__TASK_PRIORITY_LOWEST;
        case R__NETWORK_TASK_PRIORITY_LOWER:
            return H__TASK_PRIORITY_LOWER;
        case R__NETWORK_TASK_PRIORITY_NORMAL:
            return H__TASK_PRIORITY_NORMAL;
        case R__NETWORK_TASK_PRIORITY_HIGHER:
            return H__TASK_PRIORITY_HIGHER;
        case R__NETWORK_TASK_PRIORITY_HIGHEST:
            return H__TASK_PRIORITY_HIGHEST;
        case R__NETWORK_TASK_PRIORITY_CRITICAL:
            return H__TASK_PRIORITY_CRITICAL;
        default:
            return H__TASK_PRIORITY_LOWEST;
    }
}

static void
network_proxy_task(h__task * task, void * args)
{
    (void) task;
    struct r__network_proxy * network_proxy = args;
    r__network_model_dispatch_proxy(network_proxy);
}

static r__error
scheduler_lock(r__network_scheduler * network_scheduler)
{
    struct r__harmony_scheduler * scheduler = get_scheduler(network_scheduler);

    h__tasker_critical_lock(&scheduler->harmony_tasker);
    return R__ERROR_NONE;
}

static r__error
scheduler_unlock(r__network_scheduler * network_scheduler)
{
    struct r__harmony_scheduler * scheduler = get_scheduler(network_scheduler);

    h__tasker_critical_unlock(&scheduler->harmony_tasker);
    return R__ERROR_NONE;
}

static r__error
scheduler_start(r__network_scheduler * network_scheduler)
{
    struct r__harmony_scheduler * scheduler = get_scheduler(network_scheduler);

    h__tasker_execution_start(&scheduler->harmony_tasker);
    return R__ERROR_NONE;
}

static r__error
scheduler_stop(r__network_scheduler * network_scheduler)
{
    struct r__harmony_scheduler * scheduler = get_scheduler(network_scheduler);

    h__tasker_execution_stop(&scheduler->harmony_tasker);
    return R__ERROR_NONE;
}

static r__error
scheduler_create_task(
        r__network_scheduler *    network_scheduler,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy,
        r__network_task **        network_task)
{
    void * allocation;

    r__error error = r__allocator_allocate(
            R__ALLOCATOR_TYPE_NETWORK_TASK,
            sizeof(struct r__harmony_task),
            &allocation);
    if (error != R__ERROR_NONE) {
        goto FAIL_ALLOCATE_TASK;
    }
    struct r__harmony_task * harmony_task = allocation;
    r__network_model_task_ctor(
            &harmony_task->task,
            &r__harmony_network_task_vft,
            network_proxy);
    h__task_init(
            &harmony_task->harmony_task,
            &get_scheduler(network_scheduler)->harmony_tasker,
            reactive_priority_to_harmony_priority(network_task_priority),
            network_proxy_task,
            network_proxy,
            "agent task");
    h__completion_init(
            &harmony_task->harmony_completion,
            &harmony_task->harmony_task);
    *network_task = &harmony_task->task;
    return R__ERROR_NONE;
FAIL_ALLOCATE_TASK:
    return error;
}

static r__error
scheduler_delete_task(r__network_task * network_task)
{
    struct r__harmony_task * task = get_task(network_task);

    h__completion_deinit(&task->harmony_completion);
    h__task_deinit(&task->harmony_task);

    r__error error =
            r__allocator_deallocate(R__ALLOCATOR_TYPE_NETWORK_TASK, task);
    if (error != R__ERROR_NONE) {
        goto FAIL_DEALLOCATE_TASK;
    }
    return R__ERROR_NONE;
FAIL_DEALLOCATE_TASK:
    return error;
}

static r__error
scheduler_init_task(
        r__network_task *         network_task,
        r__network_scheduler *    network_scheduler,
        r__network_task_priority  network_task_priority,
        struct r__network_proxy * network_proxy)
{
    struct r__harmony_task *      task      = get_task(network_task);
    struct r__harmony_scheduler * scheduler = get_scheduler(network_scheduler);

    r__network_model_task_ctor(
            &task->task,
            &r__harmony_network_task_vft,
            network_proxy);
    h__task_init(
            &task->harmony_task,
            &scheduler->harmony_tasker,
            reactive_priority_to_harmony_priority(network_task_priority),
            network_proxy_task,
            network_proxy,
            "agent task");
    h__completion_init(&task->harmony_completion, &task->harmony_task);
    if (h__tasker_is_running(&scheduler->harmony_tasker)) {
        h__task_resume(&task->harmony_task);
    } else {
        h__task_pending(&task->harmony_task);
    }
    return R__ERROR_NONE;
}

static r__error
scheduler_deinit_task(r__network_task * network_task)
{
    struct r__harmony_task * task = get_task(network_task);

    h__completion_deinit(&task->harmony_completion);
    h__task_deinit(&task->harmony_task);
    return R__ERROR_NONE;
}

const struct r__network_scheduler_vft r__harmony_network_scheduler_vft = {
        .lock        = scheduler_lock,
        .unlock      = scheduler_unlock,
        .start       = scheduler_start,
        .stop        = scheduler_stop,
        .create_task = scheduler_create_task,
        .delete_task = scheduler_delete_task,
        .init_task   = scheduler_init_task,
        .deinit_task = scheduler_deinit_task};

static r__error
task_signal(r__network_task * network_task)
{
    struct r__harmony_task * task = get_task(network_task);

    h__completion_signal(&task->harmony_completion);
    return R__ERROR_NONE;
}

static r__error
task_wait(r__network_task * network_task)
{
    struct r__harmony_task * task = get_task(network_task);

    (void) h__completion_wait(&task->harmony_completion);
    return R__ERROR_NONE;
}

const struct r__network_task_vft r__harmony_network_task_vft = {
        .signal = task_signal,
        .wait   = task_wait};
