/*
 * stdlib_allocator.c
 *
 *  Created on: Oct 25, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/domain/common/allocator_interface.h"

/* Depends */
#include <stdlib.h>
#include "reactive/domain/error_model.h"

r__error
r__allocator_allocate(r__allocator_type allocator, size_t size, void ** memory)
{
    void * l_memory;

    (void) allocator;

    if (memory == NULL) {
        return R__ERROR_NULL_ARGUMENT;
    }
    if (size == 0) {
        *memory = NULL;
        return R__ERROR_NONE;
    }
    l_memory = malloc(size);
    if (l_memory == NULL) {
        goto FAIL_ALLOCATE_MEMORY;
    }
    *memory = l_memory;
    return R__ERROR_NONE;
FAIL_ALLOCATE_MEMORY:
    return R__ERROR_NO_MEMORY;
}

r__error
r__allocator_deallocate(r__allocator_type allocator, void * memory)
{
    (void) allocator;

    if (memory == NULL) {
        return R__ERROR_NONE;
    }
    free(memory);
    return R__ERROR_NONE;
}
