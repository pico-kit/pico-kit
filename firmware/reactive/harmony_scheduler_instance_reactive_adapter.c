/*
 * harmony_scheduler_instance.c
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

/* Implements */
#include "reactive/harmony_scheduler_instance_reactive_adapter.h"

struct r__harmony_scheduler r__default_harmony_scheduler =
        R__HARMONY_SCHEDULER_INITIALIZER(r__default_harmony_scheduler, NULL);
