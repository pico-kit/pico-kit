/*
 * harmony_scheduler_instance.h
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

#ifndef REACTIVE_ADAPTER_HARMONY_SCHEDULER_INSTANCE_H_
#define REACTIVE_ADAPTER_HARMONY_SCHEDULER_INSTANCE_H_

#include "reactive/harmony_scheduler_reactive_adapter.h"

extern struct r__harmony_scheduler r__default_harmony_scheduler;

#endif /* REACTIVE_ADAPTER_HARMONY_SCHEDULER_INSTANCE_H_ */
