/*
 * unit-tests.cpp
 *
 *  Created on: Feb 21, 2021
 *      Author: nenad
 */

#include "verifinex/domain/unit_test.h"

int
main(void)
{
    return v__unit_test_execute();
}
