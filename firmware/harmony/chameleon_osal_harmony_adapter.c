/*
 * harmony_osal.c
 *
 *  Created on: Oct 26, 2023
 *      Author: nenad
 */

/* Implements */
#include "harmony/domain/common/osal_interface.h"

/* Depends */
#include "chameleon/domain/cpu.h"
#include "chameleon/domain/critical.h"

void
h__osal_lock(h__osal * osal)
{
    (void) osal;
    c__critical_enter();
}

void
h__osal_unlock(h__osal * osal)
{
    (void) osal;
    c__critical_exit();
}

void
h__osal_signal(h__osal * osal)
{
    (void) osal;
}

void
h__osal_wait(h__osal * osal)
{
    (void) osal;
    c__cpu_sleep();
}
