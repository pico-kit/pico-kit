/*
 * any_any_harmony_port.c
 *
 *  Created on: Nov 5, 2023
 *      Author: nenad
 */

/**
 * @file
 * @brief		Port implementation for
 */

/* Implements */
#include "harmony/domain/common/port_interface.h"

/* Depends */
#include "chameleon/domain/cpu.h"

uint_fast8_t
h__port_find_first_set(uint32_t value)
{
    return c__cpu_log2(value);
}
