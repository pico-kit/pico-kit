# manifest.mk

# OSAL adapter
F_SRC_FILES_$(CONFIG_HARMONY_OSAL_ADAPTER_CHAMELEON) += \
    firmware/harmony/chameleon_osal_harmony_adapter.c

# PORT adapter
F_SRC_FILES_$(CONFIG_HARMONY_PORT_ADAPTER_CHAMELEON) += \
    firmware/harmony/chameleon_port_harmony_adapter.c
