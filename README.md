 [![pipeline status](https://gitlab.com/pico-kit/pico-kit/badges/master/pipeline.svg)](https://gitlab.com/pico-kit/pico-kit/-/commits/master) 

# Pico-Kit

*Copyright (C) Real-Time Consulting*

## Introduction

Pico-Kit is a software package containing a number of libraries and modules for
for deply embedded real-time applications. What is a software package, library
and module, in Pico-Kit context, refer to _Software granularity hierarchy_
section in Pico-Kit developers manual in [documentation/DEVELOPER_MANUAL.md].

Pico-Kit software package provides:
 - `Symphony module` - Symphony is a software module responsible for
   multi-threaded execution using real-time round-robin scheduling technique.
   The module offers various thread synchronization mechanisms like semaphores,
   queues and event flags.
 - `Harmony module` - Harmony is a software module that offers cooperative
   scheduling and task execution. The mechanics to provide the concurrent
   execution were heavily inspired by Adam Dunkels proto-threads.
   
   Cooperative scheduling is an approach to task scheduling in computer systems
   where tasks voluntarily yield control to other tasks, allowing for fair and
   efficient resource allocation. Instead of being preemptively interrupted by
   the operating system, tasks cooperate by explicitly yielding the CPU when
   they have completed their work or are in a suitable state for a context
   switch.
 - `Reactive module` - Reactive is a software module responsible for event
   generation, event dispatching and event processing.
 - `Foundation module` - offers common data structures, such as enhanced fixed
   size array, fixed size vectors, strings, linked lists, etc.
 - `Chameleon module` - defines HAL, Hardware Abstraction Layer to use simple
   peripheral types, such as general purpose input/output (GPIO) or serial
   lines (UART). Also defines, OSAL, Operating System Abstraction Layer (tasks,
   semaphores, memory allocator, timer).
 - `Verifinex module` - is a lightweight unit-testing framework that is able to
   run on contstrained embedded devices (with or without RTOS support).

## License

The software is distributed under the terms and license specified in
[LICENSE.md].

## Getting the sources

Once the Pico-Kit project is cloned execute:

```bash
git submodule update --init --recursive
```

to fetch all used sub-modules used by Pico-Kit. You can combine cloning and
submodule update in one command:

```bash
git clone --recurse-submodules pico-kit.git
```

## Architecture overview

The following diagram tries to depict abstraction level and how depedencies are
organized. On the lowest level of abstraction sits Platform. Platform is code
and tools that are supplied by hardware manufacturer. It manages low level
hardware details, access to registers and their definitions. Some manufacturers
even provide libraries for functionality such as managing some ISR handling and
simple peripheral related operations (read and write data, configuring etc).

Firmware layer may include headers and use tools from Platform layer in order
to fullfill some higher level function. A single higher level function often
requires to combine several Platform components.

Software layer contains code with highest level of abstraction. It does not
contain any Platform or Firmware specific details so it is very generic and can
be easily unit-tested. Software code is forbidden to include or use any tools
comming from Platform and Firmware.

```plantuml
@startuml
title Module abstraction layers

package Software
package Firmware
package Platform

Firmware -u-> Software: implements
Firmware -d-> Platform: uses
@enduml
```

The following diagram shows module and library dependencies:

```plantuml
@startuml
title Pico-Kit dependency graph

package "Software" as software {
  component "Symphony" <<Module>> as symphony
  component "Harmony" <<Module>> as harmony
  component "Reactive" <<Module>> as reactive
  component "Chameleon" <<Module>> as chameleon
  component "Verifinex" <<Module>> as verifinex
  component "Foundation" <<Module>> as foundation

  reactive -d-> foundation : uses
  symphony -d-> foundation : uses
  harmony -d-> foundation : uses
}

package "Firmware" as firmware {
  component "Symphony" <<Adapter>> as symphony_adapter
  component "Harmony" <<Adapter>> as harmony_adapter
  component "Reactive" <<Adapter>> as reactive_adapter
  component "Chameleon" <<Adapter>> as chameleon_adapter
}

package "Hardware & Platform" as platform {
  component CPU {
    component Core
    component ISR
  }
  component OS {
    component Lock
    component Notify
    component Timer
  }
}

firmware --u-> software : implements
firmware --d-> platform : uses
@enduml
```

## Directory organization

The Pico-Kit package files are organized in the following way:
- `build` - contains build scripts and tools used for building. The build
  system generates configuration files and build the source files accordingly
  to the configuration. There is pre-defined configuration in
  `build/configurations`. To generate new configuration start from a
  pre-defined configuration using the `menuconfig` tool.
- `documentation` - contains additional documentation for Pico-Kit package.
  Most of the features are documented through source code, but some additional
  documentation is found here as well.
- `external` - contains external repositories. These repositories contain some
  additional code from 3rd party suppliers.
- `firmware` - contains the source code which is part of `Firmware` layer.
- `software` - contains the source code which is part of `Software` layer.
  Besides working source code these directories contain unit-tests and
  integration-tests code.
- `templates` - contains some common file templates.
- `tools` - contains some additional tools for Pico-Kit package.

Pico-Kit source consists of files located in `software/<module>/domain` and
`software/<module>/feature` sub-directories, which contain generic code, and
`firmware/<module>` sub-directories, which contain adapter code. The API header
files are located in `software/<module>/feature` sub-directories.

## Documentation

The documentations consists of multiple `.md` files (plain text) and Doxygen
related files. The doxygen tools is used to generate API reference in HTML and
PDF output formats. Please, refer to [documentation/DEVELOPER_MANUAL.md] for
instructions how to build the documentation files.

## Support

If you've found an error, please file an issue at [issues].

Patches are encouraged, and may be submitted by [forking] this project and
submitting a pull request through GitHub. Please see [CONTRIBUTING.md] for more
details.

[documentation/DEVELOPER_MANUAL.md]: documentation/DEVELOPER_MANUAL.md
[CONTRIBUTING.md]: CONTRIBUTING.md
[MAINTAINERS.md]: MAINTAINERS.md
[LICENSE.md]: LICENSE.md
[issues]: pico-kit/issues/new
[forking]: pico-kit/fork

