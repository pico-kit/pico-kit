# TODO list

This is the list of things that needs to be done:

## Version 1.0
- Fix make distclean - it doesn't work since the working directory is the root
  of the project, but it needs to be `build/make`. Make it that it nabigates to
  the mentioned directory and then executes the distclean target there.
- Reduce the number of .c files in `feature` directories.
