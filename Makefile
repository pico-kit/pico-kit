#
# SPDX-License-Identifier: LGPL-3.0-only
#
# Makefile
#
# Main makefile
#
# This is the main Makefile of the Pico-Kit build system. It is used to just
# configure the build and then the other makefiles are invoked.
#

# Docker related, specifies which Dockerfile to use
DOCKER_FILE := build/make/pico_kit_make.Dockerfile
# Docker related, specifies which docker image tag to use
DOCKER_IMAGE := pico_kit_base_make
# Docker related, specifies which Makefile target is used for image
DOCKER_IMAGE_TARGET := build/make/pico_kit_make.Dockerfile.target

$(DOCKER_IMAGE_TARGET): build/make/pico_kit_make.Dockerfile
	@docker build -f $(DOCKER_FILE) -t $(DOCKER_IMAGE) .
	@touch $@

.PHONY: all
all: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native all

.PHONY: clean
clean: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native clean

.PHONY: size
size: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native size

.PHONY: test
test: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native test

.PHONY: test-clean
test-clean: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native test clean

.PHONY: distclean
distclean: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native distclean
	@$(RM) $(DOCKER_IMAGE_TARGET)
	@docker image rm $(DOCKER_IMAGE)

.PHONY: configure
configure: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native configure

.PHONY: configure-clean
configure-clean: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native configure-clean

# Rule to list available defconfigs
.PHONY: configlist
configlist: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native configlist

.PHONY: menuconfig
menuconfig: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native menuconfig

.PHONY: compile_commands
compile_commands: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native compile_commands

.PHONY: compile_commands-clean
compile_commands-clean: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native compile_commands-clean

.PHONY: format
format: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native format

.PHONY: check
check: $(DOCKER_IMAGE_TARGET)
	@docker run --rm -it -v $$(pwd):/project -u $$(id -u):$$(id -g) $(DOCKER_IMAGE) make -f Makefile.native check

.PHONY: doc
doc:
	$(VERBOSE)$(MAKE) -C build/doxygen docker-doc


