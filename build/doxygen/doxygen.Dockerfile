FROM ubuntu:jammy AS doc_build
RUN apt-get update -y && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    make plantuml graphviz imagemagick wget \
    texlive-latex-* texlive-fonts-recommended \
    texlive-base texlive-pictures texlive-font-utils && \
    rm -rf /var/lib/apt/lists/*
WORKDIR /latest
RUN wget https://www.doxygen.nl/files/doxygen-1.10.0.linux.bin.tar.gz -O - | tar -xzf - --strip-components 1 && make install && rm -rf /latest
WORKDIR /data
VOLUME ["/data"]
CMD ["make", "-C", "build/doxygen", "doc"]

