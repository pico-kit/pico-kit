
F_INC_DIRS += external/fff
F_SRC_FILES += software/verifinex/domain/test.c

F_FEATURES += debug warnings c17 werror

include $(F_PROJECT_ROOT)/build/make/compile_executable.mk

.PHONY: test-exe
test-exe: external $(F_PROJECT_ELF)
	./$(F_PROJECT_ELF)

.PHONY: test
test: external $(F_PROJECT_ELF)
	$(PRINT)"Executing valgrind with test(s)..."
	$(VERBOSE)valgrind -q --error-exitcode=1 --exit-on-first-error=yes ./$(F_PROJECT_ELF)
	$(PRINT)"valgrind OK"

.PHONY: external
external:
	$(VERBOSE)$(MAKE) -C $(F_PROJECT_ROOT)/external fetch-fff
