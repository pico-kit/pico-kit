#
# SPDX-License-Identifier: LGPL-3.0-only
#
# definitions_common.mk

# Handle the verbosity argument
# If the argument is not given assume that verbosity is off.
V ?= 0
ifeq ("$(V)","1")
VERBOSE         := # Empty space
PRINT           := @true # Empty space
else ifeq ("$(V)","0")
VERBOSE         := @ # Empty space
PRINT           := @echo # Empty space
MAKEFLAGS       += -s
else
$(error Specify either `V=0` or `V=1`)
endif

# Utilities
RM = rm -rf
CP = cp
MKDIR = mkdir -p

# Build defaults
CONFIG_FOUNDATION_BUILD_ARTIFACT_NAME ?= pico-kit
CONFIG_FOUNDATION_BUILD_COMPILER_BIN ?= gcc

# Build options
F_PROJECT_NAME ?= $(CONFIG_FOUNDATION_BUILD_ARTIFACT_NAME)
F_FEATURES_$(CONFIG_FOUNDATION_BUILD_FEATURE_CONFIGURATION_DEBUG) += debug
F_FEATURES_$(CONFIG_FOUNDATION_BUILD_FEATURE_CONFIGURATION_RELEASE) += release
F_DEFINES_$(CONFIG_FOUNDATION_BUILD_FEATURE_CONFIGURATION_RELEASE) += NDEBUG
F_FEATURES_$(CONFIG_FOUNDATION_BUILD_FEATURE_OPTIMIZATION_NONE) += nooptimization
F_FEATURES_$(CONFIG_FOUNDATION_BUILD_FEATURE_STD_C17) += c17
F_FEATURES_$(CONFIG_FOUNDATION_BUILD_FEATURE_WARNINGS) += warnings
F_COMPILER ?= $(CONFIG_FOUNDATION_BUILD_COMPILER_BIN)

# Consolidate everything that was configured to compile
F_SRC_DIRS += $(F_SRC_DIRS_y)
F_SRC_FILES += $(F_SRC_FILES_y)
F_INC_DIRS += $(F_INC_DIRS_y)
F_EXT_INC_DIRS += $(F_EXT_INC_DIRS_y)
F_DEFINES += $(F_DEFINES_y)
F_DEPENDENCIES += $(F_DEPENDENCIES_y)
F_FEATURES += $(F_FEATURES_y)

# Build directory
LOC_BUILD_DIR := builddir

# Get compiled paths to sources
LOC_SRC_DIRS := $(addprefix $(F_PROJECT_ROOT)/,$(F_SRC_DIRS))
LOC_SRC_FILES := $(addprefix $(F_PROJECT_ROOT)/,$(F_SRC_FILES))
LOC_INC_DIRS := $(addprefix $(F_PROJECT_ROOT)/,$(sort $(F_INC_DIRS)))
LOC_INC_DIRS += $(sort $(F_EXT_INC_DIRS))
LOC_DEFINES := $(sort $(F_DEFINES))
LOC_DEPENDENCIES := $(sort $(F_DEPENDENCIES))
LOC_FEATURES := $(sort $(F_FEATURES))
LOC_COMPILER := $(subst ",,$(F_COMPILER))
LOC_SYSTEM_LIBRARIES := $(sort $(F_SYSTEM_LIBRARIES))

# List of source files in all source directories
LOC_C_SOURCES := $(foreach dir,$(LOC_SRC_DIRS),$(wildcard $(dir)/*.c))
LOC_C_SOURCES += $(filter %.c,$(LOC_SRC_FILES))

# List of object files corresponding to each C source file
LOC_C_OBJECTS := $(patsubst $(F_PROJECT_ROOT)/%.c,$(LOC_BUILD_DIR)/%.o,$(LOC_C_SOURCES))

# List of source files in all source directories
LOC_CXX_SOURCES := $(foreach dir,$(LOC_SRC_DIRS),$(wildcard $(dir)/*.cpp))
LOC_CXX_SOURCES += $(filter %.cpp,$(LOC_SRC_FILES))

# List of object files corresponding to each C++ source file
LOC_CXX_OBJECTS := $(patsubst $(F_PROJECT_ROOT)/%.cpp,$(LOC_BUILD_DIR)/%.o,$(LOC_CXX_SOURCES))

# File and folder checking
LOC_C_SOURCES_EXISTING := $(foreach file,$(LOC_C_SOURCES),$(wildcard $(file)))
LOC_C_SOURCES_MISSING := $(filter-out $(LOC_C_SOURCES_EXISTING),$(LOC_C_SOURCES))
LOC_CXX_SOURCES_EXISTING := $(foreach file,$(LOC_CXX_SOURCES),$(wildcard $(file)))
LOC_CXX_SOURCES_MISSING := $(filter-out $(LOC_CXX_SOURCES_EXISTING),$(LOC_CXX_SOURCES))
LOC_SRC_DIRS_EXISTING := $(foreach dir,$(LOC_SRC_DIRS),$(wildcard $(dir)))
LOC_SRC_DIRS_MISSING := $(filter-out $(LOC_SRC_DIRS_EXISTING),$(LOC_SRC_DIRS))
