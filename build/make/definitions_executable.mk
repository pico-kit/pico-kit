#
# SPDX-License-Identifier: LGPL-3.0-only
#
# definitions_executable.mk

# Target library
F_PROJECT_LIB := $(LOC_BUILD_DIR)/lib$(F_PROJECT_NAME).a
# Target elf
F_PROJECT_ELF := $(LOC_BUILD_DIR)/$(F_PROJECT_NAME).elf

F_STATIC_LIBRARIES += $(F_PROJECT_LIB)

