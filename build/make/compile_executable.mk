#
# SPDX-License-Identifier: LGPL-3.0-only
#
# compile_executable.mk

include $(F_PROJECT_ROOT)/build/make/definitions_common.mk
include $(F_PROJECT_ROOT)/build/make/definitions_executable.mk
include $(F_PROJECT_ROOT)/build/make/variant/variant_loader.mk

# Check if the source directories exist
ifneq ($(strip $(LOC_SRC_DIRS_MISSING)),)
$(error Missing C/C++ source directories: $(LOC_SRC_DIRS_MISSING))
endif

# Check if the source files exist
ifneq ($(strip $(LOC_C_SOURCES_MISSING) $(LOC_CXX_SOURCES_MISSING)),)
$(error Missing C/C++ source files: $(LOC_C_SOURCES_MISSING) $(LOC_CXX_SOURCES_MISSING))
endif

# Check if C_SOURCES or CXX_SOURECES are empty
ifeq ($(strip $(LOC_C_SOURCES) $(LOC_CXX_SOURCES)),)
$(error No sources are specified to compile)
endif

# Print project build information
ifeq ("$(V)","0")
$(info EXECUTABLE: $(F_PROJECT_NAME))
$(info SOURCES: $(words $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)) C/C++ source file(s))
$(info DEFINES: $(LOC_DEFINES))
$(info INCLUDES: $(LOC_INC_DIRS))
$(info FEATURES: $(LOC_FEATURES))
$(info COMPILER: $(LOC_COMPILER_PATH)::$(LOC_COMPILER_TRIPLET)::$(LOC_COMPILER))
endif

# Force Make to not delete intermediate object files
.SECONDARY: $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)

.DEFAULT_GOAL := all

.PHONY: all
all: $(F_PROJECT_ELF)

$(F_PROJECT_ELF): $(F_PROJECT_LIB)

.PHONY: clean
clean:
	$(PRINT) " [RM]: $(LOC_BUILD_DIR)"
	$(VERBOSE)$(RM) $(LOC_BUILD_DIR)

.PHONY: size
size: $(F_PROJECT_ELF)
	size $<
