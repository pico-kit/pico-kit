# feature_gcc_release.mk

# In order to build a release no flags are needed for GCC

GCC_C_FLAGS += -Wall -Wextra -pedantic -Wconversion
GCC_CXX_FLAGS += -Wall -Wextra -pedantic -Wconversion -Wredundant-move
