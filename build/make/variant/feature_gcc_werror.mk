# feature_gcc_release.mk

# In order to build a release no flags are needed for GCC

GCC_C_FLAGS += -Werror
GCC_CXX_FLAGS += -Werror
