# variant_loader.mk

LOC_COMPILER ?= gcc
LOC_FEATURES ?= debug

include $(F_PROJECT_ROOT)/build/make/variant/compiler_$(LOC_COMPILER).mk
include $(patsubst %,$(F_PROJECT_ROOT)/build/make/variant/feature_$(LOC_COMPILER)_%.mk,$(LOC_FEATURES))