# compiler_gcc.mk

# Compiler toolchain
GCC_CC ?= $(F_COMPILER_PREFIX)gcc
GCC_LD ?= $(F_COMPILER_PREFIX)gcc
GCC_CXX ?= $(F_COMPILER_PREFIX)g++
GCC_AR ?= $(F_COMPILER_PREFIX)ar
GCC_SIZE ?= $(F_COMPILER_PREFIX)size

# List of dependency files corresponding to each C source file
GCC_C_DEPS := $(patsubst $(F_PROJECT_ROOT)/%.c,$(LOC_BUILD_DIR)/%.d,$(LOC_C_SOURCES))

# List of dependency files corresponding to each C++ source file
GCC_CXX_DEPS := $(patsubst $(F_PROJECT_ROOT)/%.cpp,$(LOC_BUILD_DIR)/%.d,$(LOC_CXX_SOURCES))

# List include directories for compilation
GCC_INCLUDES := $(addprefix -I,$(LOC_INC_DIRS))

# List defines for compilation
GCC_DEFINES := $(addprefix -D,$(LOC_DEFINES))

# Set C/C++ compiler flags to generate dependency files which are used by Make
GCC_C_FLAGS += $(C_FLAGS) $(GCC_DEFINES) $(GCC_INCLUDES) -MMD -MP
GCC_CXX_FLAGS += $(CXX_FLAGS) $(GCC_DEFINES) $(GCC_INCLUDES) -MMD -MP

# List of directories to create in LOC_BUILD_DIR
# We are using sorting to remove possible duplicates since we may have multiple
# objects in the same directory.
GCC_BUILD_SUBDIRS := $(sort $(dir $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)))

# List of static libraries for linking
GCC_STATIC_LIBRARIES := $(F_STATIC_LIBRARIES)

# List of system libraries for linking
GCC_SYSTEM_LIBRARIES := $(addprefix -l,$(LOC_SYSTEM_LIBRARIES))

GCC_LD_FLAGS := $(LDFLAGS)

# Create build subdirectories
$(GCC_BUILD_SUBDIRS):
	$(VERBOSE)$(MKDIR) $@

# Rule for creating executable
$(LOC_BUILD_DIR)/%.elf: $(GCC_STATIC_LIBRARIES)
	$(PRINT) " [LD STATIC LIBRARIES] $(GCC_STATIC_LIBRARIES)"
	$(PRINT) " [LD SYSTEM LIBRARIES] $(GCC_SYSTEM_LIBRARIES)"
	$(VERBOSE)$(GCC_LD) $(GCC_LD_FLAGS) -o $@ -Wl,--start-group $(GCC_STATIC_LIBRARIES) -Wl,--end-group $(GCC_SYSTEM_LIBRARIES)
	$(VERBOSE)$(GCC_SIZE) --format=berkeley $@

# Rule for creating archive
$(LOC_BUILD_DIR)/%.a: $(LOC_C_OBJECTS) $(LOC_CXX_OBJECTS)
	$(PRINT) " [AR]: $@"
	$(VERBOSE)$(AR) rcs $@ $^

# Rule for compiling source files
$(LOC_BUILD_DIR)/%.o: $(F_PROJECT_ROOT)/%.c | $(GCC_BUILD_SUBDIRS)
	$(PRINT) " [CC]: $@"
	$(VERBOSE)$(GCC_CC) $(GCC_C_FLAGS) -c -o $@ $<
	
# Rule for compiling source files
$(LOC_BUILD_DIR)/%.o: $(F_PROJECT_ROOT)/%.cpp | $(GCC_BUILD_SUBDIRS)
	$(PRINT) " [CXX]: $@"
	$(VERBOSE)$(GCC_CXX) $(GCC_CXX_FLAGS) -c -o $@ $<
	
# Include dependency files
-include $(GCC_C_DEPS)
-include $(GCC_CXX_DEPS)
