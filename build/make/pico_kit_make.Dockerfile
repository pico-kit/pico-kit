FROM ubuntu:24.04 as base

# Update and install depedencies, remove cached data
RUN apt-get -y update \
    && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
        make \
        gcc \
        g++ \
        clang-format \
        clang-tidy \
        bear \
        valgrind \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /project