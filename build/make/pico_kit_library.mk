
# SPDX-License-Identifier: LGPL-3.0-only
#
# pico_kit_library.mk

F_PROJECT_ROOT := ../..

ifndef F_KCONFIG_CONFIG
$(error F_KCONFIG_CONFIG variable is not set. Set it to point to a Kconfig file.)
endif

include $(F_PROJECT_ROOT)/$(F_KCONFIG_CONFIG)
include $(wildcard $(F_PROJECT_ROOT)/software/*/manifest.mk)
include $(wildcard $(F_PROJECT_ROOT)/firmware/*/manifest.mk)
include $(F_PROJECT_ROOT)/build/make/compile_library.mk

.PHONY: check
check:
	$(VERBOSE)find ../../software ../../firmware ../../templates \( -name "*.c" -o -name "*.cpp"  -o -name "*.h" -o -name "*.hpp" \) -exec clang-tidy --config-file=../../.clang-tidy --quiet {} +
