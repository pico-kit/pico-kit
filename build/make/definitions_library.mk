#
# SPDX-License-Identifier: LGPL-3.0-only
#
# definitions_library.mk

# Target library
F_PROJECT_LIB ?= $(LOC_BUILD_DIR)/lib$(F_PROJECT_NAME).a
